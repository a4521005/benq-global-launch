const { Given, When, Then, Before, After } = require('@cucumber/cucumber')
const { expect } = require('chai')
const fs = require('fs');
const puppeteer = require('puppeteer')
const request = require("request-promise");
const cheerio = require("cheerio");
const cicGA="?utm_source=autotest&utm_medium=CIC"

Before({timeout: 24 * 5000},async function () {
    this.browser = await puppeteer.launch({ 
        executablePath:
        "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
        // MAC路徑:
        // executablePath:
        // '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome',
        headless:true,//有無需要開視窗,false要開,true不開
        slowMo:100,// slow down by 100ms
        devtools:false//有無需要開啟開發人員工具
    })
    this.page = await this.browser.newPage()
    await this.page.setViewport({width:1200,height:1000})
    await this.page.setDefaultTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
    await this.page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
})

After({timeout: 12 * 5000},async function () {
    await this.browser.close()
})

//BL2480L
const excelToBL2480LRONS =[
    "ar-me",
    "cs-cz",
    "de-de",
    "en-ap",
    "en-eu",
    "en-hk",
    "en-uk",
    "en-us",
    "es-es",
    "es-la",
    "es-mx",
    "fr-ca",
    "fr-ch",
    "fr-fr",
    "id-id",
    "it-it",
    "ja-jp",
    "ko-kr",
    "nl-nl",
    "pl-pl",
    "pt-br",
    "ru-ru",
    "tr-tr",
    "vi-vn",
    // "zh-cn",
    "zh-hk",
    "zh-tw"
]
const excelToBL2480LResult =[]
const excelToBL2480LURL =[]

//B2B-LCD-BL2480L - publish check
const BL2480LPublishError=[]
//B2B-ARME-LCD-BL2480L-after 20220506
const afterB2BarmeBL2480L=[]
const afterB2BarmeURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ar-me/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480l.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BarmeBL2480L.push(url)
            }
    })
    return afterB2BarmeBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-CSCZ-LCD-BL2480L-after 20220506
const afterB2BcsczBL2480L=[]
const afterB2BcsczURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/cs-cz/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480l.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BcsczBL2480L.push(url)
            }
    })
    return afterB2BcsczBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-DEDE-LCD-BL2480L-after 20220506
const afterB2BdedeBL2480L=[]
const afterB2BdedeURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/de-de/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480l.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BdedeBL2480L.push(url)
            }
    })
    return afterB2BdedeBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENAP-LCD-BL2480L-after 20220506
const afterB2BenapBL2480L=[]
const afterB2BenapURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-ap/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480l.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BenapBL2480L.push(url)
            }
    })
    return afterB2BenapBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENEU-LCD-BL2480L-after 20220506
const afterB2BeneuBL2480L=[]
const afterB2BeneuURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-eu/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480l.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BeneuBL2480L.push(url)
            }
    })
    return afterB2BeneuBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENHK-LCD-BL2480L-after 20220506
const afterB2BenhkBL2480L=[]
const afterB2BenhkURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-hk/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480l.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BenhkBL2480L.push(url)
            }
    })
    return afterB2BenhkBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENUK-LCD-BL2480L-after 20220506
const afterB2BenukBL2480L=[]
const afterB2BenukURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-uk/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480l.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BenukBL2480L.push(url)
            }
    })
    return afterB2BenukBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENUS-LCD-BL2480L-after 20220506
const afterB2BenusBL2480L=[]
const afterB2BenusURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-us/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480l.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BenusBL2480L.push(url)
            }
    })
    return afterB2BenusBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ESES-LCD-BL2480L-after 20220506
const afterB2BesesBL2480L=[]
const afterB2BesesURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-es/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480l.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BesesBL2480L.push(url)
            }
    })
    return afterB2BesesBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ESLA-LCD-BL2480L-after 20220506
const afterB2BeslaBL2480L=[]
const afterB2BeslaURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/es-la/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480l.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BeslaBL2480L.push(url)
            }
    })
    return afterB2BeslaBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ESMX-LCD-BL2480L-after 20220506
const afterB2BesmxBL2480L=[]
const afterB2BesmxURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/es-mx/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480l.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BesmxBL2480L.push(url)
            }
    })
    return afterB2BesmxBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-FRCA-LCD-BL2480L-after 20220506
const afterB2BfrcaBL2480L=[]
const afterB2BfrcaURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/fr-ca/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480l.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BfrcaBL2480L.push(url)
            }
    })
    return afterB2BfrcaBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-FRCH-LCD-BL2480L-after 20220506
const afterB2BfrchBL2480L=[]
const afterB2BfrchURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/fr-ch/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480l.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BfrchBL2480L.push(url)
            }
    })
    return afterB2BfrchBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-FRFR-LCD-BL2480L-after 20220506
const afterB2BfrfrBL2480L=[]
const afterB2BfrfrURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/fr-fr/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl="bl2480l.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BfrfrBL2480L.push(url)
            }
    })
    return afterB2BfrfrBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-IDID-LCD-BL2480L-after 20220506
const afterB2BididBL2480L=[]
const afterB2BididURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/id-id/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480l.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BididBL2480L.push(url)
            }
    })
    return afterB2BididBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ITIT-LCD-BL2480L-after 20220506
const afterB2BititBL2480L=[]
const afterB2BititURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/it-it/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480l.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BititBL2480L.push(url)
            }
    })
    return afterB2BititBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-JAJP-LCD-BL2480L-after 20220506
const afterB2BjajpBL2480L=[]
const afterB2BjajpURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ja-jp/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480l.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BjajpBL2480L.push(url)
            }
    })
    return afterB2BjajpBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-KOKR-LCD-BL2480L-after 20220506
const afterB2BkokrBL2480L=[]
const afterB2BkokrURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ko-kr/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480l.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BkokrBL2480L.push(url)
            }
    })
    return afterB2BkokrBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-NLNL-LCD-BL2480L-after 20220506
const afterB2BnlnlBL2480L=[]
const afterB2BnlnlURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/nl-nl/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480l.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BnlnlBL2480L.push(url)
            }
    })
    return afterB2BnlnlBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-PLPL-LCD-BL2480L-after 20220506
const afterB2BplplBL2480L=[]
const afterB2BplplURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/pl-pl/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480l.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BplplBL2480L.push(url)
            }
    })
    return afterB2BplplBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-PTBR-LCD-BL2480L-after 20220506
const afterB2BptbrBL2480L=[]
const afterB2BptbrURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/pt-br/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480l.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BptbrBL2480L.push(url)
            }
    })
    return afterB2BptbrBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-RURU-LCD-BL2480L-after 20220506
const afterB2BruruBL2480L=[]
const afterB2BruruURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ru-ru/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480l.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BruruBL2480L.push(url)
            }
    })
    return afterB2BruruBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-TRTR-LCD-BL2480L-after 20220506
const afterB2BtrtrBL2480L=[]
const afterB2BtrtrURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/tr-tr/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480l.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BtrtrBL2480L.push(url)
            }
    })
    return afterB2BtrtrBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-VIVN-LCD-BL2480L-after 20220506
const afterB2BvivnBL2480L=[]
const afterB2BvivnURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/vi-vn/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480l.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BvivnBL2480L.push(url)
            }
    })
    return afterB2BvivnBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ZHHK-LCD-BL2480L-after 20220506
const afterB2BzhhkBL2480L=[]
const afterB2BzhhkURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/zh-hk/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl="bl2480l.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BzhhkBL2480L.push(url)
            }
    })
    return afterB2BzhhkBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ZHTW-LCD-BL2480L-after 20220506
const afterB2BzhtwBL2480L=[]
const afterB2BzhtwURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/zh-tw/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480l.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BzhtwBL2480L.push(url)
            }
    })
    return afterB2BzhtwBL2480L;
    } catch (error) {
      console.log(error);
    }
};

//LCD - BL2480L - URL name check
const BL2480LNameError=[]
const BL2480LNameErrorURL=[]
//B2B-ARME-LCD-BL2480L-name must be monitor/bl2480l.html
const wrongnameB2BarmeBL2480L=[]
const wrongnameB2BarmeURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ar-me/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ar-me"
            const publishSeries = "monitor"
            const publishUrlName = "ar-me/monitor"
            const publishProductModel = "bl2480l.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BarmeBL2480L.push(url)
            }
    })
    return wrongnameB2BarmeBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-CSCZ-LCD-BL2480L-name must be monitor/bl2480l.html
const wrongnameB2BcsczBL2480L=[]
const wrongnameB2BcsczURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/cs-cz/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "cs-cz"
            const publishSeries = "monitor"
            const publishUrlName = "cs-cz/monitor"
            const publishProductModel = "bl2480l.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BcsczBL2480L.push(url)
            }
    })
    return wrongnameB2BcsczBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-DEDE-LCD-BL2480L-name must be monitor/bl2480l.html
const wrongnameB2BdedeBL2480L=[]
const wrongnameB2BdedeURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/de-de/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "de-de"
            const publishSeries = "monitor"
            const publishUrlName = "de-de/monitor"
            const publishProductModel = "bl2480l.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BdedeBL2480L.push(url)
            }
    })
    return wrongnameB2BdedeBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENAP-LCD-BL2480L-name must be monitor/bl2480l.html
const wrongnameB2BenapBL2480L=[]
const wrongnameB2BenapURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-ap/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-ap"
            const publishSeries = "monitor"
            const publishUrlName = "en-ap/monitor"
            const publishProductModel = "bl2480l.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BenapBL2480L.push(url)
            }
    })
    return wrongnameB2BenapBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENEU-LCD-BL2480L-name must be monitor/bl2480l.html
const wrongnameB2BeneuBL2480L=[]
const wrongnameB2BeneuURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-eu/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-eu"
            const publishSeries = "monitor"
            const publishUrlName = "en-eu/monitor"
            const publishProductModel = "bl2480l.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BeneuBL2480L.push(url)
            }
    })
    return wrongnameB2BeneuBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENHK-LCD-BL2480L-name must be monitor/bl2480l.html
const wrongnameB2BenhkBL2480L=[]
const wrongnameB2BenhkURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-hk/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-hk"
            const publishSeries = "monitor"
            const publishUrlName = "en-hk/monitor"
            const publishProductModel = "bl2480l.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BenhkBL2480L.push(url)
            }
    })
    return wrongnameB2BenhkBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENUK-LCD-BL2480L-name must be monitor/bl2480l.html
const wrongnameB2BenukBL2480L=[]
const wrongnameB2BenukURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-uk/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-uk"
            const publishSeries = "monitor"
            const publishUrlName = "en-uk/monitor"
            const publishProductModel = "bl2480l.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 &&  url.indexOf(publishUrlName)<0 ){
                wrongnameB2BenukBL2480L.push(url)
            }
    })
    return wrongnameB2BenukBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENUS-LCD-BL2480L-name must be monitor/bl2480l.html
const wrongnameB2BenusBL2480L=[]
const wrongnameB2BenusURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-us/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-us"
            const publishSeries = "monitor"
            const publishUrlName = "en-us/monitor"
            const publishProductModel = "bl2480l.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 &&  url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BenusBL2480L.push(url)
            }
    })
    return wrongnameB2BenusBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ESES-LCD-BL2480L-name must be monitor/bl2480l.html
const wrongnameB2BesesBL2480L=[]
const wrongnameB2BesesURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-es/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-es"
            const publishSeries = "monitor"
            const publishUrlName = "es-es/monitor"
            const publishProductModel = "bl2480l.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BesesBL2480L.push(url)
            }
    })
    return wrongnameB2BesesBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ESLA-LCD-BL2480L-name must be monitor/bl2480l.html
const wrongnameB2BeslaBL2480L=[]
const wrongnameB2BeslaURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/es-la/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-la"
            const publishSeries = "monitor"
            const publishUrlName = "es-la/monitor"
            const publishProductModel = "bl2480l.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 &&  url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BeslaBL2480L.push(url)
            }
    })
    return wrongnameB2BeslaBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ESMX-LCD-BL2480L-name must be monitor/bl2480l.html
const wrongnameB2BesmxBL2480L=[]
const wrongnameB2BesmxURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/es-mx/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-mx"
            const publishSeries ="monitor"
            const publishUrlName = "es-mx/monitor"
            const publishProductModel = "bl2480l.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BesmxBL2480L.push(url)
            }
    })
    return wrongnameB2BesmxBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-FRCA-LCD-BL2480L-name must be monitor/bl2480l.html
const wrongnameB2BfrcaBL2480L=[]
const wrongnameB2BfrcaURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/fr-ca/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "fr-ca"
            const publishSeries = "monitor"
            const publishUrlName = "fr-ca/monitor"
            const publishProductModel = "bl2480l.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BfrcaBL2480L.push(url)
            }
    })
    return wrongnameB2BfrcaBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-FRCH-LCD-BL2480L-name must be monitor/bl2480l.html
const wrongnameB2BfrchBL2480L=[]
const wrongnameB2BfrchURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/fr-ch/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "fr-ch"
            const publishSeries = "monitor"
            const publishUrlName = "fr-ch/monitor"
            const publishProductModel = "bl2480l.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BfrchBL2480L.push(url)
            }
    })
    return wrongnameB2BfrchBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-FRFR-LCD-BL2480L-name must be monitor/bl2480l.html
const wrongnameB2BfrfrBL2480L=[]
const wrongnameB2BfrfrURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/fr-fr/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "fr-fr"
            const publishSeries = "monitor"
            const publishUrlName = "fr-fr/monitor"
            const publishProductModel = "bl2480l.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BfrfrBL2480L.push(url)
            }
    })
    return wrongnameB2BfrfrBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-IDID-LCD-BL2480L-name must be monitor/bl2480l.html
const wrongnameB2BididBL2480L=[]
const wrongnameB2BididURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/id-id/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "id-id"
            const publishSeries = "monitor"
            const publishUrlName = "id-id/monitor"
            const publishProductModel = "bl2480l.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BididBL2480L.push(url)
            }
    })
    return wrongnameB2BididBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ITIT-LCD-BL2480L-name must be monitor/bl2480l.html
const wrongnameB2BititBL2480L=[]
const wrongnameB2BititURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/it-it/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "it-it"
            const publishSeries = "monitor"
            const publishUrlName = "it-it/monitor"
            const publishProductModel = "bl2480l.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BititBL2480L.push(url)
            }
    })
    return wrongnameB2BititBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-JAJP-LCD-BL2480L-name must be monitor/bl2480l.html
const wrongnameB2BjajpBL2480L=[]
const wrongnameB2BjajpURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ja-jp/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ja-jp"
            const publishSeries = "monitor"
            const publishUrlName = "ja-jp/monitor"
            const publishProductModel = "bl2480l.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BjajpBL2480L.push(url)
            }
    })
    return wrongnameB2BjajpBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-KOKR-LCD-BL2480L-name must be monitor/bl2480l.html
const wrongnameB2BkokrBL2480L=[]
const wrongnameB2BkokrURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ko-kr/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ko-kr"
            const publishSeries = "monitor"
            const publishUrlName = "ko-kr/monitor"
            const publishProductModel = "bl2480l.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BkokrBL2480L.push(url)
            }
    })
    return wrongnameB2BkokrBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-NLNL-LCD-BL2480L-name must be monitor/bl2480l.html
const wrongnameB2BnlnlBL2480L=[]
const wrongnameB2BnlnlURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/nl-nl/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "nl-nl"
            const publishSeries = "monitor"
            const publishUrlName = "nl-nl/monitor"
            const publishProductModel = "bl2480l.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 &&  url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BnlnlBL2480L.push(url)
            }
    })
    return wrongnameB2BnlnlBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-PLPL-LCD-BL2480L-name must be monitor/bl2480l.html
const wrongnameB2BplplBL2480L=[]
const wrongnameB2BplplURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/pl-pl/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "pl-pl"
            const publishSeries = "monitor"
            const publishUrlName = "pl-pl/monitor"
            const publishProductModel = "bl2480l.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BplplBL2480L.push(url)
            }
    })
    return wrongnameB2BplplBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-PTBR-LCD-BL2480L-name must be monitor/bl2480l.html
const wrongnameB2BptbrBL2480L=[]
const wrongnameB2BptbrURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/pt-br/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "pt-br"
            const publishSeries = "monitor"
            const publishUrlName = "pt-br/monitor"
            const publishProductModel = "bl2480l.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BptbrBL2480L.push(url)
            }
    })
    return wrongnameB2BptbrBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-RURU-LCD-BL2480L-name must be monitor/bl2480l.html
const wrongnameB2BruruBL2480L=[]
const wrongnameB2BruruURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ru-ru/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ru-ru"
            const publishSeries = "monitor"
            const publishUrlName = "ru-ru/monitor"
            const publishProductModel = "bl2480l.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BruruBL2480L.push(url)
            }
    })
    return wrongnameB2BruruBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-TRTR-LCD-BL2480L-name must be monitor/bl2480l.html
const wrongnameB2BtrtrBL2480L=[]
const wrongnameB2BtrtrURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/tr-tr/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "tr-tr"
            const publishSeries = "monitor"
            const publishUrlName = "tr-tr/monitor"
            const publishProductModel = "bl2480l.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BtrtrBL2480L.push(url)
            }
    })
    return wrongnameB2BtrtrBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-VIVN-LCD-BL2480L-name must be monitor/bl2480l.html
const wrongnameB2BvivnBL2480L=[]
const wrongnameB2BvivnURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/vi-vn/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "vi-vn"
            const publishSeries = "monitor"
            const publishUrlName = "vi-vn/monitor"
            const publishProductModel = "bl2480l.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BvivnBL2480L.push(url)
            }
    })
    return wrongnameB2BvivnBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ZHHK-LCD-BL2480L-name must be monitor/bl2480l.html
const wrongnameB2BzhhkBL2480L=[]
const wrongnameB2BzhhkURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/zh-hk/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "zh-hk"
            const publishSeries = "monitor"
            const publishUrlName = "zh-hk/monitor"
            const publishProductModel = "bl2480l.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BzhhkBL2480L.push(url)
            }
    })
    return wrongnameB2BzhhkBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ZHTW-LCD-BL2480L-name must be monitor/bl2480l.html
const wrongnameB2BzhtwBL2480L=[]
const wrongnameB2BzhtwURLBL2480L = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/zh-tw/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "zh-tw"
            const publishSeries = "monitor"
            const publishUrlName = "zh-tw/monitor"
            const publishProductModel = "bl2480l.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BzhtwBL2480L.push(url)
            }
    })
    return wrongnameB2BzhtwBL2480L;
    } catch (error) {
      console.log(error);
    }
};
//Test case
Given("BL2480L must be published on global site after 20220506",{timeout: 1000 * 5000},async function(){
    //After 2022/01/01
    //global setting
    const publishModel = "bl2480l"
    const date = new Date()
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()
    function wholeMonth(){
        const getmonth = date.getMonth() + 1
        if(getmonth<10){
            wholeMonth =  "0"+getmonth 
            return wholeMonth
        }else{
            wholeMonth = getmonth 
            return wholeMonth
        }
    }
    function wholeDate(){
        const getDay = date.getDate()
        if(getDay<10){
            wholeDate =  "0"+getDay
            return wholeDate
        }else{
            wholeDate = getDay 
            return wholeDate
        }
    }
    const wholemonth = wholeMonth()
    const wholeday = wholeDate()
    const wholeyear = date.getFullYear()
    const fullDate = `${wholeyear}/${wholemonth}/${wholeday}`
    // console.log("wholemonth:",wholemonth)
    // console.log("wholeday:",wholeday)
    // console.log("wholeyear:",wholeyear)
    console.log("fullDate:",fullDate)
    // console.log("year:",year)
    // console.log("month:",month)
    // console.log("day:",day)
    const launchDate = "2022/04/01"

    //B2B-ARME-LCD-BL2480L-after 20220506
    const armeReturnedData = await afterB2BarmeURLBL2480L();
    // console.log("After returnedData",armeReturnedData)
    const armepublishCheck = armeReturnedData.length
    console.log("ar-me All publish URL-B2B LCD-BL2480L:",armeReturnedData)
    console.log(armeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480LPublishError
        if(armepublishCheck ==0){
            // afterB2BarmeBL2480L.push("ar-me")
            BL2480LPublishError.push("ar-me")

        }
    }
    //B2B-CSCZ-LCD-BL2480L-after 20220506
    const csczReturnedData = await afterB2BcsczURLBL2480L();
    // console.log("After returnedData",csczReturnedData)
    const csczpublishCheck = csczReturnedData.length
    console.log("cs-cz All publish URL-B2B LCD-BL2480L:",csczReturnedData)
    console.log(csczReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480LPublishError
        if(csczpublishCheck ==0){
            // afterB2BcsczBL2480L.push("cs-cz")
            BL2480LPublishError.push("cs-cz")

        }
    }
    //B2B-DEDE-LCD-BL2480L-after 20220506
    const dedeReturnedData = await afterB2BdedeURLBL2480L();
    // console.log("After returnedData",dedeReturnedData)
    const dedepublishCheck = dedeReturnedData.length
    console.log("de-de All publish URL-B2B LCD-BL2480L:",dedeReturnedData)
    console.log(dedeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480LPublishError
        if(dedepublishCheck ==0){
            // afterB2BdedeBL2480L.push("de-de")
            BL2480LPublishError.push("de-de")

        }
    }
    //B2B-ENAP-LCD-BL2480L-after 20220506
    const enapReturnedData = await afterB2BenapURLBL2480L();
    // console.log("After returnedData",enapReturnedData)
    const enappublishCheck = enapReturnedData.length
    console.log("en-ap All publish URL-B2B LCD-BL2480L:",enapReturnedData)
    console.log(enapReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480LPublishError
        if(enappublishCheck ==0){
            // afterB2BenapBL2480L.push("en-ap")
            BL2480LPublishError.push("en-ap")

        }
    }
    //B2B-ENEU-LCD-BL2480L-after 20220506
    const eneuReturnedData = await afterB2BeneuURLBL2480L();
    // console.log("After returnedData",eneuReturnedData)
    const eneupublishCheck = eneuReturnedData.length
    console.log("en-eu All publish URL-B2B LCD-BL2480L:",eneuReturnedData)
    console.log(eneuReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480LPublishError
        if(eneupublishCheck ==0){
            BL2480LPublishError.push("en-eu")
        }
    }
    //B2B-ENHK-LCD-BL2480L-after 20220506
    const enhkReturnedData = await afterB2BenhkURLBL2480L();
    // console.log("After returnedData",enhkReturnedData)
    const enhkpublishCheck = enhkReturnedData.length
    console.log("en-hk All publish URL-B2B LCD-BL2480L:",enhkReturnedData)
    console.log(enhkReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480LPublishError
        if(enhkpublishCheck ==0){
            BL2480LPublishError.push("en-hk")
        }
    }
    //B2B-ENUK-LCD-BL2480L-after 20220506
    const enukReturnedData = await afterB2BenukURLBL2480L();
    // console.log("After returnedData",enukReturnedData)
    const enukpublishCheck = enukReturnedData.length
    console.log("en-uk All publish URB2B LCD-BL2480L:",enukReturnedData)
    console.log(enukReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480LPublishError
        if(enukpublishCheck ==0){
            BL2480LPublishError.push("en-uk")
        }
    }
    //B2B-ENUS-LCD-BL2480L-after 20220506
    const enusReturnedData = await afterB2BenusURLBL2480L();
    // console.log("After returnedData",enusReturnedData)
    const enuspublishCheck = enusReturnedData.length
    console.log("en-us All publish URL-B2B LCD-BL2480L:",enusReturnedData)
    console.log(enusReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480LPublishError
        if(enuspublishCheck ==0){
            BL2480LPublishError.push("en-us")
        }
    }
    //B2B-ESES-LCD-BL2480L-after 20220506
    const esesReturnedData = await afterB2BesesURLBL2480L();
    // console.log("After returnedData",esesReturnedData)
    const esespublishCheck = esesReturnedData.length
    console.log("es-es All publish URL-B2B LCD-BL2480L:",esesReturnedData)
    console.log(esesReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480LPublishError
        if(esespublishCheck ==0){
            BL2480LPublishError.push("es-es")
        }
    }
    //B2B-ESLA-LCD-BL2480L-after 20220506
    const eslaReturnedData = await afterB2BeslaURLBL2480L();
    // console.log("After returnedData",eslaReturnedData)
    const eslapublishCheck = eslaReturnedData.length
    console.log("es-la All publish URL-B2B LCD-BL2480L:",eslaReturnedData)
    console.log(eslaReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480LPublishError
        if(eslapublishCheck ==0){
            BL2480LPublishError.push("es-la")
        }
    }
    //B2B-ESMX-LCD-BL2480L-after 20220506
    const esmxReturnedData = await afterB2BesmxURLBL2480L();
    // console.log("After returnedData",esmxReturnedData)
    const esmxpublishCheck = esmxReturnedData.length
    console.log("es-mx All publish URL-B2B LCD-BL2480L:",esmxReturnedData)
    console.log(esmxReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480LPublishError
        if(esmxpublishCheck ==0){
            BL2480LPublishError.push("es-mx")
        }
    }
    //B2B-FRCA-LCD-BL2480L-after 20220506
    const frcaReturnedData = await afterB2BfrcaURLBL2480L();
    // console.log("After returnedData",frcaReturnedData)
    const frcapublishCheck = frcaReturnedData.length
    console.log("fr-ca All publish URL-B2B LCD-BL2480L:",frcaReturnedData)
    console.log(frcaReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480LPublishError
        if(frcapublishCheck ==0){
            BL2480LPublishError.push("fr-ca")
        }
    }
    //B2B-FRCH-LCD-BL2480L-after 20220506
    const frchReturnedData = await afterB2BfrchURLBL2480L();
    // console.log("After returnedData",frchReturnedData)
    const frchpublishCheck = frchReturnedData.length
    console.log("fr-ch All publish URL-B2B LCD-BL2480L:",frchReturnedData)
    console.log(frchReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480LPublishError
        if(frchpublishCheck ==0){
            BL2480LPublishError.push("fr-ch")
        }
    }
    //B2B-FRFR-LCD-BL2480L-after 20220506
    const frfrReturnedData = await afterB2BfrfrURLBL2480L();
    // console.log("After returnedData",frfrReturnedData)
    const frfrpublishCheck = frfrReturnedData.length
    console.log("fr-fr All publish URL-B2B LCD-BL2480L:",frfrReturnedData)
    console.log(frfrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480LPublishError
        if(frfrpublishCheck ==0){
            BL2480LPublishError.push("fr-fr")
        }
    }
    //B2B-IDID-LCD-BL2480L-after 20220506
    const ididReturnedData = await afterB2BididURLBL2480L();
    // console.log("After returnedData",ididReturnedData)
    const ididpublishCheck = ididReturnedData.length
    console.log("id-id All publish URL-B2B LCD-BL2480L:",ididReturnedData)
    console.log(ididReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480LPublishError
        if(ididpublishCheck ==0){
            BL2480LPublishError.push("id-id")
        }
    }
    //B2B-ITIT-LCD-BL2480L-after 20220506
    const ititReturnedData = await afterB2BititURLBL2480L();
    // console.log("After returnedData",ititReturnedData)
    const ititpublishCheck = ititReturnedData.length
    console.log("it-it All publish URL-B2B LCD-BL2480L:",ititReturnedData)
    console.log(ititReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480LPublishError
        if(ititpublishCheck ==0){
            BL2480LPublishError.push("it-it")
        }
    }
    //B2B-JAJP-LCD-BL2480L-after 20220506
    const jajpReturnedData = await afterB2BjajpURLBL2480L();
    // console.log("After returnedData",jajpReturnedData)
    const jajppublishCheck = jajpReturnedData.length
    console.log("ja-jp All publish URL-B2B LCD-BL2480L:",jajpReturnedData)
    console.log(jajpReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480LPublishError
        if(jajppublishCheck ==0){
            BL2480LPublishError.push("ja-jp")
        }
    }
    //B2B-KOKR-LCD-BL2480L-after 20220506
    const kokrReturnedData = await afterB2BkokrURLBL2480L();
    // console.log("After returnedData",kokrReturnedData)
    const kokrpublishCheck = kokrReturnedData.length
    console.log("ko-kr All publish URL-B2B LCD-BL2480L:",kokrReturnedData)
    console.log(kokrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480LPublishError
        if(kokrpublishCheck ==0){
            BL2480LPublishError.push("ko-kr")
        }
    }
    //B2B-NLNL-LCD-BL2480L-after 20220506
    const nlnlReturnedData = await afterB2BnlnlURLBL2480L();
    // console.log("After returnedData",nlnlReturnedData)
    const nlnlpublishCheck = nlnlReturnedData.length
    console.log("nl-nl All publish URL-B2B LCD-BL2480L:",nlnlReturnedData)
    console.log(nlnlReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480LPublishError
        if(nlnlpublishCheck ==0){
            BL2480LPublishError.push("nl-nl")
        }
    }
    //B2B-PLPL-LCD-BL2480L-after 20220506
    const plplReturnedData = await afterB2BplplURLBL2480L();
    // console.log("After returnedData",plplReturnedData)
    const plplpublishCheck = plplReturnedData.length
    console.log("pl-pl All publish URL-B2B LCD-BL2480L:",plplReturnedData)
    console.log(plplReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480LPublishError
        if(plplpublishCheck ==0){
            BL2480LPublishError.push("pl-pl")
        }
    }
    //B2B-PTBR-LCD-BL2480L-after 20220506
    const ptbrReturnedData = await afterB2BptbrURLBL2480L();
    // console.log("After returnedData",ptbrReturnedData)
    const ptbrpublishCheck = ptbrReturnedData.length
    console.log("pt-br All publish URL-B2B LCD-BL2480L:",ptbrReturnedData)
    console.log(ptbrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480LPublishError
        if(ptbrpublishCheck ==0){
            BL2480LPublishError.push("pt-br")
        }
    }
    //B2B-RURU-LCD-BL2480L-after 20220506
    const ruruReturnedData = await afterB2BruruURLBL2480L();
    // console.log("After returnedData",ruruReturnedData)
    const rurupublishCheck = ruruReturnedData.length
    console.log("ru-ru All publish URL-B2B LCD-BL2480L:",ruruReturnedData)
    console.log(ruruReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480LPublishError
        if(rurupublishCheck ==0){
            BL2480LPublishError.push("ru-ru")
        }
    }
    //B2B-TRTR-LCD-BL2480L-after 20220506
    const trtrReturnedData = await afterB2BtrtrURLBL2480L();
    // console.log("After returnedData",trtrReturnedData)
    const trtrpublishCheck = trtrReturnedData.length
    console.log("tr-tr All publish URL-B2B LCD-BL2480L:",trtrReturnedData)
    console.log(trtrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480LPublishError
        if(trtrpublishCheck ==0){
            BL2480LPublishError.push("tr-tr")
        }
    }
    //B2B-VIVN-LCD-BL2480L-after 20220506
    const vivnReturnedData = await afterB2BvivnURLBL2480L();
    // console.log("After returnedData",vivnReturnedData)
    const vivnpublishCheck = vivnReturnedData.length
    console.log("vi-vn All publish URL-B2B LCD-BL2480L:",vivnReturnedData)
    console.log(vivnReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480LPublishError
        if(vivnpublishCheck ==0){
            BL2480LPublishError.push("vi-vn")
        }
    }
    //B2B-ZHHK-LCD-BL2480L-after 20220506
    const zhhkReturnedData = await afterB2BzhhkURLBL2480L();
    // console.log("After returnedData",zhhkReturnedData)
    const zhhkpublishCheck = zhhkReturnedData.length
    console.log("zh-hk All publish URL-B2B LCD-BL2480L:",zhhkReturnedData)
    console.log(zhhkReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480LPublishError
        if(zhhkpublishCheck ==0){
            BL2480LPublishError.push("zh-hk")
        }
    }
    //B2B-ZHTW-LCD-BL2480L-after 20220506
    const zhtwReturnedData = await afterB2BzhtwURLBL2480L();
    // console.log("After returnedData",zhtwReturnedData)
    const zhtwpublishCheck = zhtwReturnedData.length
    console.log("zh-tw All publish URL-B2B LCD-BL2480L:",zhtwReturnedData)
    console.log(zhtwReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480LPublishError
        if(zhtwpublishCheck ==0){
            BL2480LPublishError.push("zh-tw")
        }
    }
    //放全部的國家的後面
    if(BL2480LPublishError.length >0){
        console.log("Not Published Now:",BL2480LPublishError)
        throw new Error(`${publishModel} must be published on ${BL2480LPublishError} before ${launchDate}, but it is not published now. Today is ${fullDate}`)
    }
})

Then("BL2480L URL name must be 'monitor bl2480l.html'",{timeout: 1000 * 5000},async function(){
    //After 2022/04/01
    //global setting
    const publishModel = "bl2480l"
    const publishProductURLName = "monitor/bl2480l.html"
    const date = new Date()
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()
    function wholeMonth(){
        const getmonth = date.getMonth() + 1
        if(getmonth<10){
            wholeMonth =  "0"+getmonth 
            return wholeMonth
        }else{
            wholeMonth = getmonth 
            return wholeMonth
        }
    }
    function wholeDate(){
        const getDay = date.getDate()
        if(getDay<10){
            wholeDate =  "0"+getDay
            return wholeDate
        }else{
            wholeDate = getDay 
            return wholeDate
        }
    }
    const wholemonth = wholeMonth()
    const wholeday = wholeDate()
    const wholeyear = date.getFullYear()
    const fullDate = `${wholeyear}/${wholemonth}/${wholeday}`
    // console.log("wholemonth:",wholemonth)
    // console.log("wholeday:",wholeday)
    // console.log("wholeyear:",wholeyear)
    console.log("fullDate:",fullDate)
    // console.log("year:",year)
    // console.log("month:",month)
    // console.log("day:",day)
    const launchDate = "2022/04/01"
    //B2B-ARME-LCD-BL2480L-name must be monitor/bl2480l.html
    const armeReturnedData = await wrongnameB2BarmeURLBL2480L();
    const armeNameCheck = armeReturnedData.length
    console.log("ar-me wrong name URL-B2B LCD-BL2480L:",armeReturnedData)
    console.log(armeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(armeNameCheck > 0){
            BL2480LNameError.push('ar-me')
            BL2480LNameErrorURL.push(armeReturnedData)
        }
    }
    //B2B-CSCZ-LCD-BL2480L-name must be monitor/bl2480l.html
    const csczReturnedData = await wrongnameB2BcsczURLBL2480L();
    const csczNameCheck = csczReturnedData.length
    console.log("cs-cz wrong name URL-B2B LCD-BL2480L:",csczReturnedData)
    console.log(csczReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(csczNameCheck > 0){
            BL2480LNameError.push('cs-cz')
            BL2480LNameErrorURL.push(csczReturnedData)
        }
    }
    //B2B-DEDE-LCD-BL2480L-name must be monitor/bl2480l.html
    const dedeReturnedData = await wrongnameB2BdedeURLBL2480L();
    const dedeNameCheck = dedeReturnedData.length
    console.log("de-de wrong name URL-B2B LCD-BL2480L:",dedeReturnedData)
    console.log(dedeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(dedeNameCheck > 0){
            BL2480LNameError.push('de-de')
            BL2480LNameErrorURL.push(dedeReturnedData)
        }
    }
    //B2B-ENAP-LCD-BL2480L-name must be monitor/bl2480l.html
    const enapReturnedData = await wrongnameB2BenapURLBL2480L();
    const enapNameCheck = enapReturnedData.length
    console.log("en-ap wrong name URL-B2B LCD-BL2480L:",enapReturnedData)
    console.log(enapReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enapNameCheck > 0){
            BL2480LNameError.push("en-ap")
            BL2480LNameErrorURL.push(enapReturnedData)
        }
    }
    //B2B-ENEU-LCD-BL2480L-name must be monitor/bl2480l.html
    const eneuReturnedData = await wrongnameB2BeneuURLBL2480L();
    const eneuNameCheck = eneuReturnedData.length
    console.log("en-eu wrong name URL-B2B LCD-BL2480L:",eneuReturnedData)
    console.log(eneuReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(eneuNameCheck > 0){
            BL2480LNameError.push('en-eu')
            BL2480LNameErrorURL.push(eneuReturnedData)
        }
    }
    //B2B-ENHK-LCD-BL2480L-name must be monitor/bl2480l.html
    const enhkReturnedData = await wrongnameB2BenhkURLBL2480L();
    const enhkNameCheck = enhkReturnedData.length
    console.log("en-hk wrong name URL-B2B LCD-BL2480L:",enhkReturnedData)
    console.log(enhkReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enhkNameCheck > 0){
            BL2480LNameError.push("en-hk")
            BL2480LNameErrorURL.push(enhkReturnedData)
        }
    }
    //B2B-ENUK-LCD-BL2480L-name must be monitor/bl2480l.html
    const enukReturnedData = await wrongnameB2BenukURLBL2480L();
    const enukNameCheck = enukReturnedData.length
    console.log("en-uk wrong name URL-B2B LCD-BL2480L:",enukReturnedData)
    console.log(enukReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enukNameCheck > 0){
            BL2480LNameError.push('en-uk')
            BL2480LNameErrorURL.push(enukReturnedData)
        }
    }
    //B2B-ENUS-LCD-BL2480L-name must be monitor/bl2480l.html
    const enusReturnedData = await wrongnameB2BenusURLBL2480L();
    const enusNameCheck = enusReturnedData.length
    console.log("en-us wrong name URL-B2B LCD-BL2480L:",enusReturnedData)
    console.log(enusReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enusNameCheck > 0){
            BL2480LNameError.push("en-us")
            BL2480LNameErrorURL.push(enusReturnedData)
        }
    }
    //B2B-ESES-LCD-BL2480L-name must be monitor/bl2480l.html
    const esesReturnedData = await wrongnameB2BesesURLBL2480L();
    const esesNameCheck = esesReturnedData.length
    console.log("es-es wrong name URL-B2B LCD-BL2480L:",esesReturnedData)
    console.log(esesReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(esesNameCheck > 0){
            BL2480LNameError.push("es-es")
            BL2480LNameErrorURL.push(esesReturnedData)
        }
    }
    //B2B-ESLA-LCD-BL2480L-name must be monitor/bl2480l.html
    const eslaReturnedData = await wrongnameB2BeslaURLBL2480L();
    const eslaNameCheck = eslaReturnedData.length
    console.log("es-la wrong name URL-B2B LCD-BL2480L:",eslaReturnedData)
    console.log(eslaReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(eslaNameCheck > 0){
            BL2480LNameError.push("es-la")
            BL2480LNameErrorURL.push(eslaReturnedData)
        }
    }
    //B2B-ESMX-LCD-BL2480L-name must be monitor/bl2480l.html
    const esmxReturnedData = await wrongnameB2BesmxURLBL2480L();
    const esmxNameCheck = esmxReturnedData.length
    console.log("es-mx wrong name URL-B2B LCD-BL2480L:",esmxReturnedData)
    console.log(esmxReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(esmxNameCheck > 0){
            BL2480LNameError.push("es-mx")
            BL2480LNameErrorURL.push(esmxReturnedData)
        }
    }
    //B2B-FRCA-LCD-BL2480L-name must be monitor/bl2480l.html
    const frcaReturnedData = await wrongnameB2BfrcaURLBL2480L();
    const frcaNameCheck = frcaReturnedData.length
    console.log("fr-ca wrong name URL-B2B LCD-BL2480L:",frcaReturnedData)
    console.log(frcaReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(frcaNameCheck > 0){
            BL2480LNameError.push("fr-ca")
            BL2480LNameErrorURL.push(frcaReturnedData)
        }
    }
    //B2B-FRCH-LCD-BL2480L-name must be monitor/bl2480l.html
    const frchReturnedData = await wrongnameB2BfrchURLBL2480L();
    const frchNameCheck = frchReturnedData.length
    console.log("fr-ch wrong name URL-B2B LCD-BL2480L:",frchReturnedData)
    console.log(frchReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(frchNameCheck > 0){
            BL2480LNameError.push("fr-ch")
            BL2480LNameErrorURL.push(frchReturnedData)
        }
    }
    //B2B-FRFR-LCD-BL2480L-name must be monitor/bl2480l.html
    const frfrReturnedData = await wrongnameB2BfrfrURLBL2480L();
    const frfrNameCheck = frfrReturnedData.length
    console.log("fr-fr wrong name URL-B2B LCD-BL2480L:",frfrReturnedData)
    console.log(frfrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(frfrNameCheck > 0){
            BL2480LNameError.push("fr-fr")
            BL2480LNameErrorURL.push(frfrReturnedData)
        }
    }
    //B2B-IDID-LCD-BL2480L-name must be monitor/bl2480l.html
    const ididReturnedData = await wrongnameB2BididURLBL2480L();
    const ididNameCheck = ididReturnedData.length
    console.log("id-id wrong name URL-B2B LCD-BL2480L:",ididReturnedData)
    console.log(ididReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ididNameCheck > 0){
            BL2480LNameError.push("id-id")
            BL2480LNameErrorURL.push(ididReturnedData)
        }
    }
    //B2B-ITIT-LCD-BL2480L-name must be monitor/bl2480l.html
    const ititReturnedData = await wrongnameB2BititURLBL2480L();
    const ititNameCheck = ititReturnedData.length
    console.log("it-it wrong name URL-B2B LCD-BL2480L:",ititReturnedData)
    console.log(ititReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ititNameCheck > 0){
            BL2480LNameError.push("it-it")
            BL2480LNameErrorURL.push(ititReturnedData)
        }
    }
    //B2B-JAJP-LCD-BL2480L-name must be monitor/bl2480l.html
    const jajpReturnedData = await wrongnameB2BjajpURLBL2480L();
    const jajpNameCheck = jajpReturnedData.length
    console.log("ja-jp wrong name URL-B2B LCD-BL2480L:",jajpReturnedData)
    console.log(jajpReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(jajpNameCheck > 0){
            BL2480LNameError.push("ja-jp")
            BL2480LNameErrorURL.push(jajpReturnedData)
        }
    }
    //B2B-KOKR-LCD-BL2480L-name must be monitor/bl2480l.html
    const kokrReturnedData = await wrongnameB2BkokrURLBL2480L();
    const kokrNameCheck = kokrReturnedData.length
    console.log("ko-kr wrong name URL-B2B LCD-BL2480L:",kokrReturnedData)
    console.log(kokrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(kokrNameCheck > 0){
            BL2480LNameError.push("ko-kr")
            BL2480LNameErrorURL.push(kokrReturnedData)
        }
    }
    //B2B-NLNL-LCD-BL2480L-name must be monitor/bl2480l.html
    const nlnlReturnedData = await wrongnameB2BnlnlURLBL2480L();
    const nlnlNameCheck = nlnlReturnedData.length
    console.log("nl-nl wrong name URL-B2B LCD-BL2480L:",nlnlReturnedData)
    console.log(nlnlReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(nlnlNameCheck > 0){
            BL2480LNameError.push("nl-nl")
            BL2480LNameErrorURL.push(nlnlReturnedData)
        }
    }
    //B2B-PLPL-LCD-BL2480L-name must be monitor/bl2480l.html
    const plplReturnedData = await wrongnameB2BplplURLBL2480L();
    const plplNameCheck = plplReturnedData.length
    console.log("pl-pl wrong name URL-B2B LCD-BL2480L:",plplReturnedData)
    console.log(plplReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(plplNameCheck > 0){
            BL2480LNameError.push("pl-pl")
            BL2480LNameErrorURL.push(plplReturnedData)
        }
    }
    //B2B-PTBR-LCD-BL2480L-name must be monitor/bl2480l.html
    const ptbrReturnedData = await wrongnameB2BptbrURLBL2480L();
    const ptbrNameCheck = ptbrReturnedData.length
    console.log("pt-br wrong name URL-B2B LCD-BL2480L:",ptbrReturnedData)
    console.log(ptbrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ptbrNameCheck > 0){
            BL2480LNameError.push("pt-br")
            BL2480LNameErrorURL.push(ptbrReturnedData)
        }
    }
    //B2B-RURU-LCD-BL2480L-name must be monitor/bl2480l.html
    const ruruReturnedData = await wrongnameB2BruruURLBL2480L();
    const ruruNameCheck = ruruReturnedData.length
    console.log("ru-ru wrong name URL-B2B LCD-BL2480L:",ruruReturnedData)
    console.log(ruruReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ruruNameCheck > 0){
            BL2480LNameError.push("ru-ru")
            BL2480LNameErrorURL.push(ruruReturnedData)
        }
    }
    //B2B-TRTR-LCD-BL2480L-name must be monitor/bl2480l.html
    const trtrReturnedData = await wrongnameB2BtrtrURLBL2480L();
    const trtrNameCheck = trtrReturnedData.length
    console.log("tr-tr wrong name URL-B2B LCD-BL2480L:",trtrReturnedData)
    console.log(trtrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(trtrNameCheck > 0){
            BL2480LNameError.push("tr-tr")
            BL2480LNameErrorURL.push(trtrReturnedData)
        }
    }
    //B2B-VIVN-LCD-BL2480L-name must be monitor/bl2480l.html
    const vivnReturnedData = await wrongnameB2BvivnURLBL2480L();
    const vivnNameCheck = vivnReturnedData.length
    console.log("vi-vn wrong name URL-B2B LCD-BL2480L:",vivnReturnedData)
    console.log(vivnReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(vivnNameCheck > 0){
            BL2480LNameError.push("vi-vn")
            BL2480LNameErrorURL.push(vivnReturnedData)
        }
    }
    //B2B-ZHHK-LCD-BL2480L-name must be monitor/bl2480l.html
    const zhhkReturnedData = await wrongnameB2BzhhkURLBL2480L();
    const zhhkNameCheck = zhhkReturnedData.length
    console.log("zh-hk wrong name URL-B2B LCD-BL2480L:",zhhkReturnedData)
    console.log(zhhkReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(zhhkNameCheck > 0){
            BL2480LNameError.push("zh-hk")
            BL2480LNameErrorURL.push(zhhkReturnedData)
        }
    }
    //B2B-ZHTW-LCD-BL2480L-name must be monitor/bl2480l.html
    const zhtwReturnedData = await wrongnameB2BzhtwURLBL2480L();
    const zhtwNameCheck = zhtwReturnedData.length
    console.log("zh-tw wrong name URL-B2B LCD-BL2480L:",zhtwReturnedData)
    console.log(zhtwReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(zhtwNameCheck > 0){
            BL2480LNameError.push("zh-tw")
            BL2480LNameErrorURL.push(zhtwReturnedData)
        }
    }

    //放全部國家的後面
    if(BL2480LNameError.length >0){
        throw new Error(`${publishModel} URL name must be ${publishProductURLName}, but it is wrong now. Here is RO name: ${BL2480LNameError} And wrong URL: ${BL2480LNameErrorURL}`)
    }
})

//BL2480TL
const excelToBL2480TLRONS =[
    "ar-me",
    "cs-cz",
    "de-de",
    "en-ap",
    "en-eu",
    "en-hk",
    "en-uk",
    "en-us",
    "es-es",
    "es-la",
    "es-mx",
    "fr-ca",
    "fr-ch",
    "fr-fr",
    "id-id",
    "it-it",
    "ja-jp",
    "ko-kr",
    "nl-nl",
    "pl-pl",
    "pt-br",
    "ru-ru",
    "tr-tr",
    "vi-vn",
    // "zh-cn",
    "zh-hk",
    "zh-tw"
]
const excelToBL2480TLResult =[]
const excelToBL2480TLURL =[]

//B2B-LCD-BL2480TL - publish check
const BL2480TLPublishError=[]
//B2B-ARME-LCD-BL2480TL-after 20220506
const afterB2BarmeBL2480TL=[]
const afterB2BarmeURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ar-me/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BarmeBL2480TL.push(url)
            }
    })
    return afterB2BarmeBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-CSCZ-LCD-BL2480TL-after 20220506
const afterB2BcsczBL2480TL=[]
const afterB2BcsczURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/cs-cz/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BcsczBL2480TL.push(url)
            }
    })
    return afterB2BcsczBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-DEDE-LCD-BL2480TL-after 20220506
const afterB2BdedeBL2480TL=[]
const afterB2BdedeURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/de-de/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BdedeBL2480TL.push(url)
            }
    })
    return afterB2BdedeBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENAP-LCD-BL2480TL-after 20220506
const afterB2BenapBL2480TL=[]
const afterB2BenapURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-ap/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BenapBL2480TL.push(url)
            }
    })
    return afterB2BenapBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENEU-LCD-BL2480TL-after 20220506
const afterB2BeneuBL2480TL=[]
const afterB2BeneuURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-eu/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BeneuBL2480TL.push(url)
            }
    })
    return afterB2BeneuBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENHK-LCD-BL2480TL-after 20220506
const afterB2BenhkBL2480TL=[]
const afterB2BenhkURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-hk/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BenhkBL2480TL.push(url)
            }
    })
    return afterB2BenhkBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENUK-LCD-BL2480TL-after 20220506
const afterB2BenukBL2480TL=[]
const afterB2BenukURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-uk/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BenukBL2480TL.push(url)
            }
    })
    return afterB2BenukBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENUS-LCD-BL2480TL-after 20220506
const afterB2BenusBL2480TL=[]
const afterB2BenusURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-us/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BenusBL2480TL.push(url)
            }
    })
    return afterB2BenusBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ESES-LCD-BL2480TL-after 20220506
const afterB2BesesBL2480TL=[]
const afterB2BesesURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-es/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BesesBL2480TL.push(url)
            }
    })
    return afterB2BesesBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ESLA-LCD-BL2480TL-after 20220506
const afterB2BeslaBL2480TL=[]
const afterB2BeslaURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/es-la/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BeslaBL2480TL.push(url)
            }
    })
    return afterB2BeslaBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ESMX-LCD-BL2480TL-after 20220506
const afterB2BesmxBL2480TL=[]
const afterB2BesmxURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/es-mx/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BesmxBL2480TL.push(url)
            }
    })
    return afterB2BesmxBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-FRCA-LCD-BL2480TL-after 20220506
const afterB2BfrcaBL2480TL=[]
const afterB2BfrcaURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/fr-ca/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BfrcaBL2480TL.push(url)
            }
    })
    return afterB2BfrcaBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-FRCH-LCD-BL2480TL-after 20220506
const afterB2BfrchBL2480TL=[]
const afterB2BfrchURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/fr-ch/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BfrchBL2480TL.push(url)
            }
    })
    return afterB2BfrchBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-FRFR-LCD-BL2480TL-after 20220506
const afterB2BfrfrBL2480TL=[]
const afterB2BfrfrURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/fr-fr/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl="bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BfrfrBL2480TL.push(url)
            }
    })
    return afterB2BfrfrBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-IDID-LCD-BL2480TL-after 20220506
const afterB2BididBL2480TL=[]
const afterB2BididURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/id-id/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BididBL2480TL.push(url)
            }
    })
    return afterB2BididBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ITIT-LCD-BL2480TL-after 20220506
const afterB2BititBL2480TL=[]
const afterB2BititURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/it-it/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BititBL2480TL.push(url)
            }
    })
    return afterB2BititBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-JAJP-LCD-BL2480TL-after 20220506
const afterB2BjajpBL2480TL=[]
const afterB2BjajpURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ja-jp/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BjajpBL2480TL.push(url)
            }
    })
    return afterB2BjajpBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-KOKR-LCD-BL2480TL-after 20220506
const afterB2BkokrBL2480TL=[]
const afterB2BkokrURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ko-kr/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BkokrBL2480TL.push(url)
            }
    })
    return afterB2BkokrBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-NLNL-LCD-BL2480TL-after 20220506
const afterB2BnlnlBL2480TL=[]
const afterB2BnlnlURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/nl-nl/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BnlnlBL2480TL.push(url)
            }
    })
    return afterB2BnlnlBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-PLPL-LCD-BL2480TL-after 20220506
const afterB2BplplBL2480TL=[]
const afterB2BplplURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/pl-pl/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BplplBL2480TL.push(url)
            }
    })
    return afterB2BplplBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-PTBR-LCD-BL2480TL-after 20220506
const afterB2BptbrBL2480TL=[]
const afterB2BptbrURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/pt-br/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BptbrBL2480TL.push(url)
            }
    })
    return afterB2BptbrBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-RURU-LCD-BL2480TL-after 20220506
const afterB2BruruBL2480TL=[]
const afterB2BruruURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ru-ru/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BruruBL2480TL.push(url)
            }
    })
    return afterB2BruruBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-TRTR-LCD-BL2480TL-after 20220506
const afterB2BtrtrBL2480TL=[]
const afterB2BtrtrURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/tr-tr/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BtrtrBL2480TL.push(url)
            }
    })
    return afterB2BtrtrBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-VIVN-LCD-BL2480TL-after 20220506
const afterB2BvivnBL2480TL=[]
const afterB2BvivnURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/vi-vn/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BvivnBL2480TL.push(url)
            }
    })
    return afterB2BvivnBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ZHHK-LCD-BL2480TL-after 20220506
const afterB2BzhhkBL2480TL=[]
const afterB2BzhhkURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/zh-hk/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl="bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BzhhkBL2480TL.push(url)
            }
    })
    return afterB2BzhhkBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ZHTW-LCD-BL2480TL-after 20220506
const afterB2BzhtwBL2480TL=[]
const afterB2BzhtwURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/zh-tw/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BzhtwBL2480TL.push(url)
            }
    })
    return afterB2BzhtwBL2480TL;
    } catch (error) {
      console.log(error);
    }
};

//LCD - BL2480TL - URL name check
const BL2480TLNameError=[]
const BL2480TLNameErrorURL=[]
//B2B-ARME-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BarmeBL2480TL=[]
const wrongnameB2BarmeURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ar-me/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ar-me"
            const publishSeries = "monitor"
            const publishUrlName = "ar-me/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BarmeBL2480TL.push(url)
            }
    })
    return wrongnameB2BarmeBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-CSCZ-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BcsczBL2480TL=[]
const wrongnameB2BcsczURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/cs-cz/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "cs-cz"
            const publishSeries = "monitor"
            const publishUrlName = "cs-cz/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BcsczBL2480TL.push(url)
            }
    })
    return wrongnameB2BcsczBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-DEDE-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BdedeBL2480TL=[]
const wrongnameB2BdedeURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/de-de/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "de-de"
            const publishSeries = "monitor"
            const publishUrlName = "de-de/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BdedeBL2480TL.push(url)
            }
    })
    return wrongnameB2BdedeBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENAP-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BenapBL2480TL=[]
const wrongnameB2BenapURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-ap/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-ap"
            const publishSeries = "monitor"
            const publishUrlName = "en-ap/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BenapBL2480TL.push(url)
            }
    })
    return wrongnameB2BenapBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENEU-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BeneuBL2480TL=[]
const wrongnameB2BeneuURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-eu/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-eu"
            const publishSeries = "monitor"
            const publishUrlName = "en-eu/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BeneuBL2480TL.push(url)
            }
    })
    return wrongnameB2BeneuBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENHK-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BenhkBL2480TL=[]
const wrongnameB2BenhkURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-hk/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-hk"
            const publishSeries = "monitor"
            const publishUrlName = "en-hk/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BenhkBL2480TL.push(url)
            }
    })
    return wrongnameB2BenhkBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENUK-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BenukBL2480TL=[]
const wrongnameB2BenukURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-uk/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-uk"
            const publishSeries = "monitor"
            const publishUrlName = "en-uk/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 &&  url.indexOf(publishUrlName)<0 ){
                wrongnameB2BenukBL2480TL.push(url)
            }
    })
    return wrongnameB2BenukBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENUS-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BenusBL2480TL=[]
const wrongnameB2BenusURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-us/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-us"
            const publishSeries = "monitor"
            const publishUrlName = "en-us/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 &&  url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BenusBL2480TL.push(url)
            }
    })
    return wrongnameB2BenusBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ESES-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BesesBL2480TL=[]
const wrongnameB2BesesURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-es/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-es"
            const publishSeries = "monitor"
            const publishUrlName = "es-es/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BesesBL2480TL.push(url)
            }
    })
    return wrongnameB2BesesBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ESLA-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BeslaBL2480TL=[]
const wrongnameB2BeslaURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/es-la/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-la"
            const publishSeries = "monitor"
            const publishUrlName = "es-la/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 &&  url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BeslaBL2480TL.push(url)
            }
    })
    return wrongnameB2BeslaBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ESMX-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BesmxBL2480TL=[]
const wrongnameB2BesmxURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/es-mx/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-mx"
            const publishSeries ="monitor"
            const publishUrlName = "es-mx/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BesmxBL2480TL.push(url)
            }
    })
    return wrongnameB2BesmxBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-FRCA-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BfrcaBL2480TL=[]
const wrongnameB2BfrcaURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/fr-ca/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "fr-ca"
            const publishSeries = "monitor"
            const publishUrlName = "fr-ca/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BfrcaBL2480TL.push(url)
            }
    })
    return wrongnameB2BfrcaBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-FRCH-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BfrchBL2480TL=[]
const wrongnameB2BfrchURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/fr-ch/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "fr-ch"
            const publishSeries = "monitor"
            const publishUrlName = "fr-ch/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BfrchBL2480TL.push(url)
            }
    })
    return wrongnameB2BfrchBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-FRFR-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BfrfrBL2480TL=[]
const wrongnameB2BfrfrURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/fr-fr/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "fr-fr"
            const publishSeries = "monitor"
            const publishUrlName = "fr-fr/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BfrfrBL2480TL.push(url)
            }
    })
    return wrongnameB2BfrfrBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-IDID-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BididBL2480TL=[]
const wrongnameB2BididURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/id-id/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "id-id"
            const publishSeries = "monitor"
            const publishUrlName = "id-id/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BididBL2480TL.push(url)
            }
    })
    return wrongnameB2BididBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ITIT-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BititBL2480TL=[]
const wrongnameB2BititURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/it-it/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "it-it"
            const publishSeries = "monitor"
            const publishUrlName = "it-it/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BititBL2480TL.push(url)
            }
    })
    return wrongnameB2BititBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-JAJP-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BjajpBL2480TL=[]
const wrongnameB2BjajpURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ja-jp/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ja-jp"
            const publishSeries = "monitor"
            const publishUrlName = "ja-jp/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BjajpBL2480TL.push(url)
            }
    })
    return wrongnameB2BjajpBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-KOKR-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BkokrBL2480TL=[]
const wrongnameB2BkokrURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ko-kr/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ko-kr"
            const publishSeries = "monitor"
            const publishUrlName = "ko-kr/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BkokrBL2480TL.push(url)
            }
    })
    return wrongnameB2BkokrBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-NLNL-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BnlnlBL2480TL=[]
const wrongnameB2BnlnlURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/nl-nl/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "nl-nl"
            const publishSeries = "monitor"
            const publishUrlName = "nl-nl/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 &&  url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BnlnlBL2480TL.push(url)
            }
    })
    return wrongnameB2BnlnlBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-PLPL-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BplplBL2480TL=[]
const wrongnameB2BplplURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/pl-pl/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "pl-pl"
            const publishSeries = "monitor"
            const publishUrlName = "pl-pl/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BplplBL2480TL.push(url)
            }
    })
    return wrongnameB2BplplBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-PTBR-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BptbrBL2480TL=[]
const wrongnameB2BptbrURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/pt-br/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "pt-br"
            const publishSeries = "monitor"
            const publishUrlName = "pt-br/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BptbrBL2480TL.push(url)
            }
    })
    return wrongnameB2BptbrBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-RURU-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BruruBL2480TL=[]
const wrongnameB2BruruURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ru-ru/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ru-ru"
            const publishSeries = "monitor"
            const publishUrlName = "ru-ru/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BruruBL2480TL.push(url)
            }
    })
    return wrongnameB2BruruBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-TRTR-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BtrtrBL2480TL=[]
const wrongnameB2BtrtrURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/tr-tr/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "tr-tr"
            const publishSeries = "monitor"
            const publishUrlName = "tr-tr/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BtrtrBL2480TL.push(url)
            }
    })
    return wrongnameB2BtrtrBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-VIVN-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BvivnBL2480TL=[]
const wrongnameB2BvivnURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/vi-vn/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "vi-vn"
            const publishSeries = "monitor"
            const publishUrlName = "vi-vn/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BvivnBL2480TL.push(url)
            }
    })
    return wrongnameB2BvivnBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ZHHK-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BzhhkBL2480TL=[]
const wrongnameB2BzhhkURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/zh-hk/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "zh-hk"
            const publishSeries = "monitor"
            const publishUrlName = "zh-hk/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BzhhkBL2480TL.push(url)
            }
    })
    return wrongnameB2BzhhkBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ZHTW-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BzhtwBL2480TL=[]
const wrongnameB2BzhtwURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/zh-tw/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "zh-tw"
            const publishSeries = "monitor"
            const publishUrlName = "zh-tw/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BzhtwBL2480TL.push(url)
            }
    })
    return wrongnameB2BzhtwBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//Test case
Given("BL2480TL must be published on global site after 20220506",{timeout: 1000 * 5000},async function(){
    //After 2022/01/01
    //global setting
    const publishModel = "bl2480tl"
    const date = new Date()
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()
    function wholeMonth(){
        const getmonth = date.getMonth() + 1
        if(getmonth<10){
            wholeMonth =  "0"+getmonth 
            return wholeMonth
        }else{
            wholeMonth = getmonth 
            return wholeMonth
        }
    }
    function wholeDate(){
        const getDay = date.getDate()
        if(getDay<10){
            wholeDate =  "0"+getDay
            return wholeDate
        }else{
            wholeDate = getDay 
            return wholeDate
        }
    }
    const wholemonth = wholeMonth()
    const wholeday = wholeDate()
    const wholeyear = date.getFullYear()
    const fullDate = `${wholeyear}/${wholemonth}/${wholeday}`
    // console.log("wholemonth:",wholemonth)
    // console.log("wholeday:",wholeday)
    // console.log("wholeyear:",wholeyear)
    console.log("fullDate:",fullDate)
    // console.log("year:",year)
    // console.log("month:",month)
    // console.log("day:",day)
    const launchDate = "2022/04/01"

    //B2B-ARME-LCD-BL2480TL-after 20220506
    const armeReturnedData = await afterB2BarmeURLBL2480TL();
    // console.log("After returnedData",armeReturnedData)
    const armepublishCheck = armeReturnedData.length
    console.log("ar-me All publish URL-B2B LCD-BL2480TL:",armeReturnedData)
    console.log(armeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(armepublishCheck ==0){
            // afterB2BarmeBL2480TL.push("ar-me")
            BL2480TLPublishError.push("ar-me")

        }
    }
    //B2B-CSCZ-LCD-BL2480TL-after 20220506
    const csczReturnedData = await afterB2BcsczURLBL2480TL();
    // console.log("After returnedData",csczReturnedData)
    const csczpublishCheck = csczReturnedData.length
    console.log("cs-cz All publish URL-B2B LCD-BL2480TL:",csczReturnedData)
    console.log(csczReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(csczpublishCheck ==0){
            // afterB2BcsczBL2480TL.push("cs-cz")
            BL2480TLPublishError.push("cs-cz")

        }
    }
    //B2B-DEDE-LCD-BL2480TL-after 20220506
    const dedeReturnedData = await afterB2BdedeURLBL2480TL();
    // console.log("After returnedData",dedeReturnedData)
    const dedepublishCheck = dedeReturnedData.length
    console.log("de-de All publish URL-B2B LCD-BL2480TL:",dedeReturnedData)
    console.log(dedeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(dedepublishCheck ==0){
            // afterB2BdedeBL2480TL.push("de-de")
            BL2480TLPublishError.push("de-de")

        }
    }
    //B2B-ENAP-LCD-BL2480TL-after 20220506
    const enapReturnedData = await afterB2BenapURLBL2480TL();
    // console.log("After returnedData",enapReturnedData)
    const enappublishCheck = enapReturnedData.length
    console.log("en-ap All publish URL-B2B LCD-BL2480TL:",enapReturnedData)
    console.log(enapReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(enappublishCheck ==0){
            // afterB2BenapBL2480TL.push("en-ap")
            BL2480TLPublishError.push("en-ap")

        }
    }
    //B2B-ENEU-LCD-BL2480TL-after 20220506
    const eneuReturnedData = await afterB2BeneuURLBL2480TL();
    // console.log("After returnedData",eneuReturnedData)
    const eneupublishCheck = eneuReturnedData.length
    console.log("en-eu All publish URL-B2B LCD-BL2480TL:",eneuReturnedData)
    console.log(eneuReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(eneupublishCheck ==0){
            BL2480TLPublishError.push("en-eu")
        }
    }
    //B2B-ENHK-LCD-BL2480TL-after 20220506
    const enhkReturnedData = await afterB2BenhkURLBL2480TL();
    // console.log("After returnedData",enhkReturnedData)
    const enhkpublishCheck = enhkReturnedData.length
    console.log("en-hk All publish URL-B2B LCD-BL2480TL:",enhkReturnedData)
    console.log(enhkReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(enhkpublishCheck ==0){
            BL2480TLPublishError.push("en-hk")
        }
    }
    //B2B-ENUK-LCD-BL2480TL-after 20220506
    const enukReturnedData = await afterB2BenukURLBL2480TL();
    // console.log("After returnedData",enukReturnedData)
    const enukpublishCheck = enukReturnedData.length
    console.log("en-uk All publish URB2B LCD-BL2480TL:",enukReturnedData)
    console.log(enukReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(enukpublishCheck ==0){
            BL2480TLPublishError.push("en-uk")
        }
    }
    //B2B-ENUS-LCD-BL2480TL-after 20220506
    const enusReturnedData = await afterB2BenusURLBL2480TL();
    // console.log("After returnedData",enusReturnedData)
    const enuspublishCheck = enusReturnedData.length
    console.log("en-us All publish URL-B2B LCD-BL2480TL:",enusReturnedData)
    console.log(enusReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(enuspublishCheck ==0){
            BL2480TLPublishError.push("en-us")
        }
    }
    //B2B-ESES-LCD-BL2480TL-after 20220506
    const esesReturnedData = await afterB2BesesURLBL2480TL();
    // console.log("After returnedData",esesReturnedData)
    const esespublishCheck = esesReturnedData.length
    console.log("es-es All publish URL-B2B LCD-BL2480TL:",esesReturnedData)
    console.log(esesReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(esespublishCheck ==0){
            BL2480TLPublishError.push("es-es")
        }
    }
    //B2B-ESLA-LCD-BL2480TL-after 20220506
    const eslaReturnedData = await afterB2BeslaURLBL2480TL();
    // console.log("After returnedData",eslaReturnedData)
    const eslapublishCheck = eslaReturnedData.length
    console.log("es-la All publish URL-B2B LCD-BL2480TL:",eslaReturnedData)
    console.log(eslaReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(eslapublishCheck ==0){
            BL2480TLPublishError.push("es-la")
        }
    }
    //B2B-ESMX-LCD-BL2480TL-after 20220506
    const esmxReturnedData = await afterB2BesmxURLBL2480TL();
    // console.log("After returnedData",esmxReturnedData)
    const esmxpublishCheck = esmxReturnedData.length
    console.log("es-mx All publish URL-B2B LCD-BL2480TL:",esmxReturnedData)
    console.log(esmxReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(esmxpublishCheck ==0){
            BL2480TLPublishError.push("es-mx")
        }
    }
    //B2B-FRCA-LCD-BL2480TL-after 20220506
    const frcaReturnedData = await afterB2BfrcaURLBL2480TL();
    // console.log("After returnedData",frcaReturnedData)
    const frcapublishCheck = frcaReturnedData.length
    console.log("fr-ca All publish URL-B2B LCD-BL2480TL:",frcaReturnedData)
    console.log(frcaReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(frcapublishCheck ==0){
            BL2480TLPublishError.push("fr-ca")
        }
    }
    //B2B-FRCH-LCD-BL2480TL-after 20220506
    const frchReturnedData = await afterB2BfrchURLBL2480TL();
    // console.log("After returnedData",frchReturnedData)
    const frchpublishCheck = frchReturnedData.length
    console.log("fr-ch All publish URL-B2B LCD-BL2480TL:",frchReturnedData)
    console.log(frchReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(frchpublishCheck ==0){
            BL2480TLPublishError.push("fr-ch")
        }
    }
    //B2B-FRFR-LCD-BL2480TL-after 20220506
    const frfrReturnedData = await afterB2BfrfrURLBL2480TL();
    // console.log("After returnedData",frfrReturnedData)
    const frfrpublishCheck = frfrReturnedData.length
    console.log("fr-fr All publish URL-B2B LCD-BL2480TL:",frfrReturnedData)
    console.log(frfrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(frfrpublishCheck ==0){
            BL2480TLPublishError.push("fr-fr")
        }
    }
    //B2B-IDID-LCD-BL2480TL-after 20220506
    const ididReturnedData = await afterB2BididURLBL2480TL();
    // console.log("After returnedData",ididReturnedData)
    const ididpublishCheck = ididReturnedData.length
    console.log("id-id All publish URL-B2B LCD-BL2480TL:",ididReturnedData)
    console.log(ididReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(ididpublishCheck ==0){
            BL2480TLPublishError.push("id-id")
        }
    }
    //B2B-ITIT-LCD-BL2480TL-after 20220506
    const ititReturnedData = await afterB2BititURLBL2480TL();
    // console.log("After returnedData",ititReturnedData)
    const ititpublishCheck = ititReturnedData.length
    console.log("it-it All publish URL-B2B LCD-BL2480TL:",ititReturnedData)
    console.log(ititReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(ititpublishCheck ==0){
            BL2480TLPublishError.push("it-it")
        }
    }
    //B2B-JAJP-LCD-BL2480TL-after 20220506
    const jajpReturnedData = await afterB2BjajpURLBL2480TL();
    // console.log("After returnedData",jajpReturnedData)
    const jajppublishCheck = jajpReturnedData.length
    console.log("ja-jp All publish URL-B2B LCD-BL2480TL:",jajpReturnedData)
    console.log(jajpReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(jajppublishCheck ==0){
            BL2480TLPublishError.push("ja-jp")
        }
    }
    //B2B-KOKR-LCD-BL2480TL-after 20220506
    const kokrReturnedData = await afterB2BkokrURLBL2480TL();
    // console.log("After returnedData",kokrReturnedData)
    const kokrpublishCheck = kokrReturnedData.length
    console.log("ko-kr All publish URL-B2B LCD-BL2480TL:",kokrReturnedData)
    console.log(kokrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(kokrpublishCheck ==0){
            BL2480TLPublishError.push("ko-kr")
        }
    }
    //B2B-NLNL-LCD-BL2480TL-after 20220506
    const nlnlReturnedData = await afterB2BnlnlURLBL2480TL();
    // console.log("After returnedData",nlnlReturnedData)
    const nlnlpublishCheck = nlnlReturnedData.length
    console.log("nl-nl All publish URL-B2B LCD-BL2480TL:",nlnlReturnedData)
    console.log(nlnlReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(nlnlpublishCheck ==0){
            BL2480TLPublishError.push("nl-nl")
        }
    }
    //B2B-PLPL-LCD-BL2480TL-after 20220506
    const plplReturnedData = await afterB2BplplURLBL2480TL();
    // console.log("After returnedData",plplReturnedData)
    const plplpublishCheck = plplReturnedData.length
    console.log("pl-pl All publish URL-B2B LCD-BL2480TL:",plplReturnedData)
    console.log(plplReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(plplpublishCheck ==0){
            BL2480TLPublishError.push("pl-pl")
        }
    }
    //B2B-PTBR-LCD-BL2480TL-after 20220506
    const ptbrReturnedData = await afterB2BptbrURLBL2480TL();
    // console.log("After returnedData",ptbrReturnedData)
    const ptbrpublishCheck = ptbrReturnedData.length
    console.log("pt-br All publish URL-B2B LCD-BL2480TL:",ptbrReturnedData)
    console.log(ptbrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(ptbrpublishCheck ==0){
            BL2480TLPublishError.push("pt-br")
        }
    }
    //B2B-RURU-LCD-BL2480TL-after 20220506
    const ruruReturnedData = await afterB2BruruURLBL2480TL();
    // console.log("After returnedData",ruruReturnedData)
    const rurupublishCheck = ruruReturnedData.length
    console.log("ru-ru All publish URL-B2B LCD-BL2480TL:",ruruReturnedData)
    console.log(ruruReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(rurupublishCheck ==0){
            BL2480TLPublishError.push("ru-ru")
        }
    }
    //B2B-TRTR-LCD-BL2480TL-after 20220506
    const trtrReturnedData = await afterB2BtrtrURLBL2480TL();
    // console.log("After returnedData",trtrReturnedData)
    const trtrpublishCheck = trtrReturnedData.length
    console.log("tr-tr All publish URL-B2B LCD-BL2480TL:",trtrReturnedData)
    console.log(trtrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(trtrpublishCheck ==0){
            BL2480TLPublishError.push("tr-tr")
        }
    }
    //B2B-VIVN-LCD-BL2480TL-after 20220506
    const vivnReturnedData = await afterB2BvivnURLBL2480TL();
    // console.log("After returnedData",vivnReturnedData)
    const vivnpublishCheck = vivnReturnedData.length
    console.log("vi-vn All publish URL-B2B LCD-BL2480TL:",vivnReturnedData)
    console.log(vivnReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(vivnpublishCheck ==0){
            BL2480TLPublishError.push("vi-vn")
        }
    }
    //B2B-ZHHK-LCD-BL2480TL-after 20220506
    const zhhkReturnedData = await afterB2BzhhkURLBL2480TL();
    // console.log("After returnedData",zhhkReturnedData)
    const zhhkpublishCheck = zhhkReturnedData.length
    console.log("zh-hk All publish URL-B2B LCD-BL2480TL:",zhhkReturnedData)
    console.log(zhhkReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(zhhkpublishCheck ==0){
            BL2480TLPublishError.push("zh-hk")
        }
    }
    //B2B-ZHTW-LCD-BL2480TL-after 20220506
    const zhtwReturnedData = await afterB2BzhtwURLBL2480TL();
    // console.log("After returnedData",zhtwReturnedData)
    const zhtwpublishCheck = zhtwReturnedData.length
    console.log("zh-tw All publish URL-B2B LCD-BL2480TL:",zhtwReturnedData)
    console.log(zhtwReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(zhtwpublishCheck ==0){
            BL2480TLPublishError.push("zh-tw")
        }
    }
    //放全部的國家的後面
    if(BL2480TLPublishError.length >0){
        console.log("Not Published Now:",BL2480TLPublishError)
        throw new Error(`${publishModel} must be published on ${BL2480TLPublishError} before ${launchDate}, but it is not published now. Today is ${fullDate}`)
    }
})

Then("BL2480TL URL name must be 'monitor bl2480tl.html'",{timeout: 1000 * 5000},async function(){
    //After 2022/04/01
    //global setting
    const publishModel = "bl2480tl"
    const publishProductURLName = "monitor/bl2480tl.html"
    const date = new Date()
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()
    function wholeMonth(){
        const getmonth = date.getMonth() + 1
        if(getmonth<10){
            wholeMonth =  "0"+getmonth 
            return wholeMonth
        }else{
            wholeMonth = getmonth 
            return wholeMonth
        }
    }
    function wholeDate(){
        const getDay = date.getDate()
        if(getDay<10){
            wholeDate =  "0"+getDay
            return wholeDate
        }else{
            wholeDate = getDay 
            return wholeDate
        }
    }
    const wholemonth = wholeMonth()
    const wholeday = wholeDate()
    const wholeyear = date.getFullYear()
    const fullDate = `${wholeyear}/${wholemonth}/${wholeday}`
    // console.log("wholemonth:",wholemonth)
    // console.log("wholeday:",wholeday)
    // console.log("wholeyear:",wholeyear)
    console.log("fullDate:",fullDate)
    // console.log("year:",year)
    // console.log("month:",month)
    // console.log("day:",day)
    const launchDate = "2022/04/01"
    //B2B-ARME-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const armeReturnedData = await wrongnameB2BarmeURLBL2480TL();
    const armeNameCheck = armeReturnedData.length
    console.log("ar-me wrong name URL-B2B LCD-BL2480TL:",armeReturnedData)
    console.log(armeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(armeNameCheck > 0){
            BL2480TLNameError.push('ar-me')
            BL2480TLNameErrorURL.push(armeReturnedData)
        }
    }
    //B2B-CSCZ-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const csczReturnedData = await wrongnameB2BcsczURLBL2480TL();
    const csczNameCheck = csczReturnedData.length
    console.log("cs-cz wrong name URL-B2B LCD-BL2480TL:",csczReturnedData)
    console.log(csczReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(csczNameCheck > 0){
            BL2480TLNameError.push('cs-cz')
            BL2480TLNameErrorURL.push(csczReturnedData)
        }
    }
    //B2B-DEDE-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const dedeReturnedData = await wrongnameB2BdedeURLBL2480TL();
    const dedeNameCheck = dedeReturnedData.length
    console.log("de-de wrong name URL-B2B LCD-BL2480TL:",dedeReturnedData)
    console.log(dedeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(dedeNameCheck > 0){
            BL2480TLNameError.push('de-de')
            BL2480TLNameErrorURL.push(dedeReturnedData)
        }
    }
    //B2B-ENAP-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const enapReturnedData = await wrongnameB2BenapURLBL2480TL();
    const enapNameCheck = enapReturnedData.length
    console.log("en-ap wrong name URL-B2B LCD-BL2480TL:",enapReturnedData)
    console.log(enapReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enapNameCheck > 0){
            BL2480TLNameError.push("en-ap")
            BL2480TLNameErrorURL.push(enapReturnedData)
        }
    }
    //B2B-ENEU-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const eneuReturnedData = await wrongnameB2BeneuURLBL2480TL();
    const eneuNameCheck = eneuReturnedData.length
    console.log("en-eu wrong name URL-B2B LCD-BL2480TL:",eneuReturnedData)
    console.log(eneuReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(eneuNameCheck > 0){
            BL2480TLNameError.push('en-eu')
            BL2480TLNameErrorURL.push(eneuReturnedData)
        }
    }
    //B2B-ENHK-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const enhkReturnedData = await wrongnameB2BenhkURLBL2480TL();
    const enhkNameCheck = enhkReturnedData.length
    console.log("en-hk wrong name URL-B2B LCD-BL2480TL:",enhkReturnedData)
    console.log(enhkReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enhkNameCheck > 0){
            BL2480TLNameError.push("en-hk")
            BL2480TLNameErrorURL.push(enhkReturnedData)
        }
    }
    //B2B-ENUK-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const enukReturnedData = await wrongnameB2BenukURLBL2480TL();
    const enukNameCheck = enukReturnedData.length
    console.log("en-uk wrong name URL-B2B LCD-BL2480TL:",enukReturnedData)
    console.log(enukReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enukNameCheck > 0){
            BL2480TLNameError.push('en-uk')
            BL2480TLNameErrorURL.push(enukReturnedData)
        }
    }
    //B2B-ENUS-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const enusReturnedData = await wrongnameB2BenusURLBL2480TL();
    const enusNameCheck = enusReturnedData.length
    console.log("en-us wrong name URL-B2B LCD-BL2480TL:",enusReturnedData)
    console.log(enusReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enusNameCheck > 0){
            BL2480TLNameError.push("en-us")
            BL2480TLNameErrorURL.push(enusReturnedData)
        }
    }
    //B2B-ESES-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const esesReturnedData = await wrongnameB2BesesURLBL2480TL();
    const esesNameCheck = esesReturnedData.length
    console.log("es-es wrong name URL-B2B LCD-BL2480TL:",esesReturnedData)
    console.log(esesReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(esesNameCheck > 0){
            BL2480TLNameError.push("es-es")
            BL2480TLNameErrorURL.push(esesReturnedData)
        }
    }
    //B2B-ESLA-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const eslaReturnedData = await wrongnameB2BeslaURLBL2480TL();
    const eslaNameCheck = eslaReturnedData.length
    console.log("es-la wrong name URL-B2B LCD-BL2480TL:",eslaReturnedData)
    console.log(eslaReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(eslaNameCheck > 0){
            BL2480TLNameError.push("es-la")
            BL2480TLNameErrorURL.push(eslaReturnedData)
        }
    }
    //B2B-ESMX-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const esmxReturnedData = await wrongnameB2BesmxURLBL2480TL();
    const esmxNameCheck = esmxReturnedData.length
    console.log("es-mx wrong name URL-B2B LCD-BL2480TL:",esmxReturnedData)
    console.log(esmxReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(esmxNameCheck > 0){
            BL2480TLNameError.push("es-mx")
            BL2480TLNameErrorURL.push(esmxReturnedData)
        }
    }
    //B2B-FRCA-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const frcaReturnedData = await wrongnameB2BfrcaURLBL2480TL();
    const frcaNameCheck = frcaReturnedData.length
    console.log("fr-ca wrong name URL-B2B LCD-BL2480TL:",frcaReturnedData)
    console.log(frcaReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(frcaNameCheck > 0){
            BL2480TLNameError.push("fr-ca")
            BL2480TLNameErrorURL.push(frcaReturnedData)
        }
    }
    //B2B-FRCH-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const frchReturnedData = await wrongnameB2BfrchURLBL2480TL();
    const frchNameCheck = frchReturnedData.length
    console.log("fr-ch wrong name URL-B2B LCD-BL2480TL:",frchReturnedData)
    console.log(frchReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(frchNameCheck > 0){
            BL2480TLNameError.push("fr-ch")
            BL2480TLNameErrorURL.push(frchReturnedData)
        }
    }
    //B2B-FRFR-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const frfrReturnedData = await wrongnameB2BfrfrURLBL2480TL();
    const frfrNameCheck = frfrReturnedData.length
    console.log("fr-fr wrong name URL-B2B LCD-BL2480TL:",frfrReturnedData)
    console.log(frfrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(frfrNameCheck > 0){
            BL2480TLNameError.push("fr-fr")
            BL2480TLNameErrorURL.push(frfrReturnedData)
        }
    }
    //B2B-IDID-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const ididReturnedData = await wrongnameB2BididURLBL2480TL();
    const ididNameCheck = ididReturnedData.length
    console.log("id-id wrong name URL-B2B LCD-BL2480TL:",ididReturnedData)
    console.log(ididReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ididNameCheck > 0){
            BL2480TLNameError.push("id-id")
            BL2480TLNameErrorURL.push(ididReturnedData)
        }
    }
    //B2B-ITIT-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const ititReturnedData = await wrongnameB2BititURLBL2480TL();
    const ititNameCheck = ititReturnedData.length
    console.log("it-it wrong name URL-B2B LCD-BL2480TL:",ititReturnedData)
    console.log(ititReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ititNameCheck > 0){
            BL2480TLNameError.push("it-it")
            BL2480TLNameErrorURL.push(ititReturnedData)
        }
    }
    //B2B-JAJP-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const jajpReturnedData = await wrongnameB2BjajpURLBL2480TL();
    const jajpNameCheck = jajpReturnedData.length
    console.log("ja-jp wrong name URL-B2B LCD-BL2480TL:",jajpReturnedData)
    console.log(jajpReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(jajpNameCheck > 0){
            BL2480TLNameError.push("ja-jp")
            BL2480TLNameErrorURL.push(jajpReturnedData)
        }
    }
    //B2B-KOKR-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const kokrReturnedData = await wrongnameB2BkokrURLBL2480TL();
    const kokrNameCheck = kokrReturnedData.length
    console.log("ko-kr wrong name URL-B2B LCD-BL2480TL:",kokrReturnedData)
    console.log(kokrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(kokrNameCheck > 0){
            BL2480TLNameError.push("ko-kr")
            BL2480TLNameErrorURL.push(kokrReturnedData)
        }
    }
    //B2B-NLNL-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const nlnlReturnedData = await wrongnameB2BnlnlURLBL2480TL();
    const nlnlNameCheck = nlnlReturnedData.length
    console.log("nl-nl wrong name URL-B2B LCD-BL2480TL:",nlnlReturnedData)
    console.log(nlnlReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(nlnlNameCheck > 0){
            BL2480TLNameError.push("nl-nl")
            BL2480TLNameErrorURL.push(nlnlReturnedData)
        }
    }
    //B2B-PLPL-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const plplReturnedData = await wrongnameB2BplplURLBL2480TL();
    const plplNameCheck = plplReturnedData.length
    console.log("pl-pl wrong name URL-B2B LCD-BL2480TL:",plplReturnedData)
    console.log(plplReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(plplNameCheck > 0){
            BL2480TLNameError.push("pl-pl")
            BL2480TLNameErrorURL.push(plplReturnedData)
        }
    }
    //B2B-PTBR-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const ptbrReturnedData = await wrongnameB2BptbrURLBL2480TL();
    const ptbrNameCheck = ptbrReturnedData.length
    console.log("pt-br wrong name URL-B2B LCD-BL2480TL:",ptbrReturnedData)
    console.log(ptbrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ptbrNameCheck > 0){
            BL2480TLNameError.push("pt-br")
            BL2480TLNameErrorURL.push(ptbrReturnedData)
        }
    }
    //B2B-RURU-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const ruruReturnedData = await wrongnameB2BruruURLBL2480TL();
    const ruruNameCheck = ruruReturnedData.length
    console.log("ru-ru wrong name URL-B2B LCD-BL2480TL:",ruruReturnedData)
    console.log(ruruReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ruruNameCheck > 0){
            BL2480TLNameError.push("ru-ru")
            BL2480TLNameErrorURL.push(ruruReturnedData)
        }
    }
    //B2B-TRTR-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const trtrReturnedData = await wrongnameB2BtrtrURLBL2480TL();
    const trtrNameCheck = trtrReturnedData.length
    console.log("tr-tr wrong name URL-B2B LCD-BL2480TL:",trtrReturnedData)
    console.log(trtrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(trtrNameCheck > 0){
            BL2480TLNameError.push("tr-tr")
            BL2480TLNameErrorURL.push(trtrReturnedData)
        }
    }
    //B2B-VIVN-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const vivnReturnedData = await wrongnameB2BvivnURLBL2480TL();
    const vivnNameCheck = vivnReturnedData.length
    console.log("vi-vn wrong name URL-B2B LCD-BL2480TL:",vivnReturnedData)
    console.log(vivnReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(vivnNameCheck > 0){
            BL2480TLNameError.push("vi-vn")
            BL2480TLNameErrorURL.push(vivnReturnedData)
        }
    }
    //B2B-ZHHK-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const zhhkReturnedData = await wrongnameB2BzhhkURLBL2480TL();
    const zhhkNameCheck = zhhkReturnedData.length
    console.log("zh-hk wrong name URL-B2B LCD-BL2480TL:",zhhkReturnedData)
    console.log(zhhkReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(zhhkNameCheck > 0){
            BL2480TLNameError.push("zh-hk")
            BL2480TLNameErrorURL.push(zhhkReturnedData)
        }
    }
    //B2B-ZHTW-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const zhtwReturnedData = await wrongnameB2BzhtwURLBL2480TL();
    const zhtwNameCheck = zhtwReturnedData.length
    console.log("zh-tw wrong name URL-B2B LCD-BL2480TL:",zhtwReturnedData)
    console.log(zhtwReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(zhtwNameCheck > 0){
            BL2480TLNameError.push("zh-tw")
            BL2480TLNameErrorURL.push(zhtwReturnedData)
        }
    }

    //放全部國家的後面
    if(BL2480TLNameError.length >0){
        throw new Error(`${publishModel} URL name must be ${publishProductURLName}, but it is wrong now. Here is RO name: ${BL2480TLNameError} And wrong URL: ${BL2480TLNameErrorURL}`)
    }
})

//GW2480TL
const excelToGW2480TLRONS =[
    "ar-me",
    "bg-bg",
    "cs-cz",
    "de-at",
    "de-ch",
    "de-de",
    "el-gr",
    "en-ap",
    "en-au",
    "en-ba",
    "en-ca",
    "en-cee",
    "en-cy",
    "en-dk",
    "en-ee",
    "en-eu",
    "en-fi",
    "en-hk",
    "en-hr",
    "en-ie",
    "en-in",
    "en-is",
    "en-lu",
    "en-lv",
    "en-me",
    "en-mk",
    "en-mt",
    "en-my",
    "en-no",
    "en-rs",
    "en-sg",
    "en-si",
    "en-uk",
    "en-us",
    "es-ar",
    "es-co",
    "es-es",
    "es-la",
    "es-mx",
    "es-pe",
    "fr-ca",
    "fr-ch",
    "fr-fr",
    "hu-hu",
    "id-id",
    "it-it",
    "ja-jp",
    "ko-kr",
    "lt-lt",
    "nl-be",
    "nl-nl",
    "pl-pl",
    "pt-br",
    "pt-pt",
    "ro-ro",
    "ru-ru",
    "sk-sk",
    "sv-se",
    "th-th",
    "tr-tr",
    "uk-ua",
    "vi-vn",
    // "zh-cn",
    "zh-hk",
    "zh-tw"
]
const excelToGW2480TLResult =[]
const excelToGW2480TLURL =[]

//Monitor GW2480TL - publish check
const GW2480TLPublishError=[]
//B2C-ARME-Monitor-GW2480TL-after 20220506
const afterB2CarmeGW2480TL=[]
const afterB2CarmeURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ar-me/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CarmeGW2480TL.push(url)
            }
    })
    return afterB2CarmeGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-BGBG-Monitor-GW2480TL-after 20220506
const afterB2CbgbgGW2480TL=[]
const afterB2CbgbgURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/bg-bg/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CbgbgGW2480TL.push(url)
            }
    })
    return afterB2CbgbgGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-CSCZ-Monitor-GW2480TL-after 20220506
const afterB2CcsczGW2480TL=[]
const afterB2CcsczURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/cs-cz/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CcsczGW2480TL.push(url)
            }
    })
    return afterB2CcsczGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-DEAT-Monitor-GW2480TL-after 20220506
const afterB2CdeatGW2480TL=[]
const afterB2CdeatURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/de-at/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CdeatGW2480TL.push(url)
            }
    })
    return afterB2CdeatGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-DECH-Monitor-GW2480TL-after 20220506
const afterB2CdechGW2480TL=[]
const afterB2CdechURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/de-ch/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CdechGW2480TL.push(url)
            }
    })
    return afterB2CdechGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-DEDE-Monitor-GW2480TL-after 20220506
const afterB2CdedeGW2480TL=[]
const afterB2CdedeURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/de-de/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CdedeGW2480TL.push(url)
            }
    })
    return afterB2CdedeGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ELGR-Monitor-GW2480TL-after 20220506
const afterB2CelgrGW2480TL=[]
const afterB2CelgrURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/el-gr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CelgrGW2480TL.push(url)
            }
    })
    return afterB2CelgrGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENAP-Monitor-GW2480TL-after 20220506
const afterB2CenapGW2480TL=[]
const afterB2CenapURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-ap/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenapGW2480TL.push(url)
            }
    })
    return afterB2CenapGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENAU-Monitor-GW2480TL-after 20220506
const afterB2CenauGW2480TL=[]
const afterB2CenauURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-au/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenauGW2480TL.push(url)
            }
    })
    return afterB2CenauGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENBA-Monitor-GW2480TL-after 20220506
const afterB2CenbaGW2480TL=[]
const afterB2CenbaURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-ba/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenbaGW2480TL.push(url)
            }
    })
    return afterB2CenbaGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENCA-Monitor-GW2480TL-after 20220506
const afterB2CencaGW2480TL=[]
const afterB2CencaURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-ca/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CencaGW2480TL.push(url)
            }
    })
    return afterB2CencaGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENCEE-Monitor-GW2480TL-after 20220506
const afterB2CenceeGW2480TL=[]
const afterB2CenceeURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-cee/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenceeGW2480TL.push(url)
            }
    })
    return afterB2CenceeGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENCY-Monitor-GW2480TL-after 20220506
const afterB2CencyGW2480TL=[]
const afterB2CencyURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-cy/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CencyGW2480TL.push(url)
            }
    })
    return afterB2CencyGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENDK-Monitor-GW2480TL-after 20220506
const afterB2CendkGW2480TL=[]
const afterB2CendkURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-dk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CendkGW2480TL.push(url)
            }
    })
    return afterB2CendkGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENEE-Monitor-GW2480TL-after 20220506
const afterB2CeneeGW2480TL=[]
const afterB2CeneeURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-ee/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CeneeGW2480TL.push(url)
            }
    })
    return afterB2CeneeGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENEU-Monitor-GW2480TL-after 20220506
const afterB2CeneuGW2480TL=[]
const afterB2CeneuURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-eu/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CeneuGW2480TL.push(url)
            }
    })
    return afterB2CeneuGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENFI-Monitor-GW2480TL-after 20220506
const afterB2CenfiGW2480TL=[]
const afterB2CenfiURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-fi/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenfiGW2480TL.push(url)
            }
    })
    return afterB2CenfiGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENHK-Monitor-GW2480TL-after 20220506
const afterB2CenhkGW2480TL=[]
const afterB2CenhkURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-hk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenhkGW2480TL.push(url)
            }
    })
    return afterB2CenhkGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENHR-Monitor-GW2480TL-after 20220506
const afterB2CenhrGW2480TL=[]
const afterB2CenhrURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-hr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenhrGW2480TL.push(url)
            }
    })
    return afterB2CenhrGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENIE-Monitor-GW2480TL-after 20220506
const afterB2CenieGW2480TL=[]
const afterB2CenieURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-ie/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenieGW2480TL.push(url)
            }
    })
    return afterB2CenieGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENIN-Monitor-GW2480TL-after 20220506
const afterB2CeninGW2480TL=[]
const afterB2CeninURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-in/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CeninGW2480TL.push(url)
            }
    })
    return afterB2CeninGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENIS-Monitor-GW2480TL-after 20220506
const afterB2CenisGW2480TL=[]
const afterB2CenisURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-is/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenisGW2480TL.push(url)
            }
    })
    return afterB2CenisGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENLU-Monitor-GW2480TL-after 20220506
const afterB2CenluGW2480TL=[]
const afterB2CenluURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-lu/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenluGW2480TL.push(url)
            }
    })
    return afterB2CenluGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENLV-Monitor-GW2480TL-after 20220506
const afterB2CenlvGW2480TL=[]
const afterB2CenlvURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-lv/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenlvGW2480TL.push(url)
            }
    })
    return afterB2CenlvGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENME-Monitor-GW2480TL-after 20220506
const afterB2CenmeGW2480TL=[]
const afterB2CenmeURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-me/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenmeGW2480TL.push(url)
            }
    })
    return afterB2CenmeGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENMK-Monitor-GW2480TL-after 20220506
const afterB2CenmkGW2480TL=[]
const afterB2CenmkURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-mk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenmkGW2480TL.push(url)
            }
    })
    return afterB2CenmkGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENMT-Monitor-GW2480TL-after 20220506
const afterB2CenmtGW2480TL=[]
const afterB2CenmtURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-mt/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl="gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenmtGW2480TL.push(url)
            }
    })
    return afterB2CenmtGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENMY-Monitor-GW2480TL-after 20220506
const afterB2CenmyGW2480TL=[]
const afterB2CenmyURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-my/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenmyGW2480TL.push(url)
            }
    })
    return afterB2CenmyGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENNO-Monitor-GW2480TL-after 20220506
const afterB2CennoGW2480TL=[]
const afterB2CennoURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-no/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CennoGW2480TL.push(url)
            }
    })
    return afterB2CennoGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENRS-Monitor-GW2480TL-after 20220506
const afterB2CenrsGW2480TL=[]
const afterB2CenrsURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-rs/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenrsGW2480TL.push(url)
            }
    })
    return afterB2CenrsGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENSG-Monitor-GW2480TL-after 20220506
const afterB2CensgGW2480TL=[]
const afterB2CensgURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-sg/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CensgGW2480TL.push(url)
            }
    })
    return afterB2CensgGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENSI-Monitor-GW2480TL-after 20220506
const afterB2CensiGW2480TL=[]
const afterB2CensiURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-si/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CensiGW2480TL.push(url)
            }
    })
    return afterB2CensiGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENUK-Monitor-GW2480TL-after 20220506
const afterB2CenukGW2480TL=[]
const afterB2CenukURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-uk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenukGW2480TL.push(url)
            }
    })
    return afterB2CenukGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENUS-Monitor-GW2480TL-after 20220506
const afterB2CenusGW2480TL=[]
const afterB2CenusURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-us/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenusGW2480TL.push(url)
            }
    })
    return afterB2CenusGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESAR-Monitor-GW2480TL-after 20220506
const afterB2CesarGW2480TL=[]
const afterB2CesarURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-ar/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CesarGW2480TL.push(url)
            }
    })
    return afterB2CesarGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESCO-Monitor-GW2480TL-after 20220506
const afterB2CescoGW2480TL=[]
const afterB2CescoURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-co/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CescoGW2480TL.push(url)
            }
    })
    return afterB2CescoGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESES-Monitor-GW2480TL-after 20220506
const afterB2CesesGW2480TL=[]
const afterB2CesesURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-es/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CesesGW2480TL.push(url)
            }
    })
    return afterB2CesesGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESLA-Monitor-GW2480TL-after 20220506
const afterB2CeslaGW2480TL=[]
const afterB2CeslaURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/es-la/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CeslaGW2480TL.push(url)
            }
    })
    return afterB2CeslaGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESMX-Monitor-GW2480TL-after 20220506
const afterB2CesmxGW2480TL=[]
const afterB2CesmxURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/es-mx/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CesmxGW2480TL.push(url)
            }
    })
    return afterB2CesmxGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESPE-Monitor-GW2480TL-after 20220506
const afterB2CespeGW2480TL=[]
const afterB2CespeURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-pe/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CespeGW2480TL.push(url)
            }
    })
    return afterB2CespeGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-FRCA-Monitor-GW2480TL-after 20220506
const afterB2CfrcaGW2480TL=[]
const afterB2CfrcaURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/fr-ca/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CfrcaGW2480TL.push(url)
            }
    })
    return afterB2CfrcaGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-FRCH-Monitor-GW2480TL-after 20220506
const afterB2CfrchGW2480TL=[]
const afterB2CfrchURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/fr-ch/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CfrchGW2480TL.push(url)
            }
    })
    return afterB2CfrchGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-FRFR-Monitor-GW2480TL-after 20220506
const afterB2CfrfrGW2480TL=[]
const afterB2CfrfrURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/fr-fr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CfrfrGW2480TL.push(url)
            }
    })
    return afterB2CfrfrGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-HUHU-Monitor-GW2480TL-after 20220506
const afterB2ChuhuGW2480TL=[]
const afterB2ChuhuURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/hu-hu/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2ChuhuGW2480TL.push(url)
            }
    })
    return afterB2ChuhuGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-IDID-Monitor-GW2480TL-after 20220506
const afterB2CididGW2480TL=[]
const afterB2CididURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/id-id/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CididGW2480TL.push(url)
            }
    })
    return afterB2CididGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ITIT-Monitor-GW2480TL-after 20220506
const afterB2CititGW2480TL=[]
const afterB2CititURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/it-it/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CititGW2480TL.push(url)
            }
    })
    return afterB2CititGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-JAJP-Monitor-GW2480TL-after 20220506
const afterB2CjajpGW2480TL=[]
const afterB2CjajpURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ja-jp/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CjajpGW2480TL.push(url)
            }
    })
    return afterB2CjajpGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-KOKR-Monitor-GW2480TL-after 20220506
const afterB2CkokrGW2480TL=[]
const afterB2CkokrURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ko-kr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CkokrGW2480TL.push(url)
            }
    })
    return afterB2CkokrGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-LTLT-Monitor-GW2480TL-after 20220506
const afterB2CltltGW2480TL=[]
const afterB2CltltURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/lt-lt/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CltltGW2480TL.push(url)
            }
    })
    return afterB2CltltGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-NLBE-Monitor-GW2480TL-after 20220506
const afterB2CnlbeGW2480TL=[]
const afterB2CnlbeURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/nl-be/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CnlbeGW2480TL.push(url)
            }
    })
    return afterB2CnlbeGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-NLNL-Monitor-GW2480TL-after 20220506
const afterB2CnlnlGW2480TL=[]
const afterB2CnlnlURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/nl-nl/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CnlnlGW2480TL.push(url)
            }
    })
    return afterB2CnlnlGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-PLPL-Monitor-GW2480TL-after 20220506
const afterB2CplplGW2480TL=[]
const afterB2CplplURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/pl-pl/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CplplGW2480TL.push(url)
            }
    })
    return afterB2CplplGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-PTBR-Monitor-GW2480TL-after 20220506
const afterB2CptbrGW2480TL=[]
const afterB2CptbrURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/pt-br/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CptbrGW2480TL.push(url)
            }
    })
    return afterB2CptbrGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-PTPT-Monitor-GW2480TL-after 20220506
const afterB2CptptGW2480TL=[]
const afterB2CptptURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/pt-pt/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CptptGW2480TL.push(url)
            }
    })
    return afterB2CptptGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-RORO-Monitor-GW2480TL-after 20220506
const afterB2CroroGW2480TL=[]
const afterB2CroroURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/ro-ro/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CroroGW2480TL.push(url)
            }
    })
    return afterB2CroroGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-RURU-Monitor-GW2480TL-after 20220506
const afterB2CruruGW2480TL=[]
const afterB2CruruURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ru-ru/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CruruGW2480TL.push(url)
            }
    })
    return afterB2CruruGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-SKSK-Monitor-GW2480TL-after 20220506
const afterB2CskskGW2480TL=[]
const afterB2CskskURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/sk-sk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CskskGW2480TL.push(url)
            }
    })
    return afterB2CskskGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-SVSE-Monitor-GW2480TL-after 20220506
const afterB2CsvseGW2480TL=[]
const afterB2CsvseURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/sv-se/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CsvseGW2480TL.push(url)
            }
    })
    return afterB2CsvseGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-THTH-Monitor-GW2480TL-after 20220506
const afterB2CththGW2480TL=[]
const afterB2CththURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/th-th/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CththGW2480TL.push(url)
            }
    })
    return afterB2CththGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-TRTR-Monitor-GW2480TL-after 20220506
const afterB2CtrtrGW2480TL=[]
const afterB2CtrtrURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/tr-tr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CtrtrGW2480TL.push(url)
            }
    })
    return afterB2CtrtrGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-UKUA-Monitor-GW2480TL-after 20220506
const afterB2CukuaGW2480TL=[]
const afterB2CukuaURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/uk-ua/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CukuaGW2480TL.push(url)
            }
    })
    return afterB2CukuaGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-VIVN-Monitor-GW2480TL-after 20220506
const afterB2CvivnGW2480TL=[]
const afterB2CvivnURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/vi-vn/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CvivnGW2480TL.push(url)
            }
    })
    return afterB2CvivnGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ZHHK-Monitor-GW2480TL-after 20220506
const afterB2CzhhkGW2480TL=[]
const afterB2CzhhkURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/zh-hk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl="gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CzhhkGW2480TL.push(url)
            }
    })
    return afterB2CzhhkGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ZHTW-Professional Monitor accessoGW2480TL-after 20220506
const afterB2CzhtwGW2480TL=[]
const afterB2CzhtwURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/zh-tw/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "gw2480tl"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CzhtwGW2480TL.push(url)
            }
    })
    return afterB2CzhtwGW2480TL;
    } catch (error) {
      console.log(error);
    }
};

//Monitor GW2480TL - URL name check
const GW2480TLNameError=[]
const GW2480TLNameErrorURL=[]
//B2C-ARME-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CarmeGW2480TL=[]
const wrongnameB2CarmeURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ar-me/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ar-me"
            const publishSeries = "monitor"
            const publishUrl = "ar-me/monitor"
            const publishUrlName = "ar-me/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CarmeGW2480TL.push(url)
            }
    })
    return wrongnameB2CarmeGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-BGBG-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CbgbgGW2480TL=[]
const wrongnameB2CbgbgURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/bg-bg/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "bg-bg"
            const publishSeries = "monitor"
            const publishUrl = "bg-bg/monitor"
            const publishUrlName = "bg-bg/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CbgbgGW2480TL.push(url)
            }
    })
    return wrongnameB2CbgbgGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-CSCZ-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CcsczGW2480TL=[]
const wrongnameB2CcsczURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/cs-cz/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "cs-cz"
            const publishSeries = "monitor"
            const publishUrl = "cs-cz/monitor"
            const publishUrlName = "cs-cz/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CcsczGW2480TL.push(url)
            }
    })
    return wrongnameB2CcsczGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-DEAT-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CdeatGW2480TL=[]
const wrongnameB2CdeatURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/de-at/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "de-at"
            const publishSeries = "monitor"
            const publishUrl = "de-at/monitor"
            const publishUrlName = "de-at/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CdeatGW2480TL.push(url)
            }
    })
    return wrongnameB2CdeatGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-DECH-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CdechGW2480TL=[]
const wrongnameB2CdechURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/de-ch/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "de-ch"
            const publishSeries = "monitor"
            const publishUrl = "de-ch/monitor"
            const publishUrlName = "de-ch/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CdechGW2480TL.push(url)
            }
    })
    return wrongnameB2CdechGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-DEDE-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CdedeGW2480TL=[]
const wrongnameB2CdedeURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/de-de/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "de-de"
            const publishSeries = "monitor"
            const publishUrl = "de-de/monitor"
            const publishUrlName = "de-de/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CdedeGW2480TL.push(url)
            }
    })
    return wrongnameB2CdedeGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ELGR-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CelgrGW2480TL=[]
const wrongnameB2CelgrURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/el-gr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "el-gr"
            const publishSeries = "monitor"
            const publishUrl = "el-gr/monitor"
            const publishUrlName = "el-gr/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CelgrGW2480TL.push(url)
            }
    })
    return wrongnameB2CelgrGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENAP-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CenapGW2480TL=[]
const wrongnameB2CenapURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-ap/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-ap"
            const publishSeries = "monitor"
            const publishUrl = "en-ap/monitor"
            const publishUrlName = "en-ap/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CenapGW2480TL.push(url)
            }
    })
    return wrongnameB2CenapGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENAU-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CenauGW2480TL=[]
const wrongnameB2CenauURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-au/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-au"
            const publishSeries = "monitor"
            const publishUrl = "en-au/monitor"
            const publishUrlName = "en-au/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CenauGW2480TL.push(url)
            }
    })
    return wrongnameB2CenauGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENBA-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CenbaGW2480TL=[]
const wrongnameB2CenbaURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-ba/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-ba"
            const publishSeries = "monitor"
            const publishUrl = "en-ba/monitor"
            const publishUrlName = "en-ba/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CenbaGW2480TL.push(url)
            }
    })
    return wrongnameB2CenbaGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENCA-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CencaGW2480TL=[]
const wrongnameB2CencaURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-ca/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-ca"
            const publishSeries = "monitor"
            const publishUrl = "en-ca/monitor"
            const publishUrlName = "en-ca/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CencaGW2480TL.push(url)
            }
    })
    return wrongnameB2CencaGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENCEE-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CenceeGW2480TL=[]
const wrongnameB2CenceeURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-cee/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-cee"
            const publishSeries = "monitor"
            const publishUrl = "en-cee/monitor"
            const publishUrlName = "en-cee/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CenceeGW2480TL.push(url)
            }
    })
    return wrongnameB2CenceeGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENCY-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CencyGW2480TL=[]
const wrongnameB2CencyURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-cy/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-cy"
            const publishSeries = "monitor"
            const publishUrl = "en-cy/monitor"
            const publishUrlName = "en-cy/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CencyGW2480TL.push(url)
            }
    })
    return wrongnameB2CencyGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENDK-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CendkGW2480TL=[]
const wrongnameB2CendkURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-dk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-dk"
            const publishSeries = "monitor"
            const publishUrl = "en-dk/monitor"
            const publishUrlName = "en-dk/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CendkGW2480TL.push(url)
            }
    })
    return wrongnameB2CendkGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENEE-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CeneeGW2480TL=[]
const wrongnameB2CeneeURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-ee/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-ee"
            const publishSeries = "monitor"
            const publishUrl = "en-ee/monitor"
            const publishUrlName = "en-ee/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CeneeGW2480TL.push(url)
            }
    })
    return wrongnameB2CeneeGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENEU-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CeneuGW2480TL=[]
const wrongnameB2CeneuURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-eu/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-eu"
            const publishSeries = "monitor"
            const publishUrl = "en-eu/monitor"
            const publishUrlName = "en-eu/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CeneuGW2480TL.push(url)
            }
    })
    return wrongnameB2CeneuGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENFI-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CenfiGW2480TL=[]
const wrongnameB2CenfiURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-fi/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-fi"
            const publishSeries = "monitor"
            const publishUrl = "en-fi/monitor"
            const publishUrlName = "en-fi/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CenfiGW2480TL.push(url)
            }
    })
    return wrongnameB2CenfiGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENHK-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CenhkGW2480TL=[]
const wrongnameB2CenhkURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-hk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-hk"
            const publishSeries = "monitor"
            const publishUrl = "en-hk/monitor"
            const publishUrlName = "en-hk/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CenhkGW2480TL.push(url)
            }
    })
    return wrongnameB2CenhkGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENHR-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CenhrGW2480TL=[]
const wrongnameB2CenhrURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-hr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-hr"
            const publishSeries = "monitor"
            const publishUrl = "en-hr/monitor"
            const publishUrlName = "en-hr/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CenhrGW2480TL.push(url)
            }
    })
    return wrongnameB2CenhrGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENIE-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CenieGW2480TL=[]
const wrongnameB2CenieURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-ie/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-ie"
            const publishSeries = "monitor"
            const publishUrl = "en-ie/monitor"
            const publishUrlName = "en-ie/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CenieGW2480TL.push(url)
            }
    })
    return wrongnameB2CenieGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENIN-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CeninGW2480TL=[]
const wrongnameB2CeninURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-in/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-in"
            const publishSeries = "monitor"
            const publishUrl = "en-in/monitor"
            const publishUrlName = "en-in/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CeninGW2480TL.push(url)
            }
    })
    return wrongnameB2CeninGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENIS-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CenisGW2480TL=[]
const wrongnameB2CenisURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-is/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-is"
            const publishSeries = "monitor"
            const publishUrl = "en-is/monitor"
            const publishUrlName = "en-is/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CenisGW2480TL.push(url)
            }
    })
    return wrongnameB2CenisGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENLU-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CenluGW2480TL=[]
const wrongnameB2CenluURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-lu/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-lu"
            const publishSeries = "monitor"
            const publishUrl = "en-lu/monitor"
            const publishUrlName = "en-lu/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CenluGW2480TL.push(url)
            }
    })
    return wrongnameB2CenluGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENLV-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CenlvGW2480TL=[]
const wrongnameB2CenlvURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-lv/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-lv"
            const publishSeries = "monitor"
            const publishUrl = "en-lv/monitor"
            const publishUrlName = "en-lv/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CenlvGW2480TL.push(url)
            }
    })
    return wrongnameB2CenlvGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENME-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CenmeGW2480TL=[]
const wrongnameB2CenmeURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-me/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-me"
            const publishSeries = "monitor"
            const publishUrl = "en-me/monitor"
            const publishUrlName = "en-me/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CenmeGW2480TL.push(url)
            }
    })
    return wrongnameB2CenmeGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENMK-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CenmkGW2480TL=[]
const wrongnameB2CenmkURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-mk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-mk"
            const publishSeries = "monitor"
            const publishUrl = "en-mk/monitor"
            const publishUrlName = "en-mk/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CenmkGW2480TL.push(url)
            }
    })
    return wrongnameB2CenmkGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENMT-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CenmtGW2480TL=[]
const wrongnameB2CenmtURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-mt/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-mt"
            const publishSeries = "monitor"
            const publishUrl = "en-mt/monitor"
            const publishUrlName = "en-mt/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CenmtGW2480TL.push(url)
            }
    })
    return wrongnameB2CenmtGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENMY-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CenmyGW2480TL=[]
const wrongnameB2CenmyURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-my/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-my"
            const publishSeries = "monitor"
            const publishUrl = "en-my/monitor"
            const publishUrlName = "en-my/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CenmyGW2480TL.push(url)
            }
    })
    return wrongnameB2CenmyGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENNO-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CennoGW2480TL=[]
const wrongnameB2CennoURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-no/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-no"
            const publishSeries = "monitor"
            const publishUrl = "en-no/monitor"
            const publishUrlName = "en-no/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CennoGW2480TL.push(url)
            }
    })
    return wrongnameB2CennoGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENRS-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CenrsGW2480TL=[]
const wrongnameB2CenrsURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-rs/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-rs"
            const publishSeries = "monitor"
            const publishUrl = "en-rs/monitor"
            const publishUrlName = "en-rs/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CenrsGW2480TL.push(url)
            }
    })
    return wrongnameB2CenrsGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENSG-Monitor-GW2480TL-name must be monitomonitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CensgGW2480TL=[]
const wrongnameB2CensgURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-sg/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-sg"
            const publishSeries = "monitor"
            const publishUrl = "en-sg/monitor"
            const publishUrlName = "en-sg/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CensgGW2480TL.push(url)
            }
    })
    return wrongnameB2CensgGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENSI-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CensiGW2480TL=[]
const wrongnameB2CensiURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-si/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-si"
            const publishSeries = "monitor"
            const publishUrl = "en-si/monitor"
            const publishUrlName = "en-si/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CensiGW2480TL.push(url)
            }
    })
    return wrongnameB2CensiGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENUK-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CenukGW2480TL=[]
const wrongnameB2CenukURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-uk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-uk"
            const publishSeries = "monitor"
            const publishUrl = "en-uk/monitor"
            const publishUrlName = "en-uk/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CenukGW2480TL.push(url)
            }
    })
    return wrongnameB2CenukGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENUS-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CenusGW2480TL=[]
const wrongnameB2CenusURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-us/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-us"
            const publishSeries = "monitor"
            const publishUrl = "en-us/monitor"
            const publishUrlName = "en-us/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CenusGW2480TL.push(url)
            }
    })
    return wrongnameB2CenusGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESAR-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CesarGW2480TL=[]
const wrongnameB2CesarURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-ar/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-ar"
            const publishSeries = "monitor"
            const publishUrl = "es-ar/monitor"
            const publishUrlName = "es-ar/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CesarGW2480TL.push(url)
            }
    })
    return wrongnameB2CesarGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESCO-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CescoGW2480TL=[]
const wrongnameB2CescoURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-co/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-co"
            const publishSeries = "monitor"
            const publishUrl = "es-co/monitor"
            const publishUrlName = "es-co/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CescoGW2480TL.push(url)
            }
    })
    return wrongnameB2CescoGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESES-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CesesGW2480TL=[]
const wrongnameB2CesesURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-es/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-es"
            const publishSeries = "monitor"
            const publishUrl = "es-es/monitor"
            const publishUrlName = "es-es/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CesesGW2480TL.push(url)
            }
    })
    return wrongnameB2CesesGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESLA-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CeslaGW2480TL=[]
const wrongnameB2CeslaURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/es-la/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-la"
            const publishSeries = "monitor"
            const publishUrl = "es-la/monitor"
            const publishUrlName = "es-la/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CeslaGW2480TL.push(url)
            }
    })
    return wrongnameB2CeslaGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESMX-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CesmxGW2480TL=[]
const wrongnameB2CesmxURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/es-mx/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-mx"
            const publishSeries ="monitor"
            const publishUrl = "es-mx/monitor"
            const publishUrlName = "es-mx/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CesmxGW2480TL.push(url)
            }
    })
    return wrongnameB2CesmxGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESPE-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CespeGW2480TL=[]
const wrongnameB2CespeURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-pe/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-pe"
            const publishSeries = "monitor"
            const publishUrl = "es-pe/monitor"
            const publishUrlName = "es-pe/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CespeGW2480TL.push(url)
            }
    })
    return wrongnameB2CespeGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-FRCA-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CfrcaGW2480TL=[]
const wrongnameB2CfrcaURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/fr-ca/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "fr-ca"
            const publishSeries = "monitor"
            const publishUrl = "fr-ca/monitor"
            const publishUrlName = "fr-ca/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CfrcaGW2480TL.push(url)
            }
    })
    return wrongnameB2CfrcaGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-FRCH-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CfrchGW2480TL=[]
const wrongnameB2CfrchURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/fr-ch/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "fr-ch"
            const publishSeries = "monitor"
            const publishUrl = "fr-ch/monitor"
            const publishUrlName = "fr-ch/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CfrchGW2480TL.push(url)
            }
    })
    return wrongnameB2CfrchGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-FRFR-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CfrfrGW2480TL=[]
const wrongnameB2CfrfrURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/fr-fr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "fr-fr"
            const publishSeries = "monitor"
            const publishUrl = "fr-fr/monitor"
            const publishUrlName = "fr-fr/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CfrfrGW2480TL.push(url)
            }
    })
    return wrongnameB2CfrfrGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-HUHU-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2ChuhuGW2480TL=[]
const wrongnameB2ChuhuURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/hu-hu/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "hu-hu"
            const publishSeries = "monitor"
            const publishUrl = "hu-hu/monitor"
            const publishUrlName = "hu-hu/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2ChuhuGW2480TL.push(url)
            }
    })
    return wrongnameB2ChuhuGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-IDID-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CididGW2480TL=[]
const wrongnameB2CididURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/id-id/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "id-id"
            const publishSeries = "monitor"
            const publishUrl = "id-id/monitor"
            const publishUrlName = "id-id/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CididGW2480TL.push(url)
            }
    })
    return wrongnameB2CididGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ITIT-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CititGW2480TL=[]
const wrongnameB2CititURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/it-it/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "it-it"
            const publishSeries = "monitor"
            const publishUrl = "it-it/monitor"
            const publishUrlName = "it-it/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CititGW2480TL.push(url)
            }
    })
    return wrongnameB2CititGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-JAJP-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CjajpGW2480TL=[]
const wrongnameB2CjajpURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ja-jp/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ja-jp"
            const publishSeries = "monitor"
            const publishUrl = "ja-jp/monitor"
            const publishUrlName = "ja-jp/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CjajpGW2480TL.push(url)
            }
    })
    return wrongnameB2CjajpGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-KOKR-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CkokrGW2480TL=[]
const wrongnameB2CkokrURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ko-kr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ko-kr"
            const publishSeries = "monitor"
            const publishUrl = "ko-kr/monitor"
            const publishUrlName = "ko-kr/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CkokrGW2480TL.push(url)
            }
    })
    return wrongnameB2CkokrGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-LTLT-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CltltGW2480TL=[]
const wrongnameB2CltltURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/lt-lt/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "lt-lt"
            const publishSeries = "monitor"
            const publishUrl = "lt-lt/monitor"
            const publishUrlName = "lt-lt/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CltltGW2480TL.push(url)
            }
    })
    return wrongnameB2CltltGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-NLBE-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CnlbeGW2480TL=[]
const wrongnameB2CnlbeURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/nl-be/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "nl-be"
            const publishSeries = "monitor"
            const publishUrl = "nl-be/monitor"
            const publishUrlName = "nl-be/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CnlbeGW2480TL.push(url)
            }
    })
    return wrongnameB2CnlbeGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-NLNL-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CnlnlGW2480TL=[]
const wrongnameB2CnlnlURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/nl-nl/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "nl-nl"
            const publishSeries = "monitor"
            const publishUrl = "nl-nl/monitor"
            const publishUrlName = "nl-nl/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CnlnlGW2480TL.push(url)
            }
    })
    return wrongnameB2CnlnlGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-PLPL-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CplplGW2480TL=[]
const wrongnameB2CplplURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/pl-pl/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "pl-pl"
            const publishSeries = "monitor"
            const publishUrl = "pl-pl/monitor"
            const publishUrlName = "pl-pl/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CplplGW2480TL.push(url)
            }
    })
    return wrongnameB2CplplGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-PTBR-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CptbrGW2480TL=[]
const wrongnameB2CptbrURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/pt-br/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "pt-br"
            const publishSeries = "monitor"
            const publishUrl = "pt-br/monitor"
            const publishUrlName = "pt-br/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CptbrGW2480TL.push(url)
            }
    })
    return wrongnameB2CptbrGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-PTPT-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CptptGW2480TL=[]
const wrongnameB2CptptURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/pt-pt/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "pt-pt"
            const publishSeries = "monitor"
            const publishUrl = "pt-pt/monitor"
            const publishUrlName = "pt-pt/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CptptGW2480TL.push(url)
            }
    })
    return wrongnameB2CptptGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-RORO-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CroroGW2480TL=[]
const wrongnameB2CroroURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/ro-ro/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ro-ro"
            const publishSeries = "monitor"
            const publishUrl = "ro-ro/monitor"
            const publishUrlName = "ro-ro/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CroroGW2480TL.push(url)
            }
    })
    return wrongnameB2CroroGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-RURU-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CruruGW2480TL=[]
const wrongnameB2CruruURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ru-ru/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ru-ru"
            const publishSeries = "monitor"
            const publishUrl = "ru-ru/monitor"
            const publishUrlName = "ru-ru/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CruruGW2480TL.push(url)
            }
    })
    return wrongnameB2CruruGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-SKSK-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CskskGW2480TL=[]
const wrongnameB2CskskURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/sk-sk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "sk-sk"
            const publishSeries = "monitor"
            const publishUrl = "sk-sk/monitor"
            const publishUrlName = "sk-sk/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CskskGW2480TL.push(url)
            }
    })
    return wrongnameB2CskskGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-SVSE-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CsvseGW2480TL=[]
const wrongnameB2CsvseURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/sv-se/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "sv-se"
            const publishSeries = "monitor"
            const publishUrl = "sv-se/monitor"
            const publishUrlName = "sv-se/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CsvseGW2480TL.push(url)
            }
    })
    return wrongnameB2CsvseGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-THTH-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CththGW2480TL=[]
const wrongnameB2CththURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/th-th/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "th-th"
            const publishSeries = "monitor"
            const publishUrl = "th-th/monitor"
            const publishUrlName = "th-th/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CththGW2480TL.push(url)
            }
    })
    return wrongnameB2CththGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-TRTR-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CtrtrGW2480TL=[]
const wrongnameB2CtrtrURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/tr-tr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "tr-tr"
            const publishSeries = "monitor"
            const publishUrl = "tr-tr/monitor"
            const publishUrlName = "tr-tr/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CtrtrGW2480TL.push(url)
            }
    })
    return wrongnameB2CtrtrGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-UKUA-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CukuaGW2480TL=[]
const wrongnameB2CukuaURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/uk-ua/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "uk-ua"
            const publishSeries = "monitor"
            const publishUrl = "uk-ua/monitor"
            const publishUrlName = "uk-ua/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CukuaGW2480TL.push(url)
            }
    })
    return wrongnameB2CukuaGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-VIVN-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CvivnGW2480TL=[]
const wrongnameB2CvivnURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/vi-vn/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "vi-vn"
            const publishSeries = "monitor"
            const publishUrl = "vi-vn/monitor"
            const publishUrlName = "vi-vn/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CvivnGW2480TL.push(url)
            }
    })
    return wrongnameB2CvivnGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ZHHK-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CzhhkGW2480TL=[]
const wrongnameB2CzhhkURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/zh-hk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "zh-hk"
            const publishSeries = "monitor"
            const publishUrl = "zh-hk/monitor"
            const publishUrlName = "zh-hk/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CzhhkGW2480TL.push(url)
            }
    })
    return wrongnameB2CzhhkGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ZHTW-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
const wrongnameB2CzhtwGW2480TL=[]
const wrongnameB2CzhtwURLGW2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/zh-tw/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "zh-tw"
            const publishSeries = "monitor"
            const publishUrl = "zh-tw/monitor"
            const publishUrlName = "zh-tw/monitor/stylish"
            const publishProductSeries = "stylish"
            const publishProductModel = "gw2480tl"
            const publishProductModelOther = "24-inch"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishProductModelOther)<0 ){
                wrongnameB2CzhtwGW2480TL.push(url)
            }
    })
    return wrongnameB2CzhtwGW2480TL;
    } catch (error) {
      console.log(error);
    }
};
//Test case
Given("GW2480TL must be published on global site after 20220506",{timeout: 1000 * 5000},async function(){
    //After 2022/01/01
    //global setting
    const publishModel = "gw2480tl"
    const date = new Date()
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()
    function wholeMonth(){
        const getmonth = date.getMonth() + 1
        if(getmonth<10){
            wholeMonth =  "0"+getmonth 
            return wholeMonth
        }else{
            wholeMonth = getmonth 
            return wholeMonth
        }
    }
    function wholeDate(){
        const getDay = date.getDate()
        if(getDay<10){
            wholeDate =  "0"+getDay
            return wholeDate
        }else{
            wholeDate = getDay 
            return wholeDate
        }
    }
    const wholemonth = wholeMonth()
    const wholeday = wholeDate()
    const wholeyear = date.getFullYear()
    const fullDate = `${wholeyear}/${wholemonth}/${wholeday}`
    // console.log("wholemonth:",wholemonth)
    // console.log("wholeday:",wholeday)
    // console.log("wholeyear:",wholeyear)
    console.log("fullDate:",fullDate)
    // console.log("year:",year)
    // console.log("month:",month)
    // console.log("day:",day)
    const launchDate = "2022/04/01"

    //B2C-ARME-Monitor-GW2480TL-after 20220506
    const armeReturnedData = await afterB2CarmeURLGW2480TL();
    // console.log("After returnedData",armeReturnedData)
    const armepublishCheck = armeReturnedData.length
    console.log("ar-me All publish URL-B2C Monitor - GW2480TL:",armeReturnedData)
    console.log(armeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(armepublishCheck ==0){
            // afterB2CarmeGW2480TL.push("ar-me")
            GW2480TLPublishError.push("ar-me")

        }
    }
    //B2C-BGBG-Monitor-GW2480TL-after 20220506
    const bgbgReturnedData = await afterB2CbgbgURLGW2480TL();
    // console.log("After returnedData",bgbgReturnedData)
    const bgbgpublishCheck = bgbgReturnedData.length
    console.log("bg-bg All publish URL-B2C Monitor-GW2480TL:",bgbgReturnedData)
    console.log(bgbgReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(bgbgpublishCheck ==0){
            // afterB2CbgbgGW2480TL.push("bg-bg")
            GW2480TLPublishError.push("bg-bg")

        }
    }
    //B2C-CSCZ-Monitor-GW2480TL-after 20220506
    const csczReturnedData = await afterB2CcsczURLGW2480TL();
    // console.log("After returnedData",csczReturnedData)
    const csczpublishCheck = csczReturnedData.length
    console.log("cs-cz All publish URL-B2C Monitor-GW2480TL:",csczReturnedData)
    console.log(csczReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(csczpublishCheck ==0){
            // afterB2CcsczGW2480TL.push("cs-cz")
            GW2480TLPublishError.push("cs-cz")

        }
    }
    //B2C-DEAT-Monitor-GW2480TL-after 20220506
    const deatReturnedData = await afterB2CdeatURLGW2480TL();
    // console.log("After returnedData",deatReturnedData)
    const deatpublishCheck = deatReturnedData.length
    console.log("de-at All publish URL-B2C Monitor-GW2480TL:",deatReturnedData)
    console.log(deatReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(deatpublishCheck ==0){
            // afterB2Cdea-GW2480TL.push("de-at")
        GW2480TLPublishError.push("de-at")

        }
    }
    //B2C-DECH-Monitor-GW2480TL-after 20220506
    const dechReturnedData = await afterB2CdechURLGW2480TL();
    // console.log("After returnedData",dechReturnedData)
    const dechpublishCheck = dechReturnedData.length
    console.log("de-ch All publish URL-B2C Monitor-GW2480TL:",dechReturnedData)
    console.log(dechReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(dechpublishCheck ==0){
            // afterB2CdechGW2480TL.push("de-ch")
            GW2480TLPublishError.push("de-ch")

        }
    }
    //B2C-DEDE-Monitor-GW2480TL-after 20220506
    const dedeReturnedData = await afterB2CdedeURLGW2480TL();
    // console.log("After returnedData",dedeReturnedData)
    const dedepublishCheck = dedeReturnedData.length
    console.log("de-de All publish URL-B2C Monitor-GW2480TL:",dedeReturnedData)
    console.log(dedeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(dedepublishCheck ==0){
            // afterB2CdedeGW2480TL.push("de-de")
            GW2480TLPublishError.push("de-de")

        }
    }
    //B2C-ELGR-Monitor-GW2480TL-after 20220506
    const elgrReturnedData = await afterB2CelgrURLGW2480TL();
    // console.log("After returnedData",elgrReturnedData)
    const elgrpublishCheck = elgrReturnedData.length
    console.log("el-gr All publish URL-B2C Monitor-GW2480TL:",elgrReturnedData)
    console.log(elgrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(elgrpublishCheck ==0){
            // afterB2CelgrGW2480TL.push("el-gr")
            GW2480TLPublishError.push("el-gr")

        }
    }
    //B2C-ENAP-Monitor-GW2480TL-after 20220506
    const enapReturnedData = await afterB2CenapURLGW2480TL();
    // console.log("After returnedData",enapReturnedData)
    const enappublishCheck = enapReturnedData.length
    console.log("en-ap All publish URL-B2C Monitor - GW2480TL:",enapReturnedData)
    console.log(enapReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(enappublishCheck ==0){
            // afterB2CenapGW2480TL.push("en-ap")
            GW2480TLPublishError.push("en-ap")

        }
    }
    //B2C-ENAU-Monitor-GW2480TL-after 20220506
    const enauReturnedData = await afterB2CenauURLGW2480TL();
    // console.log("After returnedData",enauReturnedData)
    const enaupublishCheck = enauReturnedData.length
    console.log("en-au All publish URL-B2C Monitor - GW2480TL:",enauReturnedData)
    console.log(enauReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(enaupublishCheck ==0){
            // afterB2CenauGW2480TL.push("en-au")
            GW2480TLPublishError.push("en-au")

        }
    }
    //B2C-ENBA-Monitor-GW2480TL-after 20220506
    const enbaReturnedData = await afterB2CenbaURLGW2480TL();
    // console.log("After returnedData",enbaReturnedData)
    const enbapublishCheck = enbaReturnedData.length
    console.log("en-ba All publish URL-B2C Monitor - GW2480TL:",enbaReturnedData)
    console.log(enbaReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(enbapublishCheck ==0){
            // afterB2CenbaGW2480TL.push("en-ba")
            GW2480TLPublishError.push("en-ba")

        }
    }
    //B2C-ENCA-Monitor-GW2480TL-after 20220506
    const encaReturnedData = await afterB2CencaURLGW2480TL();
    // console.log("After returnedData",encaReturnedData)
    const encapublishCheck = encaReturnedData.length
    console.log("en-ca All publish URL-B2C Monitor - GW2480TL:",encaReturnedData)
    console.log(encaReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(encapublishCheck ==0){
            // afterB2CencaGW2480TL.push("en-ca")
            GW2480TLPublishError.push("en-ca")
        }
    }
    //B2C-ENCEE-Monitor-GW2480TL-after 20220506
    const enceeReturnedData = await afterB2CenceeURLGW2480TL();
    // console.log("After returnedData",enceeReturnedData)
    const enceepublishCheck = enceeReturnedData.length
    console.log("en-cee All publish URL-B2C Monitor - GW2480TL:",enceeReturnedData)
    console.log(enceeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(enceepublishCheck ==0){
            // afterB2CencaGW2480TL.push("en-cee")
            GW2480TLPublishError.push("en-cee")
        }
    }
    //B2C-ENCY-Monitor-GW2480TL-after 20220506
    const encyReturnedData = await afterB2CencyURLGW2480TL();
    // console.log("After returnedData",encyReturnedData)
    const encypublishCheck = encyReturnedData.length
    console.log("en-cy All publish URL-B2C Monitor - GW2480TL:",encyReturnedData)
    console.log(encyReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(encypublishCheck ==0){
            // afterB2CencaGW2480TL.push("en-cy")
            GW2480TLPublishError.push("en-cy")
        }
    }
    //B2C-ENDK-Monitor-GW2480TL-after 20220506
    const endkReturnedData = await afterB2CendkURLGW2480TL();
    // console.log("After returnedData",endkReturnedData)
    const endkpublishCheck = endkReturnedData.length
    console.log("en-dk All publish URL-B2C Monitor - GW2480TL:",endkReturnedData)
    console.log(endkReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(endkpublishCheck ==0){
            // afterB2CencaGW2480TL.push("en-dk")
            GW2480TLPublishError.push("en-dk")
        }
    }
    //B2C-ENEE-Monitor-GW2480TL-after 20220506
    const eneeReturnedData = await afterB2CeneeURLGW2480TL();
    // console.log("After returnedData",eneeReturnedData)
    const eneepublishCheck = eneeReturnedData.length
    console.log("en-ee All publish URL-B2C Monitor-GW2480TL:",eneeReturnedData)
    console.log(eneeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(eneepublishCheck ==0){
            GW2480TLPublishError.push("en-ee")
        }
    }
    //B2C-ENEU-Monitor-GW2480TL-after 20220506
    const eneuReturnedData = await afterB2CeneuURLGW2480TL();
    // console.log("After returnedData",eneuReturnedData)
    const eneupublishCheck = eneuReturnedData.length
    console.log("en-eu All publish URL-B2C Monitor-GW2480TL:",eneuReturnedData)
    console.log(eneuReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(eneupublishCheck ==0){
            GW2480TLPublishError.push("en-eu")
        }
    }
    //B2C-ENFI-Monitor-GW2480TL-after 20220506
    const enfiReturnedData = await afterB2CenfiURLGW2480TL();
    // console.log("After returnedData",enfiReturnedData)
    const enfipublishCheck = enfiReturnedData.length
    console.log("en-fi All publish URL-B2C Monitor-GW2480TL:",enfiReturnedData)
    console.log(enfiReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(enfipublishCheck ==0){
            GW2480TLPublishError.push("en-fi")
        }
    }
    //B2C-ENHK-Monitor-GW2480TL-after 20220506
    const enhkReturnedData = await afterB2CenhkURLGW2480TL();
    // console.log("After returnedData",enhkReturnedData)
    const enhkpublishCheck = enhkReturnedData.length
    console.log("en-hk All publish URL-B2C Monitor - GW2480TL:",enhkReturnedData)
    console.log(enhkReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(enhkpublishCheck ==0){
            GW2480TLPublishError.push("en-hk")
        }
    }
    //B2C-ENHR-Monitor-GW2480TL-after 20220506
    const enhrReturnedData = await afterB2CenhrURLGW2480TL();
    // console.log("After returnedData",enhrReturnedData)
    const enhrpublishCheck = enhrReturnedData.length
    console.log("en-hr All publish URL-B2C Monitor-GW2480TL:",enhrReturnedData)
    console.log(enhrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(enhrpublishCheck ==0){
            GW2480TLPublishError.push("en-hr")
        }
    }
    //B2C-ENIE-Monitor-GW2480TL-after 20220506
    const enieReturnedData = await afterB2CenieURLGW2480TL();
    // console.log("After returnedData",enieReturnedData)
    const eniepublishCheck = enieReturnedData.length
    console.log("en-ie All publish URL-B2C Monitor-GW2480TL:",enieReturnedData)
    console.log(enieReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(eniepublishCheck ==0){
            GW2480TLPublishError.push("en-ie")
        }
    }
    //B2C-ENIN-Monitor-GW2480TL-after 20220506
    const eninReturnedData = await afterB2CeninURLGW2480TL();
    // console.log("After returnedData",eninReturnedData)
    const eninpublishCheck = eninReturnedData.length
    console.log("en-in All publish URL-B2C Monitor - GW2480TL:",eninReturnedData)
    console.log(eninReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(eninpublishCheck ==0){
            GW2480TLPublishError.push("en-in")
        }
    }
    //B2C-ENIS-Monitor-GW2480TL-after 20220506
    const enisReturnedData = await afterB2CenisURLGW2480TL();
    // console.log("After returnedData",enisReturnedData)
    const enispublishCheck = enisReturnedData.length
    console.log("en-is All publish URL-B2C Monitor-GW2480TL:",enisReturnedData)
    console.log(enisReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(enispublishCheck ==0){
            GW2480TLPublishError.push("en-is")
        }
    }
    //B2C-ENLU-Monitor-GW2480TL-after 20220506
    const enluReturnedData = await afterB2CenluURLGW2480TL();
    // console.log("After returnedData",enluReturnedData)
    const enlupublishCheck = enluReturnedData.length
    console.log("en-lu All publish URL-B2C Monitor-GW2480TL:",enluReturnedData)
    console.log(enluReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(enlupublishCheck ==0){
            GW2480TLPublishError.push("en-lu")
        }
    }
    //B2C-ENLV-Monitor-GW2480TL-after 20220506
    const enlvReturnedData = await afterB2CenlvURLGW2480TL();
    // console.log("After returnedData",enlvReturnedData)
    const enlvpublishCheck = enlvReturnedData.length
    console.log("en-lv All publish URL-B2C Monitor-GW2480TL:",enlvReturnedData)
    console.log(enlvReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(enlvpublishCheck ==0){
            GW2480TLPublishError.push("en-lv")
        }
    }
    //B2C-ENME-Monitor-GW2480TL-after 20220506
    const enmeReturnedData = await afterB2CenmeURLGW2480TL();
    // console.log("After returnedData",enmeReturnedData)
    const enmepublishCheck = enmeReturnedData.length
    console.log("en-me All publish URL-B2C Monitor - GW2480TL:",enmeReturnedData)
    console.log(enmeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(enmepublishCheck ==0){
            GW2480TLPublishError.push("en-me")
        }
    }
    //B2C-ENMK-Monitor-GW2480TL-after 20220506
    const enmkReturnedData = await afterB2CenmkURLGW2480TL();
    // console.log("After returnedData",enmkReturnedData)
    const enmkpublishCheck = enmkReturnedData.length
    console.log("en-mk All publish URL-B2C Monitor-GW2480TL:",enmkReturnedData)
    console.log(enmkReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(enmkpublishCheck ==0){
            GW2480TLPublishError.push("en-mk")
        }
    }
    //B2C-ENMT-Monitor-GW2480TL-after 20220506
    const enmtReturnedData = await afterB2CenmtURLGW2480TL();
    // console.log("After returnedData",enmtReturnedData)
    const enmtpublishCheck = enmtReturnedData.length
    console.log("en-mt All publish URL-B2C Monitor-GW2480TL:",enmtReturnedData)
    console.log(enmtReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(enmtpublishCheck ==0){
            GW2480TLPublishError.push("en-mt")
        }
    }
    //B2C-ENMY-Monitor-GW2480TL-after 20220506
    const enmyReturnedData = await afterB2CenmyURLGW2480TL();
    // console.log("After returnedData",enmyReturnedData)
    const enmypublishCheck = enmyReturnedData.length
    console.log("en-my All publish URL-B2C Monitor - GW2480TL:",enmyReturnedData)
    console.log(enmyReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(enmypublishCheck ==0){
            GW2480TLPublishError.push("en-my")
        }
    }
    //B2C-ENNO-Monitor-GW2480TL-after 20220506
    const ennoReturnedData = await afterB2CennoURLGW2480TL();
    // console.log("After returnedData",ennoReturnedData)
    const ennopublishCheck = ennoReturnedData.length
    console.log("en-no All publish URL-B2C Monitor-GW2480TL:",ennoReturnedData)
    console.log(ennoReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(ennopublishCheck ==0){
            GW2480TLPublishError.push("en-no")
        }
    }
    //B2C-ENRS-Monitor-GW2480TL-after 20220506
    const enrsReturnedData = await afterB2CenrsURLGW2480TL();
    // console.log("After returnedData",enrsReturnedData)
    const enrspublishCheck = enrsReturnedData.length
    console.log("en-rs All publish URL-B2C Monitor-GW2480TL:",enrsReturnedData)
    console.log(enrsReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(enrspublishCheck ==0){
            GW2480TLPublishError.push("en-rs")
        }
    }
    //B2C-ENSG-Monitor-GW2480TL-after 20220506
    const ensgReturnedData = await afterB2CensgURLGW2480TL();
    // console.log("After returnedData",ensgReturnedData)
    const ensgpublishCheck = ensgReturnedData.length
    console.log("en-sg All publish URL-B2C Monitor - GW2480TL:",ensgReturnedData)
    console.log(ensgReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(ensgpublishCheck ==0){
            GW2480TLPublishError.push("en-sg")
        }
    }
    //B2C-ENSI-Monitor-GW2480TL-after 20220506
    const ensiReturnedData = await afterB2CensiURLGW2480TL();
    // console.log("After returnedData",ensiReturnedData)
    const ensipublishCheck = ensiReturnedData.length
    console.log("en-si All publish URL-B2C Monitor-GW2480TL:",ensiReturnedData)
    console.log(ensiReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(ensipublishCheck ==0){
            GW2480TLPublishError.push("en-si")
        }
    }
    //B2C-ENUK-Monitor-GW2480TL-after 20220506
    const enukReturnedData = await afterB2CenukURLGW2480TL();
    // console.log("After returnedData",enukReturnedData)
    const enukpublishCheck = enukReturnedData.length
    console.log("en-uk All publish URL-B2C Monitor-GW2480TL:",enukReturnedData)
    console.log(enukReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(enukpublishCheck ==0){
            GW2480TLPublishError.push("en-uk")
        }
    }
    //B2C-ENUS-Monitor-GW2480TL-after 20220506
    const enusReturnedData = await afterB2CenusURLGW2480TL();
    // console.log("After returnedData",enusReturnedData)
    const enuspublishCheck = enusReturnedData.length
    console.log("en-us All publish URL-B2C Monitor - GW2480TL:",enusReturnedData)
    console.log(enusReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(enuspublishCheck ==0){
            GW2480TLPublishError.push("en-us")
        }
    }
    //B2C-ESAR-Monitor-GW2480TL-after 20220506
    const esarReturnedData = await afterB2CesarURLGW2480TL();
    // console.log("After returnedData",esarReturnedData)
    const esarpublishCheck = esarReturnedData.length
    console.log("es-ar All publish URL-B2C Monitor - GW2480TL:",esarReturnedData)
    console.log(esarReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(esarpublishCheck ==0){
            GW2480TLPublishError.push("es-ar")
        }
    }
    //B2C-ESCO-Monitor-GW2480TL-after 20220506
    const escoReturnedData = await afterB2CescoURLGW2480TL();
    // console.log("After returnedData",escoReturnedData)
    const escopublishCheck = escoReturnedData.length
    console.log("es-co All publish URL-B2C Monitor - GW2480TL:",escoReturnedData)
    console.log(escoReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(escopublishCheck ==0){
            GW2480TLPublishError.push("es-co")
        }
    }
    //B2C-ESES-Monitor-GW2480TL-after 20220506
    const esesReturnedData = await afterB2CesesURLGW2480TL();
    // console.log("After returnedData",esesReturnedData)
    const esespublishCheck = esesReturnedData.length
    console.log("es-es All publish URL-B2C Monitor - GW2480TL:",esesReturnedData)
    console.log(esesReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(esespublishCheck ==0){
            GW2480TLPublishError.push("es-es")
        }
    }
    //B2C-ESLA-Monitor-GW2480TL-after 20220506
    const eslaReturnedData = await afterB2CeslaURLGW2480TL();
    // console.log("After returnedData",eslaReturnedData)
    const eslapublishCheck = eslaReturnedData.length
    console.log("es-la All publish URL-B2C Monitor - GW2480TL:",eslaReturnedData)
    console.log(eslaReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(eslapublishCheck ==0){
            GW2480TLPublishError.push("es-la")
        }
    }
    //B2C-ESMX-Monitor-GW2480TL-after 20220506
    const esmxReturnedData = await afterB2CesmxURLGW2480TL();
    // console.log("After returnedData",esmxReturnedData)
    const esmxpublishCheck = esmxReturnedData.length
    console.log("es-mx All publish URL-B2C Monitor - GW2480TL:",esmxReturnedData)
    console.log(esmxReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(esmxpublishCheck ==0){
            GW2480TLPublishError.push("es-mx")
        }
    }
    //B2C-ESPE-Monitor-GW2480TL-after 20220506
    const espeReturnedData = await afterB2CespeURLGW2480TL();
    // console.log("After returnedData",espeReturnedData)
    const espepublishCheck = espeReturnedData.length
    console.log("es-pe All publish URL-B2C Monitor - GW2480TL:",espeReturnedData)
    console.log(espeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(espepublishCheck ==0){
            GW2480TLPublishError.push("es-pe")
        }
    }
    //B2C-FRCA-Monitor-GW2480TL-after 20220506
    const frcaReturnedData = await afterB2CfrcaURLGW2480TL();
    // console.log("After returnedData",frcaReturnedData)
    const frcapublishCheck = frcaReturnedData.length
    console.log("fr-ca All publish URL-B2C Monitor - GW2480TL:",frcaReturnedData)
    console.log(frcaReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(frcapublishCheck ==0){
            GW2480TLPublishError.push("fr-ca")
        }
    }
    //B2C-FRCH-Monitor-GW2480TL-after 20220506
    const frchReturnedData = await afterB2CfrchURLGW2480TL();
    // console.log("After returnedData",frchReturnedData)
    const frchpublishCheck = frchReturnedData.length
    console.log("fr-ch All publish URL-B2C Monitor - GW2480TL:",frchReturnedData)
    console.log(frchReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(frchpublishCheck ==0){
            GW2480TLPublishError.push("fr-ch")
        }
    }
    //B2C-FRFR-Monitor-GW2480TL-after 20220506
    const frfrReturnedData = await afterB2CfrfrURLGW2480TL();
    // console.log("After returnedData",frfrReturnedData)
    const frfrpublishCheck = frfrReturnedData.length
    console.log("fr-fr All publish URL-B2C Monitor - GW2480TL:",frfrReturnedData)
    console.log(frfrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(frfrpublishCheck ==0){
            GW2480TLPublishError.push("fr-fr")
        }
    }
    //B2C-HUHU-Monitor-GW2480TL-after 20220506
    const huhuReturnedData = await afterB2ChuhuURLGW2480TL();
    // console.log("After returnedData",huhuReturnedData)
    const huhupublishCheck = huhuReturnedData.length
    console.log("hu-hu All publish URL-B2C Monitor - GW2480TL:",huhuReturnedData)
    console.log(huhuReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(huhupublishCheck ==0){
            GW2480TLPublishError.push("hu-hu")
        }
    }
    //B2C-IDID-Monitor-GW2480TL-after 20220506
    const ididReturnedData = await afterB2CididURLGW2480TL();
    // console.log("After returnedData",ididReturnedData)
    const ididpublishCheck = ididReturnedData.length
    console.log("id-id All publish URL-B2C Monitor - GW2480TL:",ididReturnedData)
    console.log(ididReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(ididpublishCheck ==0){
            GW2480TLPublishError.push("id-id")
        }
    }
    //B2C-ITIT-Monitor-GW2480TL-after 20220506
    const ititReturnedData = await afterB2CititURLGW2480TL();
    // console.log("After returnedData",ititReturnedData)
    const ititpublishCheck = ititReturnedData.length
    console.log("it-it All publish URL-B2C Monitor - GW2480TL:",ititReturnedData)
    console.log(ititReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(ititpublishCheck ==0){
            GW2480TLPublishError.push("it-it")
        }
    }
    //B2C-JAJP-Monitor-GW2480TL-after 20220506
    const jajpReturnedData = await afterB2CjajpURLGW2480TL();
    // console.log("After returnedData",jajpReturnedData)
    const jajppublishCheck = jajpReturnedData.length
    console.log("ja-jp All publish URL-B2C Monitor - GW2480TL:",jajpReturnedData)
    console.log(jajpReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(jajppublishCheck ==0){
            GW2480TLPublishError.push("ja-jp")
        }
    }
    //B2C-KOKR-Monitor-GW2480TL-after 20220506
    const kokrReturnedData = await afterB2CkokrURLGW2480TL();
    // console.log("After returnedData",kokrReturnedData)
    const kokrpublishCheck = kokrReturnedData.length
    console.log("ko-kr All publish URL-B2C Monitor - GW2480TL:",kokrReturnedData)
    console.log(kokrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(kokrpublishCheck ==0){
            GW2480TLPublishError.push("ko-kr")
        }
    }
    //B2C-LTLT-Monitor-GW2480TL-after 20220506
    const ltltReturnedData = await afterB2CltltURLGW2480TL();
    // console.log("After returnedData",ltltReturnedData)
    const ltltpublishCheck = ltltReturnedData.length
    console.log("lt-lt All publish URL-B2C Monitor - GW2480TL:",ltltReturnedData)
    console.log(ltltReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(ltltpublishCheck ==0){
            GW2480TLPublishError.push("lt-lt")
        }
    }
    //B2C-NLBE-Monitor-GW2480TL-after 20220506
    const nlbeReturnedData = await afterB2CnlbeURLGW2480TL();
    // console.log("After returnedData",nlbeReturnedData)
    const nlbepublishCheck = nlbeReturnedData.length
    console.log("nl-be All publish URL-B2C Monitor - GW2480TL:",nlbeReturnedData)
    console.log(nlbeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(nlbepublishCheck ==0){
            GW2480TLPublishError.push("nl-be")
        }
    }
    //B2C-NLNL-Monitor-GW2480TL-after 20220506
    const nlnlReturnedData = await afterB2CnlnlURLGW2480TL();
    // console.log("After returnedData",nlnlReturnedData)
    const nlnlpublishCheck = nlnlReturnedData.length
    console.log("nl-nl All publish URL-B2C Monitor - GW2480TL:",nlnlReturnedData)
    console.log(nlnlReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(nlnlpublishCheck ==0){
            GW2480TLPublishError.push("nl-nl")
        }
    }
    //B2C-PLPL-Monitor-GW2480TL-after 20220506
    const plplReturnedData = await afterB2CplplURLGW2480TL();
    // console.log("After returnedData",plplReturnedData)
    const plplpublishCheck = plplReturnedData.length
    console.log("pl-pl All publish URL-B2C Monitor - GW2480TL:",plplReturnedData)
    console.log(plplReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(plplpublishCheck ==0){
            GW2480TLPublishError.push("pl-pl")
        }
    }
    //B2C-PTBR-Monitor-GW2480TL-after 20220506
    const ptbrReturnedData = await afterB2CptbrURLGW2480TL();
    // console.log("After returnedData",ptbrReturnedData)
    const ptbrpublishCheck = ptbrReturnedData.length
    console.log("pt-br All publish URL-B2C Monitor - GW2480TL:",ptbrReturnedData)
    console.log(ptbrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(ptbrpublishCheck ==0){
            GW2480TLPublishError.push("pt-br")
        }
    }
    //B2C-PTPT-Monitor-GW2480TL-after 20220506
    const ptptReturnedData = await afterB2CptptURLGW2480TL();
    // console.log("After returnedData",ptptReturnedData)
    const ptptpublishCheck = ptptReturnedData.length
    console.log("pt-pt All publish URL-B2C Monitor - GW2480TL:",ptptReturnedData)
    console.log(ptptReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(ptptpublishCheck ==0){
            GW2480TLPublishError.push("pt-pt")
        }
    }
    //B2C-RORO-Monitor-GW2480TL-after 20220506
    const roroReturnedData = await afterB2CroroURLGW2480TL();
    // console.log("After returnedData",roroReturnedData)
    const roropublishCheck = roroReturnedData.length
    console.log("ro-ro All publish URL-B2C Monitor - GW2480TL:",roroReturnedData)
    console.log(roroReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(roropublishCheck ==0){
            GW2480TLPublishError.push("ro-ro")
        }
    }
    //B2C-RURU-Monitor-GW2480TL-after 20220506
    const ruruReturnedData = await afterB2CruruURLGW2480TL();
    // console.log("After returnedData",ruruReturnedData)
    const rurupublishCheck = ruruReturnedData.length
    console.log("ru-ru All publish URL-B2C Monitor - GW2480TL:",ruruReturnedData)
    console.log(ruruReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(rurupublishCheck ==0){
            GW2480TLPublishError.push("ru-ru")
        }
    }
    //B2C-SKSK-Monitor-GW2480TL-after 20220506
    const skskReturnedData = await afterB2CskskURLGW2480TL();
    // console.log("After returnedData",skskReturnedData)
    const skskpublishCheck = skskReturnedData.length
    console.log("sk-sk All publish URL-B2C Monitor - GW2480TL:",skskReturnedData)
    console.log(skskReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(skskpublishCheck ==0){
            GW2480TLPublishError.push("sk-sk")
        }
    }
    //B2C-SVSE-Monitor-GW2480TL-after 20220506
    const svseReturnedData = await afterB2CsvseURLGW2480TL();
    // console.log("After returnedData",svseReturnedData)
    const svsepublishCheck = svseReturnedData.length
    console.log("sv-se All publish URL-B2C Monitor - GW2480TL:",svseReturnedData)
    console.log(svseReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(svsepublishCheck ==0){
            GW2480TLPublishError.push("sv-se")
        }
    }
    //B2C-THTH-Monitor-GW2480TL-after 20220506
    const ththReturnedData = await afterB2CththURLGW2480TL();
    // console.log("After returnedData",ththReturnedData)
    const ththpublishCheck = ththReturnedData.length
    console.log("th-th All publish URL-B2C Monitor - GW2480TL:",ththReturnedData)
    console.log(ththReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(ththpublishCheck ==0){
            GW2480TLPublishError.push("th-th")
        }
    }
    //B2C-TRTR-Monitor-GW2480TL-after 20220506
    const trtrReturnedData = await afterB2CtrtrURLGW2480TL();
    // console.log("After returnedData",trtrReturnedData)
    const trtrpublishCheck = trtrReturnedData.length
    console.log("tr-tr All publish URL-B2C Monitor - GW2480TL:",trtrReturnedData)
    console.log(trtrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(trtrpublishCheck ==0){
            GW2480TLPublishError.push("tr-tr")
        }
    }
    //B2C-UKUA-Monitor-GW2480TL-after 20220506
    const ukuaReturnedData = await afterB2CukuaURLGW2480TL();
    // console.log("After returnedData",ukuaReturnedData)
    const ukuapublishCheck = ukuaReturnedData.length
    console.log("uk-ua All publish URL-B2C Monitor - GW2480TL:",ukuaReturnedData)
    console.log(ukuaReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(ukuapublishCheck ==0){
            GW2480TLPublishError.push("uk-ua")
        }
    }
    //B2C-VIVN-Monitor-GW2480TL-after 20220506
    const vivnReturnedData = await afterB2CvivnURLGW2480TL();
    // console.log("After returnedData",vivnReturnedData)
    const vivnpublishCheck = vivnReturnedData.length
    console.log("vi-vn All publish URL-B2C Monitor - GW2480TL:",vivnReturnedData)
    console.log(vivnReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(vivnpublishCheck ==0){
            GW2480TLPublishError.push("vi-vn")
        }
    }
    //B2C-ZHHK-Monitor-GW2480TL-after 20220506
    const zhhkReturnedData = await afterB2CzhhkURLGW2480TL();
    // console.log("After returnedData",zhhkReturnedData)
    const zhhkpublishCheck = zhhkReturnedData.length
    console.log("zh-hk All publish URL-B2C Monitor - GW2480TL:",zhhkReturnedData)
    console.log(zhhkReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(zhhkpublishCheck ==0){
            GW2480TLPublishError.push("zh-hk")
        }
    }
    //B2C-ZHTW-Monitor-GW2480TL-after 20220506
    const zhtwReturnedData = await afterB2CzhtwURLGW2480TL();
    // console.log("After returnedData",zhtwReturnedData)
    const zhtwpublishCheck = zhtwReturnedData.length
    console.log("zh-tw All publish URL-B2C Monitor - GW2480TL:",zhtwReturnedData)
    console.log(zhtwReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //GW2480TLPublishError
        if(zhtwpublishCheck ==0){
            GW2480TLPublishError.push("zh-tw")
        }
    }
    //放全部的國家的後面
    if(GW2480TLPublishError.length >0){
        console.log("Not Published Now:",GW2480TLPublishError)
        throw new Error(`${publishModel} must be published on ${GW2480TLPublishError} before ${launchDate}, but it is not published now. Today is ${fullDate}`)
    }
})

Then("GW2480TL URL name must be 'monitor stylish gw2480tl-24-inch.html'",{timeout: 1000 * 5000},async function(){
    //After 2022/04/01
    //global setting
    const publishModel = "gw2480tl"
    const publishProductURLName = "monitor/stylish/gw2480tl-24-inch.html"
    const date = new Date()
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()
    function wholeMonth(){
        const getmonth = date.getMonth() + 1
        if(getmonth<10){
            wholeMonth =  "0"+getmonth 
            return wholeMonth
        }else{
            wholeMonth = getmonth 
            return wholeMonth
        }
    }
    function wholeDate(){
        const getDay = date.getDate()
        if(getDay<10){
            wholeDate =  "0"+getDay
            return wholeDate
        }else{
            wholeDate = getDay 
            return wholeDate
        }
    }
    const wholemonth = wholeMonth()
    const wholeday = wholeDate()
    const wholeyear = date.getFullYear()
    const fullDate = `${wholeyear}/${wholemonth}/${wholeday}`
    // console.log("wholemonth:",wholemonth)
    // console.log("wholeday:",wholeday)
    // console.log("wholeyear:",wholeyear)
    console.log("fullDate:",fullDate)
    // console.log("year:",year)
    // console.log("month:",month)
    // console.log("day:",day)
    const launchDate = "2022/04/01"
    //B2C-ARME-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const armeReturnedData = await wrongnameB2CarmeURLGW2480TL();
    const armeNameCheck = armeReturnedData.length
    console.log("ar-me wrong name URL-B2C Monitor-GW2480TL:",armeReturnedData)
    console.log(armeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(armeNameCheck > 0){
            GW2480TLNameError.push('ar-me')
            GW2480TLNameErrorURL.push(armeReturnedData)
        }
    }
    //B2C-BGBG-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const bgbgReturnedData = await wrongnameB2CbgbgURLGW2480TL();
    const bgbgNameCheck = bgbgReturnedData.length
    console.log("bg-bg wrong name URL-B2C Monitor-GW2480TL:",bgbgReturnedData)
    console.log(bgbgReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(bgbgNameCheck > 0){
            GW2480TLNameError.push('bg-bg')
            GW2480TLNameErrorURL.push(bgbgReturnedData)
        }
    }
    //B2C-CSCZ-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const csczReturnedData = await wrongnameB2CcsczURLGW2480TL();
    const csczNameCheck = csczReturnedData.length
    console.log("cs-cz wrong name URL-B2C Monitor-GW2480TL:",csczReturnedData)
    console.log(csczReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(csczNameCheck > 0){
            GW2480TLNameError.push('cs-cz')
            GW2480TLNameErrorURL.push(csczReturnedData)
        }
    }
    //B2C-DEAT-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const deatReturnedData = await wrongnameB2CdeatURLGW2480TL();
    const deatNameCheck = deatReturnedData.length
    console.log("de-at wrong name URL-B2C Monitor-GW2480TL:",deatReturnedData)
    console.log(deatReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(deatNameCheck > 0){
            GW2480TLNameError.push('de-at')
            GW2480TLNameErrorURL.push(deatReturnedData)
        }
    }
    //B2C-DECH-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const dechReturnedData = await wrongnameB2CdechURLGW2480TL();
    const dechNameCheck = dechReturnedData.length
    console.log("de-ch wrong name URL-B2C Monitor-GW2480TL:",dechReturnedData)
    console.log(dechReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(dechNameCheck > 0){
            GW2480TLNameError.push('de-ch')
            GW2480TLNameErrorURL.push(dechReturnedData)
        }
    }
    //B2C-DEDE-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const dedeReturnedData = await wrongnameB2CdedeURLGW2480TL();
    const dedeNameCheck = dedeReturnedData.length
    console.log("de-de wrong name URL-B2C Monitor-GW2480TL:",dedeReturnedData)
    console.log(dedeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(dedeNameCheck > 0){
            GW2480TLNameError.push('de-de')
            GW2480TLNameErrorURL.push(dedeReturnedData)
        }
    }
    //B2C-ELGR-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const elgrReturnedData = await wrongnameB2CelgrURLGW2480TL();
    const elgrNameCheck = elgrReturnedData.length
    console.log("el-gr wrong name URL-B2C Monitor-GW2480TL:",elgrReturnedData)
    console.log(elgrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(elgrNameCheck > 0){
            GW2480TLNameError.push('el-gr')
            GW2480TLNameErrorURL.push(elgrReturnedData)
        }
    }
    //B2C-ENAP-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const enapReturnedData = await wrongnameB2CenapURLGW2480TL();
    const enapNameCheck = enapReturnedData.length
    console.log("en-ap wrong name URL-B2C Monitor-GW2480TL:",enapReturnedData)
    console.log(enapReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enapNameCheck > 0){
            GW2480TLNameError.push("en-ap")
            GW2480TLNameErrorURL.push(enapReturnedData)
        }
    }
    //B2C-ENAU-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const enauReturnedData = await wrongnameB2CenauURLGW2480TL();
    const enauNameCheck = enauReturnedData.length
    console.log("en-au wrong name URL-B2C Monitor-GW2480TL:",enauReturnedData)
    console.log(enauReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enauNameCheck > 0){
            GW2480TLNameError.push("en-au")
            GW2480TLNameErrorURL.push(enauReturnedData)
        }
    }
    //B2C-ENBA-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const enbaReturnedData = await wrongnameB2CenbaURLGW2480TL();
    const enbaNameCheck = enbaReturnedData.length
    console.log("en-ba wrong name URL-B2C Monitor-GW2480TL:",enbaReturnedData)
    console.log(enbaReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enbaNameCheck > 0){
            GW2480TLNameError.push("en-ba")
            GW2480TLNameErrorURL.push(enbaReturnedData)
        }
    }
    //B2C-ENCA-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const encaReturnedData = await wrongnameB2CencaURLGW2480TL();
    const encaNameCheck = encaReturnedData.length
    console.log("en-ca wrong name URL-B2C Monitor-GW2480TL:",encaReturnedData)
    console.log(encaReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(encaNameCheck > 0){
            GW2480TLNameError.push("en-ca")
            GW2480TLNameErrorURL.push(encaReturnedData)
        }
    }
    //B2C-ENCEE-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const enceeReturnedData = await wrongnameB2CenceeURLGW2480TL();
    const enceeNameCheck = enceeReturnedData.length
    console.log("en-cee wrong name URL-B2C Monitor-GW2480TL:",enceeReturnedData)
    console.log(enceeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enceeNameCheck > 0){
            GW2480TLNameError.push("en-cee")
            GW2480TLNameErrorURL.push(enceeReturnedData)
        }
    }
    //B2C-ENCY-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const encyReturnedData = await wrongnameB2CencyURLGW2480TL();
    const encyNameCheck = encyReturnedData.length
    console.log("en-cy wrong name URL-B2C Monitor-GW2480TL:",encyReturnedData)
    console.log(encyReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(encyNameCheck > 0){
            GW2480TLNameError.push("en-cy")
            GW2480TLNameErrorURL.push(encyReturnedData)
        }
    }
    //B2C-ENDK-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const endkReturnedData = await wrongnameB2CendkURLGW2480TL();
    const endkNameCheck = endkReturnedData.length
    console.log("en-dk wrong name URL-B2C Monitor-GW2480TL:",endkReturnedData)
    console.log(endkReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(endkNameCheck > 0){
            GW2480TLNameError.push("en-dk")
            GW2480TLNameErrorURL.push(endkReturnedData)
        }
    }
    //B2C-ENEE-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const eneeReturnedData = await wrongnameB2CeneeURLGW2480TL();
    const eneeNameCheck = eneeReturnedData.length
    console.log("en-ee wrong name URL-B2C Monitor-GW2480TL:",eneeReturnedData)
    console.log(eneeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(eneeNameCheck > 0){
            GW2480TLNameError.push('en-ee')
            GW2480TLNameErrorURL.push(eneeReturnedData)
        }
    }
    //B2C-ENEU-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const eneuReturnedData = await wrongnameB2CeneuURLGW2480TL();
    const eneuNameCheck = eneuReturnedData.length
    console.log("en-eu wrong name URL-B2C Monitor-GW2480TL:",eneuReturnedData)
    console.log(eneuReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(eneuNameCheck > 0){
            GW2480TLNameError.push('en-eu')
            GW2480TLNameErrorURL.push(eneuReturnedData)
        }
    }
    //B2C-ENFI-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const enfiReturnedData = await wrongnameB2CenfiURLGW2480TL();
    const enfiNameCheck = enfiReturnedData.length
    console.log("en-fi wrong name URL-B2C Monitor-GW2480TL:",enfiReturnedData)
    console.log(enfiReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enfiNameCheck > 0){
            GW2480TLNameError.push('en-fi')
            GW2480TLNameErrorURL.push(enfiReturnedData)
        }
    }
    //B2C-ENHK-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const enhkReturnedData = await wrongnameB2CenhkURLGW2480TL();
    const enhkNameCheck = enhkReturnedData.length
    console.log("en-hk wrong name URL-B2C Monitor-GW2480TL:",enhkReturnedData)
    console.log(enhkReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enhkNameCheck > 0){
            GW2480TLNameError.push("en-hk")
            GW2480TLNameErrorURL.push(enhkReturnedData)
        }
    }
    //B2C-ENHR-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const enhrReturnedData = await wrongnameB2CenhrURLGW2480TL();
    const enhrNameCheck = enhrReturnedData.length
    console.log("en-hr wrong name URL-B2C Monitor-GW2480TL:",enhrReturnedData)
    console.log(enhrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enhrNameCheck > 0){
            GW2480TLNameError.push('en-hr')
            GW2480TLNameErrorURL.push(enhrReturnedData)
        }
    }
    //B2C-ENIE-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const enieReturnedData = await wrongnameB2CenieURLGW2480TL();
    const enieNameCheck = enieReturnedData.length
    console.log("en-ie wrong name URL-B2C Monitor-GW2480TL:",enieReturnedData)
    console.log(enieReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enieNameCheck > 0){
            GW2480TLNameError.push('en-ie')
            GW2480TLNameErrorURL.push(enieReturnedData)
        }
    }
    //B2C-ENIN-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const eninReturnedData = await wrongnameB2CeninURLGW2480TL();
    const eninNameCheck = eninReturnedData.length
    console.log("en-in wrong name URL-B2C Monitor-GW2480TL:",eninReturnedData)
    console.log(eninReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(eninNameCheck > 0){
            GW2480TLNameError.push("en-in")
            GW2480TLNameErrorURL.push(eninReturnedData)
        }
    }
    //B2C-ENIS-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const enisReturnedData = await wrongnameB2CenisURLGW2480TL();
    const enisNameCheck = enisReturnedData.length
    console.log("en-is wrong name URL-B2C Monitor-GW2480TL:",enisReturnedData)
    console.log(enisReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enisNameCheck > 0){
            GW2480TLNameError.push('en-is')
            GW2480TLNameErrorURL.push(enisReturnedData)
        }
    }
    //B2C-ENLU-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const enluReturnedData = await wrongnameB2CenluURLGW2480TL();
    const enluNameCheck = enluReturnedData.length
    console.log("en-lu wrong name URL-B2C Monitor-GW2480TL:",enluReturnedData)
    console.log(enluReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enluNameCheck > 0){
            GW2480TLNameError.push('en-lu')
            GW2480TLNameErrorURL.push(enluReturnedData)
        }
    }
    //B2C-ENLV-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const enlvReturnedData = await wrongnameB2CenlvURLGW2480TL();
    const enlvNameCheck = enlvReturnedData.length
    console.log("en-lv wrong name URL-B2C Monitor-GW2480TL:",enlvReturnedData)
    console.log(enlvReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enlvNameCheck > 0){
            GW2480TLNameError.push('en-lv')
            GW2480TLNameErrorURL.push(enlvReturnedData)
        }
    }
    //B2C-ENME-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const enmeReturnedData = await wrongnameB2CenmeURLGW2480TL();
    const enmeNameCheck = enmeReturnedData.length
    console.log("en-me wrong name URL-B2C Monitor-GW2480TL:",enmeReturnedData)
    console.log(enmeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enmeNameCheck > 0){
            GW2480TLNameError.push("en-me")
            GW2480TLNameErrorURL.push(enmeReturnedData)
        }
    }
    //B2C-ENMK-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const enmkReturnedData = await wrongnameB2CenmkURLGW2480TL();
    const enmkNameCheck = enmkReturnedData.length
    console.log("en-mk wrong name URL-B2C Monitor-GW2480TL:",enmkReturnedData)
    console.log(enmkReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enmkNameCheck > 0){
            GW2480TLNameError.push('en-mk')
            GW2480TLNameErrorURL.push(enmkReturnedData)
        }
    }
    //B2C-ENMT-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const enmtReturnedData = await wrongnameB2CenmtURLGW2480TL();
    const enmtNameCheck = enmtReturnedData.length
    console.log("en-mt wrong name URL-B2C Monitor-GW2480TL:",enmtReturnedData)
    console.log(enmtReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enmtNameCheck > 0){
            GW2480TLNameError.push('en-mt')
            GW2480TLNameErrorURL.push(enmtReturnedData)
        }
    }
    //B2C-ENMY-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const enmyReturnedData = await wrongnameB2CenmyURLGW2480TL();
    const enmyNameCheck = enmyReturnedData.length
    console.log("en-my wrong name URL-B2C Monitor-GW2480TL:",enmyReturnedData)
    console.log(enmyReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enmyNameCheck > 0){
            GW2480TLNameError.push("en-my")
            GW2480TLNameErrorURL.push(enmyReturnedData)
        }
    }
    //B2C-ENNO-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const ennoReturnedData = await wrongnameB2CennoURLGW2480TL();
    const ennoNameCheck = ennoReturnedData.length
    console.log("en-no wrong name URL-B2C Monitor-GW2480TL:",ennoReturnedData)
    console.log(ennoReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ennoNameCheck > 0){
            GW2480TLNameError.push('en-no')
            GW2480TLNameErrorURL.push(ennoReturnedData)
        }
    }
    //B2C-ENRS-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const enrsReturnedData = await wrongnameB2CenrsURLGW2480TL();
    const enrsNameCheck = enrsReturnedData.length
    console.log("en-rs wrong name URL-B2C Monitor-GW2480TL:",enrsReturnedData)
    console.log(enrsReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enrsNameCheck > 0){
            GW2480TLNameError.push('en-rs')
            GW2480TLNameErrorURL.push(enrsReturnedData)
        }
    }
    //B2C-ENSG-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const ensgReturnedData = await wrongnameB2CensgURLGW2480TL();
    const ensgNameCheck = ensgReturnedData.length
    console.log("en-sg wrong name URL-B2C Monitor-GW2480TL:",ensgReturnedData)
    console.log(ensgReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ensgNameCheck > 0){
            GW2480TLNameError.push("en-sg")
            GW2480TLNameErrorURL.push(ensgReturnedData)
        }
    }
    //B2C-ENSI-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const ensiReturnedData = await wrongnameB2CensiURLGW2480TL();
    const ensiNameCheck = ensiReturnedData.length
    console.log("en-si wrong name URL-B2C Monitor-GW2480TL:",ensiReturnedData)
    console.log(ensiReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ensiNameCheck > 0){
            GW2480TLNameError.push('en-si')
            GW2480TLNameErrorURL.push(ensiReturnedData)
        }
    }
    //B2C-ENUK-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const enukReturnedData = await wrongnameB2CenukURLGW2480TL();
    const enukNameCheck = enukReturnedData.length
    console.log("en-uk wrong name URL-B2C Monitor-GW2480TL:",enukReturnedData)
    console.log(enukReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enukNameCheck > 0){
            GW2480TLNameError.push('en-uk')
            GW2480TLNameErrorURL.push(enukReturnedData)
        }
    }
    //B2C-ENUS-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const enusReturnedData = await wrongnameB2CenusURLGW2480TL();
    const enusNameCheck = enusReturnedData.length
    console.log("en-us wrong name URL-B2C Monitor-GW2480TL:",enusReturnedData)
    console.log(enusReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enusNameCheck > 0){
            GW2480TLNameError.push("en-us")
            GW2480TLNameErrorURL.push(enusReturnedData)
        }
    }
    //B2C-ESAR-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const esarReturnedData = await wrongnameB2CesarURLGW2480TL();
    const esarNameCheck = esarReturnedData.length
    console.log("es-ar wrong name URL-B2C Monitor-GW2480TL:",esarReturnedData)
    console.log(esarReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(esarNameCheck > 0){
            GW2480TLNameError.push("es-ar")
            GW2480TLNameErrorURL.push(esarReturnedData)
        }
    }
    //B2C-ESCO-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const escoReturnedData = await wrongnameB2CescoURLGW2480TL();
    const escoNameCheck = escoReturnedData.length
    console.log("es-co wrong name URL-B2C Monitor-GW2480TL:",escoReturnedData)
    console.log(escoReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(escoNameCheck > 0){
            GW2480TLNameError.push("es-co")
            GW2480TLNameErrorURL.push(escoReturnedData)
        }
    }
    //B2C-ESES-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const esesReturnedData = await wrongnameB2CesesURLGW2480TL();
    const esesNameCheck = esesReturnedData.length
    console.log("es-es wrong name URL-B2C Monitor-GW2480TL:",esesReturnedData)
    console.log(esesReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(esesNameCheck > 0){
            GW2480TLNameError.push("es-es")
            GW2480TLNameErrorURL.push(esesReturnedData)
        }
    }
    //B2C-ESLA-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const eslaReturnedData = await wrongnameB2CeslaURLGW2480TL();
    const eslaNameCheck = eslaReturnedData.length
    console.log("es-la wrong name URL-B2C Monitor-GW2480TL:",eslaReturnedData)
    console.log(eslaReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(eslaNameCheck > 0){
            GW2480TLNameError.push("es-la")
            GW2480TLNameErrorURL.push(eslaReturnedData)
        }
    }
    //B2C-ESMX-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const esmxReturnedData = await wrongnameB2CesmxURLGW2480TL();
    const esmxNameCheck = esmxReturnedData.length
    console.log("es-mx wrong name URL-B2C Monitor-GW2480TL:",esmxReturnedData)
    console.log(esmxReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(esmxNameCheck > 0){
            GW2480TLNameError.push("es-mx")
            GW2480TLNameErrorURL.push(esmxReturnedData)
        }
    }
    //B2C-ESPE-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const espeReturnedData = await wrongnameB2CespeURLGW2480TL();
    const espeNameCheck = espeReturnedData.length
    console.log("es-pe wrong name URL-B2C Monitor-GW2480TL:",espeReturnedData)
    console.log(espeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(espeNameCheck > 0){
            GW2480TLNameError.push("es-pe")
            GW2480TLNameErrorURL.push(espeReturnedData)
        }
    }
    //B2C-FRCA-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const frcaReturnedData = await wrongnameB2CfrcaURLGW2480TL();
    const frcaNameCheck = frcaReturnedData.length
    console.log("fr-ca wrong name URL-B2C Monitor-GW2480TL:",frcaReturnedData)
    console.log(frcaReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(frcaNameCheck > 0){
            GW2480TLNameError.push("fr-ca")
            GW2480TLNameErrorURL.push(frcaReturnedData)
        }
    }
    //B2C-FRCH-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const frchReturnedData = await wrongnameB2CfrchURLGW2480TL();
    const frchNameCheck = frchReturnedData.length
    console.log("fr-ch wrong name URL-B2C Monitor-GW2480TL:",frchReturnedData)
    console.log(frchReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(frchNameCheck > 0){
            GW2480TLNameError.push("fr-ch")
            GW2480TLNameErrorURL.push(frchReturnedData)
        }
    }
    //B2C-FRFR-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const frfrReturnedData = await wrongnameB2CfrfrURLGW2480TL();
    const frfrNameCheck = frfrReturnedData.length
    console.log("fr-fr wrong name URL-B2C Monitor-GW2480TL:",frfrReturnedData)
    console.log(frfrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(frfrNameCheck > 0){
            GW2480TLNameError.push("fr-fr")
            GW2480TLNameErrorURL.push(frfrReturnedData)
        }
    }
    //B2C-HUHU-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const huhuReturnedData = await wrongnameB2ChuhuURLGW2480TL();
    const huhuNameCheck = huhuReturnedData.length
    console.log("hu-hu wrong name URL-B2C Monitor-GW2480TL:",huhuReturnedData)
    console.log(huhuReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(huhuNameCheck > 0){
            GW2480TLNameError.push("hu-hu")
            GW2480TLNameErrorURL.push(huhuReturnedData)
        }
    }
    //B2C-IDID-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const ididReturnedData = await wrongnameB2CididURLGW2480TL();
    const ididNameCheck = ididReturnedData.length
    console.log("id-id wrong name URL-B2C Monitor-GW2480TL:",ididReturnedData)
    console.log(ididReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ididNameCheck > 0){
            GW2480TLNameError.push("id-id")
            GW2480TLNameErrorURL.push(ididReturnedData)
        }
    }
    //B2C-ITIT-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const ititReturnedData = await wrongnameB2CititURLGW2480TL();
    const ititNameCheck = ititReturnedData.length
    console.log("it-it wrong name URL-B2C Monitor-GW2480TL:",ititReturnedData)
    console.log(ititReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ititNameCheck > 0){
            GW2480TLNameError.push("it-it")
            GW2480TLNameErrorURL.push(ititReturnedData)
        }
    }
    //B2C-JAJP-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const jajpReturnedData = await wrongnameB2CjajpURLGW2480TL();
    const jajpNameCheck = jajpReturnedData.length
    console.log("ja-jp wrong name URL-B2C Monitor-GW2480TL:",jajpReturnedData)
    console.log(jajpReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(jajpNameCheck > 0){
            GW2480TLNameError.push("ja-jp")
            GW2480TLNameErrorURL.push(jajpReturnedData)
        }
    }
    //B2C-KOKR-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const kokrReturnedData = await wrongnameB2CkokrURLGW2480TL();
    const kokrNameCheck = kokrReturnedData.length
    console.log("ko-kr wrong name URL-B2C Monitor-GW2480TL:",kokrReturnedData)
    console.log(kokrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(kokrNameCheck > 0){
            GW2480TLNameError.push("ko-kr")
            GW2480TLNameErrorURL.push(kokrReturnedData)
        }
    }
    //B2C-LTLT-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const ltltReturnedData = await wrongnameB2CltltURLGW2480TL();
    const ltltNameCheck = ltltReturnedData.length
    console.log("lt-lt wrong name URL-B2C Monitor-GW2480TL:",ltltReturnedData)
    console.log(ltltReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ltltNameCheck > 0){
            GW2480TLNameError.push("lt-lt")
            GW2480TLNameErrorURL.push(ltltReturnedData)
        }
    }
    //B2C-NLBE-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const nlbeReturnedData = await wrongnameB2CnlbeURLGW2480TL();
    const nlbeNameCheck = nlbeReturnedData.length
    console.log("nl-be wrong name URL-B2C Monitor-GW2480TL:",nlbeReturnedData)
    console.log(nlbeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(nlbeNameCheck > 0){
            GW2480TLNameError.push("nl-be")
            GW2480TLNameErrorURL.push(nlbeReturnedData)
        }
    }
    //B2C-NLNL-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const nlnlReturnedData = await wrongnameB2CnlnlURLGW2480TL();
    const nlnlNameCheck = nlnlReturnedData.length
    console.log("nl-nl wrong name URL-B2C Monitor-GW2480TL:",nlnlReturnedData)
    console.log(nlnlReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(nlnlNameCheck > 0){
            GW2480TLNameError.push("nl-nl")
            GW2480TLNameErrorURL.push(nlnlReturnedData)
        }
    }
    //B2C-PLPL-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const plplReturnedData = await wrongnameB2CplplURLGW2480TL();
    const plplNameCheck = plplReturnedData.length
    console.log("pl-pl wrong name URL-B2C Monitor-GW2480TL:",plplReturnedData)
    console.log(plplReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(plplNameCheck > 0){
            GW2480TLNameError.push("pl-pl")
            GW2480TLNameErrorURL.push(plplReturnedData)
        }
    }
    //B2C-PTBR-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const ptbrReturnedData = await wrongnameB2CptbrURLGW2480TL();
    const ptbrNameCheck = ptbrReturnedData.length
    console.log("pt-br wrong name URL-B2C Monitor-GW2480TL:",ptbrReturnedData)
    console.log(ptbrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ptbrNameCheck > 0){
            GW2480TLNameError.push("pt-br")
            GW2480TLNameErrorURL.push(ptbrReturnedData)
        }
    }
    //B2C-PTPT-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const ptptReturnedData = await wrongnameB2CptptURLGW2480TL();
    const ptptNameCheck = ptptReturnedData.length
    console.log("pt-pt wrong name URL-B2C Monitor-GW2480TL:",ptptReturnedData)
    console.log(ptptReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ptptNameCheck > 0){
            GW2480TLNameError.push("pt-pt")
            GW2480TLNameErrorURL.push(ptptReturnedData)
        }
    }
    //B2C-RORO-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const roroReturnedData = await wrongnameB2CroroURLGW2480TL();
    const roroNameCheck = roroReturnedData.length
    console.log("ro-ro wrong name URL-B2C Monitor-GW2480TL:",roroReturnedData)
    console.log(roroReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(roroNameCheck > 0){
            GW2480TLNameError.push("ro-ro")
            GW2480TLNameErrorURL.push(roroReturnedData)
        }
    }
    //B2C-RURU-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const ruruReturnedData = await wrongnameB2CruruURLGW2480TL();
    const ruruNameCheck = ruruReturnedData.length
    console.log("ru-ru wrong name URL-B2C Monitor-GW2480TL:",ruruReturnedData)
    console.log(ruruReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ruruNameCheck > 0){
            GW2480TLNameError.push("ru-ru")
            GW2480TLNameErrorURL.push(ruruReturnedData)
        }
    }
    //B2C-SKSK-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const skskReturnedData = await wrongnameB2CskskURLGW2480TL();
    const skskNameCheck = skskReturnedData.length
    console.log("sk-sk wrong name URL-B2C Monitor-GW2480TL:",skskReturnedData)
    console.log(skskReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(skskNameCheck > 0){
            GW2480TLNameError.push("sk-sk")
            GW2480TLNameErrorURL.push(skskReturnedData)
        }
    }
    //B2C-SVSE-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const svseReturnedData = await wrongnameB2CsvseURLGW2480TL();
    const svseNameCheck = svseReturnedData.length
    console.log("sv-se wrong name URL-B2C Monitor-GW2480TL:",svseReturnedData)
    console.log(svseReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(svseNameCheck > 0){
            GW2480TLNameError.push("sv-se")
            GW2480TLNameErrorURL.push(svseReturnedData)
        }
    }
    //B2C-THTH-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const ththReturnedData = await wrongnameB2CththURLGW2480TL();
    const ththNameCheck = ththReturnedData.length
    console.log("th-th wrong name URL-B2C Monitor-GW2480TL:",ththReturnedData)
    console.log(ththReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ththNameCheck > 0){
            GW2480TLNameError.push("th-th")
            GW2480TLNameErrorURL.push(ththReturnedData)
        }
    }
    //B2C-TRTR-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const trtrReturnedData = await wrongnameB2CtrtrURLGW2480TL();
    const trtrNameCheck = trtrReturnedData.length
    console.log("tr-tr wrong name URL-B2C Monitor-GW2480TL:",trtrReturnedData)
    console.log(trtrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(trtrNameCheck > 0){
            GW2480TLNameError.push("tr-tr")
            GW2480TLNameErrorURL.push(trtrReturnedData)
        }
    }
    //B2C-UKUA-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const ukuaReturnedData = await wrongnameB2CukuaURLGW2480TL();
    const ukuaNameCheck = ukuaReturnedData.length
    console.log("uk-ua wrong name URL-B2C Monitor-GW2480TL:",ukuaReturnedData)
    console.log(ukuaReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ukuaNameCheck > 0){
            GW2480TLNameError.push("uk-ua")
            GW2480TLNameErrorURL.push(ukuaReturnedData)
        }
    }
    //B2C-VIVN-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const vivnReturnedData = await wrongnameB2CvivnURLGW2480TL();
    const vivnNameCheck = vivnReturnedData.length
    console.log("vi-vn wrong name URL-B2C Monitor-GW2480TL:",vivnReturnedData)
    console.log(vivnReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(vivnNameCheck > 0){
            GW2480TLNameError.push("vi-vn")
            GW2480TLNameErrorURL.push(vivnReturnedData)
        }
    }
    //B2C-ZHHK-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const zhhkReturnedData = await wrongnameB2CzhhkURLGW2480TL();
    const zhhkNameCheck = zhhkReturnedData.length
    console.log("zh-hk wrong name URL-B2C Monitor-GW2480TL:",zhhkReturnedData)
    console.log(zhhkReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(zhhkNameCheck > 0){
            GW2480TLNameError.push("zh-hk")
            GW2480TLNameErrorURL.push(zhhkReturnedData)
        }
    }
    //B2C-ZHTW-Monitor-GW2480TL-name must be monitor/stylish/gw2480tl-24-inch.html
    const zhtwReturnedData = await wrongnameB2CzhtwURLGW2480TL();
    const zhtwNameCheck = zhtwReturnedData.length
    console.log("zh-tw wrong name URL-B2C Monitor-GW2480TL:",zhtwReturnedData)
    console.log(zhtwReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(zhtwNameCheck > 0){
            GW2480TLNameError.push("zh-tw")
            GW2480TLNameErrorURL.push(zhtwReturnedData)
        }
    }

    //放全部國家的後面
    if(GW2480TLNameError.length >0){
        throw new Error(`${publishModel} URL name must be ${publishProductURLName}, but it is wrong now. Here is RO name: ${GW2480TLNameError} And wrong URL: ${GW2480TLNameErrorURL}`)
    }
})
