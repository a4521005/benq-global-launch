Feature:BenQ Global Launch
    tests:
    1. B2B/B2C products must be published
    2. B2B/B2C products must not be published
    3. B2B/B2C products series name are wrong

    # Example
    # Scenario: [B2C] SW321C
    #     Given SW321C must not be published on 'en-us' before 20211231
    #     Given SW321C must be published on 'en-us' after 20220101
    #     Then SW321C series name must be photographer
    
    # Scenario: [B2C] SW321C
    #     Given SW321C must be published on 'en-ap' before 2021/12/31
    #     Given SW321C must not be published on 'en-ap' after 2022/01/01
    #     Then SW321C series name must be photographer/sw321c.html

    # Scenario: [B2C Projector] X3000i - published on global
    #     Given X3000i must be published on global site after 20220228
    # Scenario: [B2C Projector] X3000i - URL name
    #     Then X3000i URL name must be 'projector gaming-projector x3000i.html'

    # Scenario: [B2C Projector] TH585P - published on global
    #     Given TH585P must be published on global site after 20220401
    # Scenario: [B2C Projector] TH585P - URL name
    #     Then TH585P URL name must be 'projector gaming-projector th585p.html'

    # Scenario: [B2C Projector] TH685P - published on global
    #     Given TH685P must be published on global site after 20220401
    # Scenario: [B2C Projector] TH685P - URL name
    #     Then TH685P URL name must be 'projector gaming-projector th685p.html'

    # Scenario: [B2C Professional Monitor accessory] HB27 - published on global
    #     Given HB27 must be published on global site after 20220506
    # Scenario: [B2C Professional Monitor accessory] HB27 - URL name
    #     Then HB27 URL name must be 'monitor accessory hb27.html'

    Scenario: [B2B Monitor] BL2480L - published on global
        Given BL2480L must be published on global site after 20220506
    Scenario: [B2B Monitor] BL2480L - URL name
        Then BL2480L URL name must be 'monitor bl2480l.html'

    Scenario: [B2B Monitor] BL2480TL - published on global
        Given BL2480TL must be published on global site after 20220506
    Scenario: [B2B Monitor] BL2480TL - URL name
        Then BL2480TL URL name must be 'monitor bl2480tl.html'

    Scenario: [B2C Monitor] GW2480TL - published on global
        Given GW2480TL must be published on global site after 20220506
    Scenario: [B2C Monitor] GW2480TL - URL name
        Then GW2480TL URL name must be 'monitor stylish gw2480tl-24-inch.html'