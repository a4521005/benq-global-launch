//Projector X3000i - URL name check
const X3000iNameError=[]
const X3000iNameErrorURL=[]
//B2C-ENUS-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CenusX3000i=[]
const wrongnameB2CenusURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-us/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-us"
            const publishSeries = "projector"
            const publishUrl = "en-us/projector"
            const publishUrlName = "en-us/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenusX3000i.push(url)
            }
    })
    return wrongnameB2CenusX3000i;
    } catch (error) {
      console.log(error);
    }
};

//Test case
Then("X3000i URL name must be 'projector gaming-projector x3000i.html'",{timeout: 24 * 5000},async function(){
    //After 2022/01/01
    //global setting
    const publishModel = "x3000i"
    const publishProductURLName = "projector/gaming-projector/x3000i.html"
    const date = new Date()
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()
    function wholeMonth(){
        const getmonth = date.getMonth() + 1
        if(getmonth<10){
            wholeMonth =  "0"+getmonth 
            return wholeMonth
        }else{
            wholeMonth = getmonth 
            return wholeMonth
        }
    }
    function wholeDate(){
        const getDay = date.getDate()
        if(getDay<10){
            wholeDate =  "0"+getDay
            return wholeDate
        }else{
            wholeDate = getDay 
            return wholeDate
        }
    }
    const wholemonth = wholeMonth()
    const wholeday = wholeDate()
    const wholeyear = date.getFullYear()
    const fullDate = `${wholeyear}/${wholemonth}/${wholeday}`
    // console.log("wholemonth:",wholemonth)
    // console.log("wholeday:",wholeday)
    // console.log("wholeyear:",wholeyear)
    console.log("fullDate:",fullDate)
    // console.log("year:",year)
    // console.log("month:",month)
    // console.log("day:",day)
    const launchDate = "2022/02/28"
    //B2C-ENUS-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const enusReturnedData = await wrongnameB2CenusURLX3000i();
    const enusNameCheck = enusReturnedData.length
    console.log("en-us wrong name URL-B2C Projector-X3000i:",enusReturnedData)
    console.log(enusReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enusNameCheck > 0){
            X3000iNameError.push("en-us")
            X3000iNameErrorURL.push(enusReturnedData)
        }
    }



    //放全部國家的後面
    if(X3000iNameError.length >0){
        throw new Error(`${publishModel} URL name must be ${publishProductURLName}, but it is wrong now. Here is RO name: ${X3000iNameError} And wrong URL: ${X3000iNameErrorURL}`)
    }
})


//複製這個
//benq.com
//Projector X3000i - URL name check
//B2C-ENUS-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CenusX3000i=[]
const wrongnameB2CenusURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-us/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-us"
            const publishSeries = "projector"
            const publishUrl = "en-us/projector"
            const publishUrlName = "en-us/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenusX3000i.push(url)
            }
    })
    return wrongnameB2CenusX3000i;
    } catch (error) {
      console.log(error);
    }
};


//下半部-test case部分
//test case 2: X3000i URL name must be 'projector gaming-projector x3000i.html
    //B2C-ENUS-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const enusReturnedData = await wrongnameB2CenusURLX3000i();
    const enusNameCheck = enusReturnedData.length
    console.log("en-us wrong name URL-B2C Projector-X3000i:",enusReturnedData)
    console.log(enusReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enusNameCheck > 0){
            X3000iNameError.push("en-us")
            X3000iNameErrorURL.push(enusReturnedData)
        }
    }



//benq.eu
//Projector X3000i - URL name check
//B2C-ENEU-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CeneuX3000i=[]
const wrongnameB2CeneuURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-eu/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-eu"
            const publishSeries = "projector"
            const publishUrl = "en-eu/projector"
            const publishUrlName = "en-eu/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CeneuX3000i.push(url)
            }
    })
    return wrongnameB2CeneuX3000i;
    } catch (error) {
      console.log(error);
    }
};

//下半部-test case部分
//test case 2: X3000i URL name must be 'projector gaming-projector x3000i.html
    //B2C-ENEU-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const eneuReturnedData = await wrongnameB2CeneuURLX3000i();
    const eneuNameCheck = eneuReturnedData.length
    console.log("en-eu wrong name URL-B2C Projector-X3000i:",eneuReturnedData)
    console.log(eneuReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(eneuNameCheck > 0){
            X3000iNameError.push('en-eu')
            X3000iNameErrorURL.push(eneuReturnedData)
        }
    }
