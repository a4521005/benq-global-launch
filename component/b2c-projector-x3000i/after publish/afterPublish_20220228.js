//Projector X3000i - publish check
const X3000iPublishError=[]
//B2C-ENUS-Projector-X3000i-after 20220228
const afterB2CenusX3000i=[]
const afterB2CenusURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-us/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenusX3000i.push(url)
            }
    })
    return afterB2CenusX3000i;
    } catch (error) {
      console.log(error);
    }
};

//Test case
Given("X3000i must be published on global site after 20220228",{timeout: 24 * 5000},async function(){
    //After 2022/01/01
    const enusReturnedData = await afterB2CenusURLX3000i();
    console.log("After returnedData",enusReturnedData)
    const enuspublishCheck = enusReturnedData.length
    const publishModel = "x3000i"
    const date = new Date()
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()
    function wholeMonth(){
        const getmonth = date.getMonth() + 1
        if(getmonth<10){
            wholeMonth =  "0"+getmonth 
            return wholeMonth
        }else{
            wholeMonth = getmonth 
            return wholeMonth
        }
    }
    function wholeDate(){
        const getDay = date.getDate()
        if(getDay<10){
            wholeDate =  "0"+getDay
            return wholeDate
        }else{
            wholeDate = getDay 
            return wholeDate
        }
    }
    const wholemonth = wholeMonth()
    const wholeday = wholeDate()
    const wholeyear = date.getFullYear()
    const fullDate = `${wholeyear}/${wholemonth}/${wholeday}`
    console.log("wholemonth:",wholemonth)
    console.log("wholeday:",wholeday)
    console.log("wholeyear:",wholeyear)
    console.log("fullDate:",fullDate)
    console.log("year:",year)
    console.log("month:",month)
    console.log("day:",day)
    const launchDate = "2022/02/28"
    console.log("en-us B2C Projector - X3000i:",enusReturnedData)
    console.log(enusReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(enuspublishCheck ==0){
            X3000iPublishError.push("en-us")
        }
    }


    //放全部的國家的後面
    if(X3000iPublishError.length >0){
        throw new Error(`${publishModel} must be published on ${X3000iPublishError} after  ${launchDate}, but it is not published now. Today is ${fullDate}`)
    }
})


//複製這個
//benq.com
//上半部
//Projector X3000i - publish check
//B2C-ENUS-Projector-X3000i-after 20220228
const afterB2CenusX3000i=[]
const afterB2CenusURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-us/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenusX3000i.push(url)
            }
    })
    return afterB2CenusX3000i;
    } catch (error) {
      console.log(error);
    }
};


//下半部-test case部分
//test case 1: X3000i must be published on global site after 20220228
    //B2C-ENUS-Projector-X3000i-after 20220228
    const enusReturnedData = await afterB2CenusURLX3000i();
    // console.log("After returnedData",enusReturnedData)
    const enuspublishCheck = enusReturnedData.length
    console.log("en-us All publish URL-B2C Projector - X3000i:",enusReturnedData)
    console.log(enusReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(enuspublishCheck ==0){
            X3000iPublishError.push("en-us")
        }
    }


//benq.eu
//上半部
//Projector X3000i - publish check
//B2C-ENEU-Projector-X3000i-after 20220228
const afterB2CeneuX3000i=[]
const afterB2CeneuURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-eu/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CeneuX3000i.push(url)
            }
    })
    return afterB2CeneuX3000i;
    } catch (error) {
      console.log(error);
    }
};

//下半部-test case部分
//test case 1:X3000i must be published on global site after 20220228
    //B2C-ENEU-Projector-X3000i-after 20220228
    const eneuReturnedData = await afterB2CeneuURLX3000i();
    // console.log("After returnedData",eneuReturnedData)
    const eneupublishCheck = eneuReturnedData.length
    console.log("en-eu All publish URL-B2C Projector-X3000i:",eneuReturnedData)
    console.log(eneuReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(eneupublishCheck ==0){
            X3000iPublishError.push("en-eu")
        }
    }
