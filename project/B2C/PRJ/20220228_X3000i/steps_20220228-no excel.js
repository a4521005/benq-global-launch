const { Given, When, Then, Before, After } = require('@cucumber/cucumber')
const { expect } = require('chai')
const fs = require('fs');
const puppeteer = require('puppeteer')
const request = require("request-promise");
const cheerio = require("cheerio");
const cicGA="?utm_source=autotest&utm_medium=CIC"



Before({timeout: 24 * 5000},async function () {
    this.browser = await puppeteer.launch({ 
        executablePath:
        "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
        // MAC路徑:
        // executablePath:
        // '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome',
        headless:true,//有無需要開視窗,false要開,true不開
        slowMo:100,// slow down by 100ms
        devtools:false//有無需要開啟開發人員工具
    })
    this.page = await this.browser.newPage()
    await this.page.setViewport({width:1200,height:1000})
    await this.page.setDefaultTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
    await this.page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
})

After({timeout: 12 * 5000},async function () {
    await this.browser.close()
})

//Projector X3000i - publish check
const X3000iPublishError=[]
//B2C-ARME-Projector-X3000i-after 20220228
const afterB2CarmeX3000i=[]
const afterB2CarmeURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ar-me/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CarmeX3000i.push(url)
            }
    })
    return afterB2CarmeX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-BGBG-Projector-X3000i-after 20220228
const afterB2CbgbgX3000i=[]
const afterB2CbgbgURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/bg-bg/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CbgbgX3000i.push(url)
            }
    })
    return afterB2CbgbgX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-CSCZ-Projector-X3000i-after 20220228
const afterB2CcsczX3000i=[]
const afterB2CcsczURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/cs-cz/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CcsczX3000i.push(url)
            }
    })
    return afterB2CcsczX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-DEAT-Projector-X3000i-after 20220228
const afterB2CdeatX3000i=[]
const afterB2CdeatURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/de-at/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CdeatX3000i.push(url)
            }
    })
    return afterB2CdeatX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-DECH-Projector-X3000i-after 20220228
const afterB2CdechX3000i=[]
const afterB2CdechURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/de-ch/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CdechX3000i.push(url)
            }
    })
    return afterB2CdechX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-DEDE-Projector-X3000i-after 20220228
const afterB2CdedeX3000i=[]
const afterB2CdedeURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/de-de/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CdedeX3000i.push(url)
            }
    })
    return afterB2CdedeX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ELGR-Projector-X3000i-after 20220228
const afterB2CelgrX3000i=[]
const afterB2CelgrURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/el-gr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CelgrX3000i.push(url)
            }
    })
    return afterB2CelgrX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENAP-Projector-X3000i-after 20220228
const afterB2CenapX3000i=[]
const afterB2CenapURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-ap/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenapX3000i.push(url)
            }
    })
    return afterB2CenapX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENAU-Projector-X3000i-after 20220228
const afterB2CenauX3000i=[]
const afterB2CenauURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-au/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenauX3000i.push(url)
            }
    })
    return afterB2CenauX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENBA-Projector-X3000i-after 20220228
const afterB2CenbaX3000i=[]
const afterB2CenbaURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-ba/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenbaX3000i.push(url)
            }
    })
    return afterB2CenbaX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENCA-Projector-X3000i-after 20220228
const afterB2CencaX3000i=[]
const afterB2CencaURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-ca/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CencaX3000i.push(url)
            }
    })
    return afterB2CencaX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENCEE-Projector-X3000i-after 20220228
const afterB2CenceeX3000i=[]
const afterB2CenceeURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-cee/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenceeX3000i.push(url)
            }
    })
    return afterB2CenceeX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENCY-Projector-X3000i-after 20220228
const afterB2CencyX3000i=[]
const afterB2CencyURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-cy/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CencyX3000i.push(url)
            }
    })
    return afterB2CencyX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENDK-Projector-X3000i-after 20220228
const afterB2CendkX3000i=[]
const afterB2CendkURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-dk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CendkX3000i.push(url)
            }
    })
    return afterB2CendkX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENEE-Projector-X3000i-after 20220228
const afterB2CeneeX3000i=[]
const afterB2CeneeURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-ee/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CeneeX3000i.push(url)
            }
    })
    return afterB2CeneeX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENEU-Projector-X3000i-after 20220228
const afterB2CeneuX3000i=[]
const afterB2CeneuURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-eu/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CeneuX3000i.push(url)
            }
    })
    return afterB2CeneuX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENFI-Projector-X3000i-after 20220228
const afterB2CenfiX3000i=[]
const afterB2CenfiURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-fi/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenfiX3000i.push(url)
            }
    })
    return afterB2CenfiX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENHK-Projector-X3000i-after 20220228
const afterB2CenhkX3000i=[]
const afterB2CenhkURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-hk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenhkX3000i.push(url)
            }
    })
    return afterB2CenhkX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENHR-Projector-X3000i-after 20220228
const afterB2CenhrX3000i=[]
const afterB2CenhrURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-hr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenhrX3000i.push(url)
            }
    })
    return afterB2CenhrX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENIE-Projector-X3000i-after 20220228
const afterB2CenieX3000i=[]
const afterB2CenieURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-ie/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenieX3000i.push(url)
            }
    })
    return afterB2CenieX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENIN-Projector-X3000i-after 20220228
const afterB2CeninX3000i=[]
const afterB2CeninURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-in/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CeninX3000i.push(url)
            }
    })
    return afterB2CeninX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENIS-Projector-X3000i-after 20220228
const afterB2CenisX3000i=[]
const afterB2CenisURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-is/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenisX3000i.push(url)
            }
    })
    return afterB2CenisX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENLU-Projector-X3000i-after 20220228
const afterB2CenluX3000i=[]
const afterB2CenluURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-lu/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenluX3000i.push(url)
            }
    })
    return afterB2CenluX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENLV-Projector-X3000i-after 20220228
const afterB2CenlvX3000i=[]
const afterB2CenlvURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-lv/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenlvX3000i.push(url)
            }
    })
    return afterB2CenlvX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENME-Projector-X3000i-after 20220228
const afterB2CenmeX3000i=[]
const afterB2CenmeURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-me/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenmeX3000i.push(url)
            }
    })
    return afterB2CenmeX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENMK-Projector-X3000i-after 20220228
const afterB2CenmkX3000i=[]
const afterB2CenmkURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-mk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenmkX3000i.push(url)
            }
    })
    return afterB2CenmkX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENMT-Projector-X3000i-after 20220228
const afterB2CenmtX3000i=[]
const afterB2CenmtURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-mt/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenmtX3000i.push(url)
            }
    })
    return afterB2CenmtX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENMY-Projector-X3000i-after 20220228
const afterB2CenmyX3000i=[]
const afterB2CenmyURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-my/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenmyX3000i.push(url)
            }
    })
    return afterB2CenmyX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENNO-Projector-X3000i-after 20220228
const afterB2CennoX3000i=[]
const afterB2CennoURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-no/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CennoX3000i.push(url)
            }
    })
    return afterB2CennoX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENRS-Projector-X3000i-after 20220228
const afterB2CenrsX3000i=[]
const afterB2CenrsURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-rs/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenrsX3000i.push(url)
            }
    })
    return afterB2CenrsX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENSG-Projector-X3000i-after 20220228
const afterB2CensgX3000i=[]
const afterB2CensgURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-sg/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CensgX3000i.push(url)
            }
    })
    return afterB2CensgX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENSI-Projector-X3000i-after 20220228
const afterB2CensiX3000i=[]
const afterB2CensiURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-si/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CensiX3000i.push(url)
            }
    })
    return afterB2CensiX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENUK-Projector-X3000i-after 20220228
const afterB2CenukX3000i=[]
const afterB2CenukURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-uk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenukX3000i.push(url)
            }
    })
    return afterB2CenukX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENUS-Projector-X3000i-after 20220228
const afterB2CenusX3000i=[]
const afterB2CenusURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-us/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenusX3000i.push(url)
            }
    })
    return afterB2CenusX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESAR-Projector-X3000i-after 20220228
const afterB2CesarX3000i=[]
const afterB2CesarURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-ar/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CesarX3000i.push(url)
            }
    })
    return afterB2CesarX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESCO-Projector-X3000i-after 20220228
const afterB2CescoX3000i=[]
const afterB2CescoURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-co/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CescoX3000i.push(url)
            }
    })
    return afterB2CescoX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESES-Projector-X3000i-after 20220228
const afterB2CesesX3000i=[]
const afterB2CesesURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-es/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CesesX3000i.push(url)
            }
    })
    return afterB2CesesX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESLA-Projector-X3000i-after 20220228
const afterB2CeslaX3000i=[]
const afterB2CeslaURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/es-la/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CeslaX3000i.push(url)
            }
    })
    return afterB2CeslaX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESMX-Projector-X3000i-after 20220228
const afterB2CesmxX3000i=[]
const afterB2CesmxURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/es-mx/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CesmxX3000i.push(url)
            }
    })
    return afterB2CesmxX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESPE-Projector-X3000i-after 20220228
const afterB2CespeX3000i=[]
const afterB2CespeURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-pe/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CespeX3000i.push(url)
            }
    })
    return afterB2CespeX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-FRCA-Projector-X3000i-after 20220228
const afterB2CfrcaX3000i=[]
const afterB2CfrcaURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/fr-ca/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CfrcaX3000i.push(url)
            }
    })
    return afterB2CfrcaX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-FRCH-Projector-X3000i-after 20220228
const afterB2CfrchX3000i=[]
const afterB2CfrchURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/fr-ch/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CfrchX3000i.push(url)
            }
    })
    return afterB2CfrchX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-FRFR-Projector-X3000i-after 20220228
const afterB2CfrfrX3000i=[]
const afterB2CfrfrURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/fr-fr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CfrfrX3000i.push(url)
            }
    })
    return afterB2CfrfrX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-HUHU-Projector-X3000i-after 20220228
const afterB2ChuhuX3000i=[]
const afterB2ChuhuURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/hu-hu/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2ChuhuX3000i.push(url)
            }
    })
    return afterB2ChuhuX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-IDID-Projector-X3000i-after 20220228
const afterB2CididX3000i=[]
const afterB2CididURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/id-id/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CididX3000i.push(url)
            }
    })
    return afterB2CididX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ITIT-Projector-X3000i-after 20220228
const afterB2CititX3000i=[]
const afterB2CititURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/it-it/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CititX3000i.push(url)
            }
    })
    return afterB2CititX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-JAJP-Projector-X3000i-after 20220228
const afterB2CjajpX3000i=[]
const afterB2CjajpURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ja-jp/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CjajpX3000i.push(url)
            }
    })
    return afterB2CjajpX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-KOKR-Projector-X3000i-after 20220228
const afterB2CkokrX3000i=[]
const afterB2CkokrURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ko-kr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CkokrX3000i.push(url)
            }
    })
    return afterB2CkokrX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-LTLT-Projector-X3000i-after 20220228
const afterB2CltltX3000i=[]
const afterB2CltltURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/lt-lt/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CltltX3000i.push(url)
            }
    })
    return afterB2CltltX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-NLBE-Projector-X3000i-after 20220228
const afterB2CnlbeX3000i=[]
const afterB2CnlbeURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/nl-be/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CnlbeX3000i.push(url)
            }
    })
    return afterB2CnlbeX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-NLNL-Projector-X3000i-after 20220228
const afterB2CnlnlX3000i=[]
const afterB2CnlnlURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/nl-nl/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CnlnlX3000i.push(url)
            }
    })
    return afterB2CnlnlX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-PLPL-Projector-X3000i-after 20220228
const afterB2CplplX3000i=[]
const afterB2CplplURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/pl-pl/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CplplX3000i.push(url)
            }
    })
    return afterB2CplplX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-PTBR-Projector-X3000i-after 20220228
const afterB2CptbrX3000i=[]
const afterB2CptbrURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/pt-br/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CptbrX3000i.push(url)
            }
    })
    return afterB2CptbrX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-PTPT-Projector-X3000i-after 20220228
const afterB2CptptX3000i=[]
const afterB2CptptURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/pt-pt/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CptptX3000i.push(url)
            }
    })
    return afterB2CptptX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-RORO-Projector-X3000i-after 20220228
const afterB2CroroX3000i=[]
const afterB2CroroURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/ro-ro/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CroroX3000i.push(url)
            }
    })
    return afterB2CroroX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-RURU-Projector-X3000i-after 20220228
const afterB2CruruX3000i=[]
const afterB2CruruURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ru-ru/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CruruX3000i.push(url)
            }
    })
    return afterB2CruruX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-SKSK-Projector-X3000i-after 20220228
const afterB2CskskX3000i=[]
const afterB2CskskURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/sk-sk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CskskX3000i.push(url)
            }
    })
    return afterB2CskskX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-SVSE-Projector-X3000i-after 20220228
const afterB2CsvseX3000i=[]
const afterB2CsvseURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/sv-se/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CsvseX3000i.push(url)
            }
    })
    return afterB2CsvseX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-THTH-Projector-X3000i-after 20220228
const afterB2CththX3000i=[]
const afterB2CththURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/th-th/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CththX3000i.push(url)
            }
    })
    return afterB2CththX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-TRTR-Projector-X3000i-after 20220228
const afterB2CtrtrX3000i=[]
const afterB2CtrtrURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/tr-tr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CtrtrX3000i.push(url)
            }
    })
    return afterB2CtrtrX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-UKUA-Projector-X3000i-after 20220228
const afterB2CukuaX3000i=[]
const afterB2CukuaURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/uk-ua/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CukuaX3000i.push(url)
            }
    })
    return afterB2CukuaX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-VIVN-Projector-X3000i-after 20220228
const afterB2CvivnX3000i=[]
const afterB2CvivnURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/vi-vn/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CvivnX3000i.push(url)
            }
    })
    return afterB2CvivnX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ZHHK-Projector-X3000i-after 20220228
const afterB2CzhhkX3000i=[]
const afterB2CzhhkURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/zh-hk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CzhhkX3000i.push(url)
            }
    })
    return afterB2CzhhkX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ZHTW-Projector-X3000i-after 20220228
const afterB2CzhtwX3000i=[]
const afterB2CzhtwURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/zh-tw/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CzhtwX3000i.push(url)
            }
    })
    return afterB2CzhtwX3000i;
    } catch (error) {
      console.log(error);
    }
};

//Projector X3000i - URL name check
const X3000iNameError=[]
const X3000iNameErrorURL=[]
//B2C-ARME-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CarmeX3000i=[]
const wrongnameB2CarmeURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ar-me/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ar-me"
            const publishSeries = "projector"
            const publishUrl = "ar-me/projector"
            const publishUrlName = "ar-me/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CarmeX3000i.push(url)
            }
    })
    return wrongnameB2CarmeX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-BGBG-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CbgbgX3000i=[]
const wrongnameB2CbgbgURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/bg-bg/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "bg-bg"
            const publishSeries = "projector"
            const publishUrl = "bg-bg/projector"
            const publishUrlName = "bg-bg/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CbgbgX3000i.push(url)
            }
    })
    return wrongnameB2CbgbgX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-CSCZ-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CcsczX3000i=[]
const wrongnameB2CcsczURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/cs-cz/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "cs-cz"
            const publishSeries = "projector"
            const publishUrl = "cs-cz/projector"
            const publishUrlName = "cs-cz/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CcsczX3000i.push(url)
            }
    })
    return wrongnameB2CcsczX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-DEAT-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CdeatX3000i=[]
const wrongnameB2CdeatURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/de-at/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "de-at"
            const publishSeries = "projector"
            const publishUrl = "de-at/projector"
            const publishUrlName = "de-at/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CdeatX3000i.push(url)
            }
    })
    return wrongnameB2CdeatX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-DECH-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CdechX3000i=[]
const wrongnameB2CdechURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/de-ch/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "de-ch"
            const publishSeries = "projector"
            const publishUrl = "de-ch/projector"
            const publishUrlName = "de-ch/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CdechX3000i.push(url)
            }
    })
    return wrongnameB2CdechX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-DEDE-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CdedeX3000i=[]
const wrongnameB2CdedeURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/de-de/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "de-de"
            const publishSeries = "projector"
            const publishUrl = "de-de/projector"
            const publishUrlName = "de-de/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CdedeX3000i.push(url)
            }
    })
    return wrongnameB2CdedeX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ELGR-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CelgrX3000i=[]
const wrongnameB2CelgrURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/el-gr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "el-gr"
            const publishSeries = "projector"
            const publishUrl = "el-gr/projector"
            const publishUrlName = "el-gr/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CelgrX3000i.push(url)
            }
    })
    return wrongnameB2CelgrX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENAP-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CenapX3000i=[]
const wrongnameB2CenapURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-ap/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-ap"
            const publishSeries = "projector"
            const publishUrl = "en-ap/projector"
            const publishUrlName = "en-ap/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenapX3000i.push(url)
            }
    })
    return wrongnameB2CenapX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENAU-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CenauX3000i=[]
const wrongnameB2CenauURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-au/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-au"
            const publishSeries = "projector"
            const publishUrl = "en-au/projector"
            const publishUrlName = "en-au/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenauX3000i.push(url)
            }
    })
    return wrongnameB2CenauX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENBA-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CenbaX3000i=[]
const wrongnameB2CenbaURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-ba/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-ba"
            const publishSeries = "projector"
            const publishUrl = "en-ba/projector"
            const publishUrlName = "en-ba/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenbaX3000i.push(url)
            }
    })
    return wrongnameB2CenbaX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENCA-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CencaX3000i=[]
const wrongnameB2CencaURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-ca/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-ca"
            const publishSeries = "projector"
            const publishUrl = "en-ca/projector"
            const publishUrlName = "en-ca/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CencaX3000i.push(url)
            }
    })
    return wrongnameB2CencaX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENCEE-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CenceeX3000i=[]
const wrongnameB2CenceeURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-cee/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-cee"
            const publishSeries = "projector"
            const publishUrl = "en-cee/projector"
            const publishUrlName = "en-cee/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenceeX3000i.push(url)
            }
    })
    return wrongnameB2CenceeX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENCY-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CencyX3000i=[]
const wrongnameB2CencyURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-cy/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-cy"
            const publishSeries = "projector"
            const publishUrl = "en-cy/projector"
            const publishUrlName = "en-cy/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CencyX3000i.push(url)
            }
    })
    return wrongnameB2CencyX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENDK-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CendkX3000i=[]
const wrongnameB2CendkURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-dk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-dk"
            const publishSeries = "projector"
            const publishUrl = "en-dk/projector"
            const publishUrlName = "en-dk/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CendkX3000i.push(url)
            }
    })
    return wrongnameB2CendkX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENEE-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CeneeX3000i=[]
const wrongnameB2CeneeURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-ee/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-ee"
            const publishSeries = "projector"
            const publishUrl = "en-ee/projector"
            const publishUrlName = "en-ee/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CeneeX3000i.push(url)
            }
    })
    return wrongnameB2CeneeX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENEU-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CeneuX3000i=[]
const wrongnameB2CeneuURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-eu/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-eu"
            const publishSeries = "projector"
            const publishUrl = "en-eu/projector"
            const publishUrlName = "en-eu/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CeneuX3000i.push(url)
            }
    })
    return wrongnameB2CeneuX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENFI-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CenfiX3000i=[]
const wrongnameB2CenfiURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-fi/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-fi"
            const publishSeries = "projector"
            const publishUrl = "en-fi/projector"
            const publishUrlName = "en-fi/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenfiX3000i.push(url)
            }
    })
    return wrongnameB2CenfiX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENHK-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CenhkX3000i=[]
const wrongnameB2CenhkURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-hk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-hk"
            const publishSeries = "projector"
            const publishUrl = "en-hk/projector"
            const publishUrlName = "en-hk/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenhkX3000i.push(url)
            }
    })
    return wrongnameB2CenhkX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENHR-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CenhrX3000i=[]
const wrongnameB2CenhrURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-hr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-hr"
            const publishSeries = "projector"
            const publishUrl = "en-hr/projector"
            const publishUrlName = "en-hr/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenhrX3000i.push(url)
            }
    })
    return wrongnameB2CenhrX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENIE-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CenieX3000i=[]
const wrongnameB2CenieURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-ie/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-ie"
            const publishSeries = "projector"
            const publishUrl = "en-ie/projector"
            const publishUrlName = "en-ie/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenieX3000i.push(url)
            }
    })
    return wrongnameB2CenieX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENIN-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CeninX3000i=[]
const wrongnameB2CeninURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-in/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-in"
            const publishSeries = "projector"
            const publishUrl = "en-in/projector"
            const publishUrlName = "en-in/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CeninX3000i.push(url)
            }
    })
    return wrongnameB2CeninX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENIS-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CenisX3000i=[]
const wrongnameB2CenisURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-is/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-is"
            const publishSeries = "projector"
            const publishUrl = "en-is/projector"
            const publishUrlName = "en-is/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenisX3000i.push(url)
            }
    })
    return wrongnameB2CenisX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENLU-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CenluX3000i=[]
const wrongnameB2CenluURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-lu/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-lu"
            const publishSeries = "projector"
            const publishUrl = "en-lu/projector"
            const publishUrlName = "en-lu/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenluX3000i.push(url)
            }
    })
    return wrongnameB2CenluX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENLV-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CenlvX3000i=[]
const wrongnameB2CenlvURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-lv/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-lv"
            const publishSeries = "projector"
            const publishUrl = "en-lv/projector"
            const publishUrlName = "en-lv/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenlvX3000i.push(url)
            }
    })
    return wrongnameB2CenlvX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENME-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CenmeX3000i=[]
const wrongnameB2CenmeURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-me/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-me"
            const publishSeries = "projector"
            const publishUrl = "en-me/projector"
            const publishUrlName = "en-me/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenmeX3000i.push(url)
            }
    })
    return wrongnameB2CenmeX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENMK-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CenmkX3000i=[]
const wrongnameB2CenmkURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-mk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-mk"
            const publishSeries = "projector"
            const publishUrl = "en-mk/projector"
            const publishUrlName = "en-mk/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenmkX3000i.push(url)
            }
    })
    return wrongnameB2CenmkX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENMT-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CenmtX3000i=[]
const wrongnameB2CenmtURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-mt/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-mt"
            const publishSeries = "projector"
            const publishUrl = "en-mt/projector"
            const publishUrlName = "en-mt/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenmtX3000i.push(url)
            }
    })
    return wrongnameB2CenmtX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENMY-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CenmyX3000i=[]
const wrongnameB2CenmyURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-my/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-my"
            const publishSeries = "projector"
            const publishUrl = "en-my/projector"
            const publishUrlName = "en-my/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenmyX3000i.push(url)
            }
    })
    return wrongnameB2CenmyX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENNO-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CennoX3000i=[]
const wrongnameB2CennoURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-no/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-no"
            const publishSeries = "projector"
            const publishUrl = "en-no/projector"
            const publishUrlName = "en-no/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CennoX3000i.push(url)
            }
    })
    return wrongnameB2CennoX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENRS-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CenrsX3000i=[]
const wrongnameB2CenrsURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-rs/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-rs"
            const publishSeries = "projector"
            const publishUrl = "en-rs/projector"
            const publishUrlName = "en-rs/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenrsX3000i.push(url)
            }
    })
    return wrongnameB2CenrsX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENSG-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CensgX3000i=[]
const wrongnameB2CensgURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-sg/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-sg"
            const publishSeries = "projector"
            const publishUrl = "en-sg/projector"
            const publishUrlName = "en-sg/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CensgX3000i.push(url)
            }
    })
    return wrongnameB2CensgX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENSI-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CensiX3000i=[]
const wrongnameB2CensiURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-si/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-si"
            const publishSeries = "projector"
            const publishUrl = "en-si/projector"
            const publishUrlName = "en-si/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CensiX3000i.push(url)
            }
    })
    return wrongnameB2CensiX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENUK-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CenukX3000i=[]
const wrongnameB2CenukURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-uk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-uk"
            const publishSeries = "projector"
            const publishUrl = "en-uk/projector"
            const publishUrlName = "en-uk/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenukX3000i.push(url)
            }
    })
    return wrongnameB2CenukX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENUS-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CenusX3000i=[]
const wrongnameB2CenusURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-us/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-us"
            const publishSeries = "projector"
            const publishUrl = "en-us/projector"
            const publishUrlName = "en-us/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenusX3000i.push(url)
            }
    })
    return wrongnameB2CenusX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESAR-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CesarX3000i=[]
const wrongnameB2CesarURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-ar/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-ar"
            const publishSeries = "projector"
            const publishUrl = "es-ar/projector"
            const publishUrlName = "es-ar/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CesarX3000i.push(url)
            }
    })
    return wrongnameB2CesarX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESCO-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CescoX3000i=[]
const wrongnameB2CescoURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-co/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-co"
            const publishSeries = "projector"
            const publishUrl = "es-co/projector"
            const publishUrlName = "es-co/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CescoX3000i.push(url)
            }
    })
    return wrongnameB2CescoX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESES-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CesesX3000i=[]
const wrongnameB2CesesURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-es/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-es"
            const publishSeries = "projector"
            const publishUrl = "es-es/projector"
            const publishUrlName = "es-es/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CesesX3000i.push(url)
            }
    })
    return wrongnameB2CesesX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESLA-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CeslaX3000i=[]
const wrongnameB2CeslaURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/es-la/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-la"
            const publishSeries = "projector"
            const publishUrl = "es-la/projector"
            const publishUrlName = "es-la/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CeslaX3000i.push(url)
            }
    })
    return wrongnameB2CeslaX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESMX-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CesmxX3000i=[]
const wrongnameB2CesmxURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/es-mx/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-mx"
            const publishSeries = "projector"
            const publishUrl = "es-mx/projector"
            const publishUrlName = "es-mx/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CesmxX3000i.push(url)
            }
    })
    return wrongnameB2CesmxX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESPE-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CespeX3000i=[]
const wrongnameB2CespeURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-pe/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-pe"
            const publishSeries = "projector"
            const publishUrl = "es-pe/projector"
            const publishUrlName = "es-pe/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CespeX3000i.push(url)
            }
    })
    return wrongnameB2CespeX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-FRCA-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CfrcaX3000i=[]
const wrongnameB2CfrcaURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/fr-ca/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "fr-ca"
            const publishSeries = "projector"
            const publishUrl = "fr-ca/projector"
            const publishUrlName = "fr-ca/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CfrcaX3000i.push(url)
            }
    })
    return wrongnameB2CfrcaX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-FRCH-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CfrchX3000i=[]
const wrongnameB2CfrchURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/fr-ch/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "fr-ch"
            const publishSeries = "projector"
            const publishUrl = "fr-ch/projector"
            const publishUrlName = "fr-ch/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CfrchX3000i.push(url)
            }
    })
    return wrongnameB2CfrchX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-FRFR-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CfrfrX3000i=[]
const wrongnameB2CfrfrURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/fr-fr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "fr-fr"
            const publishSeries = "projector"
            const publishUrl = "fr-fr/projector"
            const publishUrlName = "fr-fr/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CfrfrX3000i.push(url)
            }
    })
    return wrongnameB2CfrfrX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-HUHU-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2ChuhuX3000i=[]
const wrongnameB2ChuhuURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/hu-hu/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "hu-hu"
            const publishSeries = "projector"
            const publishUrl = "hu-hu/projector"
            const publishUrlName = "hu-hu/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2ChuhuX3000i.push(url)
            }
    })
    return wrongnameB2ChuhuX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-IDID-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CididX3000i=[]
const wrongnameB2CididURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/id-id/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "id-id"
            const publishSeries = "projector"
            const publishUrl = "id-id/projector"
            const publishUrlName = "id-id/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CididX3000i.push(url)
            }
    })
    return wrongnameB2CididX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ITIT-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CititX3000i=[]
const wrongnameB2CititURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/it-it/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "it-it"
            const publishSeries = "projector"
            const publishUrl = "it-it/projector"
            const publishUrlName = "it-it/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CititX3000i.push(url)
            }
    })
    return wrongnameB2CititX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-JAJP-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CjajpX3000i=[]
const wrongnameB2CjajpURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ja-jp/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ja-jp"
            const publishSeries = "projector"
            const publishUrl = "ja-jp/projector"
            const publishUrlName = "ja-jp/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CjajpX3000i.push(url)
            }
    })
    return wrongnameB2CjajpX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-KOKR-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CkokrX3000i=[]
const wrongnameB2CkokrURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ko-kr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ko-kr"
            const publishSeries = "projector"
            const publishUrl = "ko-kr/projector"
            const publishUrlName = "ko-kr/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CkokrX3000i.push(url)
            }
    })
    return wrongnameB2CkokrX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-LTLT-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CltltX3000i=[]
const wrongnameB2CltltURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/lt-lt/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "lt-lt"
            const publishSeries = "projector"
            const publishUrl = "lt-lt/projector"
            const publishUrlName = "lt-lt/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CltltX3000i.push(url)
            }
    })
    return wrongnameB2CltltX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-NLBE-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CnlbeX3000i=[]
const wrongnameB2CnlbeURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/nl-be/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "nl-be"
            const publishSeries = "projector"
            const publishUrl = "nl-be/projector"
            const publishUrlName = "nl-be/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CnlbeX3000i.push(url)
            }
    })
    return wrongnameB2CnlbeX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-NLNL-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CnlnlX3000i=[]
const wrongnameB2CnlnlURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/nl-nl/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "nl-nl"
            const publishSeries = "projector"
            const publishUrl = "nl-nl/projector"
            const publishUrlName = "nl-nl/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CnlnlX3000i.push(url)
            }
    })
    return wrongnameB2CnlnlX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-PLPL-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CplplX3000i=[]
const wrongnameB2CplplURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/pl-pl/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "pl-pl"
            const publishSeries = "projector"
            const publishUrl = "pl-pl/projector"
            const publishUrlName = "pl-pl/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CplplX3000i.push(url)
            }
    })
    return wrongnameB2CplplX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-PTBR-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CptbrX3000i=[]
const wrongnameB2CptbrURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/pt-br/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "pt-br"
            const publishSeries = "projector"
            const publishUrl = "pt-br/projector"
            const publishUrlName = "pt-br/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CptbrX3000i.push(url)
            }
    })
    return wrongnameB2CptbrX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-PTPT-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CptptX3000i=[]
const wrongnameB2CptptURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/pt-pt/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "pt-pt"
            const publishSeries = "projector"
            const publishUrl = "pt-pt/projector"
            const publishUrlName = "pt-pt/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CptptX3000i.push(url)
            }
    })
    return wrongnameB2CptptX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-RORO-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CroroX3000i=[]
const wrongnameB2CroroURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/ro-ro/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ro-ro"
            const publishSeries = "projector"
            const publishUrl = "ro-ro/projector"
            const publishUrlName = "ro-ro/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CroroX3000i.push(url)
            }
    })
    return wrongnameB2CroroX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-RURU-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CruruX3000i=[]
const wrongnameB2CruruURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ru-ru/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ru-ru"
            const publishSeries = "projector"
            const publishUrl = "ru-ru/projector"
            const publishUrlName = "ru-ru/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CruruX3000i.push(url)
            }
    })
    return wrongnameB2CruruX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-SKSK-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CskskX3000i=[]
const wrongnameB2CskskURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/sk-sk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "sk-sk"
            const publishSeries = "projector"
            const publishUrl = "sk-sk/projector"
            const publishUrlName = "sk-sk/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CskskX3000i.push(url)
            }
    })
    return wrongnameB2CskskX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-SVSE-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CsvseX3000i=[]
const wrongnameB2CsvseURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/sv-se/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "sv-se"
            const publishSeries = "projector"
            const publishUrl = "sv-se/projector"
            const publishUrlName = "sv-se/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CsvseX3000i.push(url)
            }
    })
    return wrongnameB2CsvseX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-THTH-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CththX3000i=[]
const wrongnameB2CththURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/th-th/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "th-th"
            const publishSeries = "projector"
            const publishUrl = "th-th/projector"
            const publishUrlName = "th-th/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CththX3000i.push(url)
            }
    })
    return wrongnameB2CththX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-TRTR-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CtrtrX3000i=[]
const wrongnameB2CtrtrURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/tr-tr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "tr-tr"
            const publishSeries = "projector"
            const publishUrl = "tr-tr/projector"
            const publishUrlName = "tr-tr/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CtrtrX3000i.push(url)
            }
    })
    return wrongnameB2CtrtrX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-UKUA-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CukuaX3000i=[]
const wrongnameB2CukuaURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/uk-ua/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "uk-ua"
            const publishSeries = "projector"
            const publishUrl = "uk-ua/projector"
            const publishUrlName = "uk-ua/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CukuaX3000i.push(url)
            }
    })
    return wrongnameB2CukuaX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-VIVN-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CvivnX3000i=[]
const wrongnameB2CvivnURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/vi-vn/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "vi-vn"
            const publishSeries = "projector"
            const publishUrl = "vi-vn/projector"
            const publishUrlName = "vi-vn/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CvivnX3000i.push(url)
            }
    })
    return wrongnameB2CvivnX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ZHHK-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CzhhkX3000i=[]
const wrongnameB2CzhhkURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/zh-hk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "zh-hk"
            const publishSeries = "projector"
            const publishUrl = "zh-hk/projector"
            const publishUrlName = "zh-hk/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CzhhkX3000i.push(url)
            }
    })
    return wrongnameB2CzhhkX3000i;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ZHTW-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CzhtwX3000i=[]
const wrongnameB2CzhtwURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/zh-tw/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "zh-tw"
            const publishSeries = "projector"
            const publishUrl = "zh-tw/projector"
            const publishUrlName = "zh-tw/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CzhtwX3000i.push(url)
            }
    })
    return wrongnameB2CzhtwX3000i;
    } catch (error) {
      console.log(error);
    }
};
//Test case
Given("X3000i must be published on global site after 20220228",{timeout: 1000 * 5000},async function(){
    //After 2022/01/01
    //global setting
    const publishModel = "x3000i"
    const date = new Date()
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()
    function wholeMonth(){
        const getmonth = date.getMonth() + 1
        if(getmonth<10){
            wholeMonth =  "0"+getmonth 
            return wholeMonth
        }else{
            wholeMonth = getmonth 
            return wholeMonth
        }
    }
    function wholeDate(){
        const getDay = date.getDate()
        if(getDay<10){
            wholeDate =  "0"+getDay
            return wholeDate
        }else{
            wholeDate = getDay 
            return wholeDate
        }
    }
    const wholemonth = wholeMonth()
    const wholeday = wholeDate()
    const wholeyear = date.getFullYear()
    const fullDate = `${wholeyear}/${wholemonth}/${wholeday}`
    // console.log("wholemonth:",wholemonth)
    // console.log("wholeday:",wholeday)
    // console.log("wholeyear:",wholeyear)
    console.log("fullDate:",fullDate)
    // console.log("year:",year)
    // console.log("month:",month)
    // console.log("day:",day)
    const launchDate = "2022/02/28"

    //B2C-ARME-Projector-X3000i-after 20220228
    const armeReturnedData = await afterB2CarmeURLX3000i();
    // console.log("After returnedData",armeReturnedData)
    const armepublishCheck = armeReturnedData.length
    console.log("ar-me All publish URL-B2C Projector - X3000i:",armeReturnedData)
    console.log(armeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(armepublishCheck ==0){
            // afterB2CarmeX3000i.push("ar-me")
            X3000iPublishError.push("ar-me")

        }
    }
    //B2C-BGBG-Projector-X3000i-after 20220228
    const bgbgReturnedData = await afterB2CbgbgURLX3000i();
    // console.log("After returnedData",bgbgReturnedData)
    const bgbgpublishCheck = bgbgReturnedData.length
    console.log("bg-bg All publish URL-B2C Projector-X3000i:",bgbgReturnedData)
    console.log(bgbgReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(bgbgpublishCheck ==0){
            // afterB2CbgbgX3000i.push("bg-bg")
            X3000iPublishError.push("bg-bg")

        }
    }
    //B2C-CSCZ-Projector-X3000i-after 20220228
    const csczReturnedData = await afterB2CcsczURLX3000i();
    // console.log("After returnedData",csczReturnedData)
    const csczpublishCheck = csczReturnedData.length
    console.log("cs-cz All publish URL-B2C Projector-X3000i:",csczReturnedData)
    console.log(csczReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(csczpublishCheck ==0){
            // afterB2CcsczX3000i.push("cs-cz")
            X3000iPublishError.push("cs-cz")

        }
    }
    //B2C-DEAT-Projector-X3000i-after 20220228
    const deatReturnedData = await afterB2CdeatURLX3000i();
    // console.log("After returnedData",deatReturnedData)
    const deatpublishCheck = deatReturnedData.length
    console.log("de-at All publish URL-B2C Projector-X3000i:",deatReturnedData)
    console.log(deatReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(deatpublishCheck ==0){
            // afterB2CdeatX3000i.push("de-at")
            X3000iPublishError.push("de-at")

        }
    }
    //B2C-DECH-Projector-X3000i-after 20220228
    const dechReturnedData = await afterB2CdechURLX3000i();
    // console.log("After returnedData",dechReturnedData)
    const dechpublishCheck = dechReturnedData.length
    console.log("de-ch All publish URL-B2C Projector-X3000i:",dechReturnedData)
    console.log(dechReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(dechpublishCheck ==0){
            // afterB2CdechX3000i.push("de-ch")
            X3000iPublishError.push("de-ch")

        }
    }
    //B2C-DEDE-Projector-X3000i-after 20220228
    const dedeReturnedData = await afterB2CdedeURLX3000i();
    // console.log("After returnedData",dedeReturnedData)
    const dedepublishCheck = dedeReturnedData.length
    console.log("de-de All publish URL-B2C Projector-X3000i:",dedeReturnedData)
    console.log(dedeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(dedepublishCheck ==0){
            // afterB2CdedeX3000i.push("de-de")
            X3000iPublishError.push("de-de")

        }
    }
    //B2C-ELGR-Projector-X3000i-after 20220228
    const elgrReturnedData = await afterB2CelgrURLX3000i();
    // console.log("After returnedData",elgrReturnedData)
    const elgrpublishCheck = elgrReturnedData.length
    console.log("el-gr All publish URL-B2C Projector-X3000i:",elgrReturnedData)
    console.log(elgrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(elgrpublishCheck ==0){
            // afterB2CelgrX3000i.push("el-gr")
            X3000iPublishError.push("el-gr")

        }
    }
    //B2C-ENAP-Projector-X3000i-after 20220228
    const enapReturnedData = await afterB2CenapURLX3000i();
    // console.log("After returnedData",enapReturnedData)
    const enappublishCheck = enapReturnedData.length
    console.log("en-ap All publish URL-B2C Projector - X3000i:",enapReturnedData)
    console.log(enapReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(enappublishCheck ==0){
            // afterB2CenapX3000i.push("en-ap")
            X3000iPublishError.push("en-ap")

        }
    }
    //B2C-ENAU-Projector-X3000i-after 20220228
    const enauReturnedData = await afterB2CenauURLX3000i();
    // console.log("After returnedData",enauReturnedData)
    const enaupublishCheck = enauReturnedData.length
    console.log("en-au All publish URL-B2C Projector - X3000i:",enauReturnedData)
    console.log(enauReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(enaupublishCheck ==0){
            // afterB2CenauX3000i.push("en-au")
            X3000iPublishError.push("en-au")

        }
    }
    //B2C-ENBA-Projector-X3000i-after 20220228
    const enbaReturnedData = await afterB2CenbaURLX3000i();
    // console.log("After returnedData",enbaReturnedData)
    const enbapublishCheck = enbaReturnedData.length
    console.log("en-ba All publish URL-B2C Projector - X3000i:",enbaReturnedData)
    console.log(enbaReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(enbapublishCheck ==0){
            // afterB2CenbaX3000i.push("en-ba")
            X3000iPublishError.push("en-ba")

        }
    }
    //B2C-ENCA-Projector-X3000i-after 20220228
    const encaReturnedData = await afterB2CencaURLX3000i();
    // console.log("After returnedData",encaReturnedData)
    const encapublishCheck = encaReturnedData.length
    console.log("en-ca All publish URL-B2C Projector - X3000i:",encaReturnedData)
    console.log(encaReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(encapublishCheck ==0){
            // afterB2CencaX3000i.push("en-ca")
            X3000iPublishError.push("en-ca")
        }
    }
    //B2C-ENCEE-Projector-X3000i-after 20220228
    const enceeReturnedData = await afterB2CenceeURLX3000i();
    // console.log("After returnedData",enceeReturnedData)
    const enceepublishCheck = enceeReturnedData.length
    console.log("en-cee All publish URL-B2C Projector - X3000i:",enceeReturnedData)
    console.log(enceeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(enceepublishCheck ==0){
            // afterB2CencaX3000i.push("en-cee")
            X3000iPublishError.push("en-cee")
        }
    }
    //B2C-ENCY-Projector-X3000i-after 20220228
    const encyReturnedData = await afterB2CencyURLX3000i();
    // console.log("After returnedData",encyReturnedData)
    const encypublishCheck = encyReturnedData.length
    console.log("en-cy All publish URL-B2C Projector - X3000i:",encyReturnedData)
    console.log(encyReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(encypublishCheck ==0){
            // afterB2CencaX3000i.push("en-cy")
            X3000iPublishError.push("en-cy")
        }
    }
    //B2C-ENDK-Projector-X3000i-after 20220228
    const endkReturnedData = await afterB2CendkURLX3000i();
    // console.log("After returnedData",endkReturnedData)
    const endkpublishCheck = endkReturnedData.length
    console.log("en-dk All publish URL-B2C Projector - X3000i:",endkReturnedData)
    console.log(endkReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(endkpublishCheck ==0){
            // afterB2CencaX3000i.push("en-dk")
            X3000iPublishError.push("en-dk")
        }
    }
    //B2C-ENEE-Projector-X3000i-after 20220228
    const eneeReturnedData = await afterB2CeneeURLX3000i();
    // console.log("After returnedData",eneeReturnedData)
    const eneepublishCheck = eneeReturnedData.length
    console.log("en-ee All publish URL-B2C Projector-X3000i:",eneeReturnedData)
    console.log(eneeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(eneepublishCheck ==0){
            X3000iPublishError.push("en-ee")
        }
    }
    //B2C-ENEU-Projector-X3000i-after 20220228
    const eneuReturnedData = await afterB2CeneuURLX3000i();
    // console.log("After returnedData",eneuReturnedData)
    const eneupublishCheck = eneuReturnedData.length
    console.log("en-eu All publish URL-B2C Projector-X3000i:",eneuReturnedData)
    console.log(eneuReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(eneupublishCheck ==0){
            X3000iPublishError.push("en-eu")
        }
    }
    //B2C-ENFI-Projector-X3000i-after 20220228
    const enfiReturnedData = await afterB2CenfiURLX3000i();
    // console.log("After returnedData",enfiReturnedData)
    const enfipublishCheck = enfiReturnedData.length
    console.log("en-fi All publish URL-B2C Projector-X3000i:",enfiReturnedData)
    console.log(enfiReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(enfipublishCheck ==0){
            X3000iPublishError.push("en-fi")
        }
    }
    //B2C-ENHK-Projector-X3000i-after 20220228
    const enhkReturnedData = await afterB2CenhkURLX3000i();
    // console.log("After returnedData",enhkReturnedData)
    const enhkpublishCheck = enhkReturnedData.length
    console.log("en-hk All publish URL-B2C Projector - X3000i:",enhkReturnedData)
    console.log(enhkReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(enhkpublishCheck ==0){
            X3000iPublishError.push("en-hk")
        }
    }
    //B2C-ENHR-Projector-X3000i-after 20220228
    const enhrReturnedData = await afterB2CenhrURLX3000i();
    // console.log("After returnedData",enhrReturnedData)
    const enhrpublishCheck = enhrReturnedData.length
    console.log("en-hr All publish URL-B2C Projector-X3000i:",enhrReturnedData)
    console.log(enhrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(enhrpublishCheck ==0){
            X3000iPublishError.push("en-hr")
        }
    }
    //B2C-ENIE-Projector-X3000i-after 20220228
    const enieReturnedData = await afterB2CenieURLX3000i();
    // console.log("After returnedData",enieReturnedData)
    const eniepublishCheck = enieReturnedData.length
    console.log("en-ie All publish URL-B2C Projector-X3000i:",enieReturnedData)
    console.log(enieReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(eniepublishCheck ==0){
            X3000iPublishError.push("en-ie")
        }
    }
    //B2C-ENIN-Projector-X3000i-after 20220228
    const eninReturnedData = await afterB2CeninURLX3000i();
    // console.log("After returnedData",eninReturnedData)
    const eninpublishCheck = eninReturnedData.length
    console.log("en-in All publish URL-B2C Projector - X3000i:",eninReturnedData)
    console.log(eninReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(eninpublishCheck ==0){
            X3000iPublishError.push("en-in")
        }
    }
    //B2C-ENIS-Projector-X3000i-after 20220228
    const enisReturnedData = await afterB2CenisURLX3000i();
    // console.log("After returnedData",enisReturnedData)
    const enispublishCheck = enisReturnedData.length
    console.log("en-is All publish URL-B2C Projector-X3000i:",enisReturnedData)
    console.log(enisReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(enispublishCheck ==0){
            X3000iPublishError.push("en-is")
        }
    }
    //B2C-ENLU-Projector-X3000i-after 20220228
    const enluReturnedData = await afterB2CenluURLX3000i();
    // console.log("After returnedData",enluReturnedData)
    const enlupublishCheck = enluReturnedData.length
    console.log("en-lu All publish URL-B2C Projector-X3000i:",enluReturnedData)
    console.log(enluReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(enlupublishCheck ==0){
            X3000iPublishError.push("en-lu")
        }
    }
    //B2C-ENLV-Projector-X3000i-after 20220228
    const enlvReturnedData = await afterB2CenlvURLX3000i();
    // console.log("After returnedData",enlvReturnedData)
    const enlvpublishCheck = enlvReturnedData.length
    console.log("en-lv All publish URL-B2C Projector-X3000i:",enlvReturnedData)
    console.log(enlvReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(enlvpublishCheck ==0){
            X3000iPublishError.push("en-lv")
        }
    }
    //B2C-ENME-Projector-X3000i-after 20220228
    const enmeReturnedData = await afterB2CenmeURLX3000i();
    // console.log("After returnedData",enmeReturnedData)
    const enmepublishCheck = enmeReturnedData.length
    console.log("en-me All publish URL-B2C Projector - X3000i:",enmeReturnedData)
    console.log(enmeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(enmepublishCheck ==0){
            X3000iPublishError.push("en-me")
        }
    }
    //B2C-ENMK-Projector-X3000i-after 20220228
    const enmkReturnedData = await afterB2CenmkURLX3000i();
    // console.log("After returnedData",enmkReturnedData)
    const enmkpublishCheck = enmkReturnedData.length
    console.log("en-mk All publish URL-B2C Projector-X3000i:",enmkReturnedData)
    console.log(enmkReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(enmkpublishCheck ==0){
            X3000iPublishError.push("en-mk")
        }
    }
    //B2C-ENMT-Projector-X3000i-after 20220228
    const enmtReturnedData = await afterB2CenmtURLX3000i();
    // console.log("After returnedData",enmtReturnedData)
    const enmtpublishCheck = enmtReturnedData.length
    console.log("en-mt All publish URL-B2C Projector-X3000i:",enmtReturnedData)
    console.log(enmtReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(enmtpublishCheck ==0){
            X3000iPublishError.push("en-mt")
        }
    }
    //B2C-ENMY-Projector-X3000i-after 20220228
    const enmyReturnedData = await afterB2CenmyURLX3000i();
    // console.log("After returnedData",enmyReturnedData)
    const enmypublishCheck = enmyReturnedData.length
    console.log("en-my All publish URL-B2C Projector - X3000i:",enmyReturnedData)
    console.log(enmyReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(enmypublishCheck ==0){
            X3000iPublishError.push("en-my")
        }
    }
    //B2C-ENNO-Projector-X3000i-after 20220228
    const ennoReturnedData = await afterB2CennoURLX3000i();
    // console.log("After returnedData",ennoReturnedData)
    const ennopublishCheck = ennoReturnedData.length
    console.log("en-no All publish URL-B2C Projector-X3000i:",ennoReturnedData)
    console.log(ennoReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(ennopublishCheck ==0){
            X3000iPublishError.push("en-no")
        }
    }
    //B2C-ENRS-Projector-X3000i-after 20220228
    const enrsReturnedData = await afterB2CenrsURLX3000i();
    // console.log("After returnedData",enrsReturnedData)
    const enrspublishCheck = enrsReturnedData.length
    console.log("en-rs All publish URL-B2C Projector-X3000i:",enrsReturnedData)
    console.log(enrsReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(enrspublishCheck ==0){
            X3000iPublishError.push("en-rs")
        }
    }
    //B2C-ENSG-Projector-X3000i-after 20220228
    const ensgReturnedData = await afterB2CensgURLX3000i();
    // console.log("After returnedData",ensgReturnedData)
    const ensgpublishCheck = ensgReturnedData.length
    console.log("en-sg All publish URL-B2C Projector - X3000i:",ensgReturnedData)
    console.log(ensgReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(ensgpublishCheck ==0){
            X3000iPublishError.push("en-sg")
        }
    }
    //B2C-ENSI-Projector-X3000i-after 20220228
    const ensiReturnedData = await afterB2CensiURLX3000i();
    // console.log("After returnedData",ensiReturnedData)
    const ensipublishCheck = ensiReturnedData.length
    console.log("en-si All publish URL-B2C Projector-X3000i:",ensiReturnedData)
    console.log(ensiReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(ensipublishCheck ==0){
            X3000iPublishError.push("en-si")
        }
    }
    //B2C-ENUK-Projector-X3000i-after 20220228
    const enukReturnedData = await afterB2CenukURLX3000i();
    // console.log("After returnedData",enukReturnedData)
    const enukpublishCheck = enukReturnedData.length
    console.log("en-uk All publish URL-B2C Projector-X3000i:",enukReturnedData)
    console.log(enukReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(enukpublishCheck ==0){
            X3000iPublishError.push("en-uk")
        }
    }
    //B2C-ENUS-Projector-X3000i-after 20220228
    const enusReturnedData = await afterB2CenusURLX3000i();
    // console.log("After returnedData",enusReturnedData)
    const enuspublishCheck = enusReturnedData.length
    console.log("en-us All publish URL-B2C Projector - X3000i:",enusReturnedData)
    console.log(enusReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(enuspublishCheck ==0){
            X3000iPublishError.push("en-us")
        }
    }
    //B2C-ESAR-Projector-X3000i-after 20220228
    const esarReturnedData = await afterB2CesarURLX3000i();
    // console.log("After returnedData",esarReturnedData)
    const esarpublishCheck = esarReturnedData.length
    console.log("es-ar All publish URL-B2C Projector - X3000i:",esarReturnedData)
    console.log(esarReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(esarpublishCheck ==0){
            X3000iPublishError.push("es-ar")
        }
    }
    //B2C-ESCO-Projector-X3000i-after 20220228
    const escoReturnedData = await afterB2CescoURLX3000i();
    // console.log("After returnedData",escoReturnedData)
    const escopublishCheck = escoReturnedData.length
    console.log("es-co All publish URL-B2C Projector - X3000i:",escoReturnedData)
    console.log(escoReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(escopublishCheck ==0){
            X3000iPublishError.push("es-co")
        }
    }
    //B2C-ESES-Projector-X3000i-after 20220228
    const esesReturnedData = await afterB2CesesURLX3000i();
    // console.log("After returnedData",esesReturnedData)
    const esespublishCheck = esesReturnedData.length
    console.log("es-es All publish URL-B2C Projector - X3000i:",esesReturnedData)
    console.log(esesReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(esespublishCheck ==0){
            X3000iPublishError.push("es-es")
        }
    }
    //B2C-ESLA-Projector-X3000i-after 20220228
    const eslaReturnedData = await afterB2CeslaURLX3000i();
    // console.log("After returnedData",eslaReturnedData)
    const eslapublishCheck = eslaReturnedData.length
    console.log("es-la All publish URL-B2C Projector - X3000i:",eslaReturnedData)
    console.log(eslaReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(eslapublishCheck ==0){
            X3000iPublishError.push("es-la")
        }
    }
    //B2C-ESMX-Projector-X3000i-after 20220228
    const esmxReturnedData = await afterB2CesmxURLX3000i();
    // console.log("After returnedData",esmxReturnedData)
    const esmxpublishCheck = esmxReturnedData.length
    console.log("es-mx All publish URL-B2C Projector - X3000i:",esmxReturnedData)
    console.log(esmxReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(esmxpublishCheck ==0){
            X3000iPublishError.push("es-mx")
        }
    }
    //B2C-ESPE-Projector-X3000i-after 20220228
    const espeReturnedData = await afterB2CespeURLX3000i();
    // console.log("After returnedData",espeReturnedData)
    const espepublishCheck = espeReturnedData.length
    console.log("es-pe All publish URL-B2C Projector - X3000i:",espeReturnedData)
    console.log(espeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(espepublishCheck ==0){
            X3000iPublishError.push("es-pe")
        }
    }
    //B2C-FRCA-Projector-X3000i-after 20220228
    const frcaReturnedData = await afterB2CfrcaURLX3000i();
    // console.log("After returnedData",frcaReturnedData)
    const frcapublishCheck = frcaReturnedData.length
    console.log("fr-ca All publish URL-B2C Projector - X3000i:",frcaReturnedData)
    console.log(frcaReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(frcapublishCheck ==0){
            X3000iPublishError.push("fr-ca")
        }
    }
    //B2C-FRCH-Projector-X3000i-after 20220228
    const frchReturnedData = await afterB2CfrchURLX3000i();
    // console.log("After returnedData",frchReturnedData)
    const frchpublishCheck = frchReturnedData.length
    console.log("fr-ch All publish URL-B2C Projector - X3000i:",frchReturnedData)
    console.log(frchReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(frchpublishCheck ==0){
            X3000iPublishError.push("fr-ch")
        }
    }
    //B2C-FRFR-Projector-X3000i-after 20220228
    const frfrReturnedData = await afterB2CfrfrURLX3000i();
    // console.log("After returnedData",frfrReturnedData)
    const frfrpublishCheck = frfrReturnedData.length
    console.log("fr-fr All publish URL-B2C Projector - X3000i:",frfrReturnedData)
    console.log(frfrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(frfrpublishCheck ==0){
            X3000iPublishError.push("fr-fr")
        }
    }
    //B2C-HUHU-Projector-X3000i-after 20220228
    const huhuReturnedData = await afterB2ChuhuURLX3000i();
    // console.log("After returnedData",huhuReturnedData)
    const huhupublishCheck = huhuReturnedData.length
    console.log("hu-hu All publish URL-B2C Projector - X3000i:",huhuReturnedData)
    console.log(huhuReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(huhupublishCheck ==0){
            X3000iPublishError.push("hu-hu")
        }
    }
    //B2C-IDID-Projector-X3000i-after 20220228
    const ididReturnedData = await afterB2CididURLX3000i();
    // console.log("After returnedData",ididReturnedData)
    const ididpublishCheck = ididReturnedData.length
    console.log("id-id All publish URL-B2C Projector - X3000i:",ididReturnedData)
    console.log(ididReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(ididpublishCheck ==0){
            X3000iPublishError.push("id-id")
        }
    }
    //B2C-ITIT-Projector-X3000i-after 20220228
    const ititReturnedData = await afterB2CititURLX3000i();
    // console.log("After returnedData",ititReturnedData)
    const ititpublishCheck = ititReturnedData.length
    console.log("it-it All publish URL-B2C Projector - X3000i:",ititReturnedData)
    console.log(ititReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(ititpublishCheck ==0){
            X3000iPublishError.push("it-it")
        }
    }
    //B2C-JAJP-Projector-X3000i-after 20220228
    const jajpReturnedData = await afterB2CjajpURLX3000i();
    // console.log("After returnedData",jajpReturnedData)
    const jajppublishCheck = jajpReturnedData.length
    console.log("ja-jp All publish URL-B2C Projector - X3000i:",jajpReturnedData)
    console.log(jajpReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(jajppublishCheck ==0){
            X3000iPublishError.push("ja-jp")
        }
    }
    //B2C-KOKR-Projector-X3000i-after 20220228
    const kokrReturnedData = await afterB2CkokrURLX3000i();
    // console.log("After returnedData",kokrReturnedData)
    const kokrpublishCheck = kokrReturnedData.length
    console.log("ko-kr All publish URL-B2C Projector - X3000i:",kokrReturnedData)
    console.log(kokrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(kokrpublishCheck ==0){
            X3000iPublishError.push("ko-kr")
        }
    }
    //B2C-LTLT-Projector-X3000i-after 20220228
    const ltltReturnedData = await afterB2CltltURLX3000i();
    // console.log("After returnedData",ltltReturnedData)
    const ltltpublishCheck = ltltReturnedData.length
    console.log("lt-lt All publish URL-B2C Projector - X3000i:",ltltReturnedData)
    console.log(ltltReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(ltltpublishCheck ==0){
            X3000iPublishError.push("lt-lt")
        }
    }
    //B2C-NLBE-Projector-X3000i-after 20220228
    const nlbeReturnedData = await afterB2CnlbeURLX3000i();
    // console.log("After returnedData",nlbeReturnedData)
    const nlbepublishCheck = nlbeReturnedData.length
    console.log("nl-be All publish URL-B2C Projector - X3000i:",nlbeReturnedData)
    console.log(nlbeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(nlbepublishCheck ==0){
            X3000iPublishError.push("nl-be")
        }
    }
    //B2C-NLNL-Projector-X3000i-after 20220228
    const nlnlReturnedData = await afterB2CnlnlURLX3000i();
    // console.log("After returnedData",nlnlReturnedData)
    const nlnlpublishCheck = nlnlReturnedData.length
    console.log("nl-nl All publish URL-B2C Projector - X3000i:",nlnlReturnedData)
    console.log(nlnlReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(nlnlpublishCheck ==0){
            X3000iPublishError.push("nl-nl")
        }
    }
    //B2C-PLPL-Projector-X3000i-after 20220228
    const plplReturnedData = await afterB2CplplURLX3000i();
    // console.log("After returnedData",plplReturnedData)
    const plplpublishCheck = plplReturnedData.length
    console.log("pl-pl All publish URL-B2C Projector - X3000i:",plplReturnedData)
    console.log(plplReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(plplpublishCheck ==0){
            X3000iPublishError.push("pl-pl")
        }
    }
    //B2C-PTBR-Projector-X3000i-after 20220228
    const ptbrReturnedData = await afterB2CptbrURLX3000i();
    // console.log("After returnedData",ptbrReturnedData)
    const ptbrpublishCheck = ptbrReturnedData.length
    console.log("pt-br All publish URL-B2C Projector - X3000i:",ptbrReturnedData)
    console.log(ptbrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(ptbrpublishCheck ==0){
            X3000iPublishError.push("pt-br")
        }
    }
    //B2C-PTPT-Projector-X3000i-after 20220228
    const ptptReturnedData = await afterB2CptptURLX3000i();
    // console.log("After returnedData",ptptReturnedData)
    const ptptpublishCheck = ptptReturnedData.length
    console.log("pt-pt All publish URL-B2C Projector - X3000i:",ptptReturnedData)
    console.log(ptptReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(ptptpublishCheck ==0){
            X3000iPublishError.push("pt-pt")
        }
    }
    //B2C-RORO-Projector-X3000i-after 20220228
    const roroReturnedData = await afterB2CroroURLX3000i();
    // console.log("After returnedData",roroReturnedData)
    const roropublishCheck = roroReturnedData.length
    console.log("ro-ro All publish URL-B2C Projector - X3000i:",roroReturnedData)
    console.log(roroReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(roropublishCheck ==0){
            X3000iPublishError.push("ro-ro")
        }
    }
    //B2C-RURU-Projector-X3000i-after 20220228
    const ruruReturnedData = await afterB2CruruURLX3000i();
    // console.log("After returnedData",ruruReturnedData)
    const rurupublishCheck = ruruReturnedData.length
    console.log("ru-ru All publish URL-B2C Projector - X3000i:",ruruReturnedData)
    console.log(ruruReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(rurupublishCheck ==0){
            X3000iPublishError.push("ru-ru")
        }
    }
    //B2C-SKSK-Projector-X3000i-after 20220228
    const skskReturnedData = await afterB2CskskURLX3000i();
    // console.log("After returnedData",skskReturnedData)
    const skskpublishCheck = skskReturnedData.length
    console.log("sk-sk All publish URL-B2C Projector - X3000i:",skskReturnedData)
    console.log(skskReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(skskpublishCheck ==0){
            X3000iPublishError.push("sk-sk")
        }
    }
    //B2C-SVSE-Projector-X3000i-after 20220228
    const svseReturnedData = await afterB2CsvseURLX3000i();
    // console.log("After returnedData",svseReturnedData)
    const svsepublishCheck = svseReturnedData.length
    console.log("sv-se All publish URL-B2C Projector - X3000i:",svseReturnedData)
    console.log(svseReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(svsepublishCheck ==0){
            X3000iPublishError.push("sv-se")
        }
    }
    //B2C-THTH-Projector-X3000i-after 20220228
    const ththReturnedData = await afterB2CththURLX3000i();
    // console.log("After returnedData",ththReturnedData)
    const ththpublishCheck = ththReturnedData.length
    console.log("th-th All publish URL-B2C Projector - X3000i:",ththReturnedData)
    console.log(ththReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(ththpublishCheck ==0){
            X3000iPublishError.push("th-th")
        }
    }
    //B2C-TRTR-Projector-X3000i-after 20220228
    const trtrReturnedData = await afterB2CtrtrURLX3000i();
    // console.log("After returnedData",trtrReturnedData)
    const trtrpublishCheck = trtrReturnedData.length
    console.log("tr-tr All publish URL-B2C Projector - X3000i:",trtrReturnedData)
    console.log(trtrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(trtrpublishCheck ==0){
            X3000iPublishError.push("tr-tr")
        }
    }
    //B2C-UKUA-Projector-X3000i-after 20220228
    const ukuaReturnedData = await afterB2CukuaURLX3000i();
    // console.log("After returnedData",ukuaReturnedData)
    const ukuapublishCheck = ukuaReturnedData.length
    console.log("uk-ua All publish URL-B2C Projector - X3000i:",ukuaReturnedData)
    console.log(ukuaReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(ukuapublishCheck ==0){
            X3000iPublishError.push("uk-ua")
        }
    }
    //B2C-VIVN-Projector-X3000i-after 20220228
    const vivnReturnedData = await afterB2CvivnURLX3000i();
    // console.log("After returnedData",vivnReturnedData)
    const vivnpublishCheck = vivnReturnedData.length
    console.log("vi-vn All publish URL-B2C Projector - X3000i:",vivnReturnedData)
    console.log(vivnReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(vivnpublishCheck ==0){
            X3000iPublishError.push("vi-vn")
        }
    }
    //B2C-ZHHK-Projector-X3000i-after 20220228
    const zhhkReturnedData = await afterB2CzhhkURLX3000i();
    // console.log("After returnedData",zhhkReturnedData)
    const zhhkpublishCheck = zhhkReturnedData.length
    console.log("zh-hk All publish URL-B2C Projector - X3000i:",zhhkReturnedData)
    console.log(zhhkReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(zhhkpublishCheck ==0){
            X3000iPublishError.push("zh-hk")
        }
    }
    //B2C-ZHTW-Projector-X3000i-after 20220228
    const zhtwReturnedData = await afterB2CzhtwURLX3000i();
    // console.log("After returnedData",zhtwReturnedData)
    const zhtwpublishCheck = zhtwReturnedData.length
    console.log("zh-tw All publish URL-B2C Projector - X3000i:",zhtwReturnedData)
    console.log(zhtwReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //X3000iPublishError
        if(zhtwpublishCheck ==0){
            X3000iPublishError.push("zh-tw")
        }
    }
    //放全部的國家的後面
    if(X3000iPublishError.length >0){
        console.log("Not Published Now:",X3000iPublishError)
        throw new Error(`${publishModel} must be published on ${X3000iPublishError} before ${launchDate}, but it is not published now. Today is ${fullDate}`)
    }
})

Then("X3000i URL name must be 'projector gaming-projector x3000i.html'",{timeout: 1000 * 5000},async function(){
    //After 2022/01/01
    //global setting
    const publishModel = "x3000i"
    const publishProductURLName = "projector/gaming-projector/x3000i.html"
    const date = new Date()
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()
    function wholeMonth(){
        const getmonth = date.getMonth() + 1
        if(getmonth<10){
            wholeMonth =  "0"+getmonth 
            return wholeMonth
        }else{
            wholeMonth = getmonth 
            return wholeMonth
        }
    }
    function wholeDate(){
        const getDay = date.getDate()
        if(getDay<10){
            wholeDate =  "0"+getDay
            return wholeDate
        }else{
            wholeDate = getDay 
            return wholeDate
        }
    }
    const wholemonth = wholeMonth()
    const wholeday = wholeDate()
    const wholeyear = date.getFullYear()
    const fullDate = `${wholeyear}/${wholemonth}/${wholeday}`
    // console.log("wholemonth:",wholemonth)
    // console.log("wholeday:",wholeday)
    // console.log("wholeyear:",wholeyear)
    console.log("fullDate:",fullDate)
    // console.log("year:",year)
    // console.log("month:",month)
    // console.log("day:",day)
    const launchDate = "2022/02/28"
    //B2C-ARME-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const armeReturnedData = await wrongnameB2CarmeURLX3000i();
    const armeNameCheck = armeReturnedData.length
    console.log("ar-me wrong name URL-B2C Projector-X3000i:",armeReturnedData)
    console.log(armeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(armeNameCheck > 0){
            X3000iNameError.push('ar-me')
            X3000iNameErrorURL.push(armeReturnedData)
        }
    }
    //B2C-BGBG-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const bgbgReturnedData = await wrongnameB2CbgbgURLX3000i();
    const bgbgNameCheck = bgbgReturnedData.length
    console.log("bg-bg wrong name URL-B2C Projector-X3000i:",bgbgReturnedData)
    console.log(bgbgReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(bgbgNameCheck > 0){
            X3000iNameError.push('bg-bg')
            X3000iNameErrorURL.push(bgbgReturnedData)
        }
    }
    //B2C-CSCZ-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const csczReturnedData = await wrongnameB2CcsczURLX3000i();
    const csczNameCheck = csczReturnedData.length
    console.log("cs-cz wrong name URL-B2C Projector-X3000i:",csczReturnedData)
    console.log(csczReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(csczNameCheck > 0){
            X3000iNameError.push('cs-cz')
            X3000iNameErrorURL.push(csczReturnedData)
        }
    }
    //B2C-DEAT-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const deatReturnedData = await wrongnameB2CdeatURLX3000i();
    const deatNameCheck = deatReturnedData.length
    console.log("de-at wrong name URL-B2C Projector-X3000i:",deatReturnedData)
    console.log(deatReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(deatNameCheck > 0){
            X3000iNameError.push('de-at')
            X3000iNameErrorURL.push(deatReturnedData)
        }
    }
    //B2C-DECH-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const dechReturnedData = await wrongnameB2CdechURLX3000i();
    const dechNameCheck = dechReturnedData.length
    console.log("de-ch wrong name URL-B2C Projector-X3000i:",dechReturnedData)
    console.log(dechReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(dechNameCheck > 0){
            X3000iNameError.push('de-ch')
            X3000iNameErrorURL.push(dechReturnedData)
        }
    }
    //B2C-DEDE-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const dedeReturnedData = await wrongnameB2CdedeURLX3000i();
    const dedeNameCheck = dedeReturnedData.length
    console.log("de-de wrong name URL-B2C Projector-X3000i:",dedeReturnedData)
    console.log(dedeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(dedeNameCheck > 0){
            X3000iNameError.push('de-de')
            X3000iNameErrorURL.push(dedeReturnedData)
        }
    }
    //B2C-ELGR-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const elgrReturnedData = await wrongnameB2CelgrURLX3000i();
    const elgrNameCheck = elgrReturnedData.length
    console.log("el-gr wrong name URL-B2C Projector-X3000i:",elgrReturnedData)
    console.log(elgrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(elgrNameCheck > 0){
            X3000iNameError.push('el-gr')
            X3000iNameErrorURL.push(elgrReturnedData)
        }
    }
    //B2C-ENAP-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const enapReturnedData = await wrongnameB2CenapURLX3000i();
    const enapNameCheck = enapReturnedData.length
    console.log("en-ap wrong name URL-B2C Projector-X3000i:",enapReturnedData)
    console.log(enapReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enapNameCheck > 0){
            X3000iNameError.push("en-ap")
            X3000iNameErrorURL.push(enapReturnedData)
        }
    }
    //B2C-ENAU-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const enauReturnedData = await wrongnameB2CenauURLX3000i();
    const enauNameCheck = enauReturnedData.length
    console.log("en-au wrong name URL-B2C Projector-X3000i:",enauReturnedData)
    console.log(enauReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enauNameCheck > 0){
            X3000iNameError.push("en-au")
            X3000iNameErrorURL.push(enauReturnedData)
        }
    }
    //B2C-ENBA-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const enbaReturnedData = await wrongnameB2CenbaURLX3000i();
    const enbaNameCheck = enbaReturnedData.length
    console.log("en-ba wrong name URL-B2C Projector-X3000i:",enbaReturnedData)
    console.log(enbaReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enbaNameCheck > 0){
            X3000iNameError.push("en-ba")
            X3000iNameErrorURL.push(enbaReturnedData)
        }
    }
    //B2C-ENCA-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const encaReturnedData = await wrongnameB2CencaURLX3000i();
    const encaNameCheck = encaReturnedData.length
    console.log("en-ca wrong name URL-B2C Projector-X3000i:",encaReturnedData)
    console.log(encaReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(encaNameCheck > 0){
            X3000iNameError.push("en-ca")
            X3000iNameErrorURL.push(encaReturnedData)
        }
    }
    //B2C-ENCEE-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const enceeReturnedData = await wrongnameB2CenceeURLX3000i();
    const enceeNameCheck = enceeReturnedData.length
    console.log("en-cee wrong name URL-B2C Projector-X3000i:",enceeReturnedData)
    console.log(enceeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enceeNameCheck > 0){
            X3000iNameError.push("en-cee")
            X3000iNameErrorURL.push(enceeReturnedData)
        }
    }
    //B2C-ENCY-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const encyReturnedData = await wrongnameB2CencyURLX3000i();
    const encyNameCheck = encyReturnedData.length
    console.log("en-cy wrong name URL-B2C Projector-X3000i:",encyReturnedData)
    console.log(encyReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(encyNameCheck > 0){
            X3000iNameError.push("en-cy")
            X3000iNameErrorURL.push(encyReturnedData)
        }
    }
    //B2C-ENDK-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const endkReturnedData = await wrongnameB2CendkURLX3000i();
    const endkNameCheck = endkReturnedData.length
    console.log("en-dk wrong name URL-B2C Projector-X3000i:",endkReturnedData)
    console.log(endkReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(endkNameCheck > 0){
            X3000iNameError.push("en-dk")
            X3000iNameErrorURL.push(endkReturnedData)
        }
    }
    //B2C-ENEE-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const eneeReturnedData = await wrongnameB2CeneeURLX3000i();
    const eneeNameCheck = eneeReturnedData.length
    console.log("en-ee wrong name URL-B2C Projector-X3000i:",eneeReturnedData)
    console.log(eneeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(eneeNameCheck > 0){
            X3000iNameError.push('en-ee')
            X3000iNameErrorURL.push(eneeReturnedData)
        }
    }
    //B2C-ENEU-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const eneuReturnedData = await wrongnameB2CeneuURLX3000i();
    const eneuNameCheck = eneuReturnedData.length
    console.log("en-eu wrong name URL-B2C Projector-X3000i:",eneuReturnedData)
    console.log(eneuReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(eneuNameCheck > 0){
            X3000iNameError.push('en-eu')
            X3000iNameErrorURL.push(eneuReturnedData)
        }
    }
    //B2C-ENFI-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const enfiReturnedData = await wrongnameB2CenfiURLX3000i();
    const enfiNameCheck = enfiReturnedData.length
    console.log("en-fi wrong name URL-B2C Projector-X3000i:",enfiReturnedData)
    console.log(enfiReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enfiNameCheck > 0){
            X3000iNameError.push('en-fi')
            X3000iNameErrorURL.push(enfiReturnedData)
        }
    }
    //B2C-ENHK-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const enhkReturnedData = await wrongnameB2CenhkURLX3000i();
    const enhkNameCheck = enhkReturnedData.length
    console.log("en-hk wrong name URL-B2C Projector-X3000i:",enhkReturnedData)
    console.log(enhkReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enhkNameCheck > 0){
            X3000iNameError.push("en-hk")
            X3000iNameErrorURL.push(enhkReturnedData)
        }
    }
    //B2C-ENHR-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const enhrReturnedData = await wrongnameB2CenhrURLX3000i();
    const enhrNameCheck = enhrReturnedData.length
    console.log("en-hr wrong name URL-B2C Projector-X3000i:",enhrReturnedData)
    console.log(enhrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enhrNameCheck > 0){
            X3000iNameError.push('en-hr')
            X3000iNameErrorURL.push(enhrReturnedData)
        }
    }
    //B2C-ENIE-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const enieReturnedData = await wrongnameB2CenieURLX3000i();
    const enieNameCheck = enieReturnedData.length
    console.log("en-ie wrong name URL-B2C Projector-X3000i:",enieReturnedData)
    console.log(enieReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enieNameCheck > 0){
            X3000iNameError.push('en-ie')
            X3000iNameErrorURL.push(enieReturnedData)
        }
    }
    //B2C-ENIN-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const eninReturnedData = await wrongnameB2CeninURLX3000i();
    const eninNameCheck = eninReturnedData.length
    console.log("en-in wrong name URL-B2C Projector-X3000i:",eninReturnedData)
    console.log(eninReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(eninNameCheck > 0){
            X3000iNameError.push("en-in")
            X3000iNameErrorURL.push(eninReturnedData)
        }
    }
    //B2C-ENIS-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const enisReturnedData = await wrongnameB2CenisURLX3000i();
    const enisNameCheck = enisReturnedData.length
    console.log("en-is wrong name URL-B2C Projector-X3000i:",enisReturnedData)
    console.log(enisReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enisNameCheck > 0){
            X3000iNameError.push('en-is')
            X3000iNameErrorURL.push(enisReturnedData)
        }
    }
    //B2C-ENLU-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const enluReturnedData = await wrongnameB2CenluURLX3000i();
    const enluNameCheck = enluReturnedData.length
    console.log("en-lu wrong name URL-B2C Projector-X3000i:",enluReturnedData)
    console.log(enluReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enluNameCheck > 0){
            X3000iNameError.push('en-lu')
            X3000iNameErrorURL.push(enluReturnedData)
        }
    }
    //B2C-ENLV-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const enlvReturnedData = await wrongnameB2CenlvURLX3000i();
    const enlvNameCheck = enlvReturnedData.length
    console.log("en-lv wrong name URL-B2C Projector-X3000i:",enlvReturnedData)
    console.log(enlvReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enlvNameCheck > 0){
            X3000iNameError.push('en-lv')
            X3000iNameErrorURL.push(enlvReturnedData)
        }
    }
    //B2C-ENME-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const enmeReturnedData = await wrongnameB2CenmeURLX3000i();
    const enmeNameCheck = enmeReturnedData.length
    console.log("en-me wrong name URL-B2C Projector-X3000i:",enmeReturnedData)
    console.log(enmeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enmeNameCheck > 0){
            X3000iNameError.push("en-me")
            X3000iNameErrorURL.push(enmeReturnedData)
        }
    }
    //B2C-ENMK-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const enmkReturnedData = await wrongnameB2CenmkURLX3000i();
    const enmkNameCheck = enmkReturnedData.length
    console.log("en-mk wrong name URL-B2C Projector-X3000i:",enmkReturnedData)
    console.log(enmkReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enmkNameCheck > 0){
            X3000iNameError.push('en-mk')
            X3000iNameErrorURL.push(enmkReturnedData)
        }
    }
    //B2C-ENMT-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const enmtReturnedData = await wrongnameB2CenmtURLX3000i();
    const enmtNameCheck = enmtReturnedData.length
    console.log("en-mt wrong name URL-B2C Projector-X3000i:",enmtReturnedData)
    console.log(enmtReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enmtNameCheck > 0){
            X3000iNameError.push('en-mt')
            X3000iNameErrorURL.push(enmtReturnedData)
        }
    }
    //B2C-ENMY-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const enmyReturnedData = await wrongnameB2CenmyURLX3000i();
    const enmyNameCheck = enmyReturnedData.length
    console.log("en-my wrong name URL-B2C Projector-X3000i:",enmyReturnedData)
    console.log(enmyReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enmyNameCheck > 0){
            X3000iNameError.push("en-my")
            X3000iNameErrorURL.push(enmyReturnedData)
        }
    }
    //B2C-ENNO-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const ennoReturnedData = await wrongnameB2CennoURLX3000i();
    const ennoNameCheck = ennoReturnedData.length
    console.log("en-no wrong name URL-B2C Projector-X3000i:",ennoReturnedData)
    console.log(ennoReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ennoNameCheck > 0){
            X3000iNameError.push('en-no')
            X3000iNameErrorURL.push(ennoReturnedData)
        }
    }
    //B2C-ENRS-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const enrsReturnedData = await wrongnameB2CenrsURLX3000i();
    const enrsNameCheck = enrsReturnedData.length
    console.log("en-rs wrong name URL-B2C Projector-X3000i:",enrsReturnedData)
    console.log(enrsReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enrsNameCheck > 0){
            X3000iNameError.push('en-rs')
            X3000iNameErrorURL.push(enrsReturnedData)
        }
    }
    //B2C-ENSG-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const ensgReturnedData = await wrongnameB2CensgURLX3000i();
    const ensgNameCheck = ensgReturnedData.length
    console.log("en-sg wrong name URL-B2C Projector-X3000i:",ensgReturnedData)
    console.log(ensgReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ensgNameCheck > 0){
            X3000iNameError.push("en-sg")
            X3000iNameErrorURL.push(ensgReturnedData)
        }
    }
    //B2C-ENSI-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const ensiReturnedData = await wrongnameB2CensiURLX3000i();
    const ensiNameCheck = ensiReturnedData.length
    console.log("en-si wrong name URL-B2C Projector-X3000i:",ensiReturnedData)
    console.log(ensiReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ensiNameCheck > 0){
            X3000iNameError.push('en-si')
            X3000iNameErrorURL.push(ensiReturnedData)
        }
    }
    //B2C-ENUK-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const enukReturnedData = await wrongnameB2CenukURLX3000i();
    const enukNameCheck = enukReturnedData.length
    console.log("en-uk wrong name URL-B2C Projector-X3000i:",enukReturnedData)
    console.log(enukReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enukNameCheck > 0){
            X3000iNameError.push('en-uk')
            X3000iNameErrorURL.push(enukReturnedData)
        }
    }
    //B2C-ENUS-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const enusReturnedData = await wrongnameB2CenusURLX3000i();
    const enusNameCheck = enusReturnedData.length
    console.log("en-us wrong name URL-B2C Projector-X3000i:",enusReturnedData)
    console.log(enusReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enusNameCheck > 0){
            X3000iNameError.push("en-us")
            X3000iNameErrorURL.push(enusReturnedData)
        }
    }
    //B2C-ESAR-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const esarReturnedData = await wrongnameB2CesarURLX3000i();
    const esarNameCheck = esarReturnedData.length
    console.log("es-ar wrong name URL-B2C Projector-X3000i:",esarReturnedData)
    console.log(esarReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(esarNameCheck > 0){
            X3000iNameError.push("es-ar")
            X3000iNameErrorURL.push(esarReturnedData)
        }
    }
    //B2C-ESCO-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const escoReturnedData = await wrongnameB2CescoURLX3000i();
    const escoNameCheck = escoReturnedData.length
    console.log("es-co wrong name URL-B2C Projector-X3000i:",escoReturnedData)
    console.log(escoReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(escoNameCheck > 0){
            X3000iNameError.push("es-co")
            X3000iNameErrorURL.push(escoReturnedData)
        }
    }
    //B2C-ESES-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const esesReturnedData = await wrongnameB2CesesURLX3000i();
    const esesNameCheck = esesReturnedData.length
    console.log("es-es wrong name URL-B2C Projector-X3000i:",esesReturnedData)
    console.log(esesReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(esesNameCheck > 0){
            X3000iNameError.push("es-es")
            X3000iNameErrorURL.push(esesReturnedData)
        }
    }
    //B2C-ESLA-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const eslaReturnedData = await wrongnameB2CeslaURLX3000i();
    const eslaNameCheck = eslaReturnedData.length
    console.log("es-la wrong name URL-B2C Projector-X3000i:",eslaReturnedData)
    console.log(eslaReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(eslaNameCheck > 0){
            X3000iNameError.push("es-la")
            X3000iNameErrorURL.push(eslaReturnedData)
        }
    }
    //B2C-ESMX-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const esmxReturnedData = await wrongnameB2CesmxURLX3000i();
    const esmxNameCheck = esmxReturnedData.length
    console.log("es-mx wrong name URL-B2C Projector-X3000i:",esmxReturnedData)
    console.log(esmxReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(esmxNameCheck > 0){
            X3000iNameError.push("es-mx")
            X3000iNameErrorURL.push(esmxReturnedData)
        }
    }
    //B2C-ESPE-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const espeReturnedData = await wrongnameB2CespeURLX3000i();
    const espeNameCheck = espeReturnedData.length
    console.log("es-pe wrong name URL-B2C Projector-X3000i:",espeReturnedData)
    console.log(espeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(espeNameCheck > 0){
            X3000iNameError.push("es-pe")
            X3000iNameErrorURL.push(espeReturnedData)
        }
    }
    //B2C-FRCA-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const frcaReturnedData = await wrongnameB2CfrcaURLX3000i();
    const frcaNameCheck = frcaReturnedData.length
    console.log("fr-ca wrong name URL-B2C Projector-X3000i:",frcaReturnedData)
    console.log(frcaReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(frcaNameCheck > 0){
            X3000iNameError.push("fr-ca")
            X3000iNameErrorURL.push(frcaReturnedData)
        }
    }
    //B2C-FRCH-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const frchReturnedData = await wrongnameB2CfrchURLX3000i();
    const frchNameCheck = frchReturnedData.length
    console.log("fr-ch wrong name URL-B2C Projector-X3000i:",frchReturnedData)
    console.log(frchReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(frchNameCheck > 0){
            X3000iNameError.push("fr-ch")
            X3000iNameErrorURL.push(frchReturnedData)
        }
    }
    //B2C-FRFR-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const frfrReturnedData = await wrongnameB2CfrfrURLX3000i();
    const frfrNameCheck = frfrReturnedData.length
    console.log("fr-fr wrong name URL-B2C Projector-X3000i:",frfrReturnedData)
    console.log(frfrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(frfrNameCheck > 0){
            X3000iNameError.push("fr-fr")
            X3000iNameErrorURL.push(frfrReturnedData)
        }
    }
    //B2C-HUHU-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const huhuReturnedData = await wrongnameB2ChuhuURLX3000i();
    const huhuNameCheck = huhuReturnedData.length
    console.log("hu-hu wrong name URL-B2C Projector-X3000i:",huhuReturnedData)
    console.log(huhuReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(huhuNameCheck > 0){
            X3000iNameError.push("hu-hu")
            X3000iNameErrorURL.push(huhuReturnedData)
        }
    }
    //B2C-IDID-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const ididReturnedData = await wrongnameB2CididURLX3000i();
    const ididNameCheck = ididReturnedData.length
    console.log("id-id wrong name URL-B2C Projector-X3000i:",ididReturnedData)
    console.log(ididReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ididNameCheck > 0){
            X3000iNameError.push("id-id")
            X3000iNameErrorURL.push(ididReturnedData)
        }
    }
    //B2C-ITIT-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const ititReturnedData = await wrongnameB2CititURLX3000i();
    const ititNameCheck = ititReturnedData.length
    console.log("it-it wrong name URL-B2C Projector-X3000i:",ititReturnedData)
    console.log(ititReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ititNameCheck > 0){
            X3000iNameError.push("it-it")
            X3000iNameErrorURL.push(ititReturnedData)
        }
    }
    //B2C-JAJP-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const jajpReturnedData = await wrongnameB2CjajpURLX3000i();
    const jajpNameCheck = jajpReturnedData.length
    console.log("ja-jp wrong name URL-B2C Projector-X3000i:",jajpReturnedData)
    console.log(jajpReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(jajpNameCheck > 0){
            X3000iNameError.push("ja-jp")
            X3000iNameErrorURL.push(jajpReturnedData)
        }
    }
    //B2C-KOKR-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const kokrReturnedData = await wrongnameB2CkokrURLX3000i();
    const kokrNameCheck = kokrReturnedData.length
    console.log("ko-kr wrong name URL-B2C Projector-X3000i:",kokrReturnedData)
    console.log(kokrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(kokrNameCheck > 0){
            X3000iNameError.push("ko-kr")
            X3000iNameErrorURL.push(kokrReturnedData)
        }
    }
    //B2C-LTLT-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const ltltReturnedData = await wrongnameB2CltltURLX3000i();
    const ltltNameCheck = ltltReturnedData.length
    console.log("lt-lt wrong name URL-B2C Projector-X3000i:",ltltReturnedData)
    console.log(ltltReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ltltNameCheck > 0){
            X3000iNameError.push("lt-lt")
            X3000iNameErrorURL.push(ltltReturnedData)
        }
    }
    //B2C-NLBE-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const nlbeReturnedData = await wrongnameB2CnlbeURLX3000i();
    const nlbeNameCheck = nlbeReturnedData.length
    console.log("nl-be wrong name URL-B2C Projector-X3000i:",nlbeReturnedData)
    console.log(nlbeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(nlbeNameCheck > 0){
            X3000iNameError.push("nl-be")
            X3000iNameErrorURL.push(nlbeReturnedData)
        }
    }
    //B2C-NLNL-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const nlnlReturnedData = await wrongnameB2CnlnlURLX3000i();
    const nlnlNameCheck = nlnlReturnedData.length
    console.log("nl-nl wrong name URL-B2C Projector-X3000i:",nlnlReturnedData)
    console.log(nlnlReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(nlnlNameCheck > 0){
            X3000iNameError.push("nl-nl")
            X3000iNameErrorURL.push(nlnlReturnedData)
        }
    }
    //B2C-PLPL-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const plplReturnedData = await wrongnameB2CplplURLX3000i();
    const plplNameCheck = plplReturnedData.length
    console.log("pl-pl wrong name URL-B2C Projector-X3000i:",plplReturnedData)
    console.log(plplReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(plplNameCheck > 0){
            X3000iNameError.push("pl-pl")
            X3000iNameErrorURL.push(plplReturnedData)
        }
    }
    //B2C-PTBR-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const ptbrReturnedData = await wrongnameB2CptbrURLX3000i();
    const ptbrNameCheck = ptbrReturnedData.length
    console.log("pt-br wrong name URL-B2C Projector-X3000i:",ptbrReturnedData)
    console.log(ptbrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ptbrNameCheck > 0){
            X3000iNameError.push("pt-br")
            X3000iNameErrorURL.push(ptbrReturnedData)
        }
    }
    //B2C-PTPT-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const ptptReturnedData = await wrongnameB2CptptURLX3000i();
    const ptptNameCheck = ptptReturnedData.length
    console.log("pt-pt wrong name URL-B2C Projector-X3000i:",ptptReturnedData)
    console.log(ptptReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ptptNameCheck > 0){
            X3000iNameError.push("pt-pt")
            X3000iNameErrorURL.push(ptptReturnedData)
        }
    }
    //B2C-RORO-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const roroReturnedData = await wrongnameB2CroroURLX3000i();
    const roroNameCheck = roroReturnedData.length
    console.log("ro-ro wrong name URL-B2C Projector-X3000i:",roroReturnedData)
    console.log(roroReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(roroNameCheck > 0){
            X3000iNameError.push("ro-ro")
            X3000iNameErrorURL.push(roroReturnedData)
        }
    }
    //B2C-RURU-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const ruruReturnedData = await wrongnameB2CruruURLX3000i();
    const ruruNameCheck = ruruReturnedData.length
    console.log("ru-ru wrong name URL-B2C Projector-X3000i:",ruruReturnedData)
    console.log(ruruReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ruruNameCheck > 0){
            X3000iNameError.push("ru-ru")
            X3000iNameErrorURL.push(ruruReturnedData)
        }
    }
    //B2C-SKSK-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const skskReturnedData = await wrongnameB2CskskURLX3000i();
    const skskNameCheck = skskReturnedData.length
    console.log("sk-sk wrong name URL-B2C Projector-X3000i:",skskReturnedData)
    console.log(skskReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(skskNameCheck > 0){
            X3000iNameError.push("sk-sk")
            X3000iNameErrorURL.push(skskReturnedData)
        }
    }
    //B2C-SVSE-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const svseReturnedData = await wrongnameB2CsvseURLX3000i();
    const svseNameCheck = svseReturnedData.length
    console.log("sv-se wrong name URL-B2C Projector-X3000i:",svseReturnedData)
    console.log(svseReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(svseNameCheck > 0){
            X3000iNameError.push("sv-se")
            X3000iNameErrorURL.push(svseReturnedData)
        }
    }
    //B2C-THTH-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const ththReturnedData = await wrongnameB2CththURLX3000i();
    const ththNameCheck = ththReturnedData.length
    console.log("th-th wrong name URL-B2C Projector-X3000i:",ththReturnedData)
    console.log(ththReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ththNameCheck > 0){
            X3000iNameError.push("th-th")
            X3000iNameErrorURL.push(ththReturnedData)
        }
    }
    //B2C-TRTR-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const trtrReturnedData = await wrongnameB2CtrtrURLX3000i();
    const trtrNameCheck = trtrReturnedData.length
    console.log("tr-tr wrong name URL-B2C Projector-X3000i:",trtrReturnedData)
    console.log(trtrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(trtrNameCheck > 0){
            X3000iNameError.push("tr-tr")
            X3000iNameErrorURL.push(trtrReturnedData)
        }
    }
    //B2C-UKUA-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const ukuaReturnedData = await wrongnameB2CukuaURLX3000i();
    const ukuaNameCheck = ukuaReturnedData.length
    console.log("uk-ua wrong name URL-B2C Projector-X3000i:",ukuaReturnedData)
    console.log(ukuaReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ukuaNameCheck > 0){
            X3000iNameError.push("uk-ua")
            X3000iNameErrorURL.push(ukuaReturnedData)
        }
    }
    //B2C-VIVN-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const vivnReturnedData = await wrongnameB2CvivnURLX3000i();
    const vivnNameCheck = vivnReturnedData.length
    console.log("vi-vn wrong name URL-B2C Projector-X3000i:",vivnReturnedData)
    console.log(vivnReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(vivnNameCheck > 0){
            X3000iNameError.push("vi-vn")
            X3000iNameErrorURL.push(vivnReturnedData)
        }
    }
    //B2C-ZHHK-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const zhhkReturnedData = await wrongnameB2CzhhkURLX3000i();
    const zhhkNameCheck = zhhkReturnedData.length
    console.log("zh-hk wrong name URL-B2C Projector-X3000i:",zhhkReturnedData)
    console.log(zhhkReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(zhhkNameCheck > 0){
            X3000iNameError.push("zh-hk")
            X3000iNameErrorURL.push(zhhkReturnedData)
        }
    }
    //B2C-ZHTW-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
    const zhtwReturnedData = await wrongnameB2CzhtwURLX3000i();
    const zhtwNameCheck = zhtwReturnedData.length
    console.log("zh-tw wrong name URL-B2C Projector-X3000i:",zhtwReturnedData)
    console.log(zhtwReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(zhtwNameCheck > 0){
            X3000iNameError.push("zh-tw")
            X3000iNameErrorURL.push(zhtwReturnedData)
        }
    }

    //放全部國家的後面
    if(X3000iNameError.length >0){
        throw new Error(`${publishModel} URL name must be ${publishProductURLName}, but it is wrong now. Here is RO name: ${X3000iNameError} And wrong URL: ${X3000iNameErrorURL}`)
    }
})
