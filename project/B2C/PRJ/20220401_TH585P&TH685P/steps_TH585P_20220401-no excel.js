const { Given, When, Then, Before, After } = require('@cucumber/cucumber')
const { expect } = require('chai')
const fs = require('fs');
const puppeteer = require('puppeteer')
const request = require("request-promise");
const cheerio = require("cheerio");
const cicGA="?utm_source=autotest&utm_medium=CIC"

Before({timeout: 24 * 5000},async function () {
    this.browser = await puppeteer.launch({ 
        executablePath:
        "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
        // MAC路徑:
        // executablePath:
        // '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome',
        headless:true,//有無需要開視窗,false要開,true不開
        slowMo:100,// slow down by 100ms
        devtools:false//有無需要開啟開發人員工具
    })
    this.page = await this.browser.newPage()
    await this.page.setViewport({width:1200,height:1000})
    await this.page.setDefaultTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
    await this.page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
})

After({timeout: 12 * 5000},async function () {
    await this.browser.close()
})

//TH585P
const excelToTH585PRONS =[
    "ar-me",
    "bg-bg",
    "cs-cz",
    "de-at",
    "de-ch",
    "de-de",
    "el-gr",
    "en-ap",
    "en-au",
    "en-ba",
    "en-ca",
    "en-cee",
    "en-cy",
    "en-dk",
    "en-ee",
    "en-eu",
    "en-fi",
    "en-hk",
    "en-hr",
    "en-ie",
    "en-in",
    "en-is",
    "en-lu",
    "en-lv",
    "en-me",
    "en-mk",
    "en-mt",
    "en-my",
    "en-no",
    "en-rs",
    "en-sg",
    "en-si",
    "en-uk",
    "en-us",
    "es-ar",
    "es-co",
    "es-es",
    "es-la",
    "es-mx",
    "es-pe",
    "fr-ca",
    "fr-ch",
    "fr-fr",
    "hu-hu",
    "id-id",
    "it-it",
    "ja-jp",
    "ko-kr",
    "lt-lt",
    "nl-be",
    "nl-nl",
    "pl-pl",
    "pt-br",
    "pt-pt",
    "ro-ro",
    "ru-ru",
    "sk-sk",
    "sv-se",
    "th-th",
    "tr-tr",
    "uk-ua",
    "vi-vn",
    "zh-cn",
    "zh-hk",
    "zh-tw"
]
const excelToTH585PResult =[]
const excelToTH585PURL =[]

//Projector TH585P - publish check
const TH585PPublishError=[]
//B2C-ARME-Projector-TH585P-after 20220401
const afterB2CarmeTH585P=[]
const afterB2CarmeURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ar-me/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CarmeTH585P.push(url)
            }
    })
    return afterB2CarmeTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-BGBG-Projector-TH585P-after 20220401
const afterB2CbgbgTH585P=[]
const afterB2CbgbgURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/bg-bg/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CbgbgTH585P.push(url)
            }
    })
    return afterB2CbgbgTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-CSCZ-Projector-TH585P-after 20220401
const afterB2CcsczTH585P=[]
const afterB2CcsczURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/cs-cz/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CcsczTH585P.push(url)
            }
    })
    return afterB2CcsczTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-DEAT-Projector-TH585P-after 20220401
const afterB2CdeatTH585P=[]
const afterB2CdeatURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/de-at/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CdeatTH585P.push(url)
            }
    })
    return afterB2CdeatTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-DECH-Projector-TH585P-after 20220401
const afterB2CdechTH585P=[]
const afterB2CdechURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/de-ch/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CdechTH585P.push(url)
            }
    })
    return afterB2CdechTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-DEDE-Projector-TH585P-after 20220401
const afterB2CdedeTH585P=[]
const afterB2CdedeURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/de-de/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CdedeTH585P.push(url)
            }
    })
    return afterB2CdedeTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ELGR-Projector-TH585P-after 20220401
const afterB2CelgrTH585P=[]
const afterB2CelgrURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/el-gr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CelgrTH585P.push(url)
            }
    })
    return afterB2CelgrTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENAP-Projector-TH585P-after 20220401
const afterB2CenapTH585P=[]
const afterB2CenapURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-ap/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenapTH585P.push(url)
            }
    })
    return afterB2CenapTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENAU-Projector-TH585P-after 20220401
const afterB2CenauTH585P=[]
const afterB2CenauURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-au/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenauTH585P.push(url)
            }
    })
    return afterB2CenauTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENBA-Projector-TH585P-after 20220401
const afterB2CenbaTH585P=[]
const afterB2CenbaURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-ba/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenbaTH585P.push(url)
            }
    })
    return afterB2CenbaTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENCA-Projector-TH585P-after 20220401
const afterB2CencaTH585P=[]
const afterB2CencaURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-ca/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CencaTH585P.push(url)
            }
    })
    return afterB2CencaTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENCEE-Projector-TH585P-after 20220401
const afterB2CenceeTH585P=[]
const afterB2CenceeURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-cee/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenceeTH585P.push(url)
            }
    })
    return afterB2CenceeTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENCY-Projector-TH585P-after 20220401
const afterB2CencyTH585P=[]
const afterB2CencyURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-cy/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CencyTH585P.push(url)
            }
    })
    return afterB2CencyTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENDK-Projector-TH585P-after 202202401
const afterB2CendkTH585P=[]
const afterB2CendkURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-dk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CendkTH585P.push(url)
            }
    })
    return afterB2CendkTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENEE-Projector-TH585P-after 20220401
const afterB2CeneeTH585P=[]
const afterB2CeneeURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-ee/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CeneeTH585P.push(url)
            }
    })
    return afterB2CeneeTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENEU-Projector-TH585P-after 20220401
const afterB2CeneuTH585P=[]
const afterB2CeneuURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-eu/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CeneuTH585P.push(url)
            }
    })
    return afterB2CeneuTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENFI-Projector-TH585P-after 20220401
const afterB2CenfiTH585P=[]
const afterB2CenfiURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-fi/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenfiTH585P.push(url)
            }
    })
    return afterB2CenfiTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENHK-Projector-TH585P-after 20220401
const afterB2CenhkTH585P=[]
const afterB2CenhkURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-hk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenhkTH585P.push(url)
            }
    })
    return afterB2CenhkTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENHR-Projector-TH585P-after 20220401
const afterB2CenhrTH585P=[]
const afterB2CenhrURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-hr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenhrTH585P.push(url)
            }
    })
    return afterB2CenhrTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENIE-Projector-TH585P-after 20220401
const afterB2CenieTH585P=[]
const afterB2CenieURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-ie/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenieTH585P.push(url)
            }
    })
    return afterB2CenieTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENIN-Projector-TH585P-after 20220401
const afterB2CeninTH585P=[]
const afterB2CeninURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-in/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CeninTH585P.push(url)
            }
    })
    return afterB2CeninTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENIS-Projector-TH585P-after 20220401
const afterB2CenisTH585P=[]
const afterB2CenisURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-is/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenisTH585P.push(url)
            }
    })
    return afterB2CenisTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENLU-Projector-TH585P-after 20220401
const afterB2CenluTH585P=[]
const afterB2CenluURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-lu/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenluTH585P.push(url)
            }
    })
    return afterB2CenluTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENLV-Projector-TH585P-after 20220401
const afterB2CenlvTH585P=[]
const afterB2CenlvURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-lv/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenlvTH585P.push(url)
            }
    })
    return afterB2CenlvTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENME-Projector-TH585P-after 20220401
const afterB2CenmeTH585P=[]
const afterB2CenmeURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-me/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenmeTH585P.push(url)
            }
    })
    return afterB2CenmeTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENMK-Projector-TH585P-after 20220401
const afterB2CenmkTH585P=[]
const afterB2CenmkURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-mk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenmkTH585P.push(url)
            }
    })
    return afterB2CenmkTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENMT-Projector-TH585P-after 20220401
const afterB2CenmtTH585P=[]
const afterB2CenmtURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-mt/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenmtTH585P.push(url)
            }
    })
    return afterB2CenmtTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENMY-Projector-TH585P-after 20220401
const afterB2CenmyTH585P=[]
const afterB2CenmyURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-my/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenmyTH585P.push(url)
            }
    })
    return afterB2CenmyTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENNO-Projector-TH585P-after 20220401
const afterB2CennoTH585P=[]
const afterB2CennoURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-no/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CennoTH585P.push(url)
            }
    })
    return afterB2CennoTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENRS-Projector-TH585P-after 20220401
const afterB2CenrsTH585P=[]
const afterB2CenrsURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-rs/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenrsTH585P.push(url)
            }
    })
    return afterB2CenrsTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENSG-Projector-TH585P-after 20220401
const afterB2CensgTH585P=[]
const afterB2CensgURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-sg/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CensgTH585P.push(url)
            }
    })
    return afterB2CensgTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENSI-Projector-TH585P-after 20220401
const afterB2CensiTH585P=[]
const afterB2CensiURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-si/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CensiTH585P.push(url)
            }
    })
    return afterB2CensiTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENUK-Projector-TH585P-after 20220401
const afterB2CenukTH585P=[]
const afterB2CenukURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-uk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenukTH585P.push(url)
            }
    })
    return afterB2CenukTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENUS-Projector-TH585P-after 20220401
const afterB2CenusTH585P=[]
const afterB2CenusURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-us/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenusTH585P.push(url)
            }
    })
    return afterB2CenusTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESAR-Projector-TH585P-after 20220401
const afterB2CesarTH585P=[]
const afterB2CesarURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-ar/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CesarTH585P.push(url)
            }
    })
    return afterB2CesarTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESCO-Projector-TH585P-after 20220401
const afterB2CescoTH585P=[]
const afterB2CescoURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-co/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CescoTH585P.push(url)
            }
    })
    return afterB2CescoTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESES-Projector-TH585P-after 20220401
const afterB2CesesTH585P=[]
const afterB2CesesURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-es/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CesesTH585P.push(url)
            }
    })
    return afterB2CesesTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESLA-Projector-TH585P-after 20220401
const afterB2CeslaTH585P=[]
const afterB2CeslaURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/es-la/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CeslaTH585P.push(url)
            }
    })
    return afterB2CeslaTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESMX-Projector-TH585P-after 20220401
const afterB2CesmxTH585P=[]
const afterB2CesmxURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/es-mx/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CesmxTH585P.push(url)
            }
    })
    return afterB2CesmxTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESPE-Projector-TH585P-after 20220401
const afterB2CespeTH585P=[]
const afterB2CespeURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-pe/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CespeTH585P.push(url)
            }
    })
    return afterB2CespeTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-FRCA-Projector-TH585P-after 20220401
const afterB2CfrcaTH585P=[]
const afterB2CfrcaURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/fr-ca/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CfrcaTH585P.push(url)
            }
    })
    return afterB2CfrcaTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-FRCH-Projector-TH585P-after 20220401
const afterB2CfrchTH585P=[]
const afterB2CfrchURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/fr-ch/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CfrchTH585P.push(url)
            }
    })
    return afterB2CfrchTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-FRFR-Projector-TH585P-after 20220401
const afterB2CfrfrTH585P=[]
const afterB2CfrfrURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/fr-fr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CfrfrTH585P.push(url)
            }
    })
    return afterB2CfrfrTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-HUHU-Projector-TH585P-after 20220401
const afterB2ChuhuTH585P=[]
const afterB2ChuhuURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/hu-hu/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2ChuhuTH585P.push(url)
            }
    })
    return afterB2ChuhuTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-IDID-Projector-TH585P-after 20220401
const afterB2CididTH585P=[]
const afterB2CididURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/id-id/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CididTH585P.push(url)
            }
    })
    return afterB2CididTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ITIT-Projector-TH585P-after 20220401
const afterB2CititTH585P=[]
const afterB2CititURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/it-it/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CititTH585P.push(url)
            }
    })
    return afterB2CititTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-JAJP-Projector-TH585P-after 20220401
const afterB2CjajpTH585P=[]
const afterB2CjajpURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ja-jp/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CjajpTH585P.push(url)
            }
    })
    return afterB2CjajpTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-KOKR-Projector-TH585P-after 20220401
const afterB2CkokrTH585P=[]
const afterB2CkokrURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ko-kr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CkokrTH585P.push(url)
            }
    })
    return afterB2CkokrTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-LTLT-Projector-TH585P-after 20220401
const afterB2CltltTH585P=[]
const afterB2CltltURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/lt-lt/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CltltTH585P.push(url)
            }
    })
    return afterB2CltltTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-NLBE-Projector-TH585P-after 20220401
const afterB2CnlbeTH585P=[]
const afterB2CnlbeURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/nl-be/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CnlbeTH585P.push(url)
            }
    })
    return afterB2CnlbeTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-NLNL-Projector-TH585P-after 20220401
const afterB2CnlnlTH585P=[]
const afterB2CnlnlURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/nl-nl/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CnlnlTH585P.push(url)
            }
    })
    return afterB2CnlnlTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-PLPL-Projector-TH585P-after 20220401
const afterB2CplplTH585P=[]
const afterB2CplplURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/pl-pl/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CplplTH585P.push(url)
            }
    })
    return afterB2CplplTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-PTBR-Projector-TH585P-after 20220401
const afterB2CptbrTH585P=[]
const afterB2CptbrURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/pt-br/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CptbrTH585P.push(url)
            }
    })
    return afterB2CptbrTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-PTPT-Projector-TH585P-after 20220401
const afterB2CptptTH585P=[]
const afterB2CptptURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/pt-pt/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CptptTH585P.push(url)
            }
    })
    return afterB2CptptTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-RORO-Projector-TH585P-after 20220401
const afterB2CroroTH585P=[]
const afterB2CroroURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/ro-ro/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CroroTH585P.push(url)
            }
    })
    return afterB2CroroTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-RURU-Projector-TH585P-after 20220401
const afterB2CruruTH585P=[]
const afterB2CruruURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ru-ru/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CruruTH585P.push(url)
            }
    })
    return afterB2CruruTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-SKSK-Projector-TH585P-after 20220401
const afterB2CskskTH585P=[]
const afterB2CskskURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/sk-sk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CskskTH585P.push(url)
            }
    })
    return afterB2CskskTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-SVSE-Projector-TH585P-after 20220401
const afterB2CsvseTH585P=[]
const afterB2CsvseURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/sv-se/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CsvseTH585P.push(url)
            }
    })
    return afterB2CsvseTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-THTH-Projector-TH585P-after 20220401
const afterB2CththTH585P=[]
const afterB2CththURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/th-th/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CththTH585P.push(url)
            }
    })
    return afterB2CththTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-TRTR-Projector-TH585P-after 20220401
const afterB2CtrtrTH585P=[]
const afterB2CtrtrURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/tr-tr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CtrtrTH585P.push(url)
            }
    })
    return afterB2CtrtrTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-UKUA-Projector-TH585P-after 20220401
const afterB2CukuaTH585P=[]
const afterB2CukuaURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/uk-ua/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CukuaTH585P.push(url)
            }
    })
    return afterB2CukuaTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-VIVN-Projector-TH585P-after 20220401
const afterB2CvivnTH585P=[]
const afterB2CvivnURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/vi-vn/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CvivnTH585P.push(url)
            }
    })
    return afterB2CvivnTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ZHHK-Projector-TH585P-after 20220401
const afterB2CzhhkTH585P=[]
const afterB2CzhhkURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/zh-hk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CzhhkTH585P.push(url)
            }
    })
    return afterB2CzhhkTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ZHTW-Projector-TH585P-after 20220401
const afterB2CzhtwTH585P=[]
const afterB2CzhtwURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/zh-tw/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th585p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CzhtwTH585P.push(url)
            }
    })
    return afterB2CzhtwTH585P;
    } catch (error) {
      console.log(error);
    }
};

//Projector TH585P - URL name check
const TH585PNameError=[]
const TH585PNameErrorURL=[]
//B2C-ARME-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CarmeTH585P=[]
const wrongnameB2CarmeURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ar-me/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ar-me"
            const publishSeries = "projector"
            const publishUrl = "ar-me/projector"
            const publishUrlName = "ar-me/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CarmeTH585P.push(url)
            }
    })
    return wrongnameB2CarmeTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-BGBG-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CbgbgTH585P=[]
const wrongnameB2CbgbgURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/bg-bg/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "bg-bg"
            const publishSeries = "projector"
            const publishUrl = "bg-bg/projector"
            const publishUrlName = "bg-bg/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CbgbgTH585P.push(url)
            }
    })
    return wrongnameB2CbgbgTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-CSCZ-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CcsczTH585P=[]
const wrongnameB2CcsczURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/cs-cz/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "cs-cz"
            const publishSeries = "projector"
            const publishUrl = "cs-cz/projector"
            const publishUrlName = "cs-cz/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CcsczTH585P.push(url)
            }
    })
    return wrongnameB2CcsczTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-DEAT-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CdeatTH585P=[]
const wrongnameB2CdeatURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/de-at/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "de-at"
            const publishSeries = "projector"
            const publishUrl = "de-at/projector"
            const publishUrlName = "de-at/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CdeatTH585P.push(url)
            }
    })
    return wrongnameB2CdeatTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-DECH-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CdechTH585P=[]
const wrongnameB2CdechURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/de-ch/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "de-ch"
            const publishSeries = "projector"
            const publishUrl = "de-ch/projector"
            const publishUrlName = "de-ch/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CdechTH585P.push(url)
            }
    })
    return wrongnameB2CdechTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-DEDE-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CdedeTH585P=[]
const wrongnameB2CdedeURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/de-de/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "de-de"
            const publishSeries = "projector"
            const publishUrl = "de-de/projector"
            const publishUrlName = "de-de/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CdedeTH585P.push(url)
            }
    })
    return wrongnameB2CdedeTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ELGR-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CelgrTH585P=[]
const wrongnameB2CelgrURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/el-gr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "el-gr"
            const publishSeries = "projector"
            const publishUrl = "el-gr/projector"
            const publishUrlName = "el-gr/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CelgrTH585P.push(url)
            }
    })
    return wrongnameB2CelgrTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENAP-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CenapTH585P=[]
const wrongnameB2CenapURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-ap/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-ap"
            const publishSeries = "projector"
            const publishUrl = "en-ap/projector"
            const publishUrlName = "en-ap/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenapTH585P.push(url)
            }
    })
    return wrongnameB2CenapTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENAU-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CenauTH585P=[]
const wrongnameB2CenauURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-au/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-au"
            const publishSeries = "projector"
            const publishUrl = "en-au/projector"
            const publishUrlName = "en-au/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenauTH585P.push(url)
            }
    })
    return wrongnameB2CenauTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENBA-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CenbaTH585P=[]
const wrongnameB2CenbaURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-ba/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-ba"
            const publishSeries = "projector"
            const publishUrl = "en-ba/projector"
            const publishUrlName = "en-ba/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenbaTH585P.push(url)
            }
    })
    return wrongnameB2CenbaTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENCA-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CencaTH585P=[]
const wrongnameB2CencaURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-ca/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-ca"
            const publishSeries = "projector"
            const publishUrl = "en-ca/projector"
            const publishUrlName = "en-ca/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CencaTH585P.push(url)
            }
    })
    return wrongnameB2CencaTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENCEE-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CenceeTH585P=[]
const wrongnameB2CenceeURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-cee/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-cee"
            const publishSeries = "projector"
            const publishUrl = "en-cee/projector"
            const publishUrlName = "en-cee/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenceeTH585P.push(url)
            }
    })
    return wrongnameB2CenceeTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENCY-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CencyTH585P=[]
const wrongnameB2CencyURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-cy/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-cy"
            const publishSeries = "projector"
            const publishUrl = "en-cy/projector"
            const publishUrlName = "en-cy/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CencyTH585P.push(url)
            }
    })
    return wrongnameB2CencyTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENDK-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CendkTH585P=[]
const wrongnameB2CendkURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-dk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-dk"
            const publishSeries = "projector"
            const publishUrl = "en-dk/projector"
            const publishUrlName = "en-dk/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CendkTH585P.push(url)
            }
    })
    return wrongnameB2CendkTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENEE-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CeneeTH585P=[]
const wrongnameB2CeneeURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-ee/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-ee"
            const publishSeries = "projector"
            const publishUrl = "en-ee/projector"
            const publishUrlName = "en-ee/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CeneeTH585P.push(url)
            }
    })
    return wrongnameB2CeneeTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENEU-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CeneuTH585P=[]
const wrongnameB2CeneuURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-eu/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-eu"
            const publishSeries = "projector"
            const publishUrl = "en-eu/projector"
            const publishUrlName = "en-eu/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CeneuTH585P.push(url)
            }
    })
    return wrongnameB2CeneuTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENFI-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CenfiTH585P=[]
const wrongnameB2CenfiURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-fi/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-fi"
            const publishSeries = "projector"
            const publishUrl = "en-fi/projector"
            const publishUrlName = "en-fi/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenfiTH585P.push(url)
            }
    })
    return wrongnameB2CenfiTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENHK-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CenhkTH585P=[]
const wrongnameB2CenhkURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-hk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-hk"
            const publishSeries = "projector"
            const publishUrl = "en-hk/projector"
            const publishUrlName = "en-hk/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenhkTH585P.push(url)
            }
    })
    return wrongnameB2CenhkTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENHR-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CenhrTH585P=[]
const wrongnameB2CenhrURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-hr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-hr"
            const publishSeries = "projector"
            const publishUrl = "en-hr/projector"
            const publishUrlName = "en-hr/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenhrTH585P.push(url)
            }
    })
    return wrongnameB2CenhrTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENIE-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CenieTH585P=[]
const wrongnameB2CenieURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-ie/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-ie"
            const publishSeries = "projector"
            const publishUrl = "en-ie/projector"
            const publishUrlName = "en-ie/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenieTH585P.push(url)
            }
    })
    return wrongnameB2CenieTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENIN-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CeninTH585P=[]
const wrongnameB2CeninURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-in/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-in"
            const publishSeries = "projector"
            const publishUrl = "en-in/projector"
            const publishUrlName = "en-in/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CeninTH585P.push(url)
            }
    })
    return wrongnameB2CeninTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENIS-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CenisTH585P=[]
const wrongnameB2CenisURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-is/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-is"
            const publishSeries = "projector"
            const publishUrl = "en-is/projector"
            const publishUrlName = "en-is/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenisTH585P.push(url)
            }
    })
    return wrongnameB2CenisTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENLU-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CenluTH585P=[]
const wrongnameB2CenluURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-lu/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-lu"
            const publishSeries = "projector"
            const publishUrl = "en-lu/projector"
            const publishUrlName = "en-lu/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenluTH585P.push(url)
            }
    })
    return wrongnameB2CenluTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENLV-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CenlvTH585P=[]
const wrongnameB2CenlvURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-lv/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-lv"
            const publishSeries = "projector"
            const publishUrl = "en-lv/projector"
            const publishUrlName = "en-lv/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenlvTH585P.push(url)
            }
    })
    return wrongnameB2CenlvTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENME-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CenmeTH585P=[]
const wrongnameB2CenmeURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-me/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-me"
            const publishSeries = "projector"
            const publishUrl = "en-me/projector"
            const publishUrlName = "en-me/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenmeTH585P.push(url)
            }
    })
    return wrongnameB2CenmeTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENMK-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CenmkTH585P=[]
const wrongnameB2CenmkURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-mk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-mk"
            const publishSeries = "projector"
            const publishUrl = "en-mk/projector"
            const publishUrlName = "en-mk/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenmkTH585P.push(url)
            }
    })
    return wrongnameB2CenmkTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENMT-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CenmtTH585P=[]
const wrongnameB2CenmtURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-mt/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-mt"
            const publishSeries = "projector"
            const publishUrl = "en-mt/projector"
            const publishUrlName = "en-mt/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenmtTH585P.push(url)
            }
    })
    return wrongnameB2CenmtTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENMY-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CenmyTH585P=[]
const wrongnameB2CenmyURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-my/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-my"
            const publishSeries = "projector"
            const publishUrl = "en-my/projector"
            const publishUrlName = "en-my/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenmyTH585P.push(url)
            }
    })
    return wrongnameB2CenmyTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENNO-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CennoTH585P=[]
const wrongnameB2CennoURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-no/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-no"
            const publishSeries = "projector"
            const publishUrl = "en-no/projector"
            const publishUrlName = "en-no/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CennoTH585P.push(url)
            }
    })
    return wrongnameB2CennoTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENRS-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CenrsTH585P=[]
const wrongnameB2CenrsURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-rs/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-rs"
            const publishSeries = "projector"
            const publishUrl = "en-rs/projector"
            const publishUrlName = "en-rs/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenrsTH585P.push(url)
            }
    })
    return wrongnameB2CenrsTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENSG-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CensgTH585P=[]
const wrongnameB2CensgURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-sg/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-sg"
            const publishSeries = "projector"
            const publishUrl = "en-sg/projector"
            const publishUrlName = "en-sg/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CensgTH585P.push(url)
            }
    })
    return wrongnameB2CensgTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENSI-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CensiTH585P=[]
const wrongnameB2CensiURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-si/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-si"
            const publishSeries = "projector"
            const publishUrl = "en-si/projector"
            const publishUrlName = "en-si/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CensiTH585P.push(url)
            }
    })
    return wrongnameB2CensiTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENUK-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CenukTH585P=[]
const wrongnameB2CenukURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-uk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-uk"
            const publishSeries = "projector"
            const publishUrl = "en-uk/projector"
            const publishUrlName = "en-uk/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenukTH585P.push(url)
            }
    })
    return wrongnameB2CenukTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENUS-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CenusTH585P=[]
const wrongnameB2CenusURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-us/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-us"
            const publishSeries = "projector"
            const publishUrl = "en-us/projector"
            const publishUrlName = "en-us/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenusTH585P.push(url)
            }
    })
    return wrongnameB2CenusTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESAR-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CesarTH585P=[]
const wrongnameB2CesarURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-ar/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-ar"
            const publishSeries = "projector"
            const publishUrl = "es-ar/projector"
            const publishUrlName = "es-ar/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CesarTH585P.push(url)
            }
    })
    return wrongnameB2CesarTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESCO-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CescoTH585P=[]
const wrongnameB2CescoURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-co/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-co"
            const publishSeries = "projector"
            const publishUrl = "es-co/projector"
            const publishUrlName = "es-co/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CescoTH585P.push(url)
            }
    })
    return wrongnameB2CescoTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESES-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CesesTH585P=[]
const wrongnameB2CesesURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-es/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-es"
            const publishSeries = "projector"
            const publishUrl = "es-es/projector"
            const publishUrlName = "es-es/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CesesTH585P.push(url)
            }
    })
    return wrongnameB2CesesTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESLA-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CeslaTH585P=[]
const wrongnameB2CeslaURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/es-la/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-la"
            const publishSeries = "projector"
            const publishUrl = "es-la/projector"
            const publishUrlName = "es-la/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CeslaTH585P.push(url)
            }
    })
    return wrongnameB2CeslaTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESMX-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CesmxTH585P=[]
const wrongnameB2CesmxURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/es-mx/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-mx"
            const publishSeries = "projector"
            const publishUrl = "es-mx/projector"
            const publishUrlName = "es-mx/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CesmxTH585P.push(url)
            }
    })
    return wrongnameB2CesmxTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESPE-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CespeTH585P=[]
const wrongnameB2CespeURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-pe/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-pe"
            const publishSeries = "projector"
            const publishUrl = "es-pe/projector"
            const publishUrlName = "es-pe/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CespeTH585P.push(url)
            }
    })
    return wrongnameB2CespeTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-FRCA-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CfrcaTH585P=[]
const wrongnameB2CfrcaURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/fr-ca/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "fr-ca"
            const publishSeries = "projector"
            const publishUrl = "fr-ca/projector"
            const publishUrlName = "fr-ca/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CfrcaTH585P.push(url)
            }
    })
    return wrongnameB2CfrcaTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-FRCH-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CfrchTH585P=[]
const wrongnameB2CfrchURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/fr-ch/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "fr-ch"
            const publishSeries = "projector"
            const publishUrl = "fr-ch/projector"
            const publishUrlName = "fr-ch/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CfrchTH585P.push(url)
            }
    })
    return wrongnameB2CfrchTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-FRFR-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CfrfrTH585P=[]
const wrongnameB2CfrfrURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/fr-fr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "fr-fr"
            const publishSeries = "projector"
            const publishUrl = "fr-fr/projector"
            const publishUrlName = "fr-fr/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CfrfrTH585P.push(url)
            }
    })
    return wrongnameB2CfrfrTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-HUHU-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2ChuhuTH585P=[]
const wrongnameB2ChuhuURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/hu-hu/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "hu-hu"
            const publishSeries = "projector"
            const publishUrl = "hu-hu/projector"
            const publishUrlName = "hu-hu/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2ChuhuTH585P.push(url)
            }
    })
    return wrongnameB2ChuhuTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-IDID-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CididTH585P=[]
const wrongnameB2CididURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/id-id/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "id-id"
            const publishSeries = "projector"
            const publishUrl = "id-id/projector"
            const publishUrlName = "id-id/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CididTH585P.push(url)
            }
    })
    return wrongnameB2CididTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ITIT-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CititTH585P=[]
const wrongnameB2CititURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/it-it/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "it-it"
            const publishSeries = "projector"
            const publishUrl = "it-it/projector"
            const publishUrlName = "it-it/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CititTH585P.push(url)
            }
    })
    return wrongnameB2CititTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-JAJP-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CjajpTH585P=[]
const wrongnameB2CjajpURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ja-jp/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ja-jp"
            const publishSeries = "projector"
            const publishUrl = "ja-jp/projector"
            const publishUrlName = "ja-jp/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CjajpTH585P.push(url)
            }
    })
    return wrongnameB2CjajpTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-KOKR-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CkokrTH585P=[]
const wrongnameB2CkokrURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ko-kr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ko-kr"
            const publishSeries = "projector"
            const publishUrl = "ko-kr/projector"
            const publishUrlName = "ko-kr/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CkokrTH585P.push(url)
            }
    })
    return wrongnameB2CkokrTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-LTLT-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CltltTH585P=[]
const wrongnameB2CltltURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/lt-lt/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "lt-lt"
            const publishSeries = "projector"
            const publishUrl = "lt-lt/projector"
            const publishUrlName = "lt-lt/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CltltTH585P.push(url)
            }
    })
    return wrongnameB2CltltTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-NLBE-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CnlbeTH585P=[]
const wrongnameB2CnlbeURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/nl-be/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "nl-be"
            const publishSeries = "projector"
            const publishUrl = "nl-be/projector"
            const publishUrlName = "nl-be/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CnlbeTH585P.push(url)
            }
    })
    return wrongnameB2CnlbeTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-NLNL-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CnlnlTH585P=[]
const wrongnameB2CnlnlURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/nl-nl/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "nl-nl"
            const publishSeries = "projector"
            const publishUrl = "nl-nl/projector"
            const publishUrlName = "nl-nl/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CnlnlTH585P.push(url)
            }
    })
    return wrongnameB2CnlnlTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-PLPL-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CplplTH585P=[]
const wrongnameB2CplplURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/pl-pl/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "pl-pl"
            const publishSeries = "projector"
            const publishUrl = "pl-pl/projector"
            const publishUrlName = "pl-pl/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CplplTH585P.push(url)
            }
    })
    return wrongnameB2CplplTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-PTBR-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CptbrTH585P=[]
const wrongnameB2CptbrURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/pt-br/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "pt-br"
            const publishSeries = "projector"
            const publishUrl = "pt-br/projector"
            const publishUrlName = "pt-br/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CptbrTH585P.push(url)
            }
    })
    return wrongnameB2CptbrTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-PTPT-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CptptTH585P=[]
const wrongnameB2CptptURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/pt-pt/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "pt-pt"
            const publishSeries = "projector"
            const publishUrl = "pt-pt/projector"
            const publishUrlName = "pt-pt/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CptptTH585P.push(url)
            }
    })
    return wrongnameB2CptptTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-RORO-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CroroTH585P=[]
const wrongnameB2CroroURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/ro-ro/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ro-ro"
            const publishSeries = "projector"
            const publishUrl = "ro-ro/projector"
            const publishUrlName = "ro-ro/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CroroTH585P.push(url)
            }
    })
    return wrongnameB2CroroTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-RURU-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CruruTH585P=[]
const wrongnameB2CruruURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ru-ru/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ru-ru"
            const publishSeries = "projector"
            const publishUrl = "ru-ru/projector"
            const publishUrlName = "ru-ru/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CruruTH585P.push(url)
            }
    })
    return wrongnameB2CruruTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-SKSK-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CskskTH585P=[]
const wrongnameB2CskskURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/sk-sk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "sk-sk"
            const publishSeries = "projector"
            const publishUrl = "sk-sk/projector"
            const publishUrlName = "sk-sk/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CskskTH585P.push(url)
            }
    })
    return wrongnameB2CskskTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-SVSE-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CsvseTH585P=[]
const wrongnameB2CsvseURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/sv-se/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "sv-se"
            const publishSeries = "projector"
            const publishUrl = "sv-se/projector"
            const publishUrlName = "sv-se/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CsvseTH585P.push(url)
            }
    })
    return wrongnameB2CsvseTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-THTH-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CththTH585P=[]
const wrongnameB2CththURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/th-th/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "th-th"
            const publishSeries = "projector"
            const publishUrl = "th-th/projector"
            const publishUrlName = "th-th/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CththTH585P.push(url)
            }
    })
    return wrongnameB2CththTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-TRTR-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CtrtrTH585P=[]
const wrongnameB2CtrtrURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/tr-tr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "tr-tr"
            const publishSeries = "projector"
            const publishUrl = "tr-tr/projector"
            const publishUrlName = "tr-tr/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CtrtrTH585P.push(url)
            }
    })
    return wrongnameB2CtrtrTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-UKUA-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CukuaTH585P=[]
const wrongnameB2CukuaURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/uk-ua/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "uk-ua"
            const publishSeries = "projector"
            const publishUrl = "uk-ua/projector"
            const publishUrlName = "uk-ua/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CukuaTH585P.push(url)
            }
    })
    return wrongnameB2CukuaTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-VIVN-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CvivnTH585P=[]
const wrongnameB2CvivnURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/vi-vn/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "vi-vn"
            const publishSeries = "projector"
            const publishUrl = "vi-vn/projector"
            const publishUrlName = "vi-vn/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CvivnTH585P.push(url)
            }
    })
    return wrongnameB2CvivnTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ZHHK-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CzhhkTH585P=[]
const wrongnameB2CzhhkURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/zh-hk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "zh-hk"
            const publishSeries = "projector"
            const publishUrl = "zh-hk/projector"
            const publishUrlName = "zh-hk/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CzhhkTH585P.push(url)
            }
    })
    return wrongnameB2CzhhkTH585P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ZHTW-Projector-TH585P-name must be projector/gaming-projector/th585p.html
const wrongnameB2CzhtwTH585P=[]
const wrongnameB2CzhtwURLTH585P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/zh-tw/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "zh-tw"
            const publishSeries = "projector"
            const publishUrl = "zh-tw/projector"
            const publishUrlName = "zh-tw/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th585p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CzhtwTH585P.push(url)
            }
    })
    return wrongnameB2CzhtwTH585P;
    } catch (error) {
      console.log(error);
    }
};
//Test case
Given("TH585P must be published on global site after 20220401",{timeout: 1000 * 5000},async function(){
    //After 2022/01/01
    //global setting
    const publishModel = "th585p"
    const date = new Date()
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()
    function wholeMonth(){
        const getmonth = date.getMonth() + 1
        if(getmonth<10){
            wholeMonth =  "0"+getmonth 
            return wholeMonth
        }else{
            wholeMonth = getmonth 
            return wholeMonth
        }
    }
    function wholeDate(){
        const getDay = date.getDate()
        if(getDay<10){
            wholeDate =  "0"+getDay
            return wholeDate
        }else{
            wholeDate = getDay 
            return wholeDate
        }
    }
    const wholemonth = wholeMonth()
    const wholeday = wholeDate()
    const wholeyear = date.getFullYear()
    const fullDate = `${wholeyear}/${wholemonth}/${wholeday}`
    // console.log("wholemonth:",wholemonth)
    // console.log("wholeday:",wholeday)
    // console.log("wholeyear:",wholeyear)
    console.log("fullDate:",fullDate)
    // console.log("year:",year)
    // console.log("month:",month)
    // console.log("day:",day)
    const launchDate = "2022/04/01"

    //B2C-ARME-Projector-TH585P-after 20220401
    const armeReturnedData = await afterB2CarmeURLTH585P();
    // console.log("After returnedData",armeReturnedData)
    const armepublishCheck = armeReturnedData.length
    console.log("ar-me All publish URL-B2C Projector - TH585P:",armeReturnedData)
    console.log(armeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(armepublishCheck ==0){
            // afterB2CarmeTH585P.push("ar-me")
            TH585PPublishError.push("ar-me")

        }
    }
    //B2C-BGBG-Projector-TH585P-after 20220401
    const bgbgReturnedData = await afterB2CbgbgURLTH585P();
    // console.log("After returnedData",bgbgReturnedData)
    const bgbgpublishCheck = bgbgReturnedData.length
    console.log("bg-bg All publish URL-B2C Projector-TH585P:",bgbgReturnedData)
    console.log(bgbgReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(bgbgpublishCheck ==0){
            // afterB2CbgbgTH585P.push("bg-bg")
            TH585PPublishError.push("bg-bg")

        }
    }
    //B2C-CSCZ-Projector-TH585P-after 20220401
    const csczReturnedData = await afterB2CcsczURLTH585P();
    // console.log("After returnedData",csczReturnedData)
    const csczpublishCheck = csczReturnedData.length
    console.log("cs-cz All publish URL-B2C Projector-TH585P:",csczReturnedData)
    console.log(csczReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(csczpublishCheck ==0){
            // afterB2CcsczTH585P.push("cs-cz")
            TH585PPublishError.push("cs-cz")

        }
    }
    //B2C-DEAT-Projector-TH585P-after 20220401
    const deatReturnedData = await afterB2CdeatURLTH585P();
    // console.log("After returnedData",deatReturnedData)
    const deatpublishCheck = deatReturnedData.length
    console.log("de-at All publish URL-B2C Projector-TH585P:",deatReturnedData)
    console.log(deatReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(deatpublishCheck ==0){
            // afterB2Cdea-TH585P.push("de-at")
        -TH585PPublishError.push("de-at")

        }
    }
    //B2C-DECH-Projector-TH585P-after 20220401
    const dechReturnedData = await afterB2CdechURLTH585P();
    // console.log("After returnedData",dechReturnedData)
    const dechpublishCheck = dechReturnedData.length
    console.log("de-ch All publish URL-B2C Projector-TH585P:",dechReturnedData)
    console.log(dechReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(dechpublishCheck ==0){
            // afterB2CdechTH585P.push("de-ch")
            TH585PPublishError.push("de-ch")

        }
    }
    //B2C-DEDE-Projector-TH585P-after 20220401
    const dedeReturnedData = await afterB2CdedeURLTH585P();
    // console.log("After returnedData",dedeReturnedData)
    const dedepublishCheck = dedeReturnedData.length
    console.log("de-de All publish URL-B2C Projector-TH585P:",dedeReturnedData)
    console.log(dedeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(dedepublishCheck ==0){
            // afterB2CdedeTH585P.push("de-de")
            TH585PPublishError.push("de-de")

        }
    }
    //B2C-ELGR-Projector-TH585P-after 20220401
    const elgrReturnedData = await afterB2CelgrURLTH585P();
    // console.log("After returnedData",elgrReturnedData)
    const elgrpublishCheck = elgrReturnedData.length
    console.log("el-gr All publish URL-B2C Projector-TH585P:",elgrReturnedData)
    console.log(elgrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(elgrpublishCheck ==0){
            // afterB2CelgrTH585P.push("el-gr")
            TH585PPublishError.push("el-gr")

        }
    }
    //B2C-ENAP-Projector-TH585P-after 20220401
    const enapReturnedData = await afterB2CenapURLTH585P();
    // console.log("After returnedData",enapReturnedData)
    const enappublishCheck = enapReturnedData.length
    console.log("en-ap All publish URL-B2C Projector - TH585P:",enapReturnedData)
    console.log(enapReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(enappublishCheck ==0){
            // afterB2CenapTH585P.push("en-ap")
            TH585PPublishError.push("en-ap")

        }
    }
    //B2C-ENAU-Projector-TH585P-after 20220401
    const enauReturnedData = await afterB2CenauURLTH585P();
    // console.log("After returnedData",enauReturnedData)
    const enaupublishCheck = enauReturnedData.length
    console.log("en-au All publish URL-B2C Projector - TH585P:",enauReturnedData)
    console.log(enauReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(enaupublishCheck ==0){
            // afterB2CenauTH585P.push("en-au")
            TH585PPublishError.push("en-au")

        }
    }
    //B2C-ENBA-Projector-TH585P-after 20220401
    const enbaReturnedData = await afterB2CenbaURLTH585P();
    // console.log("After returnedData",enbaReturnedData)
    const enbapublishCheck = enbaReturnedData.length
    console.log("en-ba All publish URL-B2C Projector - TH585P:",enbaReturnedData)
    console.log(enbaReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(enbapublishCheck ==0){
            // afterB2CenbaTH585P.push("en-ba")
            TH585PPublishError.push("en-ba")

        }
    }
    //B2C-ENCA-Projector-TH585P-after 20220401
    const encaReturnedData = await afterB2CencaURLTH585P();
    // console.log("After returnedData",encaReturnedData)
    const encapublishCheck = encaReturnedData.length
    console.log("en-ca All publish URL-B2C Projector - TH585P:",encaReturnedData)
    console.log(encaReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(encapublishCheck ==0){
            // afterB2CencaTH585P.push("en-ca")
            TH585PPublishError.push("en-ca")
        }
    }
    //B2C-ENCEE-Projector-TH585P-after 20220401
    const enceeReturnedData = await afterB2CenceeURLTH585P();
    // console.log("After returnedData",enceeReturnedData)
    const enceepublishCheck = enceeReturnedData.length
    console.log("en-cee All publish URL-B2C Projector - TH585P:",enceeReturnedData)
    console.log(enceeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(enceepublishCheck ==0){
            // afterB2CencaTH585P.push("en-cee")
            TH585PPublishError.push("en-cee")
        }
    }
    //B2C-ENCY-Projector-TH585P-after 20220401
    const encyReturnedData = await afterB2CencyURLTH585P();
    // console.log("After returnedData",encyReturnedData)
    const encypublishCheck = encyReturnedData.length
    console.log("en-cy All publish URL-B2C Projector - TH585P:",encyReturnedData)
    console.log(encyReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(encypublishCheck ==0){
            // afterB2CencaTH585P.push("en-cy")
            TH585PPublishError.push("en-cy")
        }
    }
    //B2C-ENDK-Projector-TH585P-after 20220401
    const endkReturnedData = await afterB2CendkURLTH585P();
    // console.log("After returnedData",endkReturnedData)
    const endkpublishCheck = endkReturnedData.length
    console.log("en-dk All publish URL-B2C Projector - TH585P:",endkReturnedData)
    console.log(endkReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(endkpublishCheck ==0){
            // afterB2CencaTH585P.push("en-dk")
            TH585PPublishError.push("en-dk")
        }
    }
    //B2C-ENEE-Projector-TH585P-after 20220401
    const eneeReturnedData = await afterB2CeneeURLTH585P();
    // console.log("After returnedData",eneeReturnedData)
    const eneepublishCheck = eneeReturnedData.length
    console.log("en-ee All publish URL-B2C Projector-TH585P:",eneeReturnedData)
    console.log(eneeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(eneepublishCheck ==0){
            TH585PPublishError.push("en-ee")
        }
    }
    //B2C-ENEU-Projector-TH585P-after 20220401
    const eneuReturnedData = await afterB2CeneuURLTH585P();
    // console.log("After returnedData",eneuReturnedData)
    const eneupublishCheck = eneuReturnedData.length
    console.log("en-eu All publish URL-B2C Projector-TH585P:",eneuReturnedData)
    console.log(eneuReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(eneupublishCheck ==0){
            TH585PPublishError.push("en-eu")
        }
    }
    //B2C-ENFI-Projector-TH585P-after 20220401
    const enfiReturnedData = await afterB2CenfiURLTH585P();
    // console.log("After returnedData",enfiReturnedData)
    const enfipublishCheck = enfiReturnedData.length
    console.log("en-fi All publish URL-B2C Projector-TH585P:",enfiReturnedData)
    console.log(enfiReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(enfipublishCheck ==0){
            TH585PPublishError.push("en-fi")
        }
    }
    //B2C-ENHK-Projector-TH585P-after 20220401
    const enhkReturnedData = await afterB2CenhkURLTH585P();
    // console.log("After returnedData",enhkReturnedData)
    const enhkpublishCheck = enhkReturnedData.length
    console.log("en-hk All publish URL-B2C Projector - TH585P:",enhkReturnedData)
    console.log(enhkReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(enhkpublishCheck ==0){
            TH585PPublishError.push("en-hk")
        }
    }
    //B2C-ENHR-Projector-TH585P-after 20220401
    const enhrReturnedData = await afterB2CenhrURLTH585P();
    // console.log("After returnedData",enhrReturnedData)
    const enhrpublishCheck = enhrReturnedData.length
    console.log("en-hr All publish URL-B2C Projector-TH585P:",enhrReturnedData)
    console.log(enhrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(enhrpublishCheck ==0){
            TH585PPublishError.push("en-hr")
        }
    }
    //B2C-ENIE-Projector-TH585P-after 20TH585P
    const enieReturnedData = await afterB2CenieURLTH585P();
    // console.log("After returnedData",enieReturnedData)
    const eniepublishCheck = enieReturnedData.length
    console.log("en-ie All publish URL-B2C Projector-TH585P:",enieReturnedData)
    console.log(enieReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(eniepublishCheck ==0){
            TH585PPublishError.push("en-ie")
        }
    }
    //B2C-ENIN-Projector-TH585P-after 20TH585P
    const eninReturnedData = await afterB2CeninURLTH585P();
    // console.log("After returnedData",eninReturnedData)
    const eninpublishCheck = eninReturnedData.length
    console.log("en-in All publish URL-B2C Projector - TH585P:",eninReturnedData)
    console.log(eninReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(eninpublishCheck ==0){
            TH585PPublishError.push("en-in")
        }
    }
    //B2C-ENIS-Projector-TH585P-after 20220401
    const enisReturnedData = await afterB2CenisURLTH585P();
    // console.log("After returnedData",enisReturnedData)
    const enispublishCheck = enisReturnedData.length
    console.log("en-is All publish URL-B2C Projector-TH585P:",enisReturnedData)
    console.log(enisReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(enispublishCheck ==0){
            TH585PPublishError.push("en-is")
        }
    }
    //B2C-ENLU-Projector-TH585P-after 20220401
    const enluReturnedData = await afterB2CenluURLTH585P();
    // console.log("After returnedData",enluReturnedData)
    const enlupublishCheck = enluReturnedData.length
    console.log("en-lu All publish URL-B2C Projector-TH585P:",enluReturnedData)
    console.log(enluReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(enlupublishCheck ==0){
            TH585PPublishError.push("en-lu")
        }
    }
    //B2C-ENLV-Projector-TH585P-after 20220401
    const enlvReturnedData = await afterB2CenlvURLTH585P();
    // console.log("After returnedData",enlvReturnedData)
    const enlvpublishCheck = enlvReturnedData.length
    console.log("en-lv All publish URL-B2C Projector-TH585P:",enlvReturnedData)
    console.log(enlvReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(enlvpublishCheck ==0){
            TH585PPublishError.push("en-lv")
        }
    }
    //B2C-ENME-Projector-TH585P-after 20220401
    const enmeReturnedData = await afterB2CenmeURLTH585P();
    // console.log("After returnedData",enmeReturnedData)
    const enmepublishCheck = enmeReturnedData.length
    console.log("en-me All publish URL-B2C Projector - TH585P:",enmeReturnedData)
    console.log(enmeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(enmepublishCheck ==0){
            TH585PPublishError.push("en-me")
        }
    }
    //B2C-ENMK-Projector-TH585P-after 20220401
    const enmkReturnedData = await afterB2CenmkURLTH585P();
    // console.log("After returnedData",enmkReturnedData)
    const enmkpublishCheck = enmkReturnedData.length
    console.log("en-mk All publish URL-B2C Projector-TH585P:",enmkReturnedData)
    console.log(enmkReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(enmkpublishCheck ==0){
            TH585PPublishError.push("en-mk")
        }
    }
    //B2C-ENMT-Projector-TH585P-after 20220401
    const enmtReturnedData = await afterB2CenmtURLTH585P();
    // console.log("After returnedData",enmtReturnedData)
    const enmtpublishCheck = enmtReturnedData.length
    console.log("en-mt All publish URL-B2C Projector-TH585P:",enmtReturnedData)
    console.log(enmtReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(enmtpublishCheck ==0){
            TH585PPublishError.push("en-mt")
        }
    }
    //B2C-ENMY-Projector-TH585P-after 20220401
    const enmyReturnedData = await afterB2CenmyURLTH585P();
    // console.log("After returnedData",enmyReturnedData)
    const enmypublishCheck = enmyReturnedData.length
    console.log("en-my All publish URL-B2C Projector - TH585P:",enmyReturnedData)
    console.log(enmyReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(enmypublishCheck ==0){
            TH585PPublishError.push("en-my")
        }
    }
    //B2C-ENNO-Projector-TH585P-after 20220401
    const ennoReturnedData = await afterB2CennoURLTH585P();
    // console.log("After returnedData",ennoReturnedData)
    const ennopublishCheck = ennoReturnedData.length
    console.log("en-no All publish URL-B2C Projector-TH585P:",ennoReturnedData)
    console.log(ennoReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(ennopublishCheck ==0){
            TH585PPublishError.push("en-no")
        }
    }
    //B2C-ENRS-Projector-TH585P-after 20220401
    const enrsReturnedData = await afterB2CenrsURLTH585P();
    // console.log("After returnedData",enrsReturnedData)
    const enrspublishCheck = enrsReturnedData.length
    console.log("en-rs All publish URL-B2C Projector-TH585P:",enrsReturnedData)
    console.log(enrsReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(enrspublishCheck ==0){
            TH585PPublishError.push("en-rs")
        }
    }
    //B2C-ENSG-Projector-TH585P-after 20220401
    const ensgReturnedData = await afterB2CensgURLTH585P();
    // console.log("After returnedData",ensgReturnedData)
    const ensgpublishCheck = ensgReturnedData.length
    console.log("en-sg All publish URL-B2C Projector - TH585P:",ensgReturnedData)
    console.log(ensgReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(ensgpublishCheck ==0){
            TH585PPublishError.push("en-sg")
        }
    }
    //B2C-ENSI-Projector-TH585P-after 20220401
    const ensiReturnedData = await afterB2CensiURLTH585P();
    // console.log("After returnedData",ensiReturnedData)
    const ensipublishCheck = ensiReturnedData.length
    console.log("en-si All publish URL-B2C Projector-TH585P:",ensiReturnedData)
    console.log(ensiReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(ensipublishCheck ==0){
            TH585PPublishError.push("en-si")
        }
    }
    //B2C-ENUK-Projector-TH585P-after 20220401
    const enukReturnedData = await afterB2CenukURLTH585P();
    // console.log("After returnedData",enukReturnedData)
    const enukpublishCheck = enukReturnedData.length
    console.log("en-uk All publish URL-B2C Projector-TH585P:",enukReturnedData)
    console.log(enukReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(enukpublishCheck ==0){
            TH585PPublishError.push("en-uk")
        }
    }
    //B2C-ENUS-Projector-TH585P-after 20220401
    const enusReturnedData = await afterB2CenusURLTH585P();
    // console.log("After returnedData",enusReturnedData)
    const enuspublishCheck = enusReturnedData.length
    console.log("en-us All publish URL-B2C Projector - TH585P:",enusReturnedData)
    console.log(enusReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(enuspublishCheck ==0){
            TH585PPublishError.push("en-us")
        }
    }
    //B2C-ESAR-Projector-TH585P-after 20220401
    const esarReturnedData = await afterB2CesarURLTH585P();
    // console.log("After returnedData",esarReturnedData)
    const esarpublishCheck = esarReturnedData.length
    console.log("es-ar All publish URL-B2C Projector - TH585P:",esarReturnedData)
    console.log(esarReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(esarpublishCheck ==0){
            TH585PPublishError.push("es-ar")
        }
    }
    //B2C-ESCO-Projector-TH585P-after 20220401
    const escoReturnedData = await afterB2CescoURLTH585P();
    // console.log("After returnedData",escoReturnedData)
    const escopublishCheck = escoReturnedData.length
    console.log("es-co All publish URL-B2C Projector - TH585P:",escoReturnedData)
    console.log(escoReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(escopublishCheck ==0){
            TH585PPublishError.push("es-co")
        }
    }
    //B2C-ESES-Projector-TH585P-after 20220401
    const esesReturnedData = await afterB2CesesURLTH585P();
    // console.log("After returnedData",esesReturnedData)
    const esespublishCheck = esesReturnedData.length
    console.log("es-es All publish URL-B2C Projector - TH585P:",esesReturnedData)
    console.log(esesReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(esespublishCheck ==0){
            TH585PPublishError.push("es-es")
        }
    }
    //B2C-ESLA-Projector-TH585P-after 20220401
    const eslaReturnedData = await afterB2CeslaURLTH585P();
    // console.log("After returnedData",eslaReturnedData)
    const eslapublishCheck = eslaReturnedData.length
    console.log("es-la All publish URL-B2C Projector - TH585P:",eslaReturnedData)
    console.log(eslaReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(eslapublishCheck ==0){
            TH585PPublishError.push("es-la")
        }
    }
    //B2C-ESMX-Projector-TH585P-after 20220401
    const esmxReturnedData = await afterB2CesmxURLTH585P();
    // console.log("After returnedData",esmxReturnedData)
    const esmxpublishCheck = esmxReturnedData.length
    console.log("es-mx All publish URL-B2C Projector - TH585P:",esmxReturnedData)
    console.log(esmxReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(esmxpublishCheck ==0){
            TH585PPublishError.push("es-mx")
        }
    }
    //B2C-ESPE-Projector-TH585P-after 20220401
    const espeReturnedData = await afterB2CespeURLTH585P();
    // console.log("After returnedData",espeReturnedData)
    const espepublishCheck = espeReturnedData.length
    console.log("es-pe All publish URL-B2C Projector - TH585P:",espeReturnedData)
    console.log(espeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(espepublishCheck ==0){
            TH585PPublishError.push("es-pe")
        }
    }
    //B2C-FRCA-Projector-TH585P-after 20220401
    const frcaReturnedData = await afterB2CfrcaURLTH585P();
    // console.log("After returnedData",frcaReturnedData)
    const frcapublishCheck = frcaReturnedData.length
    console.log("fr-ca All publish URL-B2C Projector - TH585P:",frcaReturnedData)
    console.log(frcaReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(frcapublishCheck ==0){
            TH585PPublishError.push("fr-ca")
        }
    }
    //B2C-FRCH-Projector-TH585P-after 20220401
    const frchReturnedData = await afterB2CfrchURLTH585P();
    // console.log("After returnedData",frchReturnedData)
    const frchpublishCheck = frchReturnedData.length
    console.log("fr-ch All publish URL-B2C Projector - TH585P:",frchReturnedData)
    console.log(frchReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(frchpublishCheck ==0){
            TH585PPublishError.push("fr-ch")
        }
    }
    //B2C-FRFR-Projector-TH585P-after 20220401
    const frfrReturnedData = await afterB2CfrfrURLTH585P();
    // console.log("After returnedData",frfrReturnedData)
    const frfrpublishCheck = frfrReturnedData.length
    console.log("fr-fr All publish URL-B2C Projector - TH585P:",frfrReturnedData)
    console.log(frfrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(frfrpublishCheck ==0){
            TH585PPublishError.push("fr-fr")
        }
    }
    //B2C-HUHU-Projector-TH585P-after 20220401
    const huhuReturnedData = await afterB2ChuhuURLTH585P();
    // console.log("After returnedData",huhuReturnedData)
    const huhupublishCheck = huhuReturnedData.length
    console.log("hu-hu All publish URL-B2C Projector - TH585P:",huhuReturnedData)
    console.log(huhuReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(huhupublishCheck ==0){
            TH585PPublishError.push("hu-hu")
        }
    }
    //B2C-IDID-Projector-TH585P-after 20220401
    const ididReturnedData = await afterB2CididURLTH585P();
    // console.log("After returnedData",ididReturnedData)
    const ididpublishCheck = ididReturnedData.length
    console.log("id-id All publish URL-B2C Projector - TH585P:",ididReturnedData)
    console.log(ididReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(ididpublishCheck ==0){
            TH585PPublishError.push("id-id")
        }
    }
    //B2C-ITIT-Projector-TH585P-after 20220401
    const ititReturnedData = await afterB2CititURLTH585P();
    // console.log("After returnedData",ititReturnedData)
    const ititpublishCheck = ititReturnedData.length
    console.log("it-it All publish URL-B2C Projector - TH585P:",ititReturnedData)
    console.log(ititReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(ititpublishCheck ==0){
            TH585PPublishError.push("it-it")
        }
    }
    //B2C-JAJP-Projector-TH585P-after 20220401
    const jajpReturnedData = await afterB2CjajpURLTH585P();
    // console.log("After returnedData",jajpReturnedData)
    const jajppublishCheck = jajpReturnedData.length
    console.log("ja-jp All publish URL-B2C Projector - TH585P:",jajpReturnedData)
    console.log(jajpReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(jajppublishCheck ==0){
            TH585PPublishError.push("ja-jp")
        }
    }
    //B2C-KOKR-Projector-TH585P-after 20220401
    const kokrReturnedData = await afterB2CkokrURLTH585P();
    // console.log("After returnedData",kokrReturnedData)
    const kokrpublishCheck = kokrReturnedData.length
    console.log("ko-kr All publish URL-B2C Projector - TH585P:",kokrReturnedData)
    console.log(kokrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(kokrpublishCheck ==0){
            TH585PPublishError.push("ko-kr")
        }
    }
    //B2C-LTLT-Projector-TH585P-after 20220401
    const ltltReturnedData = await afterB2CltltURLTH585P();
    // console.log("After returnedData",ltltReturnedData)
    const ltltpublishCheck = ltltReturnedData.length
    console.log("lt-lt All publish URL-B2C Projector - TH585P:",ltltReturnedData)
    console.log(ltltReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(ltltpublishCheck ==0){
            TH585PPublishError.push("lt-lt")
        }
    }
    //B2C-NLBE-Projector-TH585P-after 20220401
    const nlbeReturnedData = await afterB2CnlbeURLTH585P();
    // console.log("After returnedData",nlbeReturnedData)
    const nlbepublishCheck = nlbeReturnedData.length
    console.log("nl-be All publish URL-B2C Projector - TH585P:",nlbeReturnedData)
    console.log(nlbeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(nlbepublishCheck ==0){
            TH585PPublishError.push("nl-be")
        }
    }
    //B2C-NLNL-Projector-TH585P-after 20220401
    const nlnlReturnedData = await afterB2CnlnlURLTH585P();
    // console.log("After returnedData",nlnlReturnedData)
    const nlnlpublishCheck = nlnlReturnedData.length
    console.log("nl-nl All publish URL-B2C Projector - TH585P:",nlnlReturnedData)
    console.log(nlnlReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(nlnlpublishCheck ==0){
            TH585PPublishError.push("nl-nl")
        }
    }
    //B2C-PLPL-Projector-TH585P-after 20220401
    const plplReturnedData = await afterB2CplplURLTH585P();
    // console.log("After returnedData",plplReturnedData)
    const plplpublishCheck = plplReturnedData.length
    console.log("pl-pl All publish URL-B2C Projector - TH585P:",plplReturnedData)
    console.log(plplReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(plplpublishCheck ==0){
            TH585PPublishError.push("pl-pl")
        }
    }
    //B2C-PTBR-Projector-TH585P-after 20220401
    const ptbrReturnedData = await afterB2CptbrURLTH585P();
    // console.log("After returnedData",ptbrReturnedData)
    const ptbrpublishCheck = ptbrReturnedData.length
    console.log("pt-br All publish URL-B2C Projector - TH585P:",ptbrReturnedData)
    console.log(ptbrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(ptbrpublishCheck ==0){
            TH585PPublishError.push("pt-br")
        }
    }
    //B2C-PTPT-Projector-TH585P-after 20220401
    const ptptReturnedData = await afterB2CptptURLTH585P();
    // console.log("After returnedData",ptptReturnedData)
    const ptptpublishCheck = ptptReturnedData.length
    console.log("pt-pt All publish URL-B2C Projector - TH585P:",ptptReturnedData)
    console.log(ptptReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(ptptpublishCheck ==0){
            TH585PPublishError.push("pt-pt")
        }
    }
    //B2C-RORO-Projector-TH585P-after 20220401
    const roroReturnedData = await afterB2CroroURLTH585P();
    // console.log("After returnedData",roroReturnedData)
    const roropublishCheck = roroReturnedData.length
    console.log("ro-ro All publish URL-B2C Projector - TH585P:",roroReturnedData)
    console.log(roroReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(roropublishCheck ==0){
            TH585PPublishError.push("ro-ro")
        }
    }
    //B2C-RURU-Projector-TH585P-after 20220401
    const ruruReturnedData = await afterB2CruruURLTH585P();
    // console.log("After returnedData",ruruReturnedData)
    const rurupublishCheck = ruruReturnedData.length
    console.log("ru-ru All publish URL-B2C Projector - TH585P:",ruruReturnedData)
    console.log(ruruReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(rurupublishCheck ==0){
            TH585PPublishError.push("ru-ru")
        }
    }
    //B2C-SKSK-Projector-TH585P-after 20220401
    const skskReturnedData = await afterB2CskskURLTH585P();
    // console.log("After returnedData",skskReturnedData)
    const skskpublishCheck = skskReturnedData.length
    console.log("sk-sk All publish URL-B2C Projector - TH585P:",skskReturnedData)
    console.log(skskReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(skskpublishCheck ==0){
            TH585PPublishError.push("sk-sk")
        }
    }
    //B2C-SVSE-Projector-TH585P-after 20220401
    const svseReturnedData = await afterB2CsvseURLTH585P();
    // console.log("After returnedData",svseReturnedData)
    const svsepublishCheck = svseReturnedData.length
    console.log("sv-se All publish URL-B2C Projector - TH585P:",svseReturnedData)
    console.log(svseReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(svsepublishCheck ==0){
            TH585PPublishError.push("sv-se")
        }
    }
    //B2C-THTH-Projector-TH585P-after 20220401
    const ththReturnedData = await afterB2CththURLTH585P();
    // console.log("After returnedData",ththReturnedData)
    const ththpublishCheck = ththReturnedData.length
    console.log("th-th All publish URL-B2C Projector - TH585P:",ththReturnedData)
    console.log(ththReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(ththpublishCheck ==0){
            TH585PPublishError.push("th-th")
        }
    }
    //B2C-TRTR-Projector-TH585P-after 20220401
    const trtrReturnedData = await afterB2CtrtrURLTH585P();
    // console.log("After returnedData",trtrReturnedData)
    const trtrpublishCheck = trtrReturnedData.length
    console.log("tr-tr All publish URL-B2C Projector - TH585P:",trtrReturnedData)
    console.log(trtrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(trtrpublishCheck ==0){
            TH585PPublishError.push("tr-tr")
        }
    }
    //B2C-UKUA-Projector-TH585P-after 20220401
    const ukuaReturnedData = await afterB2CukuaURLTH585P();
    // console.log("After returnedData",ukuaReturnedData)
    const ukuapublishCheck = ukuaReturnedData.length
    console.log("uk-ua All publish URL-B2C Projector - TH585P:",ukuaReturnedData)
    console.log(ukuaReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(ukuapublishCheck ==0){
            TH585PPublishError.push("uk-ua")
        }
    }
    //B2C-VIVN-Projector-TH585P-after 20220401
    const vivnReturnedData = await afterB2CvivnURLTH585P();
    // console.log("After returnedData",vivnReturnedData)
    const vivnpublishCheck = vivnReturnedData.length
    console.log("vi-vn All publish URL-B2C Projector - TH585P:",vivnReturnedData)
    console.log(vivnReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(vivnpublishCheck ==0){
            TH585PPublishError.push("vi-vn")
        }
    }
    //B2C-ZHHK-Projector-TH585P-after 20220401
    const zhhkReturnedData = await afterB2CzhhkURLTH585P();
    // console.log("After returnedData",zhhkReturnedData)
    const zhhkpublishCheck = zhhkReturnedData.length
    console.log("zh-hk All publish URL-B2C Projector - TH585P:",zhhkReturnedData)
    console.log(zhhkReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(zhhkpublishCheck ==0){
            TH585PPublishError.push("zh-hk")
        }
    }
    //B2C-ZHTW-Projector-TH585P-after 20220401
    const zhtwReturnedData = await afterB2CzhtwURLTH585P();
    // console.log("After returnedData",zhtwReturnedData)
    const zhtwpublishCheck = zhtwReturnedData.length
    console.log("zh-tw All publish URL-B2C Projector - TH585P:",zhtwReturnedData)
    console.log(zhtwReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH585PPublishError
        if(zhtwpublishCheck ==0){
            TH585PPublishError.push("zh-tw")
        }
    }
    //放全部的國家的後面
    if(TH585PPublishError.length >0){
        console.log("Not Published Now:",TH585PPublishError)
        throw new Error(`${publishModel} must be published on ${TH585PPublishError} before ${launchDate}, but it is not published now. Today is ${fullDate}`)
    }
})

Then("TH585P URL name must be 'projector gaming-projector th585p.html'",{timeout: 1000 * 5000},async function(){
    //After 2022/04/01
    //global setting
    const publishModel = "th585p"
    const publishProductURLName = "projector/gaming-projector/th585p.html"
    const date = new Date()
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()
    function wholeMonth(){
        const getmonth = date.getMonth() + 1
        if(getmonth<10){
            wholeMonth =  "0"+getmonth 
            return wholeMonth
        }else{
            wholeMonth = getmonth 
            return wholeMonth
        }
    }
    function wholeDate(){
        const getDay = date.getDate()
        if(getDay<10){
            wholeDate =  "0"+getDay
            return wholeDate
        }else{
            wholeDate = getDay 
            return wholeDate
        }
    }
    const wholemonth = wholeMonth()
    const wholeday = wholeDate()
    const wholeyear = date.getFullYear()
    const fullDate = `${wholeyear}/${wholemonth}/${wholeday}`
    // console.log("wholemonth:",wholemonth)
    // console.log("wholeday:",wholeday)
    // console.log("wholeyear:",wholeyear)
    console.log("fullDate:",fullDate)
    // console.log("year:",year)
    // console.log("month:",month)
    // console.log("day:",day)
    const launchDate = "2022/04/01"
    //B2C-ARME-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const armeReturnedData = await wrongnameB2CarmeURLTH585P();
    const armeNameCheck = armeReturnedData.length
    console.log("ar-me wrong name URL-B2C Projector-TH585P:",armeReturnedData)
    console.log(armeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(armeNameCheck > 0){
            TH585PNameError.push('ar-me')
            TH585PNameErrorURL.push(armeReturnedData)
        }
    }
    //B2C-BGBG-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const bgbgReturnedData = await wrongnameB2CbgbgURLTH585P();
    const bgbgNameCheck = bgbgReturnedData.length
    console.log("bg-bg wrong name URL-B2C Projector-TH585P:",bgbgReturnedData)
    console.log(bgbgReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(bgbgNameCheck > 0){
            TH585PNameError.push('bg-bg')
            TH585PNameErrorURL.push(bgbgReturnedData)
        }
    }
    //B2C-CSCZ-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const csczReturnedData = await wrongnameB2CcsczURLTH585P();
    const csczNameCheck = csczReturnedData.length
    console.log("cs-cz wrong name URL-B2C Projector-TH585P:",csczReturnedData)
    console.log(csczReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(csczNameCheck > 0){
            TH585PNameError.push('cs-cz')
            TH585PNameErrorURL.push(csczReturnedData)
        }
    }
    //B2C-DEAT-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const deatReturnedData = await wrongnameB2CdeatURLTH585P();
    const deatNameCheck = deatReturnedData.length
    console.log("de-at wrong name URL-B2C Projector-TH585P:",deatReturnedData)
    console.log(deatReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(deatNameCheck > 0){
            TH585PNameError.push('de-at')
            TH585PNameErrorURL.push(deatReturnedData)
        }
    }
    //B2C-DECH-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const dechReturnedData = await wrongnameB2CdechURLTH585P();
    const dechNameCheck = dechReturnedData.length
    console.log("de-ch wrong name URL-B2C Projector-TH585P:",dechReturnedData)
    console.log(dechReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(dechNameCheck > 0){
            TH585PNameError.push('de-ch')
            TH585PNameErrorURL.push(dechReturnedData)
        }
    }
    //B2C-DEDE-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const dedeReturnedData = await wrongnameB2CdedeURLTH585P();
    const dedeNameCheck = dedeReturnedData.length
    console.log("de-de wrong name URL-B2C Projector-TH585P:",dedeReturnedData)
    console.log(dedeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(dedeNameCheck > 0){
            TH585PNameError.push('de-de')
            TH585PNameErrorURL.push(dedeReturnedData)
        }
    }
    //B2C-ELGR-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const elgrReturnedData = await wrongnameB2CelgrURLTH585P();
    const elgrNameCheck = elgrReturnedData.length
    console.log("el-gr wrong name URL-B2C Projector-TH585P:",elgrReturnedData)
    console.log(elgrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(elgrNameCheck > 0){
            TH585PNameError.push('el-gr')
            TH585PNameErrorURL.push(elgrReturnedData)
        }
    }
    //B2C-ENAP-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const enapReturnedData = await wrongnameB2CenapURLTH585P();
    const enapNameCheck = enapReturnedData.length
    console.log("en-ap wrong name URL-B2C Projector-TH585P:",enapReturnedData)
    console.log(enapReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enapNameCheck > 0){
            TH585PNameError.push("en-ap")
            TH585PNameErrorURL.push(enapReturnedData)
        }
    }
    //B2C-ENAU-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const enauReturnedData = await wrongnameB2CenauURLTH585P();
    const enauNameCheck = enauReturnedData.length
    console.log("en-au wrong name URL-B2C Projector-TH585P:",enauReturnedData)
    console.log(enauReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enauNameCheck > 0){
            TH585PNameError.push("en-au")
            TH585PNameErrorURL.push(enauReturnedData)
        }
    }
    //B2C-ENBA-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const enbaReturnedData = await wrongnameB2CenbaURLTH585P();
    const enbaNameCheck = enbaReturnedData.length
    console.log("en-ba wrong name URL-B2C Projector-TH585P:",enbaReturnedData)
    console.log(enbaReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enbaNameCheck > 0){
            TH585PNameError.push("en-ba")
            TH585PNameErrorURL.push(enbaReturnedData)
        }
    }
    //B2C-ENCA-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const encaReturnedData = await wrongnameB2CencaURLTH585P();
    const encaNameCheck = encaReturnedData.length
    console.log("en-ca wrong name URL-B2C Projector-TH585P:",encaReturnedData)
    console.log(encaReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(encaNameCheck > 0){
            TH585PNameError.push("en-ca")
            TH585PNameErrorURL.push(encaReturnedData)
        }
    }
    //B2C-ENCEE-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const enceeReturnedData = await wrongnameB2CenceeURLTH585P();
    const enceeNameCheck = enceeReturnedData.length
    console.log("en-cee wrong name URL-B2C Projector-TH585P:",enceeReturnedData)
    console.log(enceeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enceeNameCheck > 0){
            TH585PNameError.push("en-cee")
            TH585PNameErrorURL.push(enceeReturnedData)
        }
    }
    //B2C-ENCY-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const encyReturnedData = await wrongnameB2CencyURLTH585P();
    const encyNameCheck = encyReturnedData.length
    console.log("en-cy wrong name URL-B2C Projector-TH585P:",encyReturnedData)
    console.log(encyReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(encyNameCheck > 0){
            TH585PNameError.push("en-cy")
            TH585PNameErrorURL.push(encyReturnedData)
        }
    }
    //B2C-ENDK-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const endkReturnedData = await wrongnameB2CendkURLTH585P();
    const endkNameCheck = endkReturnedData.length
    console.log("en-dk wrong name URL-B2C Projector-TH585P:",endkReturnedData)
    console.log(endkReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(endkNameCheck > 0){
            TH585PNameError.push("en-dk")
            TH585PNameErrorURL.push(endkReturnedData)
        }
    }
    //B2C-ENEE-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const eneeReturnedData = await wrongnameB2CeneeURLTH585P();
    const eneeNameCheck = eneeReturnedData.length
    console.log("en-ee wrong name URL-B2C Projector-TH585P:",eneeReturnedData)
    console.log(eneeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(eneeNameCheck > 0){
            TH585PNameError.push('en-ee')
            TH585PNameErrorURL.push(eneeReturnedData)
        }
    }
    //B2C-ENEU-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const eneuReturnedData = await wrongnameB2CeneuURLTH585P();
    const eneuNameCheck = eneuReturnedData.length
    console.log("en-eu wrong name URL-B2C Projector-TH585P:",eneuReturnedData)
    console.log(eneuReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(eneuNameCheck > 0){
            TH585PNameError.push('en-eu')
            TH585PNameErrorURL.push(eneuReturnedData)
        }
    }
    //B2C-ENFI-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const enfiReturnedData = await wrongnameB2CenfiURLTH585P();
    const enfiNameCheck = enfiReturnedData.length
    console.log("en-fi wrong name URL-B2C Projector-TH585P:",enfiReturnedData)
    console.log(enfiReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enfiNameCheck > 0){
            TH585PNameError.push('en-fi')
            TH585PNameErrorURL.push(enfiReturnedData)
        }
    }
    //B2C-ENHK-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const enhkReturnedData = await wrongnameB2CenhkURLTH585P();
    const enhkNameCheck = enhkReturnedData.length
    console.log("en-hk wrong name URL-B2C Projector-TH585P:",enhkReturnedData)
    console.log(enhkReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enhkNameCheck > 0){
            TH585PNameError.push("en-hk")
            TH585PNameErrorURL.push(enhkReturnedData)
        }
    }
    //B2C-ENHR-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const enhrReturnedData = await wrongnameB2CenhrURLTH585P();
    const enhrNameCheck = enhrReturnedData.length
    console.log("en-hr wrong name URL-B2C Projector-TH585P:",enhrReturnedData)
    console.log(enhrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enhrNameCheck > 0){
            TH585PNameError.push('en-hr')
            TH585PNameErrorURL.push(enhrReturnedData)
        }
    }
    //B2C-ENIE-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const enieReturnedData = await wrongnameB2CenieURLTH585P();
    const enieNameCheck = enieReturnedData.length
    console.log("en-ie wrong name URL-B2C Projector-TH585P:",enieReturnedData)
    console.log(enieReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enieNameCheck > 0){
            TH585PNameError.push('en-ie')
            TH585PNameErrorURL.push(enieReturnedData)
        }
    }
    //B2C-ENIN-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const eninReturnedData = await wrongnameB2CeninURLTH585P();
    const eninNameCheck = eninReturnedData.length
    console.log("en-in wrong name URL-B2C Projector-TH585P:",eninReturnedData)
    console.log(eninReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(eninNameCheck > 0){
            TH585PNameError.push("en-in")
            TH585PNameErrorURL.push(eninReturnedData)
        }
    }
    //B2C-ENIS-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const enisReturnedData = await wrongnameB2CenisURLTH585P();
    const enisNameCheck = enisReturnedData.length
    console.log("en-is wrong name URL-B2C Projector-TH585P:",enisReturnedData)
    console.log(enisReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enisNameCheck > 0){
            TH585PNameError.push('en-is')
            TH585PNameErrorURL.push(enisReturnedData)
        }
    }
    //B2C-ENLU-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const enluReturnedData = await wrongnameB2CenluURLTH585P();
    const enluNameCheck = enluReturnedData.length
    console.log("en-lu wrong name URL-B2C Projector-TH585P:",enluReturnedData)
    console.log(enluReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enluNameCheck > 0){
            TH585PNameError.push('en-lu')
            TH585PNameErrorURL.push(enluReturnedData)
        }
    }
    //B2C-ENLV-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const enlvReturnedData = await wrongnameB2CenlvURLTH585P();
    const enlvNameCheck = enlvReturnedData.length
    console.log("en-lv wrong name URL-B2C Projector-TH585P:",enlvReturnedData)
    console.log(enlvReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enlvNameCheck > 0){
            TH585PNameError.push('en-lv')
            TH585PNameErrorURL.push(enlvReturnedData)
        }
    }
    //B2C-ENME-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const enmeReturnedData = await wrongnameB2CenmeURLTH585P();
    const enmeNameCheck = enmeReturnedData.length
    console.log("en-me wrong name URL-B2C Projector-TH585P:",enmeReturnedData)
    console.log(enmeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enmeNameCheck > 0){
            TH585PNameError.push("en-me")
            TH585PNameErrorURL.push(enmeReturnedData)
        }
    }
    //B2C-ENMK-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const enmkReturnedData = await wrongnameB2CenmkURLTH585P();
    const enmkNameCheck = enmkReturnedData.length
    console.log("en-mk wrong name URL-B2C Projector-TH585P:",enmkReturnedData)
    console.log(enmkReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enmkNameCheck > 0){
            TH585PNameError.push('en-mk')
            TH585PNameErrorURL.push(enmkReturnedData)
        }
    }
    //B2C-ENMT-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const enmtReturnedData = await wrongnameB2CenmtURLTH585P();
    const enmtNameCheck = enmtReturnedData.length
    console.log("en-mt wrong name URL-B2C Projector-TH585P:",enmtReturnedData)
    console.log(enmtReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enmtNameCheck > 0){
            TH585PNameError.push('en-mt')
            TH585PNameErrorURL.push(enmtReturnedData)
        }
    }
    //B2C-ENMY-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const enmyReturnedData = await wrongnameB2CenmyURLTH585P();
    const enmyNameCheck = enmyReturnedData.length
    console.log("en-my wrong name URL-B2C Projector-TH585P:",enmyReturnedData)
    console.log(enmyReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enmyNameCheck > 0){
            TH585PNameError.push("en-my")
            TH585PNameErrorURL.push(enmyReturnedData)
        }
    }
    //B2C-ENNO-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const ennoReturnedData = await wrongnameB2CennoURLTH585P();
    const ennoNameCheck = ennoReturnedData.length
    console.log("en-no wrong name URL-B2C Projector-TH585P:",ennoReturnedData)
    console.log(ennoReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ennoNameCheck > 0){
            TH585PNameError.push('en-no')
            TH585PNameErrorURL.push(ennoReturnedData)
        }
    }
    //B2C-ENRS-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const enrsReturnedData = await wrongnameB2CenrsURLTH585P();
    const enrsNameCheck = enrsReturnedData.length
    console.log("en-rs wrong name URL-B2C Projector-TH585P:",enrsReturnedData)
    console.log(enrsReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enrsNameCheck > 0){
            TH585PNameError.push('en-rs')
            TH585PNameErrorURL.push(enrsReturnedData)
        }
    }
    //B2C-ENSG-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const ensgReturnedData = await wrongnameB2CensgURLTH585P();
    const ensgNameCheck = ensgReturnedData.length
    console.log("en-sg wrong name URL-B2C Projector-TH585P:",ensgReturnedData)
    console.log(ensgReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ensgNameCheck > 0){
            TH585PNameError.push("en-sg")
            TH585PNameErrorURL.push(ensgReturnedData)
        }
    }
    //B2C-ENSI-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const ensiReturnedData = await wrongnameB2CensiURLTH585P();
    const ensiNameCheck = ensiReturnedData.length
    console.log("en-si wrong name URL-B2C Projector-TH585P:",ensiReturnedData)
    console.log(ensiReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ensiNameCheck > 0){
            TH585PNameError.push('en-si')
            TH585PNameErrorURL.push(ensiReturnedData)
        }
    }
    //B2C-ENUK-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const enukReturnedData = await wrongnameB2CenukURLTH585P();
    const enukNameCheck = enukReturnedData.length
    console.log("en-uk wrong name URL-B2C Projector-TH585P:",enukReturnedData)
    console.log(enukReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enukNameCheck > 0){
            TH585PNameError.push('en-uk')
            TH585PNameErrorURL.push(enukReturnedData)
        }
    }
    //B2C-ENUS-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const enusReturnedData = await wrongnameB2CenusURLTH585P();
    const enusNameCheck = enusReturnedData.length
    console.log("en-us wrong name URL-B2C Projector-TH585P:",enusReturnedData)
    console.log(enusReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enusNameCheck > 0){
            TH585PNameError.push("en-us")
            TH585PNameErrorURL.push(enusReturnedData)
        }
    }
    //B2C-ESAR-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const esarReturnedData = await wrongnameB2CesarURLTH585P();
    const esarNameCheck = esarReturnedData.length
    console.log("es-ar wrong name URL-B2C Projector-TH585P:",esarReturnedData)
    console.log(esarReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(esarNameCheck > 0){
            TH585PNameError.push("es-ar")
            TH585PNameErrorURL.push(esarReturnedData)
        }
    }
    //B2C-ESCO-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const escoReturnedData = await wrongnameB2CescoURLTH585P();
    const escoNameCheck = escoReturnedData.length
    console.log("es-co wrong name URL-B2C Projector-TH585P:",escoReturnedData)
    console.log(escoReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(escoNameCheck > 0){
            TH585PNameError.push("es-co")
            TH585PNameErrorURL.push(escoReturnedData)
        }
    }
    //B2C-ESES-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const esesReturnedData = await wrongnameB2CesesURLTH585P();
    const esesNameCheck = esesReturnedData.length
    console.log("es-es wrong name URL-B2C Projector-TH585P:",esesReturnedData)
    console.log(esesReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(esesNameCheck > 0){
            TH585PNameError.push("es-es")
            TH585PNameErrorURL.push(esesReturnedData)
        }
    }
    //B2C-ESLA-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const eslaReturnedData = await wrongnameB2CeslaURLTH585P();
    const eslaNameCheck = eslaReturnedData.length
    console.log("es-la wrong name URL-B2C Projector-TH585P:",eslaReturnedData)
    console.log(eslaReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(eslaNameCheck > 0){
            TH585PNameError.push("es-la")
            TH585PNameErrorURL.push(eslaReturnedData)
        }
    }
    //B2C-ESMX-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const esmxReturnedData = await wrongnameB2CesmxURLTH585P();
    const esmxNameCheck = esmxReturnedData.length
    console.log("es-mx wrong name URL-B2C Projector-TH585P:",esmxReturnedData)
    console.log(esmxReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(esmxNameCheck > 0){
            TH585PNameError.push("es-mx")
            TH585PNameErrorURL.push(esmxReturnedData)
        }
    }
    //B2C-ESPE-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const espeReturnedData = await wrongnameB2CespeURLTH585P();
    const espeNameCheck = espeReturnedData.length
    console.log("es-pe wrong name URL-B2C Projector-TH585P:",espeReturnedData)
    console.log(espeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(espeNameCheck > 0){
            TH585PNameError.push("es-pe")
            TH585PNameErrorURL.push(espeReturnedData)
        }
    }
    //B2C-FRCA-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const frcaReturnedData = await wrongnameB2CfrcaURLTH585P();
    const frcaNameCheck = frcaReturnedData.length
    console.log("fr-ca wrong name URL-B2C Projector-TH585P:",frcaReturnedData)
    console.log(frcaReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(frcaNameCheck > 0){
            TH585PNameError.push("fr-ca")
            TH585PNameErrorURL.push(frcaReturnedData)
        }
    }
    //B2C-FRCH-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const frchReturnedData = await wrongnameB2CfrchURLTH585P();
    const frchNameCheck = frchReturnedData.length
    console.log("fr-ch wrong name URL-B2C Projector-TH585P:",frchReturnedData)
    console.log(frchReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(frchNameCheck > 0){
            TH585PNameError.push("fr-ch")
            TH585PNameErrorURL.push(frchReturnedData)
        }
    }
    //B2C-FRFR-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const frfrReturnedData = await wrongnameB2CfrfrURLTH585P();
    const frfrNameCheck = frfrReturnedData.length
    console.log("fr-fr wrong name URL-B2C Projector-TH585P:",frfrReturnedData)
    console.log(frfrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(frfrNameCheck > 0){
            TH585PNameError.push("fr-fr")
            TH585PNameErrorURL.push(frfrReturnedData)
        }
    }
    //B2C-HUHU-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const huhuReturnedData = await wrongnameB2ChuhuURLTH585P();
    const huhuNameCheck = huhuReturnedData.length
    console.log("hu-hu wrong name URL-B2C Projector-TH585P:",huhuReturnedData)
    console.log(huhuReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(huhuNameCheck > 0){
            TH585PNameError.push("hu-hu")
            TH585PNameErrorURL.push(huhuReturnedData)
        }
    }
    //B2C-IDID-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const ididReturnedData = await wrongnameB2CididURLTH585P();
    const ididNameCheck = ididReturnedData.length
    console.log("id-id wrong name URL-B2C Projector-TH585P:",ididReturnedData)
    console.log(ididReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ididNameCheck > 0){
            TH585PNameError.push("id-id")
            TH585PNameErrorURL.push(ididReturnedData)
        }
    }
    //B2C-ITIT-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const ititReturnedData = await wrongnameB2CititURLTH585P();
    const ititNameCheck = ititReturnedData.length
    console.log("it-it wrong name URL-B2C Projector-TH585P:",ititReturnedData)
    console.log(ititReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ititNameCheck > 0){
            TH585PNameError.push("it-it")
            TH585PNameErrorURL.push(ititReturnedData)
        }
    }
    //B2C-JAJP-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const jajpReturnedData = await wrongnameB2CjajpURLTH585P();
    const jajpNameCheck = jajpReturnedData.length
    console.log("ja-jp wrong name URL-B2C Projector-TH585P:",jajpReturnedData)
    console.log(jajpReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(jajpNameCheck > 0){
            TH585PNameError.push("ja-jp")
            TH585PNameErrorURL.push(jajpReturnedData)
        }
    }
    //B2C-KOKR-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const kokrReturnedData = await wrongnameB2CkokrURLTH585P();
    const kokrNameCheck = kokrReturnedData.length
    console.log("ko-kr wrong name URL-B2C Projector-TH585P:",kokrReturnedData)
    console.log(kokrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(kokrNameCheck > 0){
            TH585PNameError.push("ko-kr")
            TH585PNameErrorURL.push(kokrReturnedData)
        }
    }
    //B2C-LTLT-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const ltltReturnedData = await wrongnameB2CltltURLTH585P();
    const ltltNameCheck = ltltReturnedData.length
    console.log("lt-lt wrong name URL-B2C Projector-TH585P:",ltltReturnedData)
    console.log(ltltReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ltltNameCheck > 0){
            TH585PNameError.push("lt-lt")
            TH585PNameErrorURL.push(ltltReturnedData)
        }
    }
    //B2C-NLBE-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const nlbeReturnedData = await wrongnameB2CnlbeURLTH585P();
    const nlbeNameCheck = nlbeReturnedData.length
    console.log("nl-be wrong name URL-B2C Projector-TH585P:",nlbeReturnedData)
    console.log(nlbeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(nlbeNameCheck > 0){
            TH585PNameError.push("nl-be")
            TH585PNameErrorURL.push(nlbeReturnedData)
        }
    }
    //B2C-NLNL-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const nlnlReturnedData = await wrongnameB2CnlnlURLTH585P();
    const nlnlNameCheck = nlnlReturnedData.length
    console.log("nl-nl wrong name URL-B2C Projector-TH585P:",nlnlReturnedData)
    console.log(nlnlReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(nlnlNameCheck > 0){
            TH585PNameError.push("nl-nl")
            TH585PNameErrorURL.push(nlnlReturnedData)
        }
    }
    //B2C-PLPL-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const plplReturnedData = await wrongnameB2CplplURLTH585P();
    const plplNameCheck = plplReturnedData.length
    console.log("pl-pl wrong name URL-B2C Projector-TH585P:",plplReturnedData)
    console.log(plplReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(plplNameCheck > 0){
            TH585PNameError.push("pl-pl")
            TH585PNameErrorURL.push(plplReturnedData)
        }
    }
    //B2C-PTBR-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const ptbrReturnedData = await wrongnameB2CptbrURLTH585P();
    const ptbrNameCheck = ptbrReturnedData.length
    console.log("pt-br wrong name URL-B2C Projector-TH585P:",ptbrReturnedData)
    console.log(ptbrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ptbrNameCheck > 0){
            TH585PNameError.push("pt-br")
            TH585PNameErrorURL.push(ptbrReturnedData)
        }
    }
    //B2C-PTPT-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const ptptReturnedData = await wrongnameB2CptptURLTH585P();
    const ptptNameCheck = ptptReturnedData.length
    console.log("pt-pt wrong name URL-B2C Projector-TH585P:",ptptReturnedData)
    console.log(ptptReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ptptNameCheck > 0){
            TH585PNameError.push("pt-pt")
            TH585PNameErrorURL.push(ptptReturnedData)
        }
    }
    //B2C-RORO-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const roroReturnedData = await wrongnameB2CroroURLTH585P();
    const roroNameCheck = roroReturnedData.length
    console.log("ro-ro wrong name URL-B2C Projector-TH585P:",roroReturnedData)
    console.log(roroReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(roroNameCheck > 0){
            TH585PNameError.push("ro-ro")
            TH585PNameErrorURL.push(roroReturnedData)
        }
    }
    //B2C-RURU-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const ruruReturnedData = await wrongnameB2CruruURLTH585P();
    const ruruNameCheck = ruruReturnedData.length
    console.log("ru-ru wrong name URL-B2C Projector-TH585P:",ruruReturnedData)
    console.log(ruruReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ruruNameCheck > 0){
            TH585PNameError.push("ru-ru")
            TH585PNameErrorURL.push(ruruReturnedData)
        }
    }
    //B2C-SKSK-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const skskReturnedData = await wrongnameB2CskskURLTH585P();
    const skskNameCheck = skskReturnedData.length
    console.log("sk-sk wrong name URL-B2C Projector-TH585P:",skskReturnedData)
    console.log(skskReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(skskNameCheck > 0){
            TH585PNameError.push("sk-sk")
            TH585PNameErrorURL.push(skskReturnedData)
        }
    }
    //B2C-SVSE-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const svseReturnedData = await wrongnameB2CsvseURLTH585P();
    const svseNameCheck = svseReturnedData.length
    console.log("sv-se wrong name URL-B2C Projector-TH585P:",svseReturnedData)
    console.log(svseReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(svseNameCheck > 0){
            TH585PNameError.push("sv-se")
            TH585PNameErrorURL.push(svseReturnedData)
        }
    }
    //B2C-THTH-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const ththReturnedData = await wrongnameB2CththURLTH585P();
    const ththNameCheck = ththReturnedData.length
    console.log("th-th wrong name URL-B2C Projector-TH585P:",ththReturnedData)
    console.log(ththReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ththNameCheck > 0){
            TH585PNameError.push("th-th")
            TH585PNameErrorURL.push(ththReturnedData)
        }
    }
    //B2C-TRTR-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const trtrReturnedData = await wrongnameB2CtrtrURLTH585P();
    const trtrNameCheck = trtrReturnedData.length
    console.log("tr-tr wrong name URL-B2C Projector-TH585P:",trtrReturnedData)
    console.log(trtrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(trtrNameCheck > 0){
            TH585PNameError.push("tr-tr")
            TH585PNameErrorURL.push(trtrReturnedData)
        }
    }
    //B2C-UKUA-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const ukuaReturnedData = await wrongnameB2CukuaURLTH585P();
    const ukuaNameCheck = ukuaReturnedData.length
    console.log("uk-ua wrong name URL-B2C Projector-TH585P:",ukuaReturnedData)
    console.log(ukuaReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ukuaNameCheck > 0){
            TH585PNameError.push("uk-ua")
            TH585PNameErrorURL.push(ukuaReturnedData)
        }
    }
    //B2C-VIVN-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const vivnReturnedData = await wrongnameB2CvivnURLTH585P();
    const vivnNameCheck = vivnReturnedData.length
    console.log("vi-vn wrong name URL-B2C Projector-TH585P:",vivnReturnedData)
    console.log(vivnReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(vivnNameCheck > 0){
            TH585PNameError.push("vi-vn")
            TH585PNameErrorURL.push(vivnReturnedData)
        }
    }
    //B2C-ZHHK-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const zhhkReturnedData = await wrongnameB2CzhhkURLTH585P();
    const zhhkNameCheck = zhhkReturnedData.length
    console.log("zh-hk wrong name URL-B2C Projector-TH585P:",zhhkReturnedData)
    console.log(zhhkReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(zhhkNameCheck > 0){
            TH585PNameError.push("zh-hk")
            TH585PNameErrorURL.push(zhhkReturnedData)
        }
    }
    //B2C-ZHTW-Projector-TH585P-name must be projector/gaming-projector/th585p.html
    const zhtwReturnedData = await wrongnameB2CzhtwURLTH585P();
    const zhtwNameCheck = zhtwReturnedData.length
    console.log("zh-tw wrong name URL-B2C Projector-TH585P:",zhtwReturnedData)
    console.log(zhtwReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(zhtwNameCheck > 0){
            TH585PNameError.push("zh-tw")
            TH585PNameErrorURL.push(zhtwReturnedData)
        }
    }

    //放全部國家的後面
    if(TH585PNameError.length >0){
        throw new Error(`${publishModel} URL name must be ${publishProductURLName}, but it is wrong now. Here is RO name: ${TH585PNameError} And wrong URL: ${TH585PNameErrorURL}`)
    }
})
