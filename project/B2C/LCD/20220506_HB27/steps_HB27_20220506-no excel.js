const { Given, When, Then, Before, After } = require('@cucumber/cucumber')
const { expect } = require('chai')
const fs = require('fs');
const puppeteer = require('puppeteer')
const request = require("request-promise");
const cheerio = require("cheerio");
const cicGA="?utm_source=autotest&utm_medium=CIC"

Before({timeout: 24 * 5000},async function () {
    this.browser = await puppeteer.launch({ 
        executablePath:
        "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
        // MAC路徑:
        // executablePath:
        // '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome',
        headless:true,//有無需要開視窗,false要開,true不開
        slowMo:100,// slow down by 100ms
        devtools:false//有無需要開啟開發人員工具
    })
    this.page = await this.browser.newPage()
    await this.page.setViewport({width:1200,height:1000})
    await this.page.setDefaultTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
    await this.page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
})

After({timeout: 12 * 5000},async function () {
    await this.browser.close()
})

//HB27
const excelToHB27RONS =[
    "ar-me",
    "bg-bg",
    "cs-cz",
    "de-at",
    "de-ch",
    "de-de",
    "el-gr",
    "en-ap",
    "en-au",
    "en-ba",
    "en-ca",
    "en-cee",
    "en-cy",
    "en-dk",
    "en-ee",
    "en-eu",
    "en-fi",
    "en-hk",
    "en-hr",
    "en-ie",
    "en-in",
    "en-is",
    "en-lu",
    "en-lv",
    "en-me",
    "en-mk",
    "en-mt",
    "en-my",
    "en-no",
    "en-rs",
    "en-sg",
    "en-si",
    "en-uk",
    "en-us",
    "es-ar",
    "es-co",
    "es-es",
    "es-la",
    "es-mx",
    "es-pe",
    "fr-ca",
    "fr-ch",
    "fr-fr",
    "hu-hu",
    "id-id",
    "it-it",
    "ja-jp",
    "ko-kr",
    "lt-lt",
    "nl-be",
    "nl-nl",
    "pl-pl",
    "pt-br",
    "pt-pt",
    "ro-ro",
    "ru-ru",
    "sk-sk",
    "sv-se",
    "th-th",
    "tr-tr",
    "uk-ua",
    "vi-vn",
    // "zh-cn",
    "zh-hk",
    "zh-tw"
]
const excelToHB27Result =[]
const excelToHB27URL =[]

//Professional Monitor accessory HB27 - publish check
const HB27PublishError=[]
//B2C-ARME-Professional Monitor accessory-HB27-after 20220506
const afterB2CarmeHB27=[]
const afterB2CarmeURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ar-me/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CarmeHB27.push(url)
            }
    })
    return afterB2CarmeHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-BGBG-Professional Monitor accessory-HB27-after 20220506
const afterB2CbgbgHB27=[]
const afterB2CbgbgURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/bg-bg/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CbgbgHB27.push(url)
            }
    })
    return afterB2CbgbgHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-CSCZ-Professional Monitor accessory-HB27-after 20220506
const afterB2CcsczHB27=[]
const afterB2CcsczURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/cs-cz/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CcsczHB27.push(url)
            }
    })
    return afterB2CcsczHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-DEAT-Professional Monitor accessory-HB27-after 20220506
const afterB2CdeatHB27=[]
const afterB2CdeatURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/de-at/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CdeatHB27.push(url)
            }
    })
    return afterB2CdeatHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-DECH-Professional Monitor accessory-HB27-after 20220506
const afterB2CdechHB27=[]
const afterB2CdechURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/de-ch/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CdechHB27.push(url)
            }
    })
    return afterB2CdechHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-DEDE-Professional Monitor accessory-HB27-after 20220506
const afterB2CdedeHB27=[]
const afterB2CdedeURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/de-de/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CdedeHB27.push(url)
            }
    })
    return afterB2CdedeHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ELGR-Professional Monitor accessory-HB27-after 20220506
const afterB2CelgrHB27=[]
const afterB2CelgrURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/el-gr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CelgrHB27.push(url)
            }
    })
    return afterB2CelgrHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENAP-Professional Monitor accessory-HB27-after 20220506
const afterB2CenapHB27=[]
const afterB2CenapURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-ap/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenapHB27.push(url)
            }
    })
    return afterB2CenapHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENAU-Professional Monitor accessory-HB27-after 20220506
const afterB2CenauHB27=[]
const afterB2CenauURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-au/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenauHB27.push(url)
            }
    })
    return afterB2CenauHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENBA-Professional Monitor accessory-HB27-after 20220506
const afterB2CenbaHB27=[]
const afterB2CenbaURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-ba/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenbaHB27.push(url)
            }
    })
    return afterB2CenbaHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENCA-Professional Monitor accessory-HB27-after 20220506
const afterB2CencaHB27=[]
const afterB2CencaURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-ca/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CencaHB27.push(url)
            }
    })
    return afterB2CencaHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENCEE-Professional Monitor accessory-HB27-after 20220506
const afterB2CenceeHB27=[]
const afterB2CenceeURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-cee/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenceeHB27.push(url)
            }
    })
    return afterB2CenceeHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENCY-Professional Monitor accessory-HB27-after 20220506
const afterB2CencyHB27=[]
const afterB2CencyURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-cy/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CencyHB27.push(url)
            }
    })
    return afterB2CencyHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENDK-Professional Monitor accessory-HB27-after 20220506
const afterB2CendkHB27=[]
const afterB2CendkURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-dk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CendkHB27.push(url)
            }
    })
    return afterB2CendkHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENEE-Professional Monitor accessory-HB27-after 20220506
const afterB2CeneeHB27=[]
const afterB2CeneeURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-ee/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CeneeHB27.push(url)
            }
    })
    return afterB2CeneeHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENEU-Professional Monitor accessory-HB27-after 20220506
const afterB2CeneuHB27=[]
const afterB2CeneuURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-eu/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CeneuHB27.push(url)
            }
    })
    return afterB2CeneuHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENFI-Professional Monitor accessory-HB27-after 20220506
const afterB2CenfiHB27=[]
const afterB2CenfiURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-fi/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenfiHB27.push(url)
            }
    })
    return afterB2CenfiHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENHK-Professional Monitor accessory-HB27-after 20220506
const afterB2CenhkHB27=[]
const afterB2CenhkURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-hk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenhkHB27.push(url)
            }
    })
    return afterB2CenhkHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENHR-Professional Monitor accessory-HB27-after 20220506
const afterB2CenhrHB27=[]
const afterB2CenhrURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-hr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenhrHB27.push(url)
            }
    })
    return afterB2CenhrHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENIE-Professional Monitor accessory-HB27-after 20220506
const afterB2CenieHB27=[]
const afterB2CenieURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-ie/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenieHB27.push(url)
            }
    })
    return afterB2CenieHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENIN-Professional Monitor accessory-HB27-after 20220506
const afterB2CeninHB27=[]
const afterB2CeninURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-in/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CeninHB27.push(url)
            }
    })
    return afterB2CeninHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENIS-Professional Monitor accessory-HB27-after 20220506
const afterB2CenisHB27=[]
const afterB2CenisURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-is/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenisHB27.push(url)
            }
    })
    return afterB2CenisHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENLU-Professional Monitor accessory-HB27-after 20220506
const afterB2CenluHB27=[]
const afterB2CenluURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-lu/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenluHB27.push(url)
            }
    })
    return afterB2CenluHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENLV-Professional Monitor accessory-HB27-after 20220506
const afterB2CenlvHB27=[]
const afterB2CenlvURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-lv/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenlvHB27.push(url)
            }
    })
    return afterB2CenlvHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENME-Professional Monitor accessory-HB27-after 20220506
const afterB2CenmeHB27=[]
const afterB2CenmeURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-me/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenmeHB27.push(url)
            }
    })
    return afterB2CenmeHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENMK-Professional Monitor accessory-HB27-after 20220506
const afterB2CenmkHB27=[]
const afterB2CenmkURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-mk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenmkHB27.push(url)
            }
    })
    return afterB2CenmkHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENMT-Professional Monitor accessory-HB27-after 20220506
const afterB2CenmtHB27=[]
const afterB2CenmtURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-mt/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl="hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenmtHB27.push(url)
            }
    })
    return afterB2CenmtHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENMY-Professional Monitor accessory-HB27-after 20220506
const afterB2CenmyHB27=[]
const afterB2CenmyURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-my/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenmyHB27.push(url)
            }
    })
    return afterB2CenmyHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENNO-Professional Monitor accessory-HB27-after 20220506
const afterB2CennoHB27=[]
const afterB2CennoURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-no/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CennoHB27.push(url)
            }
    })
    return afterB2CennoHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENRS-Professional Monitor accessory-HB27-after 20220506
const afterB2CenrsHB27=[]
const afterB2CenrsURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-rs/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenrsHB27.push(url)
            }
    })
    return afterB2CenrsHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENSG-Professional Monitor accessory-HB27-after 20220506
const afterB2CensgHB27=[]
const afterB2CensgURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-sg/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CensgHB27.push(url)
            }
    })
    return afterB2CensgHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENSI-Professional Monitor accessory-HB27-after 20220506
const afterB2CensiHB27=[]
const afterB2CensiURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-si/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CensiHB27.push(url)
            }
    })
    return afterB2CensiHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENUK-Professional Monitor accessory-HB27-after 20220506
const afterB2CenukHB27=[]
const afterB2CenukURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-uk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenukHB27.push(url)
            }
    })
    return afterB2CenukHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENUS-Professional Monitor accessory-HB27-after 20220506
const afterB2CenusHB27=[]
const afterB2CenusURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-us/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenusHB27.push(url)
            }
    })
    return afterB2CenusHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESAR-Professional Monitor accessory-HB27-after 20220506
const afterB2CesarHB27=[]
const afterB2CesarURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-ar/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CesarHB27.push(url)
            }
    })
    return afterB2CesarHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESCO-Professional Monitor accessory-HB27-after 20220506
const afterB2CescoHB27=[]
const afterB2CescoURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-co/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CescoHB27.push(url)
            }
    })
    return afterB2CescoHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESES-Professional Monitor accessory-HB27-after 20220506
const afterB2CesesHB27=[]
const afterB2CesesURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-es/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CesesHB27.push(url)
            }
    })
    return afterB2CesesHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESLA-Professional Monitor accessory-HB27-after 20220506
const afterB2CeslaHB27=[]
const afterB2CeslaURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/es-la/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CeslaHB27.push(url)
            }
    })
    return afterB2CeslaHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESMX-Professional Monitor accessory-HB27-after 20220506
const afterB2CesmxHB27=[]
const afterB2CesmxURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/es-mx/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CesmxHB27.push(url)
            }
    })
    return afterB2CesmxHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESPE-Professional Monitor accessory-HB27-after 20220506
const afterB2CespeHB27=[]
const afterB2CespeURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-pe/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CespeHB27.push(url)
            }
    })
    return afterB2CespeHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-FRCA-Professional Monitor accessory-HB27-after 20220506
const afterB2CfrcaHB27=[]
const afterB2CfrcaURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/fr-ca/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CfrcaHB27.push(url)
            }
    })
    return afterB2CfrcaHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-FRCH-Professional Monitor accessory-HB27-after 20220506
const afterB2CfrchHB27=[]
const afterB2CfrchURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/fr-ch/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CfrchHB27.push(url)
            }
    })
    return afterB2CfrchHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-FRFR-Professional Monitor accessory-HB27-after 20220506
const afterB2CfrfrHB27=[]
const afterB2CfrfrURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/fr-fr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CfrfrHB27.push(url)
            }
    })
    return afterB2CfrfrHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-HUHU-Professional Monitor accessory-HB27-after 20220506
const afterB2ChuhuHB27=[]
const afterB2ChuhuURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/hu-hu/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2ChuhuHB27.push(url)
            }
    })
    return afterB2ChuhuHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-IDID-Professional Monitor accessory-HB27-after 20220506
const afterB2CididHB27=[]
const afterB2CididURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/id-id/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CididHB27.push(url)
            }
    })
    return afterB2CididHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ITIT-Professional Monitor accessory-HB27-after 20220506
const afterB2CititHB27=[]
const afterB2CititURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/it-it/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CititHB27.push(url)
            }
    })
    return afterB2CititHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-JAJP-Professional Monitor accessory-HB27-after 20220506
const afterB2CjajpHB27=[]
const afterB2CjajpURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ja-jp/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CjajpHB27.push(url)
            }
    })
    return afterB2CjajpHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-KOKR-Professional Monitor accessory-HB27-after 20220506
const afterB2CkokrHB27=[]
const afterB2CkokrURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ko-kr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CkokrHB27.push(url)
            }
    })
    return afterB2CkokrHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-LTLT-Professional Monitor accessory-HB27-after 20220506
const afterB2CltltHB27=[]
const afterB2CltltURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/lt-lt/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CltltHB27.push(url)
            }
    })
    return afterB2CltltHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-NLBE-Professional Monitor accessory-HB27-after 20220506
const afterB2CnlbeHB27=[]
const afterB2CnlbeURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/nl-be/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CnlbeHB27.push(url)
            }
    })
    return afterB2CnlbeHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-NLNL-Professional Monitor accessory-HB27-after 20220506
const afterB2CnlnlHB27=[]
const afterB2CnlnlURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/nl-nl/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CnlnlHB27.push(url)
            }
    })
    return afterB2CnlnlHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-PLPL-Professional Monitor accessory-HB27-after 20220506
const afterB2CplplHB27=[]
const afterB2CplplURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/pl-pl/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CplplHB27.push(url)
            }
    })
    return afterB2CplplHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-PTBR-Professional Monitor accessory-HB27-after 20220506
const afterB2CptbrHB27=[]
const afterB2CptbrURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/pt-br/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CptbrHB27.push(url)
            }
    })
    return afterB2CptbrHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-PTPT-Professional Monitor accessory-HB27-after 20220506
const afterB2CptptHB27=[]
const afterB2CptptURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/pt-pt/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CptptHB27.push(url)
            }
    })
    return afterB2CptptHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-RORO-Professional Monitor accessory-HB27-after 20220506
const afterB2CroroHB27=[]
const afterB2CroroURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/ro-ro/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CroroHB27.push(url)
            }
    })
    return afterB2CroroHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-RURU-Professional Monitor accessory-HB27-after 20220506
const afterB2CruruHB27=[]
const afterB2CruruURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ru-ru/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CruruHB27.push(url)
            }
    })
    return afterB2CruruHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-SKSK-Professional Monitor accessory-HB27-after 20220506
const afterB2CskskHB27=[]
const afterB2CskskURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/sk-sk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CskskHB27.push(url)
            }
    })
    return afterB2CskskHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-SVSE-Professional Monitor accessory-HB27-after 20220506
const afterB2CsvseHB27=[]
const afterB2CsvseURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/sv-se/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CsvseHB27.push(url)
            }
    })
    return afterB2CsvseHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-THTH-Professional Monitor accessory-HB27-after 20220506
const afterB2CththHB27=[]
const afterB2CththURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/th-th/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CththHB27.push(url)
            }
    })
    return afterB2CththHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-TRTR-Professional Monitor accessory-HB27-after 20220506
const afterB2CtrtrHB27=[]
const afterB2CtrtrURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/tr-tr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CtrtrHB27.push(url)
            }
    })
    return afterB2CtrtrHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-UKUA-Professional Monitor accessory-HB27-after 20220506
const afterB2CukuaHB27=[]
const afterB2CukuaURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/uk-ua/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CukuaHB27.push(url)
            }
    })
    return afterB2CukuaHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-VIVN-Professional Monitor accessory-HB27-after 20220506
const afterB2CvivnHB27=[]
const afterB2CvivnURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/vi-vn/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CvivnHB27.push(url)
            }
    })
    return afterB2CvivnHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ZHHK-Professional Monitor accessory-HB27-after 20220506
const afterB2CzhhkHB27=[]
const afterB2CzhhkURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/zh-hk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl="hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CzhhkHB27.push(url)
            }
    })
    return afterB2CzhhkHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ZHTW-Professional Monitor accessory-HB27-after 20220506
const afterB2CzhtwHB27=[]
const afterB2CzhtwURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/zh-tw/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "hb27.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CzhtwHB27.push(url)
            }
    })
    return afterB2CzhtwHB27;
    } catch (error) {
      console.log(error);
    }
};

//Professional Monitor accessory HB27 - URL name check
const HB27NameError=[]
const HB27NameErrorURL=[]
//B2C-ARME-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CarmeHB27=[]
const wrongnameB2CarmeURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ar-me/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ar-me"
            const publishSeries = "monitor"
            const publishUrl = "ar-me/monitor"
            const publishUrlName = "ar-me/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CarmeHB27.push(url)
            }
    })
    return wrongnameB2CarmeHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-BGBG-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CbgbgHB27=[]
const wrongnameB2CbgbgURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/bg-bg/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "bg-bg"
            const publishSeries = "monitor"
            const publishUrl = "bg-bg/monitor"
            const publishUrlName = "bg-bg/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CbgbgHB27.push(url)
            }
    })
    return wrongnameB2CbgbgHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-CSCZ-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CcsczHB27=[]
const wrongnameB2CcsczURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/cs-cz/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "cs-cz"
            const publishSeries = "monitor"
            const publishUrl = "cs-cz/monitor"
            const publishUrlName = "cs-cz/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CcsczHB27.push(url)
            }
    })
    return wrongnameB2CcsczHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-DEAT-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CdeatHB27=[]
const wrongnameB2CdeatURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/de-at/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "de-at"
            const publishSeries = "monitor"
            const publishUrl = "de-at/monitor"
            const publishUrlName = "de-at/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CdeatHB27.push(url)
            }
    })
    return wrongnameB2CdeatHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-DECH-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CdechHB27=[]
const wrongnameB2CdechURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/de-ch/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "de-ch"
            const publishSeries = "monitor"
            const publishUrl = "de-ch/monitor"
            const publishUrlName = "de-ch/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CdechHB27.push(url)
            }
    })
    return wrongnameB2CdechHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-DEDE-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CdedeHB27=[]
const wrongnameB2CdedeURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/de-de/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "de-de"
            const publishSeries = "monitor"
            const publishUrl = "de-de/monitor"
            const publishUrlName = "de-de/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CdedeHB27.push(url)
            }
    })
    return wrongnameB2CdedeHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ELGR-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CelgrHB27=[]
const wrongnameB2CelgrURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/el-gr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "el-gr"
            const publishSeries = "monitor"
            const publishUrl = "el-gr/monitor"
            const publishUrlName = "el-gr/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CelgrHB27.push(url)
            }
    })
    return wrongnameB2CelgrHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENAP-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CenapHB27=[]
const wrongnameB2CenapURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-ap/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-ap"
            const publishSeries = "monitor"
            const publishUrl = "en-ap/monitor"
            const publishUrlName = "en-ap/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenapHB27.push(url)
            }
    })
    return wrongnameB2CenapHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENAU-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CenauHB27=[]
const wrongnameB2CenauURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-au/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-au"
            const publishSeries = "monitor"
            const publishUrl = "en-au/monitor"
            const publishUrlName = "en-au/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenauHB27.push(url)
            }
    })
    return wrongnameB2CenauHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENBA-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CenbaHB27=[]
const wrongnameB2CenbaURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-ba/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-ba"
            const publishSeries = "monitor"
            const publishUrl = "en-ba/monitor"
            const publishUrlName = "en-ba/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenbaHB27.push(url)
            }
    })
    return wrongnameB2CenbaHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENCA-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CencaHB27=[]
const wrongnameB2CencaURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-ca/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-ca"
            const publishSeries = "monitor"
            const publishUrl = "en-ca/monitor"
            const publishUrlName = "en-ca/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CencaHB27.push(url)
            }
    })
    return wrongnameB2CencaHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENCEE-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CenceeHB27=[]
const wrongnameB2CenceeURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-cee/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-cee"
            const publishSeries = "monitor"
            const publishUrl = "en-cee/monitor"
            const publishUrlName = "en-cee/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenceeHB27.push(url)
            }
    })
    return wrongnameB2CenceeHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENCY-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CencyHB27=[]
const wrongnameB2CencyURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-cy/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-cy"
            const publishSeries = "monitor"
            const publishUrl = "en-cy/monitor"
            const publishUrlName = "en-cy/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CencyHB27.push(url)
            }
    })
    return wrongnameB2CencyHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENDK-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CendkHB27=[]
const wrongnameB2CendkURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-dk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-dk"
            const publishSeries = "monitor"
            const publishUrl = "en-dk/monitor"
            const publishUrlName = "en-dk/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CendkHB27.push(url)
            }
    })
    return wrongnameB2CendkHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENEE-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CeneeHB27=[]
const wrongnameB2CeneeURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-ee/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-ee"
            const publishSeries = "monitor"
            const publishUrl = "en-ee/monitor"
            const publishUrlName = "en-ee/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CeneeHB27.push(url)
            }
    })
    return wrongnameB2CeneeHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENEU-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CeneuHB27=[]
const wrongnameB2CeneuURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-eu/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-eu"
            const publishSeries = "monitor"
            const publishUrl = "en-eu/monitor"
            const publishUrlName = "en-eu/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CeneuHB27.push(url)
            }
    })
    return wrongnameB2CeneuHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENFI-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CenfiHB27=[]
const wrongnameB2CenfiURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-fi/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-fi"
            const publishSeries = "monitor"
            const publishUrl = "en-fi/monitor"
            const publishUrlName = "en-fi/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenfiHB27.push(url)
            }
    })
    return wrongnameB2CenfiHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENHK-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CenhkHB27=[]
const wrongnameB2CenhkURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-hk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-hk"
            const publishSeries = "monitor"
            const publishUrl = "en-hk/monitor"
            const publishUrlName = "en-hk/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenhkHB27.push(url)
            }
    })
    return wrongnameB2CenhkHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENHR-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CenhrHB27=[]
const wrongnameB2CenhrURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-hr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-hr"
            const publishSeries = "monitor"
            const publishUrl = "en-hr/monitor"
            const publishUrlName = "en-hr/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenhrHB27.push(url)
            }
    })
    return wrongnameB2CenhrHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENIE-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CenieHB27=[]
const wrongnameB2CenieURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-ie/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-ie"
            const publishSeries = "monitor"
            const publishUrl = "en-ie/monitor"
            const publishUrlName = "en-ie/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenieHB27.push(url)
            }
    })
    return wrongnameB2CenieHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENIN-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CeninHB27=[]
const wrongnameB2CeninURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-in/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-in"
            const publishSeries = "monitor"
            const publishUrl = "en-in/monitor"
            const publishUrlName = "en-in/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CeninHB27.push(url)
            }
    })
    return wrongnameB2CeninHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENIS-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CenisHB27=[]
const wrongnameB2CenisURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-is/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-is"
            const publishSeries = "monitor"
            const publishUrl = "en-is/monitor"
            const publishUrlName = "en-is/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenisHB27.push(url)
            }
    })
    return wrongnameB2CenisHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENLU-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CenluHB27=[]
const wrongnameB2CenluURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-lu/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-lu"
            const publishSeries = "monitor"
            const publishUrl = "en-lu/monitor"
            const publishUrlName = "en-lu/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenluHB27.push(url)
            }
    })
    return wrongnameB2CenluHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENLV-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CenlvHB27=[]
const wrongnameB2CenlvURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-lv/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-lv"
            const publishSeries = "monitor"
            const publishUrl = "en-lv/monitor"
            const publishUrlName = "en-lv/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenlvHB27.push(url)
            }
    })
    return wrongnameB2CenlvHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENME-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CenmeHB27=[]
const wrongnameB2CenmeURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-me/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-me"
            const publishSeries = "monitor"
            const publishUrl = "en-me/monitor"
            const publishUrlName = "en-me/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenmeHB27.push(url)
            }
    })
    return wrongnameB2CenmeHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENMK-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CenmkHB27=[]
const wrongnameB2CenmkURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-mk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-mk"
            const publishSeries = "monitor"
            const publishUrl = "en-mk/monitor"
            const publishUrlName = "en-mk/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenmkHB27.push(url)
            }
    })
    return wrongnameB2CenmkHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENMT-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CenmtHB27=[]
const wrongnameB2CenmtURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-mt/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-mt"
            const publishSeries = "monitor"
            const publishUrl = "en-mt/monitor"
            const publishUrlName = "en-mt/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenmtHB27.push(url)
            }
    })
    return wrongnameB2CenmtHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENMY-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CenmyHB27=[]
const wrongnameB2CenmyURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-my/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-my"
            const publishSeries = "monitor"
            const publishUrl = "en-my/monitor"
            const publishUrlName = "en-my/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenmyHB27.push(url)
            }
    })
    return wrongnameB2CenmyHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENNO-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CennoHB27=[]
const wrongnameB2CennoURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-no/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-no"
            const publishSeries = "monitor"
            const publishUrl = "en-no/monitor"
            const publishUrlName = "en-no/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CennoHB27.push(url)
            }
    })
    return wrongnameB2CennoHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENRS-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CenrsHB27=[]
const wrongnameB2CenrsURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-rs/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-rs"
            const publishSeries = "monitor"
            const publishUrl = "en-rs/monitor"
            const publishUrlName = "en-rs/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenrsHB27.push(url)
            }
    })
    return wrongnameB2CenrsHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENSG-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CensgHB27=[]
const wrongnameB2CensgURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-sg/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-sg"
            const publishSeries = "monitor"
            const publishUrl = "en-sg/monitor"
            const publishUrlName = "en-sg/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CensgHB27.push(url)
            }
    })
    return wrongnameB2CensgHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENSI-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CensiHB27=[]
const wrongnameB2CensiURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-si/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-si"
            const publishSeries = "monitor"
            const publishUrl = "en-si/monitor"
            const publishUrlName = "en-si/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CensiHB27.push(url)
            }
    })
    return wrongnameB2CensiHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENUK-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CenukHB27=[]
const wrongnameB2CenukURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-uk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-uk"
            const publishSeries = "monitor"
            const publishUrl = "en-uk/monitor"
            const publishUrlName = "en-uk/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenukHB27.push(url)
            }
    })
    return wrongnameB2CenukHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENUS-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CenusHB27=[]
const wrongnameB2CenusURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-us/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-us"
            const publishSeries = "monitor"
            const publishUrl = "en-us/monitor"
            const publishUrlName = "en-us/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenusHB27.push(url)
            }
    })
    return wrongnameB2CenusHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESAR-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CesarHB27=[]
const wrongnameB2CesarURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-ar/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-ar"
            const publishSeries = "monitor"
            const publishUrl = "es-ar/monitor"
            const publishUrlName = "es-ar/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CesarHB27.push(url)
            }
    })
    return wrongnameB2CesarHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESCO-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CescoHB27=[]
const wrongnameB2CescoURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-co/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-co"
            const publishSeries = "monitor"
            const publishUrl = "es-co/monitor"
            const publishUrlName = "es-co/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CescoHB27.push(url)
            }
    })
    return wrongnameB2CescoHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESES-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CesesHB27=[]
const wrongnameB2CesesURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-es/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-es"
            const publishSeries = "monitor"
            const publishUrl = "es-es/monitor"
            const publishUrlName = "es-es/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CesesHB27.push(url)
            }
    })
    return wrongnameB2CesesHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESLA-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CeslaHB27=[]
const wrongnameB2CeslaURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/es-la/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-la"
            const publishSeries = "monitor"
            const publishUrl = "es-la/monitor"
            const publishUrlName = "es-la/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CeslaHB27.push(url)
            }
    })
    return wrongnameB2CeslaHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESMX-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CesmxHB27=[]
const wrongnameB2CesmxURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/es-mx/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-mx"
            const publishSeries ="monitor"
            const publishUrl = "es-mx/monitor"
            const publishUrlName = "es-mx/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CesmxHB27.push(url)
            }
    })
    return wrongnameB2CesmxHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESPE-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CespeHB27=[]
const wrongnameB2CespeURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-pe/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-pe"
            const publishSeries = "monitor"
            const publishUrl = "es-pe/monitor"
            const publishUrlName = "es-pe/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CespeHB27.push(url)
            }
    })
    return wrongnameB2CespeHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-FRCA-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CfrcaHB27=[]
const wrongnameB2CfrcaURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/fr-ca/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "fr-ca"
            const publishSeries = "monitor"
            const publishUrl = "fr-ca/monitor"
            const publishUrlName = "fr-ca/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CfrcaHB27.push(url)
            }
    })
    return wrongnameB2CfrcaHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-FRCH-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CfrchHB27=[]
const wrongnameB2CfrchURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/fr-ch/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "fr-ch"
            const publishSeries = "monitor"
            const publishUrl = "fr-ch/monitor"
            const publishUrlName = "fr-ch/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CfrchHB27.push(url)
            }
    })
    return wrongnameB2CfrchHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-FRFR-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CfrfrHB27=[]
const wrongnameB2CfrfrURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/fr-fr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "fr-fr"
            const publishSeries = "monitor"
            const publishUrl = "fr-fr/monitor"
            const publishUrlName = "fr-fr/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CfrfrHB27.push(url)
            }
    })
    return wrongnameB2CfrfrHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-HUHU-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2ChuhuHB27=[]
const wrongnameB2ChuhuURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/hu-hu/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "hu-hu"
            const publishSeries = "monitor"
            const publishUrl = "hu-hu/monitor"
            const publishUrlName = "hu-hu/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2ChuhuHB27.push(url)
            }
    })
    return wrongnameB2ChuhuHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-IDID-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CididHB27=[]
const wrongnameB2CididURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/id-id/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "id-id"
            const publishSeries = "monitor"
            const publishUrl = "id-id/monitor"
            const publishUrlName = "id-id/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CididHB27.push(url)
            }
    })
    return wrongnameB2CididHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ITIT-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CititHB27=[]
const wrongnameB2CititURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/it-it/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "it-it"
            const publishSeries = "monitor"
            const publishUrl = "it-it/monitor"
            const publishUrlName = "it-it/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CititHB27.push(url)
            }
    })
    return wrongnameB2CititHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-JAJP-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CjajpHB27=[]
const wrongnameB2CjajpURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ja-jp/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ja-jp"
            const publishSeries = "monitor"
            const publishUrl = "ja-jp/monitor"
            const publishUrlName = "ja-jp/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CjajpHB27.push(url)
            }
    })
    return wrongnameB2CjajpHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-KOKR-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CkokrHB27=[]
const wrongnameB2CkokrURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ko-kr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ko-kr"
            const publishSeries = "monitor"
            const publishUrl = "ko-kr/monitor"
            const publishUrlName = "ko-kr/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CkokrHB27.push(url)
            }
    })
    return wrongnameB2CkokrHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-LTLT-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CltltHB27=[]
const wrongnameB2CltltURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/lt-lt/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "lt-lt"
            const publishSeries = "monitor"
            const publishUrl = "lt-lt/monitor"
            const publishUrlName = "lt-lt/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CltltHB27.push(url)
            }
    })
    return wrongnameB2CltltHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-NLBE-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CnlbeHB27=[]
const wrongnameB2CnlbeURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/nl-be/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "nl-be"
            const publishSeries = "monitor"
            const publishUrl = "nl-be/monitor"
            const publishUrlName = "nl-be/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CnlbeHB27.push(url)
            }
    })
    return wrongnameB2CnlbeHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-NLNL-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CnlnlHB27=[]
const wrongnameB2CnlnlURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/nl-nl/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "nl-nl"
            const publishSeries = "monitor"
            const publishUrl = "nl-nl/monitor"
            const publishUrlName = "nl-nl/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CnlnlHB27.push(url)
            }
    })
    return wrongnameB2CnlnlHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-PLPL-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CplplHB27=[]
const wrongnameB2CplplURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/pl-pl/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "pl-pl"
            const publishSeries = "monitor"
            const publishUrl = "pl-pl/monitor"
            const publishUrlName = "pl-pl/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CplplHB27.push(url)
            }
    })
    return wrongnameB2CplplHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-PTBR-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CptbrHB27=[]
const wrongnameB2CptbrURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/pt-br/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "pt-br"
            const publishSeries = "monitor"
            const publishUrl = "pt-br/monitor"
            const publishUrlName = "pt-br/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CptbrHB27.push(url)
            }
    })
    return wrongnameB2CptbrHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-PTPT-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CptptHB27=[]
const wrongnameB2CptptURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/pt-pt/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "pt-pt"
            const publishSeries = "monitor"
            const publishUrl = "pt-pt/monitor"
            const publishUrlName = "pt-pt/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CptptHB27.push(url)
            }
    })
    return wrongnameB2CptptHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-RORO-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CroroHB27=[]
const wrongnameB2CroroURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/ro-ro/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ro-ro"
            const publishSeries = "monitor"
            const publishUrl = "ro-ro/monitor"
            const publishUrlName = "ro-ro/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CroroHB27.push(url)
            }
    })
    return wrongnameB2CroroHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-RURU-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CruruHB27=[]
const wrongnameB2CruruURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ru-ru/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ru-ru"
            const publishSeries = "monitor"
            const publishUrl = "ru-ru/monitor"
            const publishUrlName = "ru-ru/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CruruHB27.push(url)
            }
    })
    return wrongnameB2CruruHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-SKSK-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CskskHB27=[]
const wrongnameB2CskskURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/sk-sk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "sk-sk"
            const publishSeries = "monitor"
            const publishUrl = "sk-sk/monitor"
            const publishUrlName = "sk-sk/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CskskHB27.push(url)
            }
    })
    return wrongnameB2CskskHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-SVSE-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CsvseHB27=[]
const wrongnameB2CsvseURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/sv-se/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "sv-se"
            const publishSeries = "monitor"
            const publishUrl = "sv-se/monitor"
            const publishUrlName = "sv-se/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CsvseHB27.push(url)
            }
    })
    return wrongnameB2CsvseHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-THTH-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CththHB27=[]
const wrongnameB2CththURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/th-th/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "th-th"
            const publishSeries = "monitor"
            const publishUrl = "th-th/monitor"
            const publishUrlName = "th-th/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CththHB27.push(url)
            }
    })
    return wrongnameB2CththHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-TRTR-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CtrtrHB27=[]
const wrongnameB2CtrtrURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/tr-tr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "tr-tr"
            const publishSeries = "monitor"
            const publishUrl = "tr-tr/monitor"
            const publishUrlName = "tr-tr/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CtrtrHB27.push(url)
            }
    })
    return wrongnameB2CtrtrHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-UKUA-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CukuaHB27=[]
const wrongnameB2CukuaURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/uk-ua/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "uk-ua"
            const publishSeries = "monitor"
            const publishUrl = "uk-ua/monitor"
            const publishUrlName = "uk-ua/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CukuaHB27.push(url)
            }
    })
    return wrongnameB2CukuaHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-VIVN-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CvivnHB27=[]
const wrongnameB2CvivnURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/vi-vn/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "vi-vn"
            const publishSeries = "monitor"
            const publishUrl = "vi-vn/monitor"
            const publishUrlName = "vi-vn/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CvivnHB27.push(url)
            }
    })
    return wrongnameB2CvivnHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ZHHK-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CzhhkHB27=[]
const wrongnameB2CzhhkURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/zh-hk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "zh-hk"
            const publishSeries = "monitor"
            const publishUrl = "zh-hk/monitor"
            const publishUrlName = "zh-hk/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CzhhkHB27.push(url)
            }
    })
    return wrongnameB2CzhhkHB27;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ZHTW-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
const wrongnameB2CzhtwHB27=[]
const wrongnameB2CzhtwURLHB27 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/zh-tw/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "zh-tw"
            const publishSeries = "monitor"
            const publishUrl = "zh-tw/monitor"
            const publishUrlName = "zh-tw/monitor/accessory"
            const publishProductSeries = "accessory"
            const publishProductModel = "hb27.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CzhtwHB27.push(url)
            }
    })
    return wrongnameB2CzhtwHB27;
    } catch (error) {
      console.log(error);
    }
};
//Test case
Given("HB27 must be published on global site after 20220506",{timeout: 1000 * 5000},async function(){
    //After 2022/01/01
    //global setting
    const publishModel = "hb27"
    const date = new Date()
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()
    function wholeMonth(){
        const getmonth = date.getMonth() + 1
        if(getmonth<10){
            wholeMonth =  "0"+getmonth 
            return wholeMonth
        }else{
            wholeMonth = getmonth 
            return wholeMonth
        }
    }
    function wholeDate(){
        const getDay = date.getDate()
        if(getDay<10){
            wholeDate =  "0"+getDay
            return wholeDate
        }else{
            wholeDate = getDay 
            return wholeDate
        }
    }
    const wholemonth = wholeMonth()
    const wholeday = wholeDate()
    const wholeyear = date.getFullYear()
    const fullDate = `${wholeyear}/${wholemonth}/${wholeday}`
    // console.log("wholemonth:",wholemonth)
    // console.log("wholeday:",wholeday)
    // console.log("wholeyear:",wholeyear)
    console.log("fullDate:",fullDate)
    // console.log("year:",year)
    // console.log("month:",month)
    // console.log("day:",day)
    const launchDate = "2022/04/01"

    //B2C-ARME-Professional Monitor accessory-HB27-after 20220506
    const armeReturnedData = await afterB2CarmeURLHB27();
    // console.log("After returnedData",armeReturnedData)
    const armepublishCheck = armeReturnedData.length
    console.log("ar-me All publish URL-B2C Professional Monitor accessory - HB27:",armeReturnedData)
    console.log(armeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(armepublishCheck ==0){
            // afterB2CarmeHB27.push("ar-me")
            HB27PublishError.push("ar-me")

        }
    }
    //B2C-BGBG-Professional Monitor accessory-HB27-after 20220506
    const bgbgReturnedData = await afterB2CbgbgURLHB27();
    // console.log("After returnedData",bgbgReturnedData)
    const bgbgpublishCheck = bgbgReturnedData.length
    console.log("bg-bg All publish URL-B2C Professional Monitor accessory-HB27:",bgbgReturnedData)
    console.log(bgbgReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(bgbgpublishCheck ==0){
            // afterB2CbgbgHB27.push("bg-bg")
            HB27PublishError.push("bg-bg")

        }
    }
    //B2C-CSCZ-Professional Monitor accessory-HB27-after 20220506
    const csczReturnedData = await afterB2CcsczURLHB27();
    // console.log("After returnedData",csczReturnedData)
    const csczpublishCheck = csczReturnedData.length
    console.log("cs-cz All publish URL-B2C Professional Monitor accessory-HB27:",csczReturnedData)
    console.log(csczReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(csczpublishCheck ==0){
            // afterB2CcsczHB27.push("cs-cz")
            HB27PublishError.push("cs-cz")

        }
    }
    //B2C-DEAT-Professional Monitor accessory-HB27-after 20220506
    const deatReturnedData = await afterB2CdeatURLHB27();
    // console.log("After returnedData",deatReturnedData)
    const deatpublishCheck = deatReturnedData.length
    console.log("de-at All publish URL-B2C Professional Monitor accessory-HB27:",deatReturnedData)
    console.log(deatReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(deatpublishCheck ==0){
            // afterB2Cdea-HB27.push("de-at")
        HB27PublishError.push("de-at")

        }
    }
    //B2C-DECH-Professional Monitor accessory-HB27-after 20220506
    const dechReturnedData = await afterB2CdechURLHB27();
    // console.log("After returnedData",dechReturnedData)
    const dechpublishCheck = dechReturnedData.length
    console.log("de-ch All publish URL-B2C Professional Monitor accessory-HB27:",dechReturnedData)
    console.log(dechReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(dechpublishCheck ==0){
            // afterB2CdechHB27.push("de-ch")
            HB27PublishError.push("de-ch")

        }
    }
    //B2C-DEDE-Professional Monitor accessory-HB27-after 20220506
    const dedeReturnedData = await afterB2CdedeURLHB27();
    // console.log("After returnedData",dedeReturnedData)
    const dedepublishCheck = dedeReturnedData.length
    console.log("de-de All publish URL-B2C Professional Monitor accessory-HB27:",dedeReturnedData)
    console.log(dedeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(dedepublishCheck ==0){
            // afterB2CdedeHB27.push("de-de")
            HB27PublishError.push("de-de")

        }
    }
    //B2C-ELGR-Professional Monitor accessory-HB27-after 20220506
    const elgrReturnedData = await afterB2CelgrURLHB27();
    // console.log("After returnedData",elgrReturnedData)
    const elgrpublishCheck = elgrReturnedData.length
    console.log("el-gr All publish URL-B2C Professional Monitor accessory-HB27:",elgrReturnedData)
    console.log(elgrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(elgrpublishCheck ==0){
            // afterB2CelgrHB27.push("el-gr")
            HB27PublishError.push("el-gr")

        }
    }
    //B2C-ENAP-Professional Monitor accessory-HB27-after 20220506
    const enapReturnedData = await afterB2CenapURLHB27();
    // console.log("After returnedData",enapReturnedData)
    const enappublishCheck = enapReturnedData.length
    console.log("en-ap All publish URL-B2C Professional Monitor accessory - HB27:",enapReturnedData)
    console.log(enapReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(enappublishCheck ==0){
            // afterB2CenapHB27.push("en-ap")
            HB27PublishError.push("en-ap")

        }
    }
    //B2C-ENAU-Professional Monitor accessory-HB27-after 20220506
    const enauReturnedData = await afterB2CenauURLHB27();
    // console.log("After returnedData",enauReturnedData)
    const enaupublishCheck = enauReturnedData.length
    console.log("en-au All publish URL-B2C Professional Monitor accessory - HB27:",enauReturnedData)
    console.log(enauReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(enaupublishCheck ==0){
            // afterB2CenauHB27.push("en-au")
            HB27PublishError.push("en-au")

        }
    }
    //B2C-ENBA-Professional Monitor accessory-HB27-after 20220506
    const enbaReturnedData = await afterB2CenbaURLHB27();
    // console.log("After returnedData",enbaReturnedData)
    const enbapublishCheck = enbaReturnedData.length
    console.log("en-ba All publish URL-B2C Professional Monitor accessory - HB27:",enbaReturnedData)
    console.log(enbaReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(enbapublishCheck ==0){
            // afterB2CenbaHB27.push("en-ba")
            HB27PublishError.push("en-ba")

        }
    }
    //B2C-ENCA-Professional Monitor accessory-HB27-after 20220506
    const encaReturnedData = await afterB2CencaURLHB27();
    // console.log("After returnedData",encaReturnedData)
    const encapublishCheck = encaReturnedData.length
    console.log("en-ca All publish URL-B2C Professional Monitor accessory - HB27:",encaReturnedData)
    console.log(encaReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(encapublishCheck ==0){
            // afterB2CencaHB27.push("en-ca")
            HB27PublishError.push("en-ca")
        }
    }
    //B2C-ENCEE-Professional Monitor accessory-HB27-after 20220506
    const enceeReturnedData = await afterB2CenceeURLHB27();
    // console.log("After returnedData",enceeReturnedData)
    const enceepublishCheck = enceeReturnedData.length
    console.log("en-cee All publish URL-B2C Professional Monitor accessory - HB27:",enceeReturnedData)
    console.log(enceeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(enceepublishCheck ==0){
            // afterB2CencaHB27.push("en-cee")
            HB27PublishError.push("en-cee")
        }
    }
    //B2C-ENCY-Professional Monitor accessory-HB27-after 20220506
    const encyReturnedData = await afterB2CencyURLHB27();
    // console.log("After returnedData",encyReturnedData)
    const encypublishCheck = encyReturnedData.length
    console.log("en-cy All publish URL-B2C Professional Monitor accessory - HB27:",encyReturnedData)
    console.log(encyReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(encypublishCheck ==0){
            // afterB2CencaHB27.push("en-cy")
            HB27PublishError.push("en-cy")
        }
    }
    //B2C-ENDK-Professional Monitor accessory-HB27-after 20220506
    const endkReturnedData = await afterB2CendkURLHB27();
    // console.log("After returnedData",endkReturnedData)
    const endkpublishCheck = endkReturnedData.length
    console.log("en-dk All publish URL-B2C Professional Monitor accessory - HB27:",endkReturnedData)
    console.log(endkReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(endkpublishCheck ==0){
            // afterB2CencaHB27.push("en-dk")
            HB27PublishError.push("en-dk")
        }
    }
    //B2C-ENEE-Professional Monitor accessory-HB27-after 20220506
    const eneeReturnedData = await afterB2CeneeURLHB27();
    // console.log("After returnedData",eneeReturnedData)
    const eneepublishCheck = eneeReturnedData.length
    console.log("en-ee All publish URL-B2C Professional Monitor accessory-HB27:",eneeReturnedData)
    console.log(eneeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(eneepublishCheck ==0){
            HB27PublishError.push("en-ee")
        }
    }
    //B2C-ENEU-Professional Monitor accessory-HB27-after 20220506
    const eneuReturnedData = await afterB2CeneuURLHB27();
    // console.log("After returnedData",eneuReturnedData)
    const eneupublishCheck = eneuReturnedData.length
    console.log("en-eu All publish URL-B2C Professional Monitor accessory-HB27:",eneuReturnedData)
    console.log(eneuReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(eneupublishCheck ==0){
            HB27PublishError.push("en-eu")
        }
    }
    //B2C-ENFI-Professional Monitor accessory-HB27-after 20220506
    const enfiReturnedData = await afterB2CenfiURLHB27();
    // console.log("After returnedData",enfiReturnedData)
    const enfipublishCheck = enfiReturnedData.length
    console.log("en-fi All publish URL-B2C Professional Monitor accessory-HB27:",enfiReturnedData)
    console.log(enfiReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(enfipublishCheck ==0){
            HB27PublishError.push("en-fi")
        }
    }
    //B2C-ENHK-Professional Monitor accessory-HB27-after 20220506
    const enhkReturnedData = await afterB2CenhkURLHB27();
    // console.log("After returnedData",enhkReturnedData)
    const enhkpublishCheck = enhkReturnedData.length
    console.log("en-hk All publish URL-B2C Professional Monitor accessory - HB27:",enhkReturnedData)
    console.log(enhkReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(enhkpublishCheck ==0){
            HB27PublishError.push("en-hk")
        }
    }
    //B2C-ENHR-Professional Monitor accessory-HB27-after 20220506
    const enhrReturnedData = await afterB2CenhrURLHB27();
    // console.log("After returnedData",enhrReturnedData)
    const enhrpublishCheck = enhrReturnedData.length
    console.log("en-hr All publish URL-B2C Professional Monitor accessory-HB27:",enhrReturnedData)
    console.log(enhrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(enhrpublishCheck ==0){
            HB27PublishError.push("en-hr")
        }
    }
    //B2C-ENIE-Professional Monitor accessory-HB27-after 20220506
    const enieReturnedData = await afterB2CenieURLHB27();
    // console.log("After returnedData",enieReturnedData)
    const eniepublishCheck = enieReturnedData.length
    console.log("en-ie All publish URL-B2C Professional Monitor accessory-HB27:",enieReturnedData)
    console.log(enieReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(eniepublishCheck ==0){
            HB27PublishError.push("en-ie")
        }
    }
    //B2C-ENIN-Professional Monitor accessory-HB27-after 20220506
    const eninReturnedData = await afterB2CeninURLHB27();
    // console.log("After returnedData",eninReturnedData)
    const eninpublishCheck = eninReturnedData.length
    console.log("en-in All publish URL-B2C Professional Monitor accessory - HB27:",eninReturnedData)
    console.log(eninReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(eninpublishCheck ==0){
            HB27PublishError.push("en-in")
        }
    }
    //B2C-ENIS-Professional Monitor accessory-HB27-after 20220506
    const enisReturnedData = await afterB2CenisURLHB27();
    // console.log("After returnedData",enisReturnedData)
    const enispublishCheck = enisReturnedData.length
    console.log("en-is All publish URL-B2C Professional Monitor accessory-HB27:",enisReturnedData)
    console.log(enisReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(enispublishCheck ==0){
            HB27PublishError.push("en-is")
        }
    }
    //B2C-ENLU-Professional Monitor accessory-HB27-after 20220506
    const enluReturnedData = await afterB2CenluURLHB27();
    // console.log("After returnedData",enluReturnedData)
    const enlupublishCheck = enluReturnedData.length
    console.log("en-lu All publish URL-B2C Professional Monitor accessory-HB27:",enluReturnedData)
    console.log(enluReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(enlupublishCheck ==0){
            HB27PublishError.push("en-lu")
        }
    }
    //B2C-ENLV-Professional Monitor accessory-HB27-after 20220506
    const enlvReturnedData = await afterB2CenlvURLHB27();
    // console.log("After returnedData",enlvReturnedData)
    const enlvpublishCheck = enlvReturnedData.length
    console.log("en-lv All publish URL-B2C Professional Monitor accessory-HB27:",enlvReturnedData)
    console.log(enlvReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(enlvpublishCheck ==0){
            HB27PublishError.push("en-lv")
        }
    }
    //B2C-ENME-Professional Monitor accessory-HB27-after 20220506
    const enmeReturnedData = await afterB2CenmeURLHB27();
    // console.log("After returnedData",enmeReturnedData)
    const enmepublishCheck = enmeReturnedData.length
    console.log("en-me All publish URL-B2C Professional Monitor accessory - HB27:",enmeReturnedData)
    console.log(enmeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(enmepublishCheck ==0){
            HB27PublishError.push("en-me")
        }
    }
    //B2C-ENMK-Professional Monitor accessory-HB27-after 20220506
    const enmkReturnedData = await afterB2CenmkURLHB27();
    // console.log("After returnedData",enmkReturnedData)
    const enmkpublishCheck = enmkReturnedData.length
    console.log("en-mk All publish URL-B2C Professional Monitor accessory-HB27:",enmkReturnedData)
    console.log(enmkReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(enmkpublishCheck ==0){
            HB27PublishError.push("en-mk")
        }
    }
    //B2C-ENMT-Professional Monitor accessory-HB27-after 20220506
    const enmtReturnedData = await afterB2CenmtURLHB27();
    // console.log("After returnedData",enmtReturnedData)
    const enmtpublishCheck = enmtReturnedData.length
    console.log("en-mt All publish URL-B2C Professional Monitor accessory-HB27:",enmtReturnedData)
    console.log(enmtReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(enmtpublishCheck ==0){
            HB27PublishError.push("en-mt")
        }
    }
    //B2C-ENMY-Professional Monitor accessory-HB27-after 20220506
    const enmyReturnedData = await afterB2CenmyURLHB27();
    // console.log("After returnedData",enmyReturnedData)
    const enmypublishCheck = enmyReturnedData.length
    console.log("en-my All publish URL-B2C Professional Monitor accessory - HB27:",enmyReturnedData)
    console.log(enmyReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(enmypublishCheck ==0){
            HB27PublishError.push("en-my")
        }
    }
    //B2C-ENNO-Professional Monitor accessory-HB27-after 20220506
    const ennoReturnedData = await afterB2CennoURLHB27();
    // console.log("After returnedData",ennoReturnedData)
    const ennopublishCheck = ennoReturnedData.length
    console.log("en-no All publish URL-B2C Professional Monitor accessory-HB27:",ennoReturnedData)
    console.log(ennoReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(ennopublishCheck ==0){
            HB27PublishError.push("en-no")
        }
    }
    //B2C-ENRS-Professional Monitor accessory-HB27-after 20220506
    const enrsReturnedData = await afterB2CenrsURLHB27();
    // console.log("After returnedData",enrsReturnedData)
    const enrspublishCheck = enrsReturnedData.length
    console.log("en-rs All publish URL-B2C Professional Monitor accessory-HB27:",enrsReturnedData)
    console.log(enrsReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(enrspublishCheck ==0){
            HB27PublishError.push("en-rs")
        }
    }
    //B2C-ENSG-Professional Monitor accessory-HB27-after 20220506
    const ensgReturnedData = await afterB2CensgURLHB27();
    // console.log("After returnedData",ensgReturnedData)
    const ensgpublishCheck = ensgReturnedData.length
    console.log("en-sg All publish URL-B2C Professional Monitor accessory - HB27:",ensgReturnedData)
    console.log(ensgReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(ensgpublishCheck ==0){
            HB27PublishError.push("en-sg")
        }
    }
    //B2C-ENSI-Professional Monitor accessory-HB27-after 20220506
    const ensiReturnedData = await afterB2CensiURLHB27();
    // console.log("After returnedData",ensiReturnedData)
    const ensipublishCheck = ensiReturnedData.length
    console.log("en-si All publish URL-B2C Professional Monitor accessory-HB27:",ensiReturnedData)
    console.log(ensiReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(ensipublishCheck ==0){
            HB27PublishError.push("en-si")
        }
    }
    //B2C-ENUK-Professional Monitor accessory-HB27-after 20220506
    const enukReturnedData = await afterB2CenukURLHB27();
    // console.log("After returnedData",enukReturnedData)
    const enukpublishCheck = enukReturnedData.length
    console.log("en-uk All publish URL-B2C Professional Monitor accessory-HB27:",enukReturnedData)
    console.log(enukReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(enukpublishCheck ==0){
            HB27PublishError.push("en-uk")
        }
    }
    //B2C-ENUS-Professional Monitor accessory-HB27-after 20220506
    const enusReturnedData = await afterB2CenusURLHB27();
    // console.log("After returnedData",enusReturnedData)
    const enuspublishCheck = enusReturnedData.length
    console.log("en-us All publish URL-B2C Professional Monitor accessory - HB27:",enusReturnedData)
    console.log(enusReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(enuspublishCheck ==0){
            HB27PublishError.push("en-us")
        }
    }
    //B2C-ESAR-Professional Monitor accessory-HB27-after 20220506
    const esarReturnedData = await afterB2CesarURLHB27();
    // console.log("After returnedData",esarReturnedData)
    const esarpublishCheck = esarReturnedData.length
    console.log("es-ar All publish URL-B2C Professional Monitor accessory - HB27:",esarReturnedData)
    console.log(esarReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(esarpublishCheck ==0){
            HB27PublishError.push("es-ar")
        }
    }
    //B2C-ESCO-Professional Monitor accessory-HB27-after 20220506
    const escoReturnedData = await afterB2CescoURLHB27();
    // console.log("After returnedData",escoReturnedData)
    const escopublishCheck = escoReturnedData.length
    console.log("es-co All publish URL-B2C Professional Monitor accessory - HB27:",escoReturnedData)
    console.log(escoReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(escopublishCheck ==0){
            HB27PublishError.push("es-co")
        }
    }
    //B2C-ESES-Professional Monitor accessory-HB27-after 20220506
    const esesReturnedData = await afterB2CesesURLHB27();
    // console.log("After returnedData",esesReturnedData)
    const esespublishCheck = esesReturnedData.length
    console.log("es-es All publish URL-B2C Professional Monitor accessory - HB27:",esesReturnedData)
    console.log(esesReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(esespublishCheck ==0){
            HB27PublishError.push("es-es")
        }
    }
    //B2C-ESLA-Professional Monitor accessory-HB27-after 20220506
    const eslaReturnedData = await afterB2CeslaURLHB27();
    // console.log("After returnedData",eslaReturnedData)
    const eslapublishCheck = eslaReturnedData.length
    console.log("es-la All publish URL-B2C Professional Monitor accessory - HB27:",eslaReturnedData)
    console.log(eslaReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(eslapublishCheck ==0){
            HB27PublishError.push("es-la")
        }
    }
    //B2C-ESMX-Professional Monitor accessory-HB27-after 20220506
    const esmxReturnedData = await afterB2CesmxURLHB27();
    // console.log("After returnedData",esmxReturnedData)
    const esmxpublishCheck = esmxReturnedData.length
    console.log("es-mx All publish URL-B2C Professional Monitor accessory - HB27:",esmxReturnedData)
    console.log(esmxReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(esmxpublishCheck ==0){
            HB27PublishError.push("es-mx")
        }
    }
    //B2C-ESPE-Professional Monitor accessory-HB27-after 20220506
    const espeReturnedData = await afterB2CespeURLHB27();
    // console.log("After returnedData",espeReturnedData)
    const espepublishCheck = espeReturnedData.length
    console.log("es-pe All publish URL-B2C Professional Monitor accessory - HB27:",espeReturnedData)
    console.log(espeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(espepublishCheck ==0){
            HB27PublishError.push("es-pe")
        }
    }
    //B2C-FRCA-Professional Monitor accessory-HB27-after 20220506
    const frcaReturnedData = await afterB2CfrcaURLHB27();
    // console.log("After returnedData",frcaReturnedData)
    const frcapublishCheck = frcaReturnedData.length
    console.log("fr-ca All publish URL-B2C Professional Monitor accessory - HB27:",frcaReturnedData)
    console.log(frcaReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(frcapublishCheck ==0){
            HB27PublishError.push("fr-ca")
        }
    }
    //B2C-FRCH-Professional Monitor accessory-HB27-after 20220506
    const frchReturnedData = await afterB2CfrchURLHB27();
    // console.log("After returnedData",frchReturnedData)
    const frchpublishCheck = frchReturnedData.length
    console.log("fr-ch All publish URL-B2C Professional Monitor accessory - HB27:",frchReturnedData)
    console.log(frchReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(frchpublishCheck ==0){
            HB27PublishError.push("fr-ch")
        }
    }
    //B2C-FRFR-Professional Monitor accessory-HB27-after 20220506
    const frfrReturnedData = await afterB2CfrfrURLHB27();
    // console.log("After returnedData",frfrReturnedData)
    const frfrpublishCheck = frfrReturnedData.length
    console.log("fr-fr All publish URL-B2C Professional Monitor accessory - HB27:",frfrReturnedData)
    console.log(frfrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(frfrpublishCheck ==0){
            HB27PublishError.push("fr-fr")
        }
    }
    //B2C-HUHU-Professional Monitor accessory-HB27-after 20220506
    const huhuReturnedData = await afterB2ChuhuURLHB27();
    // console.log("After returnedData",huhuReturnedData)
    const huhupublishCheck = huhuReturnedData.length
    console.log("hu-hu All publish URL-B2C Professional Monitor accessory - HB27:",huhuReturnedData)
    console.log(huhuReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(huhupublishCheck ==0){
            HB27PublishError.push("hu-hu")
        }
    }
    //B2C-IDID-Professional Monitor accessory-HB27-after 20220506
    const ididReturnedData = await afterB2CididURLHB27();
    // console.log("After returnedData",ididReturnedData)
    const ididpublishCheck = ididReturnedData.length
    console.log("id-id All publish URL-B2C Professional Monitor accessory - HB27:",ididReturnedData)
    console.log(ididReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(ididpublishCheck ==0){
            HB27PublishError.push("id-id")
        }
    }
    //B2C-ITIT-Professional Monitor accessory-HB27-after 20220506
    const ititReturnedData = await afterB2CititURLHB27();
    // console.log("After returnedData",ititReturnedData)
    const ititpublishCheck = ititReturnedData.length
    console.log("it-it All publish URL-B2C Professional Monitor accessory - HB27:",ititReturnedData)
    console.log(ititReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(ititpublishCheck ==0){
            HB27PublishError.push("it-it")
        }
    }
    //B2C-JAJP-Professional Monitor accessory-HB27-after 20220506
    const jajpReturnedData = await afterB2CjajpURLHB27();
    // console.log("After returnedData",jajpReturnedData)
    const jajppublishCheck = jajpReturnedData.length
    console.log("ja-jp All publish URL-B2C Professional Monitor accessory - HB27:",jajpReturnedData)
    console.log(jajpReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(jajppublishCheck ==0){
            HB27PublishError.push("ja-jp")
        }
    }
    //B2C-KOKR-Professional Monitor accessory-HB27-after 20220506
    const kokrReturnedData = await afterB2CkokrURLHB27();
    // console.log("After returnedData",kokrReturnedData)
    const kokrpublishCheck = kokrReturnedData.length
    console.log("ko-kr All publish URL-B2C Professional Monitor accessory - HB27:",kokrReturnedData)
    console.log(kokrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(kokrpublishCheck ==0){
            HB27PublishError.push("ko-kr")
        }
    }
    //B2C-LTLT-Professional Monitor accessory-HB27-after 20220506
    const ltltReturnedData = await afterB2CltltURLHB27();
    // console.log("After returnedData",ltltReturnedData)
    const ltltpublishCheck = ltltReturnedData.length
    console.log("lt-lt All publish URL-B2C Professional Monitor accessory - HB27:",ltltReturnedData)
    console.log(ltltReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(ltltpublishCheck ==0){
            HB27PublishError.push("lt-lt")
        }
    }
    //B2C-NLBE-Professional Monitor accessory-HB27-after 20220506
    const nlbeReturnedData = await afterB2CnlbeURLHB27();
    // console.log("After returnedData",nlbeReturnedData)
    const nlbepublishCheck = nlbeReturnedData.length
    console.log("nl-be All publish URL-B2C Professional Monitor accessory - HB27:",nlbeReturnedData)
    console.log(nlbeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(nlbepublishCheck ==0){
            HB27PublishError.push("nl-be")
        }
    }
    //B2C-NLNL-Professional Monitor accessory-HB27-after 20220506
    const nlnlReturnedData = await afterB2CnlnlURLHB27();
    // console.log("After returnedData",nlnlReturnedData)
    const nlnlpublishCheck = nlnlReturnedData.length
    console.log("nl-nl All publish URL-B2C Professional Monitor accessory - HB27:",nlnlReturnedData)
    console.log(nlnlReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(nlnlpublishCheck ==0){
            HB27PublishError.push("nl-nl")
        }
    }
    //B2C-PLPL-Professional Monitor accessory-HB27-after 20220506
    const plplReturnedData = await afterB2CplplURLHB27();
    // console.log("After returnedData",plplReturnedData)
    const plplpublishCheck = plplReturnedData.length
    console.log("pl-pl All publish URL-B2C Professional Monitor accessory - HB27:",plplReturnedData)
    console.log(plplReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(plplpublishCheck ==0){
            HB27PublishError.push("pl-pl")
        }
    }
    //B2C-PTBR-Professional Monitor accessory-HB27-after 20220506
    const ptbrReturnedData = await afterB2CptbrURLHB27();
    // console.log("After returnedData",ptbrReturnedData)
    const ptbrpublishCheck = ptbrReturnedData.length
    console.log("pt-br All publish URL-B2C Professional Monitor accessory - HB27:",ptbrReturnedData)
    console.log(ptbrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(ptbrpublishCheck ==0){
            HB27PublishError.push("pt-br")
        }
    }
    //B2C-PTPT-Professional Monitor accessory-HB27-after 20220506
    const ptptReturnedData = await afterB2CptptURLHB27();
    // console.log("After returnedData",ptptReturnedData)
    const ptptpublishCheck = ptptReturnedData.length
    console.log("pt-pt All publish URL-B2C Professional Monitor accessory - HB27:",ptptReturnedData)
    console.log(ptptReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(ptptpublishCheck ==0){
            HB27PublishError.push("pt-pt")
        }
    }
    //B2C-RORO-Professional Monitor accessory-HB27-after 20220506
    const roroReturnedData = await afterB2CroroURLHB27();
    // console.log("After returnedData",roroReturnedData)
    const roropublishCheck = roroReturnedData.length
    console.log("ro-ro All publish URL-B2C Professional Monitor accessory - HB27:",roroReturnedData)
    console.log(roroReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(roropublishCheck ==0){
            HB27PublishError.push("ro-ro")
        }
    }
    //B2C-RURU-Professional Monitor accessory-HB27-after 20220506
    const ruruReturnedData = await afterB2CruruURLHB27();
    // console.log("After returnedData",ruruReturnedData)
    const rurupublishCheck = ruruReturnedData.length
    console.log("ru-ru All publish URL-B2C Professional Monitor accessory - HB27:",ruruReturnedData)
    console.log(ruruReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(rurupublishCheck ==0){
            HB27PublishError.push("ru-ru")
        }
    }
    //B2C-SKSK-Professional Monitor accessory-HB27-after 20220506
    const skskReturnedData = await afterB2CskskURLHB27();
    // console.log("After returnedData",skskReturnedData)
    const skskpublishCheck = skskReturnedData.length
    console.log("sk-sk All publish URL-B2C Professional Monitor accessory - HB27:",skskReturnedData)
    console.log(skskReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(skskpublishCheck ==0){
            HB27PublishError.push("sk-sk")
        }
    }
    //B2C-SVSE-Professional Monitor accessory-HB27-after 20220506
    const svseReturnedData = await afterB2CsvseURLHB27();
    // console.log("After returnedData",svseReturnedData)
    const svsepublishCheck = svseReturnedData.length
    console.log("sv-se All publish URL-B2C Professional Monitor accessory - HB27:",svseReturnedData)
    console.log(svseReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(svsepublishCheck ==0){
            HB27PublishError.push("sv-se")
        }
    }
    //B2C-THTH-Professional Monitor accessory-HB27-after 20220506
    const ththReturnedData = await afterB2CththURLHB27();
    // console.log("After returnedData",ththReturnedData)
    const ththpublishCheck = ththReturnedData.length
    console.log("th-th All publish URL-B2C Professional Monitor accessory - HB27:",ththReturnedData)
    console.log(ththReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(ththpublishCheck ==0){
            HB27PublishError.push("th-th")
        }
    }
    //B2C-TRTR-Professional Monitor accessory-HB27-after 20220506
    const trtrReturnedData = await afterB2CtrtrURLHB27();
    // console.log("After returnedData",trtrReturnedData)
    const trtrpublishCheck = trtrReturnedData.length
    console.log("tr-tr All publish URL-B2C Professional Monitor accessory - HB27:",trtrReturnedData)
    console.log(trtrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(trtrpublishCheck ==0){
            HB27PublishError.push("tr-tr")
        }
    }
    //B2C-UKUA-Professional Monitor accessory-HB27-after 20220506
    const ukuaReturnedData = await afterB2CukuaURLHB27();
    // console.log("After returnedData",ukuaReturnedData)
    const ukuapublishCheck = ukuaReturnedData.length
    console.log("uk-ua All publish URL-B2C Professional Monitor accessory - HB27:",ukuaReturnedData)
    console.log(ukuaReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(ukuapublishCheck ==0){
            HB27PublishError.push("uk-ua")
        }
    }
    //B2C-VIVN-Professional Monitor accessory-HB27-after 20220506
    const vivnReturnedData = await afterB2CvivnURLHB27();
    // console.log("After returnedData",vivnReturnedData)
    const vivnpublishCheck = vivnReturnedData.length
    console.log("vi-vn All publish URL-B2C Professional Monitor accessory - HB27:",vivnReturnedData)
    console.log(vivnReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(vivnpublishCheck ==0){
            HB27PublishError.push("vi-vn")
        }
    }
    //B2C-ZHHK-Professional Monitor accessory-HB27-after 20220506
    const zhhkReturnedData = await afterB2CzhhkURLHB27();
    // console.log("After returnedData",zhhkReturnedData)
    const zhhkpublishCheck = zhhkReturnedData.length
    console.log("zh-hk All publish URL-B2C Professional Monitor accessory - HB27:",zhhkReturnedData)
    console.log(zhhkReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(zhhkpublishCheck ==0){
            HB27PublishError.push("zh-hk")
        }
    }
    //B2C-ZHTW-Professional Monitor accessory-HB27-after 20220506
    const zhtwReturnedData = await afterB2CzhtwURLHB27();
    // console.log("After returnedData",zhtwReturnedData)
    const zhtwpublishCheck = zhtwReturnedData.length
    console.log("zh-tw All publish URL-B2C Professional Monitor accessory - HB27:",zhtwReturnedData)
    console.log(zhtwReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //HB27PublishError
        if(zhtwpublishCheck ==0){
            HB27PublishError.push("zh-tw")
        }
    }
    //放全部的國家的後面
    if(HB27PublishError.length >0){
        console.log("Not Published Now:",HB27PublishError)
        throw new Error(`${publishModel} must be published on ${HB27PublishError} before ${launchDate}, but it is not published now. Today is ${fullDate}`)
    }
})

Then("HB27 URL name must be 'monitor accessory hb27.html'",{timeout: 1000 * 5000},async function(){
    //After 2022/04/01
    //global setting
    const publishModel = "hb27"
    const publishProductURLName = "monitor/accessory/hb27.html"
    const date = new Date()
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()
    function wholeMonth(){
        const getmonth = date.getMonth() + 1
        if(getmonth<10){
            wholeMonth =  "0"+getmonth 
            return wholeMonth
        }else{
            wholeMonth = getmonth 
            return wholeMonth
        }
    }
    function wholeDate(){
        const getDay = date.getDate()
        if(getDay<10){
            wholeDate =  "0"+getDay
            return wholeDate
        }else{
            wholeDate = getDay 
            return wholeDate
        }
    }
    const wholemonth = wholeMonth()
    const wholeday = wholeDate()
    const wholeyear = date.getFullYear()
    const fullDate = `${wholeyear}/${wholemonth}/${wholeday}`
    // console.log("wholemonth:",wholemonth)
    // console.log("wholeday:",wholeday)
    // console.log("wholeyear:",wholeyear)
    console.log("fullDate:",fullDate)
    // console.log("year:",year)
    // console.log("month:",month)
    // console.log("day:",day)
    const launchDate = "2022/04/01"
    //B2C-ARME-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const armeReturnedData = await wrongnameB2CarmeURLHB27();
    const armeNameCheck = armeReturnedData.length
    console.log("ar-me wrong name URL-B2C Professional Monitor accessory-HB27:",armeReturnedData)
    console.log(armeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(armeNameCheck > 0){
            HB27NameError.push('ar-me')
            HB27NameErrorURL.push(armeReturnedData)
        }
    }
    //B2C-BGBG-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const bgbgReturnedData = await wrongnameB2CbgbgURLHB27();
    const bgbgNameCheck = bgbgReturnedData.length
    console.log("bg-bg wrong name URL-B2C Professional Monitor accessory-HB27:",bgbgReturnedData)
    console.log(bgbgReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(bgbgNameCheck > 0){
            HB27NameError.push('bg-bg')
            HB27NameErrorURL.push(bgbgReturnedData)
        }
    }
    //B2C-CSCZ-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const csczReturnedData = await wrongnameB2CcsczURLHB27();
    const csczNameCheck = csczReturnedData.length
    console.log("cs-cz wrong name URL-B2C Professional Monitor accessory-HB27:",csczReturnedData)
    console.log(csczReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(csczNameCheck > 0){
            HB27NameError.push('cs-cz')
            HB27NameErrorURL.push(csczReturnedData)
        }
    }
    //B2C-DEAT-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const deatReturnedData = await wrongnameB2CdeatURLHB27();
    const deatNameCheck = deatReturnedData.length
    console.log("de-at wrong name URL-B2C Professional Monitor accessory-HB27:",deatReturnedData)
    console.log(deatReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(deatNameCheck > 0){
            HB27NameError.push('de-at')
            HB27NameErrorURL.push(deatReturnedData)
        }
    }
    //B2C-DECH-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const dechReturnedData = await wrongnameB2CdechURLHB27();
    const dechNameCheck = dechReturnedData.length
    console.log("de-ch wrong name URL-B2C Professional Monitor accessory-HB27:",dechReturnedData)
    console.log(dechReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(dechNameCheck > 0){
            HB27NameError.push('de-ch')
            HB27NameErrorURL.push(dechReturnedData)
        }
    }
    //B2C-DEDE-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const dedeReturnedData = await wrongnameB2CdedeURLHB27();
    const dedeNameCheck = dedeReturnedData.length
    console.log("de-de wrong name URL-B2C Professional Monitor accessory-HB27:",dedeReturnedData)
    console.log(dedeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(dedeNameCheck > 0){
            HB27NameError.push('de-de')
            HB27NameErrorURL.push(dedeReturnedData)
        }
    }
    //B2C-ELGR-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const elgrReturnedData = await wrongnameB2CelgrURLHB27();
    const elgrNameCheck = elgrReturnedData.length
    console.log("el-gr wrong name URL-B2C Professional Monitor accessory-HB27:",elgrReturnedData)
    console.log(elgrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(elgrNameCheck > 0){
            HB27NameError.push('el-gr')
            HB27NameErrorURL.push(elgrReturnedData)
        }
    }
    //B2C-ENAP-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const enapReturnedData = await wrongnameB2CenapURLHB27();
    const enapNameCheck = enapReturnedData.length
    console.log("en-ap wrong name URL-B2C Professional Monitor accessory-HB27:",enapReturnedData)
    console.log(enapReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enapNameCheck > 0){
            HB27NameError.push("en-ap")
            HB27NameErrorURL.push(enapReturnedData)
        }
    }
    //B2C-ENAU-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const enauReturnedData = await wrongnameB2CenauURLHB27();
    const enauNameCheck = enauReturnedData.length
    console.log("en-au wrong name URL-B2C Professional Monitor accessory-HB27:",enauReturnedData)
    console.log(enauReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enauNameCheck > 0){
            HB27NameError.push("en-au")
            HB27NameErrorURL.push(enauReturnedData)
        }
    }
    //B2C-ENBA-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const enbaReturnedData = await wrongnameB2CenbaURLHB27();
    const enbaNameCheck = enbaReturnedData.length
    console.log("en-ba wrong name URL-B2C Professional Monitor accessory-HB27:",enbaReturnedData)
    console.log(enbaReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enbaNameCheck > 0){
            HB27NameError.push("en-ba")
            HB27NameErrorURL.push(enbaReturnedData)
        }
    }
    //B2C-ENCA-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const encaReturnedData = await wrongnameB2CencaURLHB27();
    const encaNameCheck = encaReturnedData.length
    console.log("en-ca wrong name URL-B2C Professional Monitor accessory-HB27:",encaReturnedData)
    console.log(encaReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(encaNameCheck > 0){
            HB27NameError.push("en-ca")
            HB27NameErrorURL.push(encaReturnedData)
        }
    }
    //B2C-ENCEE-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const enceeReturnedData = await wrongnameB2CenceeURLHB27();
    const enceeNameCheck = enceeReturnedData.length
    console.log("en-cee wrong name URL-B2C Professional Monitor accessory-HB27:",enceeReturnedData)
    console.log(enceeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enceeNameCheck > 0){
            HB27NameError.push("en-cee")
            HB27NameErrorURL.push(enceeReturnedData)
        }
    }
    //B2C-ENCY-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const encyReturnedData = await wrongnameB2CencyURLHB27();
    const encyNameCheck = encyReturnedData.length
    console.log("en-cy wrong name URL-B2C Professional Monitor accessory-HB27:",encyReturnedData)
    console.log(encyReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(encyNameCheck > 0){
            HB27NameError.push("en-cy")
            HB27NameErrorURL.push(encyReturnedData)
        }
    }
    //B2C-ENDK-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const endkReturnedData = await wrongnameB2CendkURLHB27();
    const endkNameCheck = endkReturnedData.length
    console.log("en-dk wrong name URL-B2C Professional Monitor accessory-HB27:",endkReturnedData)
    console.log(endkReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(endkNameCheck > 0){
            HB27NameError.push("en-dk")
            HB27NameErrorURL.push(endkReturnedData)
        }
    }
    //B2C-ENEE-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const eneeReturnedData = await wrongnameB2CeneeURLHB27();
    const eneeNameCheck = eneeReturnedData.length
    console.log("en-ee wrong name URL-B2C Professional Monitor accessory-HB27:",eneeReturnedData)
    console.log(eneeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(eneeNameCheck > 0){
            HB27NameError.push('en-ee')
            HB27NameErrorURL.push(eneeReturnedData)
        }
    }
    //B2C-ENEU-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const eneuReturnedData = await wrongnameB2CeneuURLHB27();
    const eneuNameCheck = eneuReturnedData.length
    console.log("en-eu wrong name URL-B2C Professional Monitor accessory-HB27:",eneuReturnedData)
    console.log(eneuReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(eneuNameCheck > 0){
            HB27NameError.push('en-eu')
            HB27NameErrorURL.push(eneuReturnedData)
        }
    }
    //B2C-ENFI-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const enfiReturnedData = await wrongnameB2CenfiURLHB27();
    const enfiNameCheck = enfiReturnedData.length
    console.log("en-fi wrong name URL-B2C Professional Monitor accessory-HB27:",enfiReturnedData)
    console.log(enfiReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enfiNameCheck > 0){
            HB27NameError.push('en-fi')
            HB27NameErrorURL.push(enfiReturnedData)
        }
    }
    //B2C-ENHK-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const enhkReturnedData = await wrongnameB2CenhkURLHB27();
    const enhkNameCheck = enhkReturnedData.length
    console.log("en-hk wrong name URL-B2C Professional Monitor accessory-HB27:",enhkReturnedData)
    console.log(enhkReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enhkNameCheck > 0){
            HB27NameError.push("en-hk")
            HB27NameErrorURL.push(enhkReturnedData)
        }
    }
    //B2C-ENHR-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const enhrReturnedData = await wrongnameB2CenhrURLHB27();
    const enhrNameCheck = enhrReturnedData.length
    console.log("en-hr wrong name URL-B2C Professional Monitor accessory-HB27:",enhrReturnedData)
    console.log(enhrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enhrNameCheck > 0){
            HB27NameError.push('en-hr')
            HB27NameErrorURL.push(enhrReturnedData)
        }
    }
    //B2C-ENIE-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const enieReturnedData = await wrongnameB2CenieURLHB27();
    const enieNameCheck = enieReturnedData.length
    console.log("en-ie wrong name URL-B2C Professional Monitor accessory-HB27:",enieReturnedData)
    console.log(enieReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enieNameCheck > 0){
            HB27NameError.push('en-ie')
            HB27NameErrorURL.push(enieReturnedData)
        }
    }
    //B2C-ENIN-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const eninReturnedData = await wrongnameB2CeninURLHB27();
    const eninNameCheck = eninReturnedData.length
    console.log("en-in wrong name URL-B2C Professional Monitor accessory-HB27:",eninReturnedData)
    console.log(eninReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(eninNameCheck > 0){
            HB27NameError.push("en-in")
            HB27NameErrorURL.push(eninReturnedData)
        }
    }
    //B2C-ENIS-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const enisReturnedData = await wrongnameB2CenisURLHB27();
    const enisNameCheck = enisReturnedData.length
    console.log("en-is wrong name URL-B2C Professional Monitor accessory-HB27:",enisReturnedData)
    console.log(enisReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enisNameCheck > 0){
            HB27NameError.push('en-is')
            HB27NameErrorURL.push(enisReturnedData)
        }
    }
    //B2C-ENLU-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const enluReturnedData = await wrongnameB2CenluURLHB27();
    const enluNameCheck = enluReturnedData.length
    console.log("en-lu wrong name URL-B2C Professional Monitor accessory-HB27:",enluReturnedData)
    console.log(enluReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enluNameCheck > 0){
            HB27NameError.push('en-lu')
            HB27NameErrorURL.push(enluReturnedData)
        }
    }
    //B2C-ENLV-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const enlvReturnedData = await wrongnameB2CenlvURLHB27();
    const enlvNameCheck = enlvReturnedData.length
    console.log("en-lv wrong name URL-B2C Professional Monitor accessory-HB27:",enlvReturnedData)
    console.log(enlvReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enlvNameCheck > 0){
            HB27NameError.push('en-lv')
            HB27NameErrorURL.push(enlvReturnedData)
        }
    }
    //B2C-ENME-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const enmeReturnedData = await wrongnameB2CenmeURLHB27();
    const enmeNameCheck = enmeReturnedData.length
    console.log("en-me wrong name URL-B2C Professional Monitor accessory-HB27:",enmeReturnedData)
    console.log(enmeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enmeNameCheck > 0){
            HB27NameError.push("en-me")
            HB27NameErrorURL.push(enmeReturnedData)
        }
    }
    //B2C-ENMK-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const enmkReturnedData = await wrongnameB2CenmkURLHB27();
    const enmkNameCheck = enmkReturnedData.length
    console.log("en-mk wrong name URL-B2C Professional Monitor accessory-HB27:",enmkReturnedData)
    console.log(enmkReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enmkNameCheck > 0){
            HB27NameError.push('en-mk')
            HB27NameErrorURL.push(enmkReturnedData)
        }
    }
    //B2C-ENMT-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const enmtReturnedData = await wrongnameB2CenmtURLHB27();
    const enmtNameCheck = enmtReturnedData.length
    console.log("en-mt wrong name URL-B2C Professional Monitor accessory-HB27:",enmtReturnedData)
    console.log(enmtReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enmtNameCheck > 0){
            HB27NameError.push('en-mt')
            HB27NameErrorURL.push(enmtReturnedData)
        }
    }
    //B2C-ENMY-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const enmyReturnedData = await wrongnameB2CenmyURLHB27();
    const enmyNameCheck = enmyReturnedData.length
    console.log("en-my wrong name URL-B2C Professional Monitor accessory-HB27:",enmyReturnedData)
    console.log(enmyReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enmyNameCheck > 0){
            HB27NameError.push("en-my")
            HB27NameErrorURL.push(enmyReturnedData)
        }
    }
    //B2C-ENNO-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const ennoReturnedData = await wrongnameB2CennoURLHB27();
    const ennoNameCheck = ennoReturnedData.length
    console.log("en-no wrong name URL-B2C Professional Monitor accessory-HB27:",ennoReturnedData)
    console.log(ennoReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ennoNameCheck > 0){
            HB27NameError.push('en-no')
            HB27NameErrorURL.push(ennoReturnedData)
        }
    }
    //B2C-ENRS-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const enrsReturnedData = await wrongnameB2CenrsURLHB27();
    const enrsNameCheck = enrsReturnedData.length
    console.log("en-rs wrong name URL-B2C Professional Monitor accessory-HB27:",enrsReturnedData)
    console.log(enrsReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enrsNameCheck > 0){
            HB27NameError.push('en-rs')
            HB27NameErrorURL.push(enrsReturnedData)
        }
    }
    //B2C-ENSG-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const ensgReturnedData = await wrongnameB2CensgURLHB27();
    const ensgNameCheck = ensgReturnedData.length
    console.log("en-sg wrong name URL-B2C Professional Monitor accessory-HB27:",ensgReturnedData)
    console.log(ensgReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ensgNameCheck > 0){
            HB27NameError.push("en-sg")
            HB27NameErrorURL.push(ensgReturnedData)
        }
    }
    //B2C-ENSI-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const ensiReturnedData = await wrongnameB2CensiURLHB27();
    const ensiNameCheck = ensiReturnedData.length
    console.log("en-si wrong name URL-B2C Professional Monitor accessory-HB27:",ensiReturnedData)
    console.log(ensiReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ensiNameCheck > 0){
            HB27NameError.push('en-si')
            HB27NameErrorURL.push(ensiReturnedData)
        }
    }
    //B2C-ENUK-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const enukReturnedData = await wrongnameB2CenukURLHB27();
    const enukNameCheck = enukReturnedData.length
    console.log("en-uk wrong name URL-B2C Professional Monitor accessory-HB27:",enukReturnedData)
    console.log(enukReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enukNameCheck > 0){
            HB27NameError.push('en-uk')
            HB27NameErrorURL.push(enukReturnedData)
        }
    }
    //B2C-ENUS-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const enusReturnedData = await wrongnameB2CenusURLHB27();
    const enusNameCheck = enusReturnedData.length
    console.log("en-us wrong name URL-B2C Professional Monitor accessory-HB27:",enusReturnedData)
    console.log(enusReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enusNameCheck > 0){
            HB27NameError.push("en-us")
            HB27NameErrorURL.push(enusReturnedData)
        }
    }
    //B2C-ESAR-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const esarReturnedData = await wrongnameB2CesarURLHB27();
    const esarNameCheck = esarReturnedData.length
    console.log("es-ar wrong name URL-B2C Professional Monitor accessory-HB27:",esarReturnedData)
    console.log(esarReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(esarNameCheck > 0){
            HB27NameError.push("es-ar")
            HB27NameErrorURL.push(esarReturnedData)
        }
    }
    //B2C-ESCO-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const escoReturnedData = await wrongnameB2CescoURLHB27();
    const escoNameCheck = escoReturnedData.length
    console.log("es-co wrong name URL-B2C Professional Monitor accessory-HB27:",escoReturnedData)
    console.log(escoReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(escoNameCheck > 0){
            HB27NameError.push("es-co")
            HB27NameErrorURL.push(escoReturnedData)
        }
    }
    //B2C-ESES-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const esesReturnedData = await wrongnameB2CesesURLHB27();
    const esesNameCheck = esesReturnedData.length
    console.log("es-es wrong name URL-B2C Professional Monitor accessory-HB27:",esesReturnedData)
    console.log(esesReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(esesNameCheck > 0){
            HB27NameError.push("es-es")
            HB27NameErrorURL.push(esesReturnedData)
        }
    }
    //B2C-ESLA-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const eslaReturnedData = await wrongnameB2CeslaURLHB27();
    const eslaNameCheck = eslaReturnedData.length
    console.log("es-la wrong name URL-B2C Professional Monitor accessory-HB27:",eslaReturnedData)
    console.log(eslaReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(eslaNameCheck > 0){
            HB27NameError.push("es-la")
            HB27NameErrorURL.push(eslaReturnedData)
        }
    }
    //B2C-ESMX-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const esmxReturnedData = await wrongnameB2CesmxURLHB27();
    const esmxNameCheck = esmxReturnedData.length
    console.log("es-mx wrong name URL-B2C Professional Monitor accessory-HB27:",esmxReturnedData)
    console.log(esmxReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(esmxNameCheck > 0){
            HB27NameError.push("es-mx")
            HB27NameErrorURL.push(esmxReturnedData)
        }
    }
    //B2C-ESPE-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const espeReturnedData = await wrongnameB2CespeURLHB27();
    const espeNameCheck = espeReturnedData.length
    console.log("es-pe wrong name URL-B2C Professional Monitor accessory-HB27:",espeReturnedData)
    console.log(espeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(espeNameCheck > 0){
            HB27NameError.push("es-pe")
            HB27NameErrorURL.push(espeReturnedData)
        }
    }
    //B2C-FRCA-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const frcaReturnedData = await wrongnameB2CfrcaURLHB27();
    const frcaNameCheck = frcaReturnedData.length
    console.log("fr-ca wrong name URL-B2C Professional Monitor accessory-HB27:",frcaReturnedData)
    console.log(frcaReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(frcaNameCheck > 0){
            HB27NameError.push("fr-ca")
            HB27NameErrorURL.push(frcaReturnedData)
        }
    }
    //B2C-FRCH-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const frchReturnedData = await wrongnameB2CfrchURLHB27();
    const frchNameCheck = frchReturnedData.length
    console.log("fr-ch wrong name URL-B2C Professional Monitor accessory-HB27:",frchReturnedData)
    console.log(frchReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(frchNameCheck > 0){
            HB27NameError.push("fr-ch")
            HB27NameErrorURL.push(frchReturnedData)
        }
    }
    //B2C-FRFR-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const frfrReturnedData = await wrongnameB2CfrfrURLHB27();
    const frfrNameCheck = frfrReturnedData.length
    console.log("fr-fr wrong name URL-B2C Professional Monitor accessory-HB27:",frfrReturnedData)
    console.log(frfrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(frfrNameCheck > 0){
            HB27NameError.push("fr-fr")
            HB27NameErrorURL.push(frfrReturnedData)
        }
    }
    //B2C-HUHU-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const huhuReturnedData = await wrongnameB2ChuhuURLHB27();
    const huhuNameCheck = huhuReturnedData.length
    console.log("hu-hu wrong name URL-B2C Professional Monitor accessory-HB27:",huhuReturnedData)
    console.log(huhuReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(huhuNameCheck > 0){
            HB27NameError.push("hu-hu")
            HB27NameErrorURL.push(huhuReturnedData)
        }
    }
    //B2C-IDID-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const ididReturnedData = await wrongnameB2CididURLHB27();
    const ididNameCheck = ididReturnedData.length
    console.log("id-id wrong name URL-B2C Professional Monitor accessory-HB27:",ididReturnedData)
    console.log(ididReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ididNameCheck > 0){
            HB27NameError.push("id-id")
            HB27NameErrorURL.push(ididReturnedData)
        }
    }
    //B2C-ITIT-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const ititReturnedData = await wrongnameB2CititURLHB27();
    const ititNameCheck = ititReturnedData.length
    console.log("it-it wrong name URL-B2C Professional Monitor accessory-HB27:",ititReturnedData)
    console.log(ititReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ititNameCheck > 0){
            HB27NameError.push("it-it")
            HB27NameErrorURL.push(ititReturnedData)
        }
    }
    //B2C-JAJP-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const jajpReturnedData = await wrongnameB2CjajpURLHB27();
    const jajpNameCheck = jajpReturnedData.length
    console.log("ja-jp wrong name URL-B2C Professional Monitor accessory-HB27:",jajpReturnedData)
    console.log(jajpReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(jajpNameCheck > 0){
            HB27NameError.push("ja-jp")
            HB27NameErrorURL.push(jajpReturnedData)
        }
    }
    //B2C-KOKR-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const kokrReturnedData = await wrongnameB2CkokrURLHB27();
    const kokrNameCheck = kokrReturnedData.length
    console.log("ko-kr wrong name URL-B2C Professional Monitor accessory-HB27:",kokrReturnedData)
    console.log(kokrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(kokrNameCheck > 0){
            HB27NameError.push("ko-kr")
            HB27NameErrorURL.push(kokrReturnedData)
        }
    }
    //B2C-LTLT-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const ltltReturnedData = await wrongnameB2CltltURLHB27();
    const ltltNameCheck = ltltReturnedData.length
    console.log("lt-lt wrong name URL-B2C Professional Monitor accessory-HB27:",ltltReturnedData)
    console.log(ltltReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ltltNameCheck > 0){
            HB27NameError.push("lt-lt")
            HB27NameErrorURL.push(ltltReturnedData)
        }
    }
    //B2C-NLBE-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const nlbeReturnedData = await wrongnameB2CnlbeURLHB27();
    const nlbeNameCheck = nlbeReturnedData.length
    console.log("nl-be wrong name URL-B2C Professional Monitor accessory-HB27:",nlbeReturnedData)
    console.log(nlbeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(nlbeNameCheck > 0){
            HB27NameError.push("nl-be")
            HB27NameErrorURL.push(nlbeReturnedData)
        }
    }
    //B2C-NLNL-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const nlnlReturnedData = await wrongnameB2CnlnlURLHB27();
    const nlnlNameCheck = nlnlReturnedData.length
    console.log("nl-nl wrong name URL-B2C Professional Monitor accessory-HB27:",nlnlReturnedData)
    console.log(nlnlReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(nlnlNameCheck > 0){
            HB27NameError.push("nl-nl")
            HB27NameErrorURL.push(nlnlReturnedData)
        }
    }
    //B2C-PLPL-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const plplReturnedData = await wrongnameB2CplplURLHB27();
    const plplNameCheck = plplReturnedData.length
    console.log("pl-pl wrong name URL-B2C Professional Monitor accessory-HB27:",plplReturnedData)
    console.log(plplReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(plplNameCheck > 0){
            HB27NameError.push("pl-pl")
            HB27NameErrorURL.push(plplReturnedData)
        }
    }
    //B2C-PTBR-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const ptbrReturnedData = await wrongnameB2CptbrURLHB27();
    const ptbrNameCheck = ptbrReturnedData.length
    console.log("pt-br wrong name URL-B2C Professional Monitor accessory-HB27:",ptbrReturnedData)
    console.log(ptbrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ptbrNameCheck > 0){
            HB27NameError.push("pt-br")
            HB27NameErrorURL.push(ptbrReturnedData)
        }
    }
    //B2C-PTPT-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const ptptReturnedData = await wrongnameB2CptptURLHB27();
    const ptptNameCheck = ptptReturnedData.length
    console.log("pt-pt wrong name URL-B2C Professional Monitor accessory-HB27:",ptptReturnedData)
    console.log(ptptReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ptptNameCheck > 0){
            HB27NameError.push("pt-pt")
            HB27NameErrorURL.push(ptptReturnedData)
        }
    }
    //B2C-RORO-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const roroReturnedData = await wrongnameB2CroroURLHB27();
    const roroNameCheck = roroReturnedData.length
    console.log("ro-ro wrong name URL-B2C Professional Monitor accessory-HB27:",roroReturnedData)
    console.log(roroReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(roroNameCheck > 0){
            HB27NameError.push("ro-ro")
            HB27NameErrorURL.push(roroReturnedData)
        }
    }
    //B2C-RURU-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const ruruReturnedData = await wrongnameB2CruruURLHB27();
    const ruruNameCheck = ruruReturnedData.length
    console.log("ru-ru wrong name URL-B2C Professional Monitor accessory-HB27:",ruruReturnedData)
    console.log(ruruReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ruruNameCheck > 0){
            HB27NameError.push("ru-ru")
            HB27NameErrorURL.push(ruruReturnedData)
        }
    }
    //B2C-SKSK-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const skskReturnedData = await wrongnameB2CskskURLHB27();
    const skskNameCheck = skskReturnedData.length
    console.log("sk-sk wrong name URL-B2C Professional Monitor accessory-HB27:",skskReturnedData)
    console.log(skskReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(skskNameCheck > 0){
            HB27NameError.push("sk-sk")
            HB27NameErrorURL.push(skskReturnedData)
        }
    }
    //B2C-SVSE-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const svseReturnedData = await wrongnameB2CsvseURLHB27();
    const svseNameCheck = svseReturnedData.length
    console.log("sv-se wrong name URL-B2C Professional Monitor accessory-HB27:",svseReturnedData)
    console.log(svseReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(svseNameCheck > 0){
            HB27NameError.push("sv-se")
            HB27NameErrorURL.push(svseReturnedData)
        }
    }
    //B2C-THTH-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const ththReturnedData = await wrongnameB2CththURLHB27();
    const ththNameCheck = ththReturnedData.length
    console.log("th-th wrong name URL-B2C Professional Monitor accessory-HB27:",ththReturnedData)
    console.log(ththReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ththNameCheck > 0){
            HB27NameError.push("th-th")
            HB27NameErrorURL.push(ththReturnedData)
        }
    }
    //B2C-TRTR-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const trtrReturnedData = await wrongnameB2CtrtrURLHB27();
    const trtrNameCheck = trtrReturnedData.length
    console.log("tr-tr wrong name URL-B2C Professional Monitor accessory-HB27:",trtrReturnedData)
    console.log(trtrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(trtrNameCheck > 0){
            HB27NameError.push("tr-tr")
            HB27NameErrorURL.push(trtrReturnedData)
        }
    }
    //B2C-UKUA-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const ukuaReturnedData = await wrongnameB2CukuaURLHB27();
    const ukuaNameCheck = ukuaReturnedData.length
    console.log("uk-ua wrong name URL-B2C Professional Monitor accessory-HB27:",ukuaReturnedData)
    console.log(ukuaReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ukuaNameCheck > 0){
            HB27NameError.push("uk-ua")
            HB27NameErrorURL.push(ukuaReturnedData)
        }
    }
    //B2C-VIVN-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const vivnReturnedData = await wrongnameB2CvivnURLHB27();
    const vivnNameCheck = vivnReturnedData.length
    console.log("vi-vn wrong name URL-B2C Professional Monitor accessory-HB27:",vivnReturnedData)
    console.log(vivnReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(vivnNameCheck > 0){
            HB27NameError.push("vi-vn")
            HB27NameErrorURL.push(vivnReturnedData)
        }
    }
    //B2C-ZHHK-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const zhhkReturnedData = await wrongnameB2CzhhkURLHB27();
    const zhhkNameCheck = zhhkReturnedData.length
    console.log("zh-hk wrong name URL-B2C Professional Monitor accessory-HB27:",zhhkReturnedData)
    console.log(zhhkReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(zhhkNameCheck > 0){
            HB27NameError.push("zh-hk")
            HB27NameErrorURL.push(zhhkReturnedData)
        }
    }
    //B2C-ZHTW-Professional Monitor accessory-HB27-name must be monitor/accessory/hb27.html
    const zhtwReturnedData = await wrongnameB2CzhtwURLHB27();
    const zhtwNameCheck = zhtwReturnedData.length
    console.log("zh-tw wrong name URL-B2C Professional Monitor accessory-HB27:",zhtwReturnedData)
    console.log(zhtwReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(zhtwNameCheck > 0){
            HB27NameError.push("zh-tw")
            HB27NameErrorURL.push(zhtwReturnedData)
        }
    }

    //放全部國家的後面
    if(HB27NameError.length >0){
        throw new Error(`${publishModel} URL name must be ${publishProductURLName}, but it is wrong now. Here is RO name: ${HB27NameError} And wrong URL: ${HB27NameErrorURL}`)
    }
})
