const { Given, When, Then, Before, After } = require('@cucumber/cucumber')
const { expect } = require('chai')
const fs = require('fs');
const puppeteer = require('puppeteer')
const request = require("request-promise");
const cheerio = require("cheerio");
const cicGA="?utm_source=autotest&utm_medium=CIC"

Before({timeout: 24 * 5000},async function () {
    this.browser = await puppeteer.launch({ 
        executablePath:
        "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
        // MAC路徑:
        // executablePath:
        // '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome',
        headless:true,//有無需要開視窗,false要開,true不開
        slowMo:100,// slow down by 100ms
        devtools:false//有無需要開啟開發人員工具
    })
    this.page = await this.browser.newPage()
    await this.page.setViewport({width:1200,height:1000})
    await this.page.setDefaultTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
    await this.page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
})

After({timeout: 12 * 5000},async function () {
    await this.browser.close()
})

//BL2480TL
const excelToBL2480TLRONS =[
    "ar-me",
    "cs-cz",
    "de-de",
    "en-ap",
    "en-eu",
    "en-hk",
    "en-uk",
    "en-us",
    "es-es",
    "es-la",
    "es-mx",
    "fr-ca",
    "fr-ch",
    "fr-fr",
    "id-id",
    "it-it",
    "ja-jp",
    "ko-kr",
    "nl-nl",
    "pl-pl",
    "pt-br",
    "ru-ru",
    "tr-tr",
    "vi-vn",
    // "zh-cn",
    "zh-hk",
    "zh-tw"
]
const excelToBL2480TLResult =[]
const excelToBL2480TLURL =[]

//B2B-LCD-BL2480TL - publish check
const BL2480TLPublishError=[]
//B2B-ARME-LCD-BL2480TL-after 20220506
const afterB2BarmeBL2480TL=[]
const afterB2BarmeURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ar-me/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BarmeBL2480TL.push(url)
            }
    })
    return afterB2BarmeBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-CSCZ-LCD-BL2480TL-after 20220506
const afterB2BcsczBL2480TL=[]
const afterB2BcsczURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/cs-cz/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BcsczBL2480TL.push(url)
            }
    })
    return afterB2BcsczBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-DEDE-LCD-BL2480TL-after 20220506
const afterB2BdedeBL2480TL=[]
const afterB2BdedeURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/de-de/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BdedeBL2480TL.push(url)
            }
    })
    return afterB2BdedeBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENAP-LCD-BL2480TL-after 20220506
const afterB2BenapBL2480TL=[]
const afterB2BenapURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-ap/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BenapBL2480TL.push(url)
            }
    })
    return afterB2BenapBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENEU-LCD-BL2480TL-after 20220506
const afterB2BeneuBL2480TL=[]
const afterB2BeneuURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-eu/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BeneuBL2480TL.push(url)
            }
    })
    return afterB2BeneuBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENHK-LCD-BL2480TL-after 20220506
const afterB2BenhkBL2480TL=[]
const afterB2BenhkURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-hk/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BenhkBL2480TL.push(url)
            }
    })
    return afterB2BenhkBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENUK-LCD-BL2480TL-after 20220506
const afterB2BenukBL2480TL=[]
const afterB2BenukURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-uk/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BenukBL2480TL.push(url)
            }
    })
    return afterB2BenukBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENUS-LCD-BL2480TL-after 20220506
const afterB2BenusBL2480TL=[]
const afterB2BenusURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-us/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BenusBL2480TL.push(url)
            }
    })
    return afterB2BenusBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ESES-LCD-BL2480TL-after 20220506
const afterB2BesesBL2480TL=[]
const afterB2BesesURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-es/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BesesBL2480TL.push(url)
            }
    })
    return afterB2BesesBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ESLA-LCD-BL2480TL-after 20220506
const afterB2BeslaBL2480TL=[]
const afterB2BeslaURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/es-la/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BeslaBL2480TL.push(url)
            }
    })
    return afterB2BeslaBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ESMX-LCD-BL2480TL-after 20220506
const afterB2BesmxBL2480TL=[]
const afterB2BesmxURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/es-mx/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BesmxBL2480TL.push(url)
            }
    })
    return afterB2BesmxBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-FRCA-LCD-BL2480TL-after 20220506
const afterB2BfrcaBL2480TL=[]
const afterB2BfrcaURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/fr-ca/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BfrcaBL2480TL.push(url)
            }
    })
    return afterB2BfrcaBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-FRCH-LCD-BL2480TL-after 20220506
const afterB2BfrchBL2480TL=[]
const afterB2BfrchURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/fr-ch/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BfrchBL2480TL.push(url)
            }
    })
    return afterB2BfrchBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-FRFR-LCD-BL2480TL-after 20220506
const afterB2BfrfrBL2480TL=[]
const afterB2BfrfrURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/fr-fr/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl="bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BfrfrBL2480TL.push(url)
            }
    })
    return afterB2BfrfrBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-IDID-LCD-BL2480TL-after 20220506
const afterB2BididBL2480TL=[]
const afterB2BididURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/id-id/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BididBL2480TL.push(url)
            }
    })
    return afterB2BididBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ITIT-LCD-BL2480TL-after 20220506
const afterB2BititBL2480TL=[]
const afterB2BititURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/it-it/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BititBL2480TL.push(url)
            }
    })
    return afterB2BititBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-JAJP-LCD-BL2480TL-after 20220506
const afterB2BjajpBL2480TL=[]
const afterB2BjajpURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ja-jp/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BjajpBL2480TL.push(url)
            }
    })
    return afterB2BjajpBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-KOKR-LCD-BL2480TL-after 20220506
const afterB2BkokrBL2480TL=[]
const afterB2BkokrURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ko-kr/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BkokrBL2480TL.push(url)
            }
    })
    return afterB2BkokrBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-NLNL-LCD-BL2480TL-after 20220506
const afterB2BnlnlBL2480TL=[]
const afterB2BnlnlURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/nl-nl/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BnlnlBL2480TL.push(url)
            }
    })
    return afterB2BnlnlBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-PLPL-LCD-BL2480TL-after 20220506
const afterB2BplplBL2480TL=[]
const afterB2BplplURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/pl-pl/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BplplBL2480TL.push(url)
            }
    })
    return afterB2BplplBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-PTBR-LCD-BL2480TL-after 20220506
const afterB2BptbrBL2480TL=[]
const afterB2BptbrURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/pt-br/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BptbrBL2480TL.push(url)
            }
    })
    return afterB2BptbrBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-RURU-LCD-BL2480TL-after 20220506
const afterB2BruruBL2480TL=[]
const afterB2BruruURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ru-ru/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BruruBL2480TL.push(url)
            }
    })
    return afterB2BruruBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-TRTR-LCD-BL2480TL-after 20220506
const afterB2BtrtrBL2480TL=[]
const afterB2BtrtrURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/tr-tr/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BtrtrBL2480TL.push(url)
            }
    })
    return afterB2BtrtrBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-VIVN-LCD-BL2480TL-after 20220506
const afterB2BvivnBL2480TL=[]
const afterB2BvivnURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/vi-vn/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BvivnBL2480TL.push(url)
            }
    })
    return afterB2BvivnBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ZHHK-LCD-BL2480TL-after 20220506
const afterB2BzhhkBL2480TL=[]
const afterB2BzhhkURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/zh-hk/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl="bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BzhhkBL2480TL.push(url)
            }
    })
    return afterB2BzhhkBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ZHTW-LCD-BL2480TL-after 20220506
const afterB2BzhtwBL2480TL=[]
const afterB2BzhtwURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/zh-tw/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "bl2480tl.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BzhtwBL2480TL.push(url)
            }
    })
    return afterB2BzhtwBL2480TL;
    } catch (error) {
      console.log(error);
    }
};

//LCD - BL2480TL - URL name check
const BL2480TLNameError=[]
const BL2480TLNameErrorURL=[]
//B2B-ARME-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BarmeBL2480TL=[]
const wrongnameB2BarmeURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ar-me/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ar-me"
            const publishSeries = "monitor"
            const publishUrlName = "ar-me/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BarmeBL2480TL.push(url)
            }
    })
    return wrongnameB2BarmeBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-CSCZ-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BcsczBL2480TL=[]
const wrongnameB2BcsczURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/cs-cz/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "cs-cz"
            const publishSeries = "monitor"
            const publishUrlName = "cs-cz/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BcsczBL2480TL.push(url)
            }
    })
    return wrongnameB2BcsczBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-DEDE-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BdedeBL2480TL=[]
const wrongnameB2BdedeURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/de-de/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "de-de"
            const publishSeries = "monitor"
            const publishUrlName = "de-de/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BdedeBL2480TL.push(url)
            }
    })
    return wrongnameB2BdedeBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENAP-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BenapBL2480TL=[]
const wrongnameB2BenapURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-ap/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-ap"
            const publishSeries = "monitor"
            const publishUrlName = "en-ap/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BenapBL2480TL.push(url)
            }
    })
    return wrongnameB2BenapBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENEU-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BeneuBL2480TL=[]
const wrongnameB2BeneuURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-eu/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-eu"
            const publishSeries = "monitor"
            const publishUrlName = "en-eu/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BeneuBL2480TL.push(url)
            }
    })
    return wrongnameB2BeneuBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENHK-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BenhkBL2480TL=[]
const wrongnameB2BenhkURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-hk/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-hk"
            const publishSeries = "monitor"
            const publishUrlName = "en-hk/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BenhkBL2480TL.push(url)
            }
    })
    return wrongnameB2BenhkBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENUK-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BenukBL2480TL=[]
const wrongnameB2BenukURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-uk/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-uk"
            const publishSeries = "monitor"
            const publishUrlName = "en-uk/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 &&  url.indexOf(publishUrlName)<0 ){
                wrongnameB2BenukBL2480TL.push(url)
            }
    })
    return wrongnameB2BenukBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENUS-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BenusBL2480TL=[]
const wrongnameB2BenusURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-us/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-us"
            const publishSeries = "monitor"
            const publishUrlName = "en-us/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 &&  url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BenusBL2480TL.push(url)
            }
    })
    return wrongnameB2BenusBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ESES-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BesesBL2480TL=[]
const wrongnameB2BesesURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-es/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-es"
            const publishSeries = "monitor"
            const publishUrlName = "es-es/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BesesBL2480TL.push(url)
            }
    })
    return wrongnameB2BesesBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ESLA-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BeslaBL2480TL=[]
const wrongnameB2BeslaURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/es-la/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-la"
            const publishSeries = "monitor"
            const publishUrlName = "es-la/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 &&  url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BeslaBL2480TL.push(url)
            }
    })
    return wrongnameB2BeslaBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ESMX-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BesmxBL2480TL=[]
const wrongnameB2BesmxURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/es-mx/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-mx"
            const publishSeries ="monitor"
            const publishUrlName = "es-mx/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BesmxBL2480TL.push(url)
            }
    })
    return wrongnameB2BesmxBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-FRCA-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BfrcaBL2480TL=[]
const wrongnameB2BfrcaURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/fr-ca/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "fr-ca"
            const publishSeries = "monitor"
            const publishUrlName = "fr-ca/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BfrcaBL2480TL.push(url)
            }
    })
    return wrongnameB2BfrcaBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-FRCH-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BfrchBL2480TL=[]
const wrongnameB2BfrchURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/fr-ch/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "fr-ch"
            const publishSeries = "monitor"
            const publishUrlName = "fr-ch/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BfrchBL2480TL.push(url)
            }
    })
    return wrongnameB2BfrchBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-FRFR-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BfrfrBL2480TL=[]
const wrongnameB2BfrfrURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/fr-fr/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "fr-fr"
            const publishSeries = "monitor"
            const publishUrlName = "fr-fr/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BfrfrBL2480TL.push(url)
            }
    })
    return wrongnameB2BfrfrBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-IDID-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BididBL2480TL=[]
const wrongnameB2BididURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/id-id/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "id-id"
            const publishSeries = "monitor"
            const publishUrlName = "id-id/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BididBL2480TL.push(url)
            }
    })
    return wrongnameB2BididBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ITIT-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BititBL2480TL=[]
const wrongnameB2BititURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/it-it/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "it-it"
            const publishSeries = "monitor"
            const publishUrlName = "it-it/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BititBL2480TL.push(url)
            }
    })
    return wrongnameB2BititBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-JAJP-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BjajpBL2480TL=[]
const wrongnameB2BjajpURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ja-jp/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ja-jp"
            const publishSeries = "monitor"
            const publishUrlName = "ja-jp/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BjajpBL2480TL.push(url)
            }
    })
    return wrongnameB2BjajpBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-KOKR-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BkokrBL2480TL=[]
const wrongnameB2BkokrURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ko-kr/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ko-kr"
            const publishSeries = "monitor"
            const publishUrlName = "ko-kr/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BkokrBL2480TL.push(url)
            }
    })
    return wrongnameB2BkokrBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-NLNL-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BnlnlBL2480TL=[]
const wrongnameB2BnlnlURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/nl-nl/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "nl-nl"
            const publishSeries = "monitor"
            const publishUrlName = "nl-nl/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 &&  url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BnlnlBL2480TL.push(url)
            }
    })
    return wrongnameB2BnlnlBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-PLPL-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BplplBL2480TL=[]
const wrongnameB2BplplURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/pl-pl/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "pl-pl"
            const publishSeries = "monitor"
            const publishUrlName = "pl-pl/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BplplBL2480TL.push(url)
            }
    })
    return wrongnameB2BplplBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-PTBR-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BptbrBL2480TL=[]
const wrongnameB2BptbrURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/pt-br/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "pt-br"
            const publishSeries = "monitor"
            const publishUrlName = "pt-br/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BptbrBL2480TL.push(url)
            }
    })
    return wrongnameB2BptbrBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-RURU-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BruruBL2480TL=[]
const wrongnameB2BruruURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ru-ru/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ru-ru"
            const publishSeries = "monitor"
            const publishUrlName = "ru-ru/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BruruBL2480TL.push(url)
            }
    })
    return wrongnameB2BruruBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-TRTR-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BtrtrBL2480TL=[]
const wrongnameB2BtrtrURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/tr-tr/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "tr-tr"
            const publishSeries = "monitor"
            const publishUrlName = "tr-tr/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BtrtrBL2480TL.push(url)
            }
    })
    return wrongnameB2BtrtrBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-VIVN-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BvivnBL2480TL=[]
const wrongnameB2BvivnURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/vi-vn/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "vi-vn"
            const publishSeries = "monitor"
            const publishUrlName = "vi-vn/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BvivnBL2480TL.push(url)
            }
    })
    return wrongnameB2BvivnBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ZHHK-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BzhhkBL2480TL=[]
const wrongnameB2BzhhkURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/zh-hk/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "zh-hk"
            const publishSeries = "monitor"
            const publishUrlName = "zh-hk/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BzhhkBL2480TL.push(url)
            }
    })
    return wrongnameB2BzhhkBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ZHTW-LCD-BL2480TL-name must be monitor/bl2480tl.html
const wrongnameB2BzhtwBL2480TL=[]
const wrongnameB2BzhtwURLBL2480TL = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/zh-tw/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "zh-tw"
            const publishSeries = "monitor"
            const publishUrlName = "zh-tw/monitor"
            const publishProductModel = "bl2480tl.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BzhtwBL2480TL.push(url)
            }
    })
    return wrongnameB2BzhtwBL2480TL;
    } catch (error) {
      console.log(error);
    }
};
//Test case
Given("BL2480TL must be published on global site after 20220506",{timeout: 1000 * 5000},async function(){
    //After 2022/01/01
    //global setting
    const publishModel = "bl2480tl"
    const date = new Date()
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()
    function wholeMonth(){
        const getmonth = date.getMonth() + 1
        if(getmonth<10){
            wholeMonth =  "0"+getmonth 
            return wholeMonth
        }else{
            wholeMonth = getmonth 
            return wholeMonth
        }
    }
    function wholeDate(){
        const getDay = date.getDate()
        if(getDay<10){
            wholeDate =  "0"+getDay
            return wholeDate
        }else{
            wholeDate = getDay 
            return wholeDate
        }
    }
    const wholemonth = wholeMonth()
    const wholeday = wholeDate()
    const wholeyear = date.getFullYear()
    const fullDate = `${wholeyear}/${wholemonth}/${wholeday}`
    // console.log("wholemonth:",wholemonth)
    // console.log("wholeday:",wholeday)
    // console.log("wholeyear:",wholeyear)
    console.log("fullDate:",fullDate)
    // console.log("year:",year)
    // console.log("month:",month)
    // console.log("day:",day)
    const launchDate = "2022/04/01"

    //B2B-ARME-LCD-BL2480TL-after 20220506
    const armeReturnedData = await afterB2BarmeURLBL2480TL();
    // console.log("After returnedData",armeReturnedData)
    const armepublishCheck = armeReturnedData.length
    console.log("ar-me All publish URL-B2B LCD-BL2480TL:",armeReturnedData)
    console.log(armeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(armepublishCheck ==0){
            // afterB2BarmeBL2480TL.push("ar-me")
            BL2480TLPublishError.push("ar-me")

        }
    }
    //B2B-CSCZ-LCD-BL2480TL-after 20220506
    const csczReturnedData = await afterB2BcsczURLBL2480TL();
    // console.log("After returnedData",csczReturnedData)
    const csczpublishCheck = csczReturnedData.length
    console.log("cs-cz All publish URL-B2B LCD-BL2480TL:",csczReturnedData)
    console.log(csczReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(csczpublishCheck ==0){
            // afterB2BcsczBL2480TL.push("cs-cz")
            BL2480TLPublishError.push("cs-cz")

        }
    }
    //B2B-DEDE-LCD-BL2480TL-after 20220506
    const dedeReturnedData = await afterB2BdedeURLBL2480TL();
    // console.log("After returnedData",dedeReturnedData)
    const dedepublishCheck = dedeReturnedData.length
    console.log("de-de All publish URL-B2B LCD-BL2480TL:",dedeReturnedData)
    console.log(dedeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(dedepublishCheck ==0){
            // afterB2BdedeBL2480TL.push("de-de")
            BL2480TLPublishError.push("de-de")

        }
    }
    //B2B-ENAP-LCD-BL2480TL-after 20220506
    const enapReturnedData = await afterB2BenapURLBL2480TL();
    // console.log("After returnedData",enapReturnedData)
    const enappublishCheck = enapReturnedData.length
    console.log("en-ap All publish URL-B2B LCD-BL2480TL:",enapReturnedData)
    console.log(enapReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(enappublishCheck ==0){
            // afterB2BenapBL2480TL.push("en-ap")
            BL2480TLPublishError.push("en-ap")

        }
    }
    //B2B-ENEU-LCD-BL2480TL-after 20220506
    const eneuReturnedData = await afterB2BeneuURLBL2480TL();
    // console.log("After returnedData",eneuReturnedData)
    const eneupublishCheck = eneuReturnedData.length
    console.log("en-eu All publish URL-B2B LCD-BL2480TL:",eneuReturnedData)
    console.log(eneuReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(eneupublishCheck ==0){
            BL2480TLPublishError.push("en-eu")
        }
    }
    //B2B-ENHK-LCD-BL2480TL-after 20220506
    const enhkReturnedData = await afterB2BenhkURLBL2480TL();
    // console.log("After returnedData",enhkReturnedData)
    const enhkpublishCheck = enhkReturnedData.length
    console.log("en-hk All publish URL-B2B LCD-BL2480TL:",enhkReturnedData)
    console.log(enhkReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(enhkpublishCheck ==0){
            BL2480TLPublishError.push("en-hk")
        }
    }
    //B2B-ENUK-LCD-BL2480TL-after 20220506
    const enukReturnedData = await afterB2BenukURLBL2480TL();
    // console.log("After returnedData",enukReturnedData)
    const enukpublishCheck = enukReturnedData.length
    console.log("en-uk All publish URB2B LCD-BL2480TL:",enukReturnedData)
    console.log(enukReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(enukpublishCheck ==0){
            BL2480TLPublishError.push("en-uk")
        }
    }
    //B2B-ENUS-LCD-BL2480TL-after 20220506
    const enusReturnedData = await afterB2BenusURLBL2480TL();
    // console.log("After returnedData",enusReturnedData)
    const enuspublishCheck = enusReturnedData.length
    console.log("en-us All publish URL-B2B LCD-BL2480TL:",enusReturnedData)
    console.log(enusReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(enuspublishCheck ==0){
            BL2480TLPublishError.push("en-us")
        }
    }
    //B2B-ESES-LCD-BL2480TL-after 20220506
    const esesReturnedData = await afterB2BesesURLBL2480TL();
    // console.log("After returnedData",esesReturnedData)
    const esespublishCheck = esesReturnedData.length
    console.log("es-es All publish URL-B2B LCD-BL2480TL:",esesReturnedData)
    console.log(esesReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(esespublishCheck ==0){
            BL2480TLPublishError.push("es-es")
        }
    }
    //B2B-ESLA-LCD-BL2480TL-after 20220506
    const eslaReturnedData = await afterB2BeslaURLBL2480TL();
    // console.log("After returnedData",eslaReturnedData)
    const eslapublishCheck = eslaReturnedData.length
    console.log("es-la All publish URL-B2B LCD-BL2480TL:",eslaReturnedData)
    console.log(eslaReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(eslapublishCheck ==0){
            BL2480TLPublishError.push("es-la")
        }
    }
    //B2B-ESMX-LCD-BL2480TL-after 20220506
    const esmxReturnedData = await afterB2BesmxURLBL2480TL();
    // console.log("After returnedData",esmxReturnedData)
    const esmxpublishCheck = esmxReturnedData.length
    console.log("es-mx All publish URL-B2B LCD-BL2480TL:",esmxReturnedData)
    console.log(esmxReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(esmxpublishCheck ==0){
            BL2480TLPublishError.push("es-mx")
        }
    }
    //B2B-FRCA-LCD-BL2480TL-after 20220506
    const frcaReturnedData = await afterB2BfrcaURLBL2480TL();
    // console.log("After returnedData",frcaReturnedData)
    const frcapublishCheck = frcaReturnedData.length
    console.log("fr-ca All publish URL-B2B LCD-BL2480TL:",frcaReturnedData)
    console.log(frcaReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(frcapublishCheck ==0){
            BL2480TLPublishError.push("fr-ca")
        }
    }
    //B2B-FRCH-LCD-BL2480TL-after 20220506
    const frchReturnedData = await afterB2BfrchURLBL2480TL();
    // console.log("After returnedData",frchReturnedData)
    const frchpublishCheck = frchReturnedData.length
    console.log("fr-ch All publish URL-B2B LCD-BL2480TL:",frchReturnedData)
    console.log(frchReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(frchpublishCheck ==0){
            BL2480TLPublishError.push("fr-ch")
        }
    }
    //B2B-FRFR-LCD-BL2480TL-after 20220506
    const frfrReturnedData = await afterB2BfrfrURLBL2480TL();
    // console.log("After returnedData",frfrReturnedData)
    const frfrpublishCheck = frfrReturnedData.length
    console.log("fr-fr All publish URL-B2B LCD-BL2480TL:",frfrReturnedData)
    console.log(frfrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(frfrpublishCheck ==0){
            BL2480TLPublishError.push("fr-fr")
        }
    }
    //B2B-IDID-LCD-BL2480TL-after 20220506
    const ididReturnedData = await afterB2BididURLBL2480TL();
    // console.log("After returnedData",ididReturnedData)
    const ididpublishCheck = ididReturnedData.length
    console.log("id-id All publish URL-B2B LCD-BL2480TL:",ididReturnedData)
    console.log(ididReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(ididpublishCheck ==0){
            BL2480TLPublishError.push("id-id")
        }
    }
    //B2B-ITIT-LCD-BL2480TL-after 20220506
    const ititReturnedData = await afterB2BititURLBL2480TL();
    // console.log("After returnedData",ititReturnedData)
    const ititpublishCheck = ititReturnedData.length
    console.log("it-it All publish URL-B2B LCD-BL2480TL:",ititReturnedData)
    console.log(ititReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(ititpublishCheck ==0){
            BL2480TLPublishError.push("it-it")
        }
    }
    //B2B-JAJP-LCD-BL2480TL-after 20220506
    const jajpReturnedData = await afterB2BjajpURLBL2480TL();
    // console.log("After returnedData",jajpReturnedData)
    const jajppublishCheck = jajpReturnedData.length
    console.log("ja-jp All publish URL-B2B LCD-BL2480TL:",jajpReturnedData)
    console.log(jajpReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(jajppublishCheck ==0){
            BL2480TLPublishError.push("ja-jp")
        }
    }
    //B2B-KOKR-LCD-BL2480TL-after 20220506
    const kokrReturnedData = await afterB2BkokrURLBL2480TL();
    // console.log("After returnedData",kokrReturnedData)
    const kokrpublishCheck = kokrReturnedData.length
    console.log("ko-kr All publish URL-B2B LCD-BL2480TL:",kokrReturnedData)
    console.log(kokrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(kokrpublishCheck ==0){
            BL2480TLPublishError.push("ko-kr")
        }
    }
    //B2B-NLNL-LCD-BL2480TL-after 20220506
    const nlnlReturnedData = await afterB2BnlnlURLBL2480TL();
    // console.log("After returnedData",nlnlReturnedData)
    const nlnlpublishCheck = nlnlReturnedData.length
    console.log("nl-nl All publish URL-B2B LCD-BL2480TL:",nlnlReturnedData)
    console.log(nlnlReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(nlnlpublishCheck ==0){
            BL2480TLPublishError.push("nl-nl")
        }
    }
    //B2B-PLPL-LCD-BL2480TL-after 20220506
    const plplReturnedData = await afterB2BplplURLBL2480TL();
    // console.log("After returnedData",plplReturnedData)
    const plplpublishCheck = plplReturnedData.length
    console.log("pl-pl All publish URL-B2B LCD-BL2480TL:",plplReturnedData)
    console.log(plplReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(plplpublishCheck ==0){
            BL2480TLPublishError.push("pl-pl")
        }
    }
    //B2B-PTBR-LCD-BL2480TL-after 20220506
    const ptbrReturnedData = await afterB2BptbrURLBL2480TL();
    // console.log("After returnedData",ptbrReturnedData)
    const ptbrpublishCheck = ptbrReturnedData.length
    console.log("pt-br All publish URL-B2B LCD-BL2480TL:",ptbrReturnedData)
    console.log(ptbrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(ptbrpublishCheck ==0){
            BL2480TLPublishError.push("pt-br")
        }
    }
    //B2B-RURU-LCD-BL2480TL-after 20220506
    const ruruReturnedData = await afterB2BruruURLBL2480TL();
    // console.log("After returnedData",ruruReturnedData)
    const rurupublishCheck = ruruReturnedData.length
    console.log("ru-ru All publish URL-B2B LCD-BL2480TL:",ruruReturnedData)
    console.log(ruruReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(rurupublishCheck ==0){
            BL2480TLPublishError.push("ru-ru")
        }
    }
    //B2B-TRTR-LCD-BL2480TL-after 20220506
    const trtrReturnedData = await afterB2BtrtrURLBL2480TL();
    // console.log("After returnedData",trtrReturnedData)
    const trtrpublishCheck = trtrReturnedData.length
    console.log("tr-tr All publish URL-B2B LCD-BL2480TL:",trtrReturnedData)
    console.log(trtrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(trtrpublishCheck ==0){
            BL2480TLPublishError.push("tr-tr")
        }
    }
    //B2B-VIVN-LCD-BL2480TL-after 20220506
    const vivnReturnedData = await afterB2BvivnURLBL2480TL();
    // console.log("After returnedData",vivnReturnedData)
    const vivnpublishCheck = vivnReturnedData.length
    console.log("vi-vn All publish URL-B2B LCD-BL2480TL:",vivnReturnedData)
    console.log(vivnReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(vivnpublishCheck ==0){
            BL2480TLPublishError.push("vi-vn")
        }
    }
    //B2B-ZHHK-LCD-BL2480TL-after 20220506
    const zhhkReturnedData = await afterB2BzhhkURLBL2480TL();
    // console.log("After returnedData",zhhkReturnedData)
    const zhhkpublishCheck = zhhkReturnedData.length
    console.log("zh-hk All publish URL-B2B LCD-BL2480TL:",zhhkReturnedData)
    console.log(zhhkReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(zhhkpublishCheck ==0){
            BL2480TLPublishError.push("zh-hk")
        }
    }
    //B2B-ZHTW-LCD-BL2480TL-after 20220506
    const zhtwReturnedData = await afterB2BzhtwURLBL2480TL();
    // console.log("After returnedData",zhtwReturnedData)
    const zhtwpublishCheck = zhtwReturnedData.length
    console.log("zh-tw All publish URL-B2B LCD-BL2480TL:",zhtwReturnedData)
    console.log(zhtwReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //BL2480TLPublishError
        if(zhtwpublishCheck ==0){
            BL2480TLPublishError.push("zh-tw")
        }
    }
    //放全部的國家的後面
    if(BL2480TLPublishError.length >0){
        console.log("Not Published Now:",BL2480TLPublishError)
        throw new Error(`${publishModel} must be published on ${BL2480TLPublishError} before ${launchDate}, but it is not published now. Today is ${fullDate}`)
    }
})

Then("BL2480TL URL name must be 'monitor bl2480tl.html'",{timeout: 1000 * 5000},async function(){
    //After 2022/04/01
    //global setting
    const publishModel = "bl2480tl"
    const publishProductURLName = "monitor/bl2480tl.html"
    const date = new Date()
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()
    function wholeMonth(){
        const getmonth = date.getMonth() + 1
        if(getmonth<10){
            wholeMonth =  "0"+getmonth 
            return wholeMonth
        }else{
            wholeMonth = getmonth 
            return wholeMonth
        }
    }
    function wholeDate(){
        const getDay = date.getDate()
        if(getDay<10){
            wholeDate =  "0"+getDay
            return wholeDate
        }else{
            wholeDate = getDay 
            return wholeDate
        }
    }
    const wholemonth = wholeMonth()
    const wholeday = wholeDate()
    const wholeyear = date.getFullYear()
    const fullDate = `${wholeyear}/${wholemonth}/${wholeday}`
    // console.log("wholemonth:",wholemonth)
    // console.log("wholeday:",wholeday)
    // console.log("wholeyear:",wholeyear)
    console.log("fullDate:",fullDate)
    // console.log("year:",year)
    // console.log("month:",month)
    // console.log("day:",day)
    const launchDate = "2022/04/01"
    //B2B-ARME-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const armeReturnedData = await wrongnameB2BarmeURLBL2480TL();
    const armeNameCheck = armeReturnedData.length
    console.log("ar-me wrong name URL-B2B LCD-BL2480TL:",armeReturnedData)
    console.log(armeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(armeNameCheck > 0){
            BL2480TLNameError.push('ar-me')
            BL2480TLNameErrorURL.push(armeReturnedData)
        }
    }
    //B2B-CSCZ-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const csczReturnedData = await wrongnameB2BcsczURLBL2480TL();
    const csczNameCheck = csczReturnedData.length
    console.log("cs-cz wrong name URL-B2B LCD-BL2480TL:",csczReturnedData)
    console.log(csczReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(csczNameCheck > 0){
            BL2480TLNameError.push('cs-cz')
            BL2480TLNameErrorURL.push(csczReturnedData)
        }
    }
    //B2B-DEDE-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const dedeReturnedData = await wrongnameB2BdedeURLBL2480TL();
    const dedeNameCheck = dedeReturnedData.length
    console.log("de-de wrong name URL-B2B LCD-BL2480TL:",dedeReturnedData)
    console.log(dedeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(dedeNameCheck > 0){
            BL2480TLNameError.push('de-de')
            BL2480TLNameErrorURL.push(dedeReturnedData)
        }
    }
    //B2B-ENAP-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const enapReturnedData = await wrongnameB2BenapURLBL2480TL();
    const enapNameCheck = enapReturnedData.length
    console.log("en-ap wrong name URL-B2B LCD-BL2480TL:",enapReturnedData)
    console.log(enapReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enapNameCheck > 0){
            BL2480TLNameError.push("en-ap")
            BL2480TLNameErrorURL.push(enapReturnedData)
        }
    }
    //B2B-ENEU-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const eneuReturnedData = await wrongnameB2BeneuURLBL2480TL();
    const eneuNameCheck = eneuReturnedData.length
    console.log("en-eu wrong name URL-B2B LCD-BL2480TL:",eneuReturnedData)
    console.log(eneuReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(eneuNameCheck > 0){
            BL2480TLNameError.push('en-eu')
            BL2480TLNameErrorURL.push(eneuReturnedData)
        }
    }
    //B2B-ENHK-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const enhkReturnedData = await wrongnameB2BenhkURLBL2480TL();
    const enhkNameCheck = enhkReturnedData.length
    console.log("en-hk wrong name URL-B2B LCD-BL2480TL:",enhkReturnedData)
    console.log(enhkReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enhkNameCheck > 0){
            BL2480TLNameError.push("en-hk")
            BL2480TLNameErrorURL.push(enhkReturnedData)
        }
    }
    //B2B-ENUK-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const enukReturnedData = await wrongnameB2BenukURLBL2480TL();
    const enukNameCheck = enukReturnedData.length
    console.log("en-uk wrong name URL-B2B LCD-BL2480TL:",enukReturnedData)
    console.log(enukReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enukNameCheck > 0){
            BL2480TLNameError.push('en-uk')
            BL2480TLNameErrorURL.push(enukReturnedData)
        }
    }
    //B2B-ENUS-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const enusReturnedData = await wrongnameB2BenusURLBL2480TL();
    const enusNameCheck = enusReturnedData.length
    console.log("en-us wrong name URL-B2B LCD-BL2480TL:",enusReturnedData)
    console.log(enusReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enusNameCheck > 0){
            BL2480TLNameError.push("en-us")
            BL2480TLNameErrorURL.push(enusReturnedData)
        }
    }
    //B2B-ESES-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const esesReturnedData = await wrongnameB2BesesURLBL2480TL();
    const esesNameCheck = esesReturnedData.length
    console.log("es-es wrong name URL-B2B LCD-BL2480TL:",esesReturnedData)
    console.log(esesReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(esesNameCheck > 0){
            BL2480TLNameError.push("es-es")
            BL2480TLNameErrorURL.push(esesReturnedData)
        }
    }
    //B2B-ESLA-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const eslaReturnedData = await wrongnameB2BeslaURLBL2480TL();
    const eslaNameCheck = eslaReturnedData.length
    console.log("es-la wrong name URL-B2B LCD-BL2480TL:",eslaReturnedData)
    console.log(eslaReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(eslaNameCheck > 0){
            BL2480TLNameError.push("es-la")
            BL2480TLNameErrorURL.push(eslaReturnedData)
        }
    }
    //B2B-ESMX-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const esmxReturnedData = await wrongnameB2BesmxURLBL2480TL();
    const esmxNameCheck = esmxReturnedData.length
    console.log("es-mx wrong name URL-B2B LCD-BL2480TL:",esmxReturnedData)
    console.log(esmxReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(esmxNameCheck > 0){
            BL2480TLNameError.push("es-mx")
            BL2480TLNameErrorURL.push(esmxReturnedData)
        }
    }
    //B2B-FRCA-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const frcaReturnedData = await wrongnameB2BfrcaURLBL2480TL();
    const frcaNameCheck = frcaReturnedData.length
    console.log("fr-ca wrong name URL-B2B LCD-BL2480TL:",frcaReturnedData)
    console.log(frcaReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(frcaNameCheck > 0){
            BL2480TLNameError.push("fr-ca")
            BL2480TLNameErrorURL.push(frcaReturnedData)
        }
    }
    //B2B-FRCH-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const frchReturnedData = await wrongnameB2BfrchURLBL2480TL();
    const frchNameCheck = frchReturnedData.length
    console.log("fr-ch wrong name URL-B2B LCD-BL2480TL:",frchReturnedData)
    console.log(frchReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(frchNameCheck > 0){
            BL2480TLNameError.push("fr-ch")
            BL2480TLNameErrorURL.push(frchReturnedData)
        }
    }
    //B2B-FRFR-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const frfrReturnedData = await wrongnameB2BfrfrURLBL2480TL();
    const frfrNameCheck = frfrReturnedData.length
    console.log("fr-fr wrong name URL-B2B LCD-BL2480TL:",frfrReturnedData)
    console.log(frfrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(frfrNameCheck > 0){
            BL2480TLNameError.push("fr-fr")
            BL2480TLNameErrorURL.push(frfrReturnedData)
        }
    }
    //B2B-IDID-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const ididReturnedData = await wrongnameB2BididURLBL2480TL();
    const ididNameCheck = ididReturnedData.length
    console.log("id-id wrong name URL-B2B LCD-BL2480TL:",ididReturnedData)
    console.log(ididReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ididNameCheck > 0){
            BL2480TLNameError.push("id-id")
            BL2480TLNameErrorURL.push(ididReturnedData)
        }
    }
    //B2B-ITIT-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const ititReturnedData = await wrongnameB2BititURLBL2480TL();
    const ititNameCheck = ititReturnedData.length
    console.log("it-it wrong name URL-B2B LCD-BL2480TL:",ititReturnedData)
    console.log(ititReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ititNameCheck > 0){
            BL2480TLNameError.push("it-it")
            BL2480TLNameErrorURL.push(ititReturnedData)
        }
    }
    //B2B-JAJP-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const jajpReturnedData = await wrongnameB2BjajpURLBL2480TL();
    const jajpNameCheck = jajpReturnedData.length
    console.log("ja-jp wrong name URL-B2B LCD-BL2480TL:",jajpReturnedData)
    console.log(jajpReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(jajpNameCheck > 0){
            BL2480TLNameError.push("ja-jp")
            BL2480TLNameErrorURL.push(jajpReturnedData)
        }
    }
    //B2B-KOKR-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const kokrReturnedData = await wrongnameB2BkokrURLBL2480TL();
    const kokrNameCheck = kokrReturnedData.length
    console.log("ko-kr wrong name URL-B2B LCD-BL2480TL:",kokrReturnedData)
    console.log(kokrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(kokrNameCheck > 0){
            BL2480TLNameError.push("ko-kr")
            BL2480TLNameErrorURL.push(kokrReturnedData)
        }
    }
    //B2B-NLNL-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const nlnlReturnedData = await wrongnameB2BnlnlURLBL2480TL();
    const nlnlNameCheck = nlnlReturnedData.length
    console.log("nl-nl wrong name URL-B2B LCD-BL2480TL:",nlnlReturnedData)
    console.log(nlnlReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(nlnlNameCheck > 0){
            BL2480TLNameError.push("nl-nl")
            BL2480TLNameErrorURL.push(nlnlReturnedData)
        }
    }
    //B2B-PLPL-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const plplReturnedData = await wrongnameB2BplplURLBL2480TL();
    const plplNameCheck = plplReturnedData.length
    console.log("pl-pl wrong name URL-B2B LCD-BL2480TL:",plplReturnedData)
    console.log(plplReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(plplNameCheck > 0){
            BL2480TLNameError.push("pl-pl")
            BL2480TLNameErrorURL.push(plplReturnedData)
        }
    }
    //B2B-PTBR-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const ptbrReturnedData = await wrongnameB2BptbrURLBL2480TL();
    const ptbrNameCheck = ptbrReturnedData.length
    console.log("pt-br wrong name URL-B2B LCD-BL2480TL:",ptbrReturnedData)
    console.log(ptbrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ptbrNameCheck > 0){
            BL2480TLNameError.push("pt-br")
            BL2480TLNameErrorURL.push(ptbrReturnedData)
        }
    }
    //B2B-RURU-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const ruruReturnedData = await wrongnameB2BruruURLBL2480TL();
    const ruruNameCheck = ruruReturnedData.length
    console.log("ru-ru wrong name URL-B2B LCD-BL2480TL:",ruruReturnedData)
    console.log(ruruReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ruruNameCheck > 0){
            BL2480TLNameError.push("ru-ru")
            BL2480TLNameErrorURL.push(ruruReturnedData)
        }
    }
    //B2B-TRTR-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const trtrReturnedData = await wrongnameB2BtrtrURLBL2480TL();
    const trtrNameCheck = trtrReturnedData.length
    console.log("tr-tr wrong name URL-B2B LCD-BL2480TL:",trtrReturnedData)
    console.log(trtrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(trtrNameCheck > 0){
            BL2480TLNameError.push("tr-tr")
            BL2480TLNameErrorURL.push(trtrReturnedData)
        }
    }
    //B2B-VIVN-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const vivnReturnedData = await wrongnameB2BvivnURLBL2480TL();
    const vivnNameCheck = vivnReturnedData.length
    console.log("vi-vn wrong name URL-B2B LCD-BL2480TL:",vivnReturnedData)
    console.log(vivnReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(vivnNameCheck > 0){
            BL2480TLNameError.push("vi-vn")
            BL2480TLNameErrorURL.push(vivnReturnedData)
        }
    }
    //B2B-ZHHK-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const zhhkReturnedData = await wrongnameB2BzhhkURLBL2480TL();
    const zhhkNameCheck = zhhkReturnedData.length
    console.log("zh-hk wrong name URL-B2B LCD-BL2480TL:",zhhkReturnedData)
    console.log(zhhkReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(zhhkNameCheck > 0){
            BL2480TLNameError.push("zh-hk")
            BL2480TLNameErrorURL.push(zhhkReturnedData)
        }
    }
    //B2B-ZHTW-LCD-BL2480TL-name must be monitor/bl2480tl.html
    const zhtwReturnedData = await wrongnameB2BzhtwURLBL2480TL();
    const zhtwNameCheck = zhtwReturnedData.length
    console.log("zh-tw wrong name URL-B2B LCD-BL2480TL:",zhtwReturnedData)
    console.log(zhtwReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(zhtwNameCheck > 0){
            BL2480TLNameError.push("zh-tw")
            BL2480TLNameErrorURL.push(zhtwReturnedData)
        }
    }

    //放全部國家的後面
    if(BL2480TLNameError.length >0){
        throw new Error(`${publishModel} URL name must be ${publishProductURLName}, but it is wrong now. Here is RO name: ${BL2480TLNameError} And wrong URL: ${BL2480TLNameErrorURL}`)
    }
})
