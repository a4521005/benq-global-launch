const { Given, When, Then, Before, After } = require('@cucumber/cucumber')
const { expect } = require('chai')
const fs = require('fs');
const puppeteer = require('puppeteer')
const request = require("request-promise");
const cheerio = require("cheerio");
const cicGA="?utm_source=autotest&utm_medium=CIC"

Before({timeout: 24 * 5000},async function () {
    this.browser = await puppeteer.launch({ 
        executablePath:
        "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
        // MAC路徑:
        // executablePath:
        // '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome',
        headless:true,//有無需要開視窗,false要開,true不開
        slowMo:100,// slow down by 100ms
        devtools:false//有無需要開啟開發人員工具
    })
    this.page = await this.browser.newPage()
    await this.page.setViewport({width:1200,height:1000})
    await this.page.setDefaultTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
    await this.page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
})

After({timeout: 12 * 5000},async function () {
    await this.browser.close()
})

//TH685P
const excelToTH685PRONS =[
    "ar-me",
    "bg-bg",
    "cs-cz",
    "de-at",
    "de-ch",
    "de-de",
    "el-gr",
    "en-ap",
    "en-au",
    "en-ba",
    "en-ca",
    "en-cee",
    "en-cy",
    "en-dk",
    "en-ee",
    "en-eu",
    "en-fi",
    "en-hk",
    "en-hr",
    "en-ie",
    "en-in",
    "en-is",
    "en-lu",
    "en-lv",
    "en-me",
    "en-mk",
    "en-mt",
    "en-my",
    "en-no",
    "en-rs",
    "en-sg",
    "en-si",
    "en-uk",
    "en-us",
    "es-ar",
    "es-co",
    "es-es",
    "es-la",
    "es-mx",
    "es-pe",
    "fr-ca",
    "fr-ch",
    "fr-fr",
    "hu-hu",
    "id-id",
    "it-it",
    "ja-jp",
    "ko-kr",
    "lt-lt",
    "nl-be",
    "nl-nl",
    "pl-pl",
    "pt-br",
    "pt-pt",
    "ro-ro",
    "ru-ru",
    "sk-sk",
    "sv-se",
    "th-th",
    "tr-tr",
    "uk-ua",
    "vi-vn",
    "zh-cn",
    "zh-hk",
    "zh-tw"
]
const excelToTH685PResult =[]
const excelToTH685PURL =[]

//Projector TH685P - publish check
const TH685PPublishError=[]
//B2C-ARME-Projector-TH685P-after 20220401
const afterB2CarmeTH685P=[]
const afterB2CarmeURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ar-me/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CarmeTH685P.push(url)
            }
    })
    return afterB2CarmeTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-BGBG-Projector-TH685P-after 20220401
const afterB2CbgbgTH685P=[]
const afterB2CbgbgURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/bg-bg/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CbgbgTH685P.push(url)
            }
    })
    return afterB2CbgbgTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-CSCZ-Projector-TH685P-after 20220401
const afterB2CcsczTH685P=[]
const afterB2CcsczURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/cs-cz/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CcsczTH685P.push(url)
            }
    })
    return afterB2CcsczTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-DEAT-Projector-TH685P-after 20220401
const afterB2CdeatTH685P=[]
const afterB2CdeatURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/de-at/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CdeatTH685P.push(url)
            }
    })
    return afterB2CdeatTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-DECH-Projector-TH685P-after 20220401
const afterB2CdechTH685P=[]
const afterB2CdechURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/de-ch/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CdechTH685P.push(url)
            }
    })
    return afterB2CdechTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-DEDE-Projector-TH685P-after 20220401
const afterB2CdedeTH685P=[]
const afterB2CdedeURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/de-de/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CdedeTH685P.push(url)
            }
    })
    return afterB2CdedeTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ELGR-Projector-TH685P-after 20220401
const afterB2CelgrTH685P=[]
const afterB2CelgrURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/el-gr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CelgrTH685P.push(url)
            }
    })
    return afterB2CelgrTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENAP-Projector-TH685P-after 20220401
const afterB2CenapTH685P=[]
const afterB2CenapURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-ap/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenapTH685P.push(url)
            }
    })
    return afterB2CenapTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENAU-Projector-TH685P-after 20220401
const afterB2CenauTH685P=[]
const afterB2CenauURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-au/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenauTH685P.push(url)
            }
    })
    return afterB2CenauTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENBA-Projector-TH685P-after 20220401
const afterB2CenbaTH685P=[]
const afterB2CenbaURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-ba/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenbaTH685P.push(url)
            }
    })
    return afterB2CenbaTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENCA-Projector-TH685P-after 20220401
const afterB2CencaTH685P=[]
const afterB2CencaURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-ca/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CencaTH685P.push(url)
            }
    })
    return afterB2CencaTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENCEE-Projector-TH685P-after 20220401
const afterB2CenceeTH685P=[]
const afterB2CenceeURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-cee/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenceeTH685P.push(url)
            }
    })
    return afterB2CenceeTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENCY-Projector-TH685P-after 20220401
const afterB2CencyTH685P=[]
const afterB2CencyURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-cy/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CencyTH685P.push(url)
            }
    })
    return afterB2CencyTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENDK-Projector-TH685P-after 202202401
const afterB2CendkTH685P=[]
const afterB2CendkURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-dk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CendkTH685P.push(url)
            }
    })
    return afterB2CendkTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENEE-Projector-TH685P-after 20220401
const afterB2CeneeTH685P=[]
const afterB2CeneeURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-ee/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CeneeTH685P.push(url)
            }
    })
    return afterB2CeneeTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENEU-Projector-TH685P-after 20220401
const afterB2CeneuTH685P=[]
const afterB2CeneuURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-eu/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CeneuTH685P.push(url)
            }
    })
    return afterB2CeneuTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENFI-Projector-TH685P-after 20220401
const afterB2CenfiTH685P=[]
const afterB2CenfiURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-fi/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenfiTH685P.push(url)
            }
    })
    return afterB2CenfiTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENHK-Projector-TH685P-after 20220401
const afterB2CenhkTH685P=[]
const afterB2CenhkURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-hk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenhkTH685P.push(url)
            }
    })
    return afterB2CenhkTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENHR-Projector-TH685P-after 20220401
const afterB2CenhrTH685P=[]
const afterB2CenhrURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-hr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenhrTH685P.push(url)
            }
    })
    return afterB2CenhrTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENIE-Projector-TH685P-after 20220401
const afterB2CenieTH685P=[]
const afterB2CenieURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-ie/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenieTH685P.push(url)
            }
    })
    return afterB2CenieTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENIN-Projector-TH685P-after 20220401
const afterB2CeninTH685P=[]
const afterB2CeninURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-in/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CeninTH685P.push(url)
            }
    })
    return afterB2CeninTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENIS-Projector-TH685P-after 20220401
const afterB2CenisTH685P=[]
const afterB2CenisURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-is/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenisTH685P.push(url)
            }
    })
    return afterB2CenisTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENLU-Projector-TH685P-after 20220401
const afterB2CenluTH685P=[]
const afterB2CenluURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-lu/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenluTH685P.push(url)
            }
    })
    return afterB2CenluTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENLV-Projector-TH685P-after 20220401
const afterB2CenlvTH685P=[]
const afterB2CenlvURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-lv/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenlvTH685P.push(url)
            }
    })
    return afterB2CenlvTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENME-Projector-TH685P-after 20220401
const afterB2CenmeTH685P=[]
const afterB2CenmeURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-me/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenmeTH685P.push(url)
            }
    })
    return afterB2CenmeTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENMK-Projector-TH685P-after 20220401
const afterB2CenmkTH685P=[]
const afterB2CenmkURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-mk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenmkTH685P.push(url)
            }
    })
    return afterB2CenmkTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENMT-Projector-TH685P-after 20220401
const afterB2CenmtTH685P=[]
const afterB2CenmtURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-mt/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenmtTH685P.push(url)
            }
    })
    return afterB2CenmtTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENMY-Projector-TH685P-after 20220401
const afterB2CenmyTH685P=[]
const afterB2CenmyURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-my/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenmyTH685P.push(url)
            }
    })
    return afterB2CenmyTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENNO-Projector-TH685P-after 20220401
const afterB2CennoTH685P=[]
const afterB2CennoURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-no/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CennoTH685P.push(url)
            }
    })
    return afterB2CennoTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENRS-Projector-TH685P-after 20220401
const afterB2CenrsTH685P=[]
const afterB2CenrsURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-rs/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenrsTH685P.push(url)
            }
    })
    return afterB2CenrsTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENSG-Projector-TH685P-after 20220401
const afterB2CensgTH685P=[]
const afterB2CensgURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-sg/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CensgTH685P.push(url)
            }
    })
    return afterB2CensgTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENSI-Projector-TH685P-after 20220401
const afterB2CensiTH685P=[]
const afterB2CensiURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-si/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CensiTH685P.push(url)
            }
    })
    return afterB2CensiTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENUK-Projector-TH685P-after 20220401
const afterB2CenukTH685P=[]
const afterB2CenukURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-uk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenukTH685P.push(url)
            }
    })
    return afterB2CenukTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENUS-Projector-TH685P-after 20220401
const afterB2CenusTH685P=[]
const afterB2CenusURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-us/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenusTH685P.push(url)
            }
    })
    return afterB2CenusTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESAR-Projector-TH685P-after 20220401
const afterB2CesarTH685P=[]
const afterB2CesarURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-ar/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CesarTH685P.push(url)
            }
    })
    return afterB2CesarTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESCO-Projector-TH685P-after 20220401
const afterB2CescoTH685P=[]
const afterB2CescoURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-co/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CescoTH685P.push(url)
            }
    })
    return afterB2CescoTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESES-Projector-TH685P-after 20220401
const afterB2CesesTH685P=[]
const afterB2CesesURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-es/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CesesTH685P.push(url)
            }
    })
    return afterB2CesesTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESLA-Projector-TH685P-after 20220401
const afterB2CeslaTH685P=[]
const afterB2CeslaURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/es-la/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CeslaTH685P.push(url)
            }
    })
    return afterB2CeslaTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESMX-Projector-TH685P-after 20220401
const afterB2CesmxTH685P=[]
const afterB2CesmxURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/es-mx/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CesmxTH685P.push(url)
            }
    })
    return afterB2CesmxTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESPE-Projector-TH685P-after 20220401
const afterB2CespeTH685P=[]
const afterB2CespeURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-pe/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CespeTH685P.push(url)
            }
    })
    return afterB2CespeTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-FRCA-Projector-TH685P-after 20220401
const afterB2CfrcaTH685P=[]
const afterB2CfrcaURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/fr-ca/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CfrcaTH685P.push(url)
            }
    })
    return afterB2CfrcaTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-FRCH-Projector-TH685P-after 20220401
const afterB2CfrchTH685P=[]
const afterB2CfrchURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/fr-ch/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CfrchTH685P.push(url)
            }
    })
    return afterB2CfrchTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-FRFR-Projector-TH685P-after 20220401
const afterB2CfrfrTH685P=[]
const afterB2CfrfrURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/fr-fr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CfrfrTH685P.push(url)
            }
    })
    return afterB2CfrfrTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-HUHU-Projector-TH685P-after 20220401
const afterB2ChuhuTH685P=[]
const afterB2ChuhuURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/hu-hu/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2ChuhuTH685P.push(url)
            }
    })
    return afterB2ChuhuTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-IDID-Projector-TH685P-after 20220401
const afterB2CididTH685P=[]
const afterB2CididURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/id-id/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CididTH685P.push(url)
            }
    })
    return afterB2CididTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ITIT-Projector-TH685P-after 20220401
const afterB2CititTH685P=[]
const afterB2CititURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/it-it/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CititTH685P.push(url)
            }
    })
    return afterB2CititTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-JAJP-Projector-TH685P-after 20220401
const afterB2CjajpTH685P=[]
const afterB2CjajpURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ja-jp/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CjajpTH685P.push(url)
            }
    })
    return afterB2CjajpTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-KOKR-Projector-TH685P-after 20220401
const afterB2CkokrTH685P=[]
const afterB2CkokrURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ko-kr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CkokrTH685P.push(url)
            }
    })
    return afterB2CkokrTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-LTLT-Projector-TH685P-after 20220401
const afterB2CltltTH685P=[]
const afterB2CltltURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/lt-lt/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CltltTH685P.push(url)
            }
    })
    return afterB2CltltTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-NLBE-Projector-TH685P-after 20220401
const afterB2CnlbeTH685P=[]
const afterB2CnlbeURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/nl-be/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CnlbeTH685P.push(url)
            }
    })
    return afterB2CnlbeTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-NLNL-Projector-TH685P-after 20220401
const afterB2CnlnlTH685P=[]
const afterB2CnlnlURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/nl-nl/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CnlnlTH685P.push(url)
            }
    })
    return afterB2CnlnlTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-PLPL-Projector-TH685P-after 20220401
const afterB2CplplTH685P=[]
const afterB2CplplURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/pl-pl/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CplplTH685P.push(url)
            }
    })
    return afterB2CplplTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-PTBR-Projector-TH685P-after 20220401
const afterB2CptbrTH685P=[]
const afterB2CptbrURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/pt-br/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CptbrTH685P.push(url)
            }
    })
    return afterB2CptbrTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-PTPT-Projector-TH685P-after 20220401
const afterB2CptptTH685P=[]
const afterB2CptptURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/pt-pt/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CptptTH685P.push(url)
            }
    })
    return afterB2CptptTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-RORO-Projector-TH685P-after 20220401
const afterB2CroroTH685P=[]
const afterB2CroroURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/ro-ro/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CroroTH685P.push(url)
            }
    })
    return afterB2CroroTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-RURU-Projector-TH685P-after 20220401
const afterB2CruruTH685P=[]
const afterB2CruruURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ru-ru/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CruruTH685P.push(url)
            }
    })
    return afterB2CruruTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-SKSK-Projector-TH685P-after 20220401
const afterB2CskskTH685P=[]
const afterB2CskskURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/sk-sk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CskskTH685P.push(url)
            }
    })
    return afterB2CskskTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-SVSE-Projector-TH685P-after 20220401
const afterB2CsvseTH685P=[]
const afterB2CsvseURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/sv-se/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CsvseTH685P.push(url)
            }
    })
    return afterB2CsvseTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-THTH-Projector-TH685P-after 20220401
const afterB2CththTH685P=[]
const afterB2CththURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/th-th/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CththTH685P.push(url)
            }
    })
    return afterB2CththTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-TRTR-Projector-TH685P-after 20220401
const afterB2CtrtrTH685P=[]
const afterB2CtrtrURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/tr-tr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CtrtrTH685P.push(url)
            }
    })
    return afterB2CtrtrTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-UKUA-Projector-TH685P-after 20220401
const afterB2CukuaTH685P=[]
const afterB2CukuaURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/uk-ua/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CukuaTH685P.push(url)
            }
    })
    return afterB2CukuaTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-VIVN-Projector-TH685P-after 20220401
const afterB2CvivnTH685P=[]
const afterB2CvivnURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/vi-vn/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CvivnTH685P.push(url)
            }
    })
    return afterB2CvivnTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ZHHK-Projector-TH685P-after 20220401
const afterB2CzhhkTH685P=[]
const afterB2CzhhkURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/zh-hk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CzhhkTH685P.push(url)
            }
    })
    return afterB2CzhhkTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ZHTW-Projector-TH685P-after 20220401
const afterB2CzhtwTH685P=[]
const afterB2CzhtwURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/zh-tw/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "th685p.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CzhtwTH685P.push(url)
            }
    })
    return afterB2CzhtwTH685P;
    } catch (error) {
      console.log(error);
    }
};

//Projector TH685P - URL name check
const TH685PNameError=[]
const TH685PNameErrorURL=[]
//B2C-ARME-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CarmeTH685P=[]
const wrongnameB2CarmeURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ar-me/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ar-me"
            const publishSeries = "projector"
            const publishUrl = "ar-me/projector"
            const publishUrlName = "ar-me/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CarmeTH685P.push(url)
            }
    })
    return wrongnameB2CarmeTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-BGBG-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CbgbgTH685P=[]
const wrongnameB2CbgbgURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/bg-bg/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "bg-bg"
            const publishSeries = "projector"
            const publishUrl = "bg-bg/projector"
            const publishUrlName = "bg-bg/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CbgbgTH685P.push(url)
            }
    })
    return wrongnameB2CbgbgTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-CSCZ-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CcsczTH685P=[]
const wrongnameB2CcsczURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/cs-cz/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "cs-cz"
            const publishSeries = "projector"
            const publishUrl = "cs-cz/projector"
            const publishUrlName = "cs-cz/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CcsczTH685P.push(url)
            }
    })
    return wrongnameB2CcsczTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-DEAT-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CdeatTH685P=[]
const wrongnameB2CdeatURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/de-at/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "de-at"
            const publishSeries = "projector"
            const publishUrl = "de-at/projector"
            const publishUrlName = "de-at/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CdeatTH685P.push(url)
            }
    })
    return wrongnameB2CdeatTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-DECH-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CdechTH685P=[]
const wrongnameB2CdechURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/de-ch/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "de-ch"
            const publishSeries = "projector"
            const publishUrl = "de-ch/projector"
            const publishUrlName = "de-ch/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CdechTH685P.push(url)
            }
    })
    return wrongnameB2CdechTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-DEDE-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CdedeTH685P=[]
const wrongnameB2CdedeURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/de-de/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "de-de"
            const publishSeries = "projector"
            const publishUrl = "de-de/projector"
            const publishUrlName = "de-de/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CdedeTH685P.push(url)
            }
    })
    return wrongnameB2CdedeTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ELGR-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CelgrTH685P=[]
const wrongnameB2CelgrURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/el-gr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "el-gr"
            const publishSeries = "projector"
            const publishUrl = "el-gr/projector"
            const publishUrlName = "el-gr/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CelgrTH685P.push(url)
            }
    })
    return wrongnameB2CelgrTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENAP-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CenapTH685P=[]
const wrongnameB2CenapURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-ap/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-ap"
            const publishSeries = "projector"
            const publishUrl = "en-ap/projector"
            const publishUrlName = "en-ap/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenapTH685P.push(url)
            }
    })
    return wrongnameB2CenapTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENAU-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CenauTH685P=[]
const wrongnameB2CenauURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-au/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-au"
            const publishSeries = "projector"
            const publishUrl = "en-au/projector"
            const publishUrlName = "en-au/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenauTH685P.push(url)
            }
    })
    return wrongnameB2CenauTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENBA-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CenbaTH685P=[]
const wrongnameB2CenbaURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-ba/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-ba"
            const publishSeries = "projector"
            const publishUrl = "en-ba/projector"
            const publishUrlName = "en-ba/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenbaTH685P.push(url)
            }
    })
    return wrongnameB2CenbaTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENCA-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CencaTH685P=[]
const wrongnameB2CencaURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-ca/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-ca"
            const publishSeries = "projector"
            const publishUrl = "en-ca/projector"
            const publishUrlName = "en-ca/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CencaTH685P.push(url)
            }
    })
    return wrongnameB2CencaTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENCEE-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CenceeTH685P=[]
const wrongnameB2CenceeURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-cee/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-cee"
            const publishSeries = "projector"
            const publishUrl = "en-cee/projector"
            const publishUrlName = "en-cee/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenceeTH685P.push(url)
            }
    })
    return wrongnameB2CenceeTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENCY-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CencyTH685P=[]
const wrongnameB2CencyURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-cy/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-cy"
            const publishSeries = "projector"
            const publishUrl = "en-cy/projector"
            const publishUrlName = "en-cy/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CencyTH685P.push(url)
            }
    })
    return wrongnameB2CencyTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENDK-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CendkTH685P=[]
const wrongnameB2CendkURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-dk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-dk"
            const publishSeries = "projector"
            const publishUrl = "en-dk/projector"
            const publishUrlName = "en-dk/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CendkTH685P.push(url)
            }
    })
    return wrongnameB2CendkTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENEE-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CeneeTH685P=[]
const wrongnameB2CeneeURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-ee/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-ee"
            const publishSeries = "projector"
            const publishUrl = "en-ee/projector"
            const publishUrlName = "en-ee/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CeneeTH685P.push(url)
            }
    })
    return wrongnameB2CeneeTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENEU-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CeneuTH685P=[]
const wrongnameB2CeneuURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-eu/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-eu"
            const publishSeries = "projector"
            const publishUrl = "en-eu/projector"
            const publishUrlName = "en-eu/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CeneuTH685P.push(url)
            }
    })
    return wrongnameB2CeneuTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENFI-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CenfiTH685P=[]
const wrongnameB2CenfiURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-fi/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-fi"
            const publishSeries = "projector"
            const publishUrl = "en-fi/projector"
            const publishUrlName = "en-fi/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenfiTH685P.push(url)
            }
    })
    return wrongnameB2CenfiTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENHK-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CenhkTH685P=[]
const wrongnameB2CenhkURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-hk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-hk"
            const publishSeries = "projector"
            const publishUrl = "en-hk/projector"
            const publishUrlName = "en-hk/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenhkTH685P.push(url)
            }
    })
    return wrongnameB2CenhkTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENHR-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CenhrTH685P=[]
const wrongnameB2CenhrURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-hr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-hr"
            const publishSeries = "projector"
            const publishUrl = "en-hr/projector"
            const publishUrlName = "en-hr/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenhrTH685P.push(url)
            }
    })
    return wrongnameB2CenhrTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENIE-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CenieTH685P=[]
const wrongnameB2CenieURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-ie/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-ie"
            const publishSeries = "projector"
            const publishUrl = "en-ie/projector"
            const publishUrlName = "en-ie/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenieTH685P.push(url)
            }
    })
    return wrongnameB2CenieTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENIN-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CeninTH685P=[]
const wrongnameB2CeninURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-in/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-in"
            const publishSeries = "projector"
            const publishUrl = "en-in/projector"
            const publishUrlName = "en-in/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CeninTH685P.push(url)
            }
    })
    return wrongnameB2CeninTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENIS-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CenisTH685P=[]
const wrongnameB2CenisURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-is/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-is"
            const publishSeries = "projector"
            const publishUrl = "en-is/projector"
            const publishUrlName = "en-is/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenisTH685P.push(url)
            }
    })
    return wrongnameB2CenisTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENLU-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CenluTH685P=[]
const wrongnameB2CenluURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-lu/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-lu"
            const publishSeries = "projector"
            const publishUrl = "en-lu/projector"
            const publishUrlName = "en-lu/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenluTH685P.push(url)
            }
    })
    return wrongnameB2CenluTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENLV-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CenlvTH685P=[]
const wrongnameB2CenlvURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-lv/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-lv"
            const publishSeries = "projector"
            const publishUrl = "en-lv/projector"
            const publishUrlName = "en-lv/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenlvTH685P.push(url)
            }
    })
    return wrongnameB2CenlvTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENME-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CenmeTH685P=[]
const wrongnameB2CenmeURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-me/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-me"
            const publishSeries = "projector"
            const publishUrl = "en-me/projector"
            const publishUrlName = "en-me/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenmeTH685P.push(url)
            }
    })
    return wrongnameB2CenmeTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENMK-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CenmkTH685P=[]
const wrongnameB2CenmkURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-mk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-mk"
            const publishSeries = "projector"
            const publishUrl = "en-mk/projector"
            const publishUrlName = "en-mk/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenmkTH685P.push(url)
            }
    })
    return wrongnameB2CenmkTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENMT-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CenmtTH685P=[]
const wrongnameB2CenmtURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-mt/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-mt"
            const publishSeries = "projector"
            const publishUrl = "en-mt/projector"
            const publishUrlName = "en-mt/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenmtTH685P.push(url)
            }
    })
    return wrongnameB2CenmtTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENMY-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CenmyTH685P=[]
const wrongnameB2CenmyURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-my/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-my"
            const publishSeries = "projector"
            const publishUrl = "en-my/projector"
            const publishUrlName = "en-my/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenmyTH685P.push(url)
            }
    })
    return wrongnameB2CenmyTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENNO-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CennoTH685P=[]
const wrongnameB2CennoURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-no/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-no"
            const publishSeries = "projector"
            const publishUrl = "en-no/projector"
            const publishUrlName = "en-no/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CennoTH685P.push(url)
            }
    })
    return wrongnameB2CennoTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENRS-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CenrsTH685P=[]
const wrongnameB2CenrsURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-rs/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-rs"
            const publishSeries = "projector"
            const publishUrl = "en-rs/projector"
            const publishUrlName = "en-rs/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenrsTH685P.push(url)
            }
    })
    return wrongnameB2CenrsTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENSG-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CensgTH685P=[]
const wrongnameB2CensgURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-sg/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-sg"
            const publishSeries = "projector"
            const publishUrl = "en-sg/projector"
            const publishUrlName = "en-sg/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CensgTH685P.push(url)
            }
    })
    return wrongnameB2CensgTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENSI-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CensiTH685P=[]
const wrongnameB2CensiURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-si/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-si"
            const publishSeries = "projector"
            const publishUrl = "en-si/projector"
            const publishUrlName = "en-si/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CensiTH685P.push(url)
            }
    })
    return wrongnameB2CensiTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENUK-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CenukTH685P=[]
const wrongnameB2CenukURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-uk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-uk"
            const publishSeries = "projector"
            const publishUrl = "en-uk/projector"
            const publishUrlName = "en-uk/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenukTH685P.push(url)
            }
    })
    return wrongnameB2CenukTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ENUS-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CenusTH685P=[]
const wrongnameB2CenusURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-us/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-us"
            const publishSeries = "projector"
            const publishUrl = "en-us/projector"
            const publishUrlName = "en-us/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenusTH685P.push(url)
            }
    })
    return wrongnameB2CenusTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESAR-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CesarTH685P=[]
const wrongnameB2CesarURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-ar/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-ar"
            const publishSeries = "projector"
            const publishUrl = "es-ar/projector"
            const publishUrlName = "es-ar/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CesarTH685P.push(url)
            }
    })
    return wrongnameB2CesarTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESCO-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CescoTH685P=[]
const wrongnameB2CescoURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-co/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-co"
            const publishSeries = "projector"
            const publishUrl = "es-co/projector"
            const publishUrlName = "es-co/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CescoTH685P.push(url)
            }
    })
    return wrongnameB2CescoTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESES-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CesesTH685P=[]
const wrongnameB2CesesURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-es/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-es"
            const publishSeries = "projector"
            const publishUrl = "es-es/projector"
            const publishUrlName = "es-es/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CesesTH685P.push(url)
            }
    })
    return wrongnameB2CesesTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESLA-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CeslaTH685P=[]
const wrongnameB2CeslaURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/es-la/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-la"
            const publishSeries = "projector"
            const publishUrl = "es-la/projector"
            const publishUrlName = "es-la/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CeslaTH685P.push(url)
            }
    })
    return wrongnameB2CeslaTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESMX-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CesmxTH685P=[]
const wrongnameB2CesmxURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/es-mx/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-mx"
            const publishSeries = "projector"
            const publishUrl = "es-mx/projector"
            const publishUrlName = "es-mx/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CesmxTH685P.push(url)
            }
    })
    return wrongnameB2CesmxTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ESPE-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CespeTH685P=[]
const wrongnameB2CespeURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-pe/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-pe"
            const publishSeries = "projector"
            const publishUrl = "es-pe/projector"
            const publishUrlName = "es-pe/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CespeTH685P.push(url)
            }
    })
    return wrongnameB2CespeTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-FRCA-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CfrcaTH685P=[]
const wrongnameB2CfrcaURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/fr-ca/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "fr-ca"
            const publishSeries = "projector"
            const publishUrl = "fr-ca/projector"
            const publishUrlName = "fr-ca/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CfrcaTH685P.push(url)
            }
    })
    return wrongnameB2CfrcaTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-FRCH-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CfrchTH685P=[]
const wrongnameB2CfrchURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/fr-ch/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "fr-ch"
            const publishSeries = "projector"
            const publishUrl = "fr-ch/projector"
            const publishUrlName = "fr-ch/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CfrchTH685P.push(url)
            }
    })
    return wrongnameB2CfrchTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-FRFR-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CfrfrTH685P=[]
const wrongnameB2CfrfrURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/fr-fr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "fr-fr"
            const publishSeries = "projector"
            const publishUrl = "fr-fr/projector"
            const publishUrlName = "fr-fr/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CfrfrTH685P.push(url)
            }
    })
    return wrongnameB2CfrfrTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-HUHU-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2ChuhuTH685P=[]
const wrongnameB2ChuhuURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/hu-hu/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "hu-hu"
            const publishSeries = "projector"
            const publishUrl = "hu-hu/projector"
            const publishUrlName = "hu-hu/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2ChuhuTH685P.push(url)
            }
    })
    return wrongnameB2ChuhuTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-IDID-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CididTH685P=[]
const wrongnameB2CididURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/id-id/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "id-id"
            const publishSeries = "projector"
            const publishUrl = "id-id/projector"
            const publishUrlName = "id-id/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CididTH685P.push(url)
            }
    })
    return wrongnameB2CididTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ITIT-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CititTH685P=[]
const wrongnameB2CititURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/it-it/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "it-it"
            const publishSeries = "projector"
            const publishUrl = "it-it/projector"
            const publishUrlName = "it-it/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CititTH685P.push(url)
            }
    })
    return wrongnameB2CititTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-JAJP-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CjajpTH685P=[]
const wrongnameB2CjajpURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ja-jp/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ja-jp"
            const publishSeries = "projector"
            const publishUrl = "ja-jp/projector"
            const publishUrlName = "ja-jp/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CjajpTH685P.push(url)
            }
    })
    return wrongnameB2CjajpTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-KOKR-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CkokrTH685P=[]
const wrongnameB2CkokrURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ko-kr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ko-kr"
            const publishSeries = "projector"
            const publishUrl = "ko-kr/projector"
            const publishUrlName = "ko-kr/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CkokrTH685P.push(url)
            }
    })
    return wrongnameB2CkokrTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-LTLT-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CltltTH685P=[]
const wrongnameB2CltltURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/lt-lt/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "lt-lt"
            const publishSeries = "projector"
            const publishUrl = "lt-lt/projector"
            const publishUrlName = "lt-lt/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CltltTH685P.push(url)
            }
    })
    return wrongnameB2CltltTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-NLBE-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CnlbeTH685P=[]
const wrongnameB2CnlbeURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/nl-be/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "nl-be"
            const publishSeries = "projector"
            const publishUrl = "nl-be/projector"
            const publishUrlName = "nl-be/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CnlbeTH685P.push(url)
            }
    })
    return wrongnameB2CnlbeTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-NLNL-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CnlnlTH685P=[]
const wrongnameB2CnlnlURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/nl-nl/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "nl-nl"
            const publishSeries = "projector"
            const publishUrl = "nl-nl/projector"
            const publishUrlName = "nl-nl/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CnlnlTH685P.push(url)
            }
    })
    return wrongnameB2CnlnlTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-PLPL-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CplplTH685P=[]
const wrongnameB2CplplURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/pl-pl/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "pl-pl"
            const publishSeries = "projector"
            const publishUrl = "pl-pl/projector"
            const publishUrlName = "pl-pl/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CplplTH685P.push(url)
            }
    })
    return wrongnameB2CplplTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-PTBR-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CptbrTH685P=[]
const wrongnameB2CptbrURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/pt-br/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "pt-br"
            const publishSeries = "projector"
            const publishUrl = "pt-br/projector"
            const publishUrlName = "pt-br/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CptbrTH685P.push(url)
            }
    })
    return wrongnameB2CptbrTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-PTPT-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CptptTH685P=[]
const wrongnameB2CptptURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/pt-pt/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "pt-pt"
            const publishSeries = "projector"
            const publishUrl = "pt-pt/projector"
            const publishUrlName = "pt-pt/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CptptTH685P.push(url)
            }
    })
    return wrongnameB2CptptTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-RORO-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CroroTH685P=[]
const wrongnameB2CroroURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/ro-ro/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ro-ro"
            const publishSeries = "projector"
            const publishUrl = "ro-ro/projector"
            const publishUrlName = "ro-ro/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CroroTH685P.push(url)
            }
    })
    return wrongnameB2CroroTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-RURU-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CruruTH685P=[]
const wrongnameB2CruruURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ru-ru/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ru-ru"
            const publishSeries = "projector"
            const publishUrl = "ru-ru/projector"
            const publishUrlName = "ru-ru/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CruruTH685P.push(url)
            }
    })
    return wrongnameB2CruruTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-SKSK-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CskskTH685P=[]
const wrongnameB2CskskURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/sk-sk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "sk-sk"
            const publishSeries = "projector"
            const publishUrl = "sk-sk/projector"
            const publishUrlName = "sk-sk/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CskskTH685P.push(url)
            }
    })
    return wrongnameB2CskskTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-SVSE-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CsvseTH685P=[]
const wrongnameB2CsvseURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/sv-se/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "sv-se"
            const publishSeries = "projector"
            const publishUrl = "sv-se/projector"
            const publishUrlName = "sv-se/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CsvseTH685P.push(url)
            }
    })
    return wrongnameB2CsvseTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-THTH-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CththTH685P=[]
const wrongnameB2CththURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/th-th/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "th-th"
            const publishSeries = "projector"
            const publishUrl = "th-th/projector"
            const publishUrlName = "th-th/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CththTH685P.push(url)
            }
    })
    return wrongnameB2CththTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-TRTR-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CtrtrTH685P=[]
const wrongnameB2CtrtrURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/tr-tr/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "tr-tr"
            const publishSeries = "projector"
            const publishUrl = "tr-tr/projector"
            const publishUrlName = "tr-tr/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CtrtrTH685P.push(url)
            }
    })
    return wrongnameB2CtrtrTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-UKUA-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CukuaTH685P=[]
const wrongnameB2CukuaURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/uk-ua/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "uk-ua"
            const publishSeries = "projector"
            const publishUrl = "uk-ua/projector"
            const publishUrlName = "uk-ua/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CukuaTH685P.push(url)
            }
    })
    return wrongnameB2CukuaTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-VIVN-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CvivnTH685P=[]
const wrongnameB2CvivnURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/vi-vn/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "vi-vn"
            const publishSeries = "projector"
            const publishUrl = "vi-vn/projector"
            const publishUrlName = "vi-vn/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CvivnTH685P.push(url)
            }
    })
    return wrongnameB2CvivnTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ZHHK-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CzhhkTH685P=[]
const wrongnameB2CzhhkURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/zh-hk/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "zh-hk"
            const publishSeries = "projector"
            const publishUrl = "zh-hk/projector"
            const publishUrlName = "zh-hk/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CzhhkTH685P.push(url)
            }
    })
    return wrongnameB2CzhhkTH685P;
    } catch (error) {
      console.log(error);
    }
};
//B2C-ZHTW-Projector-TH685P-name must be projector/gaming-projector/th685p.html
const wrongnameB2CzhtwTH685P=[]
const wrongnameB2CzhtwURLTH685P = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/zh-tw/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "zh-tw"
            const publishSeries = "projector"
            const publishUrl = "zh-tw/projector"
            const publishUrlName = "zh-tw/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "th685p.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CzhtwTH685P.push(url)
            }
    })
    return wrongnameB2CzhtwTH685P;
    } catch (error) {
      console.log(error);
    }
};
//Test case
Given("TH685P must be published on global site after 20220401",{timeout: 1000 * 5000},async function(){
    //After 2022/01/01
    //global setting
    const publishModel = "th685p"
    const date = new Date()
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()
    function wholeMonth(){
        const getmonth = date.getMonth() + 1
        if(getmonth<10){
            wholeMonth =  "0"+getmonth 
            return wholeMonth
        }else{
            wholeMonth = getmonth 
            return wholeMonth
        }
    }
    function wholeDate(){
        const getDay = date.getDate()
        if(getDay<10){
            wholeDate =  "0"+getDay
            return wholeDate
        }else{
            wholeDate = getDay 
            return wholeDate
        }
    }
    const wholemonth = wholeMonth()
    const wholeday = wholeDate()
    const wholeyear = date.getFullYear()
    const fullDate = `${wholeyear}/${wholemonth}/${wholeday}`
    // console.log("wholemonth:",wholemonth)
    // console.log("wholeday:",wholeday)
    // console.log("wholeyear:",wholeyear)
    console.log("fullDate:",fullDate)
    // console.log("year:",year)
    // console.log("month:",month)
    // console.log("day:",day)
    const launchDate = "2022/04/01"

    //B2C-ARME-Projector-TH685P-after 20220401
    const armeReturnedData = await afterB2CarmeURLTH685P();
    // console.log("After returnedData",armeReturnedData)
    const armepublishCheck = armeReturnedData.length
    console.log("ar-me All publish URL-B2C Projector - TH685P:",armeReturnedData)
    console.log(armeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(armepublishCheck ==0){
            // afterB2CarmeTH685P.push("ar-me")
            TH685PPublishError.push("ar-me")

        }
    }
    //B2C-BGBG-Projector-TH685P-after 20220401
    const bgbgReturnedData = await afterB2CbgbgURLTH685P();
    // console.log("After returnedData",bgbgReturnedData)
    const bgbgpublishCheck = bgbgReturnedData.length
    console.log("bg-bg All publish URL-B2C Projector-TH685P:",bgbgReturnedData)
    console.log(bgbgReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(bgbgpublishCheck ==0){
            // afterB2CbgbgTH685P.push("bg-bg")
            TH685PPublishError.push("bg-bg")

        }
    }
    //B2C-CSCZ-Projector-TH685P-after 20220401
    const csczReturnedData = await afterB2CcsczURLTH685P();
    // console.log("After returnedData",csczReturnedData)
    const csczpublishCheck = csczReturnedData.length
    console.log("cs-cz All publish URL-B2C Projector-TH685P:",csczReturnedData)
    console.log(csczReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(csczpublishCheck ==0){
            // afterB2CcsczTH685P.push("cs-cz")
            TH685PPublishError.push("cs-cz")

        }
    }
    //B2C-DEAT-Projector-TH685P-after 20220401
    const deatReturnedData = await afterB2CdeatURLTH685P();
    // console.log("After returnedData",deatReturnedData)
    const deatpublishCheck = deatReturnedData.length
    console.log("de-at All publish URL-B2C Projector-TH685P:",deatReturnedData)
    console.log(deatReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(deatpublishCheck ==0){
            // afterB2Cdea-TH685P.push("de-at")
        -TH685PPublishError.push("de-at")

        }
    }
    //B2C-DECH-Projector-TH685P-after 20220401
    const dechReturnedData = await afterB2CdechURLTH685P();
    // console.log("After returnedData",dechReturnedData)
    const dechpublishCheck = dechReturnedData.length
    console.log("de-ch All publish URL-B2C Projector-TH685P:",dechReturnedData)
    console.log(dechReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(dechpublishCheck ==0){
            // afterB2CdechTH685P.push("de-ch")
            TH685PPublishError.push("de-ch")

        }
    }
    //B2C-DEDE-Projector-TH685P-after 20220401
    const dedeReturnedData = await afterB2CdedeURLTH685P();
    // console.log("After returnedData",dedeReturnedData)
    const dedepublishCheck = dedeReturnedData.length
    console.log("de-de All publish URL-B2C Projector-TH685P:",dedeReturnedData)
    console.log(dedeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(dedepublishCheck ==0){
            // afterB2CdedeTH685P.push("de-de")
            TH685PPublishError.push("de-de")

        }
    }
    //B2C-ELGR-Projector-TH685P-after 20220401
    const elgrReturnedData = await afterB2CelgrURLTH685P();
    // console.log("After returnedData",elgrReturnedData)
    const elgrpublishCheck = elgrReturnedData.length
    console.log("el-gr All publish URL-B2C Projector-TH685P:",elgrReturnedData)
    console.log(elgrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(elgrpublishCheck ==0){
            // afterB2CelgrTH685P.push("el-gr")
            TH685PPublishError.push("el-gr")

        }
    }
    //B2C-ENAP-Projector-TH685P-after 20220401
    const enapReturnedData = await afterB2CenapURLTH685P();
    // console.log("After returnedData",enapReturnedData)
    const enappublishCheck = enapReturnedData.length
    console.log("en-ap All publish URL-B2C Projector - TH685P:",enapReturnedData)
    console.log(enapReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(enappublishCheck ==0){
            // afterB2CenapTH685P.push("en-ap")
            TH685PPublishError.push("en-ap")

        }
    }
    //B2C-ENAU-Projector-TH685P-after 20220401
    const enauReturnedData = await afterB2CenauURLTH685P();
    // console.log("After returnedData",enauReturnedData)
    const enaupublishCheck = enauReturnedData.length
    console.log("en-au All publish URL-B2C Projector - TH685P:",enauReturnedData)
    console.log(enauReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(enaupublishCheck ==0){
            // afterB2CenauTH685P.push("en-au")
            TH685PPublishError.push("en-au")

        }
    }
    //B2C-ENBA-Projector-TH685P-after 20220401
    const enbaReturnedData = await afterB2CenbaURLTH685P();
    // console.log("After returnedData",enbaReturnedData)
    const enbapublishCheck = enbaReturnedData.length
    console.log("en-ba All publish URL-B2C Projector - TH685P:",enbaReturnedData)
    console.log(enbaReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(enbapublishCheck ==0){
            // afterB2CenbaTH685P.push("en-ba")
            TH685PPublishError.push("en-ba")

        }
    }
    //B2C-ENCA-Projector-TH685P-after 20220401
    const encaReturnedData = await afterB2CencaURLTH685P();
    // console.log("After returnedData",encaReturnedData)
    const encapublishCheck = encaReturnedData.length
    console.log("en-ca All publish URL-B2C Projector - TH685P:",encaReturnedData)
    console.log(encaReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(encapublishCheck ==0){
            // afterB2CencaTH685P.push("en-ca")
            TH685PPublishError.push("en-ca")
        }
    }
    //B2C-ENCEE-Projector-TH685P-after 20220401
    const enceeReturnedData = await afterB2CenceeURLTH685P();
    // console.log("After returnedData",enceeReturnedData)
    const enceepublishCheck = enceeReturnedData.length
    console.log("en-cee All publish URL-B2C Projector - TH685P:",enceeReturnedData)
    console.log(enceeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(enceepublishCheck ==0){
            // afterB2CencaTH685P.push("en-cee")
            TH685PPublishError.push("en-cee")
        }
    }
    //B2C-ENCY-Projector-TH685P-after 20220401
    const encyReturnedData = await afterB2CencyURLTH685P();
    // console.log("After returnedData",encyReturnedData)
    const encypublishCheck = encyReturnedData.length
    console.log("en-cy All publish URL-B2C Projector - TH685P:",encyReturnedData)
    console.log(encyReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(encypublishCheck ==0){
            // afterB2CencaTH685P.push("en-cy")
            TH685PPublishError.push("en-cy")
        }
    }
    //B2C-ENDK-Projector-TH685P-after 20220401
    const endkReturnedData = await afterB2CendkURLTH685P();
    // console.log("After returnedData",endkReturnedData)
    const endkpublishCheck = endkReturnedData.length
    console.log("en-dk All publish URL-B2C Projector - TH685P:",endkReturnedData)
    console.log(endkReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(endkpublishCheck ==0){
            // afterB2CencaTH685P.push("en-dk")
            TH685PPublishError.push("en-dk")
        }
    }
    //B2C-ENEE-Projector-TH685P-after 20220401
    const eneeReturnedData = await afterB2CeneeURLTH685P();
    // console.log("After returnedData",eneeReturnedData)
    const eneepublishCheck = eneeReturnedData.length
    console.log("en-ee All publish URL-B2C Projector-TH685P:",eneeReturnedData)
    console.log(eneeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(eneepublishCheck ==0){
            TH685PPublishError.push("en-ee")
        }
    }
    //B2C-ENEU-Projector-TH685P-after 20220401
    const eneuReturnedData = await afterB2CeneuURLTH685P();
    // console.log("After returnedData",eneuReturnedData)
    const eneupublishCheck = eneuReturnedData.length
    console.log("en-eu All publish URL-B2C Projector-TH685P:",eneuReturnedData)
    console.log(eneuReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(eneupublishCheck ==0){
            TH685PPublishError.push("en-eu")
        }
    }
    //B2C-ENFI-Projector-TH685P-after 20220401
    const enfiReturnedData = await afterB2CenfiURLTH685P();
    // console.log("After returnedData",enfiReturnedData)
    const enfipublishCheck = enfiReturnedData.length
    console.log("en-fi All publish URL-B2C Projector-TH685P:",enfiReturnedData)
    console.log(enfiReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(enfipublishCheck ==0){
            TH685PPublishError.push("en-fi")
        }
    }
    //B2C-ENHK-Projector-TH685P-after 20220401
    const enhkReturnedData = await afterB2CenhkURLTH685P();
    // console.log("After returnedData",enhkReturnedData)
    const enhkpublishCheck = enhkReturnedData.length
    console.log("en-hk All publish URL-B2C Projector - TH685P:",enhkReturnedData)
    console.log(enhkReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(enhkpublishCheck ==0){
            TH685PPublishError.push("en-hk")
        }
    }
    //B2C-ENHR-Projector-TH685P-after 20220401
    const enhrReturnedData = await afterB2CenhrURLTH685P();
    // console.log("After returnedData",enhrReturnedData)
    const enhrpublishCheck = enhrReturnedData.length
    console.log("en-hr All publish URL-B2C Projector-TH685P:",enhrReturnedData)
    console.log(enhrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(enhrpublishCheck ==0){
            TH685PPublishError.push("en-hr")
        }
    }
    //B2C-ENIE-Projector-TH685P-after 20TH685P
    const enieReturnedData = await afterB2CenieURLTH685P();
    // console.log("After returnedData",enieReturnedData)
    const eniepublishCheck = enieReturnedData.length
    console.log("en-ie All publish URL-B2C Projector-TH685P:",enieReturnedData)
    console.log(enieReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(eniepublishCheck ==0){
            TH685PPublishError.push("en-ie")
        }
    }
    //B2C-ENIN-Projector-TH685P-after 20TH685P
    const eninReturnedData = await afterB2CeninURLTH685P();
    // console.log("After returnedData",eninReturnedData)
    const eninpublishCheck = eninReturnedData.length
    console.log("en-in All publish URL-B2C Projector - TH685P:",eninReturnedData)
    console.log(eninReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(eninpublishCheck ==0){
            TH685PPublishError.push("en-in")
        }
    }
    //B2C-ENIS-Projector-TH685P-after 20220401
    const enisReturnedData = await afterB2CenisURLTH685P();
    // console.log("After returnedData",enisReturnedData)
    const enispublishCheck = enisReturnedData.length
    console.log("en-is All publish URL-B2C Projector-TH685P:",enisReturnedData)
    console.log(enisReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(enispublishCheck ==0){
            TH685PPublishError.push("en-is")
        }
    }
    //B2C-ENLU-Projector-TH685P-after 20220401
    const enluReturnedData = await afterB2CenluURLTH685P();
    // console.log("After returnedData",enluReturnedData)
    const enlupublishCheck = enluReturnedData.length
    console.log("en-lu All publish URL-B2C Projector-TH685P:",enluReturnedData)
    console.log(enluReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(enlupublishCheck ==0){
            TH685PPublishError.push("en-lu")
        }
    }
    //B2C-ENLV-Projector-TH685P-after 20220401
    const enlvReturnedData = await afterB2CenlvURLTH685P();
    // console.log("After returnedData",enlvReturnedData)
    const enlvpublishCheck = enlvReturnedData.length
    console.log("en-lv All publish URL-B2C Projector-TH685P:",enlvReturnedData)
    console.log(enlvReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(enlvpublishCheck ==0){
            TH685PPublishError.push("en-lv")
        }
    }
    //B2C-ENME-Projector-TH685P-after 20220401
    const enmeReturnedData = await afterB2CenmeURLTH685P();
    // console.log("After returnedData",enmeReturnedData)
    const enmepublishCheck = enmeReturnedData.length
    console.log("en-me All publish URL-B2C Projector - TH685P:",enmeReturnedData)
    console.log(enmeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(enmepublishCheck ==0){
            TH685PPublishError.push("en-me")
        }
    }
    //B2C-ENMK-Projector-TH685P-after 20220401
    const enmkReturnedData = await afterB2CenmkURLTH685P();
    // console.log("After returnedData",enmkReturnedData)
    const enmkpublishCheck = enmkReturnedData.length
    console.log("en-mk All publish URL-B2C Projector-TH685P:",enmkReturnedData)
    console.log(enmkReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(enmkpublishCheck ==0){
            TH685PPublishError.push("en-mk")
        }
    }
    //B2C-ENMT-Projector-TH685P-after 20220401
    const enmtReturnedData = await afterB2CenmtURLTH685P();
    // console.log("After returnedData",enmtReturnedData)
    const enmtpublishCheck = enmtReturnedData.length
    console.log("en-mt All publish URL-B2C Projector-TH685P:",enmtReturnedData)
    console.log(enmtReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(enmtpublishCheck ==0){
            TH685PPublishError.push("en-mt")
        }
    }
    //B2C-ENMY-Projector-TH685P-after 20220401
    const enmyReturnedData = await afterB2CenmyURLTH685P();
    // console.log("After returnedData",enmyReturnedData)
    const enmypublishCheck = enmyReturnedData.length
    console.log("en-my All publish URL-B2C Projector - TH685P:",enmyReturnedData)
    console.log(enmyReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(enmypublishCheck ==0){
            TH685PPublishError.push("en-my")
        }
    }
    //B2C-ENNO-Projector-TH685P-after 20220401
    const ennoReturnedData = await afterB2CennoURLTH685P();
    // console.log("After returnedData",ennoReturnedData)
    const ennopublishCheck = ennoReturnedData.length
    console.log("en-no All publish URL-B2C Projector-TH685P:",ennoReturnedData)
    console.log(ennoReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(ennopublishCheck ==0){
            TH685PPublishError.push("en-no")
        }
    }
    //B2C-ENRS-Projector-TH685P-after 20220401
    const enrsReturnedData = await afterB2CenrsURLTH685P();
    // console.log("After returnedData",enrsReturnedData)
    const enrspublishCheck = enrsReturnedData.length
    console.log("en-rs All publish URL-B2C Projector-TH685P:",enrsReturnedData)
    console.log(enrsReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(enrspublishCheck ==0){
            TH685PPublishError.push("en-rs")
        }
    }
    //B2C-ENSG-Projector-TH685P-after 20220401
    const ensgReturnedData = await afterB2CensgURLTH685P();
    // console.log("After returnedData",ensgReturnedData)
    const ensgpublishCheck = ensgReturnedData.length
    console.log("en-sg All publish URL-B2C Projector - TH685P:",ensgReturnedData)
    console.log(ensgReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(ensgpublishCheck ==0){
            TH685PPublishError.push("en-sg")
        }
    }
    //B2C-ENSI-Projector-TH685P-after 20220401
    const ensiReturnedData = await afterB2CensiURLTH685P();
    // console.log("After returnedData",ensiReturnedData)
    const ensipublishCheck = ensiReturnedData.length
    console.log("en-si All publish URL-B2C Projector-TH685P:",ensiReturnedData)
    console.log(ensiReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(ensipublishCheck ==0){
            TH685PPublishError.push("en-si")
        }
    }
    //B2C-ENUK-Projector-TH685P-after 20220401
    const enukReturnedData = await afterB2CenukURLTH685P();
    // console.log("After returnedData",enukReturnedData)
    const enukpublishCheck = enukReturnedData.length
    console.log("en-uk All publish URL-B2C Projector-TH685P:",enukReturnedData)
    console.log(enukReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(enukpublishCheck ==0){
            TH685PPublishError.push("en-uk")
        }
    }
    //B2C-ENUS-Projector-TH685P-after 20220401
    const enusReturnedData = await afterB2CenusURLTH685P();
    // console.log("After returnedData",enusReturnedData)
    const enuspublishCheck = enusReturnedData.length
    console.log("en-us All publish URL-B2C Projector - TH685P:",enusReturnedData)
    console.log(enusReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(enuspublishCheck ==0){
            TH685PPublishError.push("en-us")
        }
    }
    //B2C-ESAR-Projector-TH685P-after 20220401
    const esarReturnedData = await afterB2CesarURLTH685P();
    // console.log("After returnedData",esarReturnedData)
    const esarpublishCheck = esarReturnedData.length
    console.log("es-ar All publish URL-B2C Projector - TH685P:",esarReturnedData)
    console.log(esarReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(esarpublishCheck ==0){
            TH685PPublishError.push("es-ar")
        }
    }
    //B2C-ESCO-Projector-TH685P-after 20220401
    const escoReturnedData = await afterB2CescoURLTH685P();
    // console.log("After returnedData",escoReturnedData)
    const escopublishCheck = escoReturnedData.length
    console.log("es-co All publish URL-B2C Projector - TH685P:",escoReturnedData)
    console.log(escoReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(escopublishCheck ==0){
            TH685PPublishError.push("es-co")
        }
    }
    //B2C-ESES-Projector-TH685P-after 20220401
    const esesReturnedData = await afterB2CesesURLTH685P();
    // console.log("After returnedData",esesReturnedData)
    const esespublishCheck = esesReturnedData.length
    console.log("es-es All publish URL-B2C Projector - TH685P:",esesReturnedData)
    console.log(esesReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(esespublishCheck ==0){
            TH685PPublishError.push("es-es")
        }
    }
    //B2C-ESLA-Projector-TH685P-after 20220401
    const eslaReturnedData = await afterB2CeslaURLTH685P();
    // console.log("After returnedData",eslaReturnedData)
    const eslapublishCheck = eslaReturnedData.length
    console.log("es-la All publish URL-B2C Projector - TH685P:",eslaReturnedData)
    console.log(eslaReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(eslapublishCheck ==0){
            TH685PPublishError.push("es-la")
        }
    }
    //B2C-ESMX-Projector-TH685P-after 20220401
    const esmxReturnedData = await afterB2CesmxURLTH685P();
    // console.log("After returnedData",esmxReturnedData)
    const esmxpublishCheck = esmxReturnedData.length
    console.log("es-mx All publish URL-B2C Projector - TH685P:",esmxReturnedData)
    console.log(esmxReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(esmxpublishCheck ==0){
            TH685PPublishError.push("es-mx")
        }
    }
    //B2C-ESPE-Projector-TH685P-after 20220401
    const espeReturnedData = await afterB2CespeURLTH685P();
    // console.log("After returnedData",espeReturnedData)
    const espepublishCheck = espeReturnedData.length
    console.log("es-pe All publish URL-B2C Projector - TH685P:",espeReturnedData)
    console.log(espeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(espepublishCheck ==0){
            TH685PPublishError.push("es-pe")
        }
    }
    //B2C-FRCA-Projector-TH685P-after 20220401
    const frcaReturnedData = await afterB2CfrcaURLTH685P();
    // console.log("After returnedData",frcaReturnedData)
    const frcapublishCheck = frcaReturnedData.length
    console.log("fr-ca All publish URL-B2C Projector - TH685P:",frcaReturnedData)
    console.log(frcaReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(frcapublishCheck ==0){
            TH685PPublishError.push("fr-ca")
        }
    }
    //B2C-FRCH-Projector-TH685P-after 20220401
    const frchReturnedData = await afterB2CfrchURLTH685P();
    // console.log("After returnedData",frchReturnedData)
    const frchpublishCheck = frchReturnedData.length
    console.log("fr-ch All publish URL-B2C Projector - TH685P:",frchReturnedData)
    console.log(frchReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(frchpublishCheck ==0){
            TH685PPublishError.push("fr-ch")
        }
    }
    //B2C-FRFR-Projector-TH685P-after 20220401
    const frfrReturnedData = await afterB2CfrfrURLTH685P();
    // console.log("After returnedData",frfrReturnedData)
    const frfrpublishCheck = frfrReturnedData.length
    console.log("fr-fr All publish URL-B2C Projector - TH685P:",frfrReturnedData)
    console.log(frfrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(frfrpublishCheck ==0){
            TH685PPublishError.push("fr-fr")
        }
    }
    //B2C-HUHU-Projector-TH685P-after 20220401
    const huhuReturnedData = await afterB2ChuhuURLTH685P();
    // console.log("After returnedData",huhuReturnedData)
    const huhupublishCheck = huhuReturnedData.length
    console.log("hu-hu All publish URL-B2C Projector - TH685P:",huhuReturnedData)
    console.log(huhuReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(huhupublishCheck ==0){
            TH685PPublishError.push("hu-hu")
        }
    }
    //B2C-IDID-Projector-TH685P-after 20220401
    const ididReturnedData = await afterB2CididURLTH685P();
    // console.log("After returnedData",ididReturnedData)
    const ididpublishCheck = ididReturnedData.length
    console.log("id-id All publish URL-B2C Projector - TH685P:",ididReturnedData)
    console.log(ididReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(ididpublishCheck ==0){
            TH685PPublishError.push("id-id")
        }
    }
    //B2C-ITIT-Projector-TH685P-after 20220401
    const ititReturnedData = await afterB2CititURLTH685P();
    // console.log("After returnedData",ititReturnedData)
    const ititpublishCheck = ititReturnedData.length
    console.log("it-it All publish URL-B2C Projector - TH685P:",ititReturnedData)
    console.log(ititReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(ititpublishCheck ==0){
            TH685PPublishError.push("it-it")
        }
    }
    //B2C-JAJP-Projector-TH685P-after 20220401
    const jajpReturnedData = await afterB2CjajpURLTH685P();
    // console.log("After returnedData",jajpReturnedData)
    const jajppublishCheck = jajpReturnedData.length
    console.log("ja-jp All publish URL-B2C Projector - TH685P:",jajpReturnedData)
    console.log(jajpReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(jajppublishCheck ==0){
            TH685PPublishError.push("ja-jp")
        }
    }
    //B2C-KOKR-Projector-TH685P-after 20220401
    const kokrReturnedData = await afterB2CkokrURLTH685P();
    // console.log("After returnedData",kokrReturnedData)
    const kokrpublishCheck = kokrReturnedData.length
    console.log("ko-kr All publish URL-B2C Projector - TH685P:",kokrReturnedData)
    console.log(kokrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(kokrpublishCheck ==0){
            TH685PPublishError.push("ko-kr")
        }
    }
    //B2C-LTLT-Projector-TH685P-after 20220401
    const ltltReturnedData = await afterB2CltltURLTH685P();
    // console.log("After returnedData",ltltReturnedData)
    const ltltpublishCheck = ltltReturnedData.length
    console.log("lt-lt All publish URL-B2C Projector - TH685P:",ltltReturnedData)
    console.log(ltltReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(ltltpublishCheck ==0){
            TH685PPublishError.push("lt-lt")
        }
    }
    //B2C-NLBE-Projector-TH685P-after 20220401
    const nlbeReturnedData = await afterB2CnlbeURLTH685P();
    // console.log("After returnedData",nlbeReturnedData)
    const nlbepublishCheck = nlbeReturnedData.length
    console.log("nl-be All publish URL-B2C Projector - TH685P:",nlbeReturnedData)
    console.log(nlbeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(nlbepublishCheck ==0){
            TH685PPublishError.push("nl-be")
        }
    }
    //B2C-NLNL-Projector-TH685P-after 20220401
    const nlnlReturnedData = await afterB2CnlnlURLTH685P();
    // console.log("After returnedData",nlnlReturnedData)
    const nlnlpublishCheck = nlnlReturnedData.length
    console.log("nl-nl All publish URL-B2C Projector - TH685P:",nlnlReturnedData)
    console.log(nlnlReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(nlnlpublishCheck ==0){
            TH685PPublishError.push("nl-nl")
        }
    }
    //B2C-PLPL-Projector-TH685P-after 20220401
    const plplReturnedData = await afterB2CplplURLTH685P();
    // console.log("After returnedData",plplReturnedData)
    const plplpublishCheck = plplReturnedData.length
    console.log("pl-pl All publish URL-B2C Projector - TH685P:",plplReturnedData)
    console.log(plplReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(plplpublishCheck ==0){
            TH685PPublishError.push("pl-pl")
        }
    }
    //B2C-PTBR-Projector-TH685P-after 20220401
    const ptbrReturnedData = await afterB2CptbrURLTH685P();
    // console.log("After returnedData",ptbrReturnedData)
    const ptbrpublishCheck = ptbrReturnedData.length
    console.log("pt-br All publish URL-B2C Projector - TH685P:",ptbrReturnedData)
    console.log(ptbrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(ptbrpublishCheck ==0){
            TH685PPublishError.push("pt-br")
        }
    }
    //B2C-PTPT-Projector-TH685P-after 20220401
    const ptptReturnedData = await afterB2CptptURLTH685P();
    // console.log("After returnedData",ptptReturnedData)
    const ptptpublishCheck = ptptReturnedData.length
    console.log("pt-pt All publish URL-B2C Projector - TH685P:",ptptReturnedData)
    console.log(ptptReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(ptptpublishCheck ==0){
            TH685PPublishError.push("pt-pt")
        }
    }
    //B2C-RORO-Projector-TH685P-after 20220401
    const roroReturnedData = await afterB2CroroURLTH685P();
    // console.log("After returnedData",roroReturnedData)
    const roropublishCheck = roroReturnedData.length
    console.log("ro-ro All publish URL-B2C Projector - TH685P:",roroReturnedData)
    console.log(roroReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(roropublishCheck ==0){
            TH685PPublishError.push("ro-ro")
        }
    }
    //B2C-RURU-Projector-TH685P-after 20220401
    const ruruReturnedData = await afterB2CruruURLTH685P();
    // console.log("After returnedData",ruruReturnedData)
    const rurupublishCheck = ruruReturnedData.length
    console.log("ru-ru All publish URL-B2C Projector - TH685P:",ruruReturnedData)
    console.log(ruruReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(rurupublishCheck ==0){
            TH685PPublishError.push("ru-ru")
        }
    }
    //B2C-SKSK-Projector-TH685P-after 20220401
    const skskReturnedData = await afterB2CskskURLTH685P();
    // console.log("After returnedData",skskReturnedData)
    const skskpublishCheck = skskReturnedData.length
    console.log("sk-sk All publish URL-B2C Projector - TH685P:",skskReturnedData)
    console.log(skskReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(skskpublishCheck ==0){
            TH685PPublishError.push("sk-sk")
        }
    }
    //B2C-SVSE-Projector-TH685P-after 20220401
    const svseReturnedData = await afterB2CsvseURLTH685P();
    // console.log("After returnedData",svseReturnedData)
    const svsepublishCheck = svseReturnedData.length
    console.log("sv-se All publish URL-B2C Projector - TH685P:",svseReturnedData)
    console.log(svseReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(svsepublishCheck ==0){
            TH685PPublishError.push("sv-se")
        }
    }
    //B2C-THTH-Projector-TH685P-after 20220401
    const ththReturnedData = await afterB2CththURLTH685P();
    // console.log("After returnedData",ththReturnedData)
    const ththpublishCheck = ththReturnedData.length
    console.log("th-th All publish URL-B2C Projector - TH685P:",ththReturnedData)
    console.log(ththReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(ththpublishCheck ==0){
            TH685PPublishError.push("th-th")
        }
    }
    //B2C-TRTR-Projector-TH685P-after 20220401
    const trtrReturnedData = await afterB2CtrtrURLTH685P();
    // console.log("After returnedData",trtrReturnedData)
    const trtrpublishCheck = trtrReturnedData.length
    console.log("tr-tr All publish URL-B2C Projector - TH685P:",trtrReturnedData)
    console.log(trtrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(trtrpublishCheck ==0){
            TH685PPublishError.push("tr-tr")
        }
    }
    //B2C-UKUA-Projector-TH685P-after 20220401
    const ukuaReturnedData = await afterB2CukuaURLTH685P();
    // console.log("After returnedData",ukuaReturnedData)
    const ukuapublishCheck = ukuaReturnedData.length
    console.log("uk-ua All publish URL-B2C Projector - TH685P:",ukuaReturnedData)
    console.log(ukuaReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(ukuapublishCheck ==0){
            TH685PPublishError.push("uk-ua")
        }
    }
    //B2C-VIVN-Projector-TH685P-after 20220401
    const vivnReturnedData = await afterB2CvivnURLTH685P();
    // console.log("After returnedData",vivnReturnedData)
    const vivnpublishCheck = vivnReturnedData.length
    console.log("vi-vn All publish URL-B2C Projector - TH685P:",vivnReturnedData)
    console.log(vivnReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(vivnpublishCheck ==0){
            TH685PPublishError.push("vi-vn")
        }
    }
    //B2C-ZHHK-Projector-TH685P-after 20220401
    const zhhkReturnedData = await afterB2CzhhkURLTH685P();
    // console.log("After returnedData",zhhkReturnedData)
    const zhhkpublishCheck = zhhkReturnedData.length
    console.log("zh-hk All publish URL-B2C Projector - TH685P:",zhhkReturnedData)
    console.log(zhhkReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(zhhkpublishCheck ==0){
            TH685PPublishError.push("zh-hk")
        }
    }
    //B2C-ZHTW-Projector-TH685P-after 20220401
    const zhtwReturnedData = await afterB2CzhtwURLTH685P();
    // console.log("After returnedData",zhtwReturnedData)
    const zhtwpublishCheck = zhtwReturnedData.length
    console.log("zh-tw All publish URL-B2C Projector - TH685P:",zhtwReturnedData)
    console.log(zhtwReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //TH685PPublishError
        if(zhtwpublishCheck ==0){
            TH685PPublishError.push("zh-tw")
        }
    }
    //放全部的國家的後面
    if(TH685PPublishError.length >0){
        console.log("Not Published Now:",TH685PPublishError)
        throw new Error(`${publishModel} must be published on ${TH685PPublishError} before ${launchDate}, but it is not published now. Today is ${fullDate}`)
    }
})

Then("TH685P URL name must be 'projector gaming-projector th685p.html'",{timeout: 1000 * 5000},async function(){
    //After 2022/04/01
    //global setting
    const publishModel = "th685p"
    const publishProductURLName = "projector/gaming-projector/th685p.html"
    const date = new Date()
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()
    function wholeMonth(){
        const getmonth = date.getMonth() + 1
        if(getmonth<10){
            wholeMonth =  "0"+getmonth 
            return wholeMonth
        }else{
            wholeMonth = getmonth 
            return wholeMonth
        }
    }
    function wholeDate(){
        const getDay = date.getDate()
        if(getDay<10){
            wholeDate =  "0"+getDay
            return wholeDate
        }else{
            wholeDate = getDay 
            return wholeDate
        }
    }
    const wholemonth = wholeMonth()
    const wholeday = wholeDate()
    const wholeyear = date.getFullYear()
    const fullDate = `${wholeyear}/${wholemonth}/${wholeday}`
    // console.log("wholemonth:",wholemonth)
    // console.log("wholeday:",wholeday)
    // console.log("wholeyear:",wholeyear)
    console.log("fullDate:",fullDate)
    // console.log("year:",year)
    // console.log("month:",month)
    // console.log("day:",day)
    const launchDate = "2022/04/01"
    //B2C-ARME-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const armeReturnedData = await wrongnameB2CarmeURLTH685P();
    const armeNameCheck = armeReturnedData.length
    console.log("ar-me wrong name URL-B2C Projector-TH685P:",armeReturnedData)
    console.log(armeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(armeNameCheck > 0){
            TH685PNameError.push('ar-me')
            TH685PNameErrorURL.push(armeReturnedData)
        }
    }
    //B2C-BGBG-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const bgbgReturnedData = await wrongnameB2CbgbgURLTH685P();
    const bgbgNameCheck = bgbgReturnedData.length
    console.log("bg-bg wrong name URL-B2C Projector-TH685P:",bgbgReturnedData)
    console.log(bgbgReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(bgbgNameCheck > 0){
            TH685PNameError.push('bg-bg')
            TH685PNameErrorURL.push(bgbgReturnedData)
        }
    }
    //B2C-CSCZ-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const csczReturnedData = await wrongnameB2CcsczURLTH685P();
    const csczNameCheck = csczReturnedData.length
    console.log("cs-cz wrong name URL-B2C Projector-TH685P:",csczReturnedData)
    console.log(csczReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(csczNameCheck > 0){
            TH685PNameError.push('cs-cz')
            TH685PNameErrorURL.push(csczReturnedData)
        }
    }
    //B2C-DEAT-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const deatReturnedData = await wrongnameB2CdeatURLTH685P();
    const deatNameCheck = deatReturnedData.length
    console.log("de-at wrong name URL-B2C Projector-TH685P:",deatReturnedData)
    console.log(deatReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(deatNameCheck > 0){
            TH685PNameError.push('de-at')
            TH685PNameErrorURL.push(deatReturnedData)
        }
    }
    //B2C-DECH-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const dechReturnedData = await wrongnameB2CdechURLTH685P();
    const dechNameCheck = dechReturnedData.length
    console.log("de-ch wrong name URL-B2C Projector-TH685P:",dechReturnedData)
    console.log(dechReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(dechNameCheck > 0){
            TH685PNameError.push('de-ch')
            TH685PNameErrorURL.push(dechReturnedData)
        }
    }
    //B2C-DEDE-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const dedeReturnedData = await wrongnameB2CdedeURLTH685P();
    const dedeNameCheck = dedeReturnedData.length
    console.log("de-de wrong name URL-B2C Projector-TH685P:",dedeReturnedData)
    console.log(dedeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(dedeNameCheck > 0){
            TH685PNameError.push('de-de')
            TH685PNameErrorURL.push(dedeReturnedData)
        }
    }
    //B2C-ELGR-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const elgrReturnedData = await wrongnameB2CelgrURLTH685P();
    const elgrNameCheck = elgrReturnedData.length
    console.log("el-gr wrong name URL-B2C Projector-TH685P:",elgrReturnedData)
    console.log(elgrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(elgrNameCheck > 0){
            TH685PNameError.push('el-gr')
            TH685PNameErrorURL.push(elgrReturnedData)
        }
    }
    //B2C-ENAP-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const enapReturnedData = await wrongnameB2CenapURLTH685P();
    const enapNameCheck = enapReturnedData.length
    console.log("en-ap wrong name URL-B2C Projector-TH685P:",enapReturnedData)
    console.log(enapReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enapNameCheck > 0){
            TH685PNameError.push("en-ap")
            TH685PNameErrorURL.push(enapReturnedData)
        }
    }
    //B2C-ENAU-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const enauReturnedData = await wrongnameB2CenauURLTH685P();
    const enauNameCheck = enauReturnedData.length
    console.log("en-au wrong name URL-B2C Projector-TH685P:",enauReturnedData)
    console.log(enauReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enauNameCheck > 0){
            TH685PNameError.push("en-au")
            TH685PNameErrorURL.push(enauReturnedData)
        }
    }
    //B2C-ENBA-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const enbaReturnedData = await wrongnameB2CenbaURLTH685P();
    const enbaNameCheck = enbaReturnedData.length
    console.log("en-ba wrong name URL-B2C Projector-TH685P:",enbaReturnedData)
    console.log(enbaReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enbaNameCheck > 0){
            TH685PNameError.push("en-ba")
            TH685PNameErrorURL.push(enbaReturnedData)
        }
    }
    //B2C-ENCA-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const encaReturnedData = await wrongnameB2CencaURLTH685P();
    const encaNameCheck = encaReturnedData.length
    console.log("en-ca wrong name URL-B2C Projector-TH685P:",encaReturnedData)
    console.log(encaReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(encaNameCheck > 0){
            TH685PNameError.push("en-ca")
            TH685PNameErrorURL.push(encaReturnedData)
        }
    }
    //B2C-ENCEE-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const enceeReturnedData = await wrongnameB2CenceeURLTH685P();
    const enceeNameCheck = enceeReturnedData.length
    console.log("en-cee wrong name URL-B2C Projector-TH685P:",enceeReturnedData)
    console.log(enceeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enceeNameCheck > 0){
            TH685PNameError.push("en-cee")
            TH685PNameErrorURL.push(enceeReturnedData)
        }
    }
    //B2C-ENCY-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const encyReturnedData = await wrongnameB2CencyURLTH685P();
    const encyNameCheck = encyReturnedData.length
    console.log("en-cy wrong name URL-B2C Projector-TH685P:",encyReturnedData)
    console.log(encyReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(encyNameCheck > 0){
            TH685PNameError.push("en-cy")
            TH685PNameErrorURL.push(encyReturnedData)
        }
    }
    //B2C-ENDK-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const endkReturnedData = await wrongnameB2CendkURLTH685P();
    const endkNameCheck = endkReturnedData.length
    console.log("en-dk wrong name URL-B2C Projector-TH685P:",endkReturnedData)
    console.log(endkReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(endkNameCheck > 0){
            TH685PNameError.push("en-dk")
            TH685PNameErrorURL.push(endkReturnedData)
        }
    }
    //B2C-ENEE-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const eneeReturnedData = await wrongnameB2CeneeURLTH685P();
    const eneeNameCheck = eneeReturnedData.length
    console.log("en-ee wrong name URL-B2C Projector-TH685P:",eneeReturnedData)
    console.log(eneeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(eneeNameCheck > 0){
            TH685PNameError.push('en-ee')
            TH685PNameErrorURL.push(eneeReturnedData)
        }
    }
    //B2C-ENEU-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const eneuReturnedData = await wrongnameB2CeneuURLTH685P();
    const eneuNameCheck = eneuReturnedData.length
    console.log("en-eu wrong name URL-B2C Projector-TH685P:",eneuReturnedData)
    console.log(eneuReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(eneuNameCheck > 0){
            TH685PNameError.push('en-eu')
            TH685PNameErrorURL.push(eneuReturnedData)
        }
    }
    //B2C-ENFI-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const enfiReturnedData = await wrongnameB2CenfiURLTH685P();
    const enfiNameCheck = enfiReturnedData.length
    console.log("en-fi wrong name URL-B2C Projector-TH685P:",enfiReturnedData)
    console.log(enfiReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enfiNameCheck > 0){
            TH685PNameError.push('en-fi')
            TH685PNameErrorURL.push(enfiReturnedData)
        }
    }
    //B2C-ENHK-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const enhkReturnedData = await wrongnameB2CenhkURLTH685P();
    const enhkNameCheck = enhkReturnedData.length
    console.log("en-hk wrong name URL-B2C Projector-TH685P:",enhkReturnedData)
    console.log(enhkReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enhkNameCheck > 0){
            TH685PNameError.push("en-hk")
            TH685PNameErrorURL.push(enhkReturnedData)
        }
    }
    //B2C-ENHR-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const enhrReturnedData = await wrongnameB2CenhrURLTH685P();
    const enhrNameCheck = enhrReturnedData.length
    console.log("en-hr wrong name URL-B2C Projector-TH685P:",enhrReturnedData)
    console.log(enhrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enhrNameCheck > 0){
            TH685PNameError.push('en-hr')
            TH685PNameErrorURL.push(enhrReturnedData)
        }
    }
    //B2C-ENIE-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const enieReturnedData = await wrongnameB2CenieURLTH685P();
    const enieNameCheck = enieReturnedData.length
    console.log("en-ie wrong name URL-B2C Projector-TH685P:",enieReturnedData)
    console.log(enieReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enieNameCheck > 0){
            TH685PNameError.push('en-ie')
            TH685PNameErrorURL.push(enieReturnedData)
        }
    }
    //B2C-ENIN-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const eninReturnedData = await wrongnameB2CeninURLTH685P();
    const eninNameCheck = eninReturnedData.length
    console.log("en-in wrong name URL-B2C Projector-TH685P:",eninReturnedData)
    console.log(eninReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(eninNameCheck > 0){
            TH685PNameError.push("en-in")
            TH685PNameErrorURL.push(eninReturnedData)
        }
    }
    //B2C-ENIS-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const enisReturnedData = await wrongnameB2CenisURLTH685P();
    const enisNameCheck = enisReturnedData.length
    console.log("en-is wrong name URL-B2C Projector-TH685P:",enisReturnedData)
    console.log(enisReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enisNameCheck > 0){
            TH685PNameError.push('en-is')
            TH685PNameErrorURL.push(enisReturnedData)
        }
    }
    //B2C-ENLU-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const enluReturnedData = await wrongnameB2CenluURLTH685P();
    const enluNameCheck = enluReturnedData.length
    console.log("en-lu wrong name URL-B2C Projector-TH685P:",enluReturnedData)
    console.log(enluReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enluNameCheck > 0){
            TH685PNameError.push('en-lu')
            TH685PNameErrorURL.push(enluReturnedData)
        }
    }
    //B2C-ENLV-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const enlvReturnedData = await wrongnameB2CenlvURLTH685P();
    const enlvNameCheck = enlvReturnedData.length
    console.log("en-lv wrong name URL-B2C Projector-TH685P:",enlvReturnedData)
    console.log(enlvReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enlvNameCheck > 0){
            TH685PNameError.push('en-lv')
            TH685PNameErrorURL.push(enlvReturnedData)
        }
    }
    //B2C-ENME-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const enmeReturnedData = await wrongnameB2CenmeURLTH685P();
    const enmeNameCheck = enmeReturnedData.length
    console.log("en-me wrong name URL-B2C Projector-TH685P:",enmeReturnedData)
    console.log(enmeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enmeNameCheck > 0){
            TH685PNameError.push("en-me")
            TH685PNameErrorURL.push(enmeReturnedData)
        }
    }
    //B2C-ENMK-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const enmkReturnedData = await wrongnameB2CenmkURLTH685P();
    const enmkNameCheck = enmkReturnedData.length
    console.log("en-mk wrong name URL-B2C Projector-TH685P:",enmkReturnedData)
    console.log(enmkReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enmkNameCheck > 0){
            TH685PNameError.push('en-mk')
            TH685PNameErrorURL.push(enmkReturnedData)
        }
    }
    //B2C-ENMT-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const enmtReturnedData = await wrongnameB2CenmtURLTH685P();
    const enmtNameCheck = enmtReturnedData.length
    console.log("en-mt wrong name URL-B2C Projector-TH685P:",enmtReturnedData)
    console.log(enmtReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enmtNameCheck > 0){
            TH685PNameError.push('en-mt')
            TH685PNameErrorURL.push(enmtReturnedData)
        }
    }
    //B2C-ENMY-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const enmyReturnedData = await wrongnameB2CenmyURLTH685P();
    const enmyNameCheck = enmyReturnedData.length
    console.log("en-my wrong name URL-B2C Projector-TH685P:",enmyReturnedData)
    console.log(enmyReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enmyNameCheck > 0){
            TH685PNameError.push("en-my")
            TH685PNameErrorURL.push(enmyReturnedData)
        }
    }
    //B2C-ENNO-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const ennoReturnedData = await wrongnameB2CennoURLTH685P();
    const ennoNameCheck = ennoReturnedData.length
    console.log("en-no wrong name URL-B2C Projector-TH685P:",ennoReturnedData)
    console.log(ennoReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ennoNameCheck > 0){
            TH685PNameError.push('en-no')
            TH685PNameErrorURL.push(ennoReturnedData)
        }
    }
    //B2C-ENRS-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const enrsReturnedData = await wrongnameB2CenrsURLTH685P();
    const enrsNameCheck = enrsReturnedData.length
    console.log("en-rs wrong name URL-B2C Projector-TH685P:",enrsReturnedData)
    console.log(enrsReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enrsNameCheck > 0){
            TH685PNameError.push('en-rs')
            TH685PNameErrorURL.push(enrsReturnedData)
        }
    }
    //B2C-ENSG-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const ensgReturnedData = await wrongnameB2CensgURLTH685P();
    const ensgNameCheck = ensgReturnedData.length
    console.log("en-sg wrong name URL-B2C Projector-TH685P:",ensgReturnedData)
    console.log(ensgReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ensgNameCheck > 0){
            TH685PNameError.push("en-sg")
            TH685PNameErrorURL.push(ensgReturnedData)
        }
    }
    //B2C-ENSI-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const ensiReturnedData = await wrongnameB2CensiURLTH685P();
    const ensiNameCheck = ensiReturnedData.length
    console.log("en-si wrong name URL-B2C Projector-TH685P:",ensiReturnedData)
    console.log(ensiReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ensiNameCheck > 0){
            TH685PNameError.push('en-si')
            TH685PNameErrorURL.push(ensiReturnedData)
        }
    }
    //B2C-ENUK-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const enukReturnedData = await wrongnameB2CenukURLTH685P();
    const enukNameCheck = enukReturnedData.length
    console.log("en-uk wrong name URL-B2C Projector-TH685P:",enukReturnedData)
    console.log(enukReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enukNameCheck > 0){
            TH685PNameError.push('en-uk')
            TH685PNameErrorURL.push(enukReturnedData)
        }
    }
    //B2C-ENUS-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const enusReturnedData = await wrongnameB2CenusURLTH685P();
    const enusNameCheck = enusReturnedData.length
    console.log("en-us wrong name URL-B2C Projector-TH685P:",enusReturnedData)
    console.log(enusReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enusNameCheck > 0){
            TH685PNameError.push("en-us")
            TH685PNameErrorURL.push(enusReturnedData)
        }
    }
    //B2C-ESAR-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const esarReturnedData = await wrongnameB2CesarURLTH685P();
    const esarNameCheck = esarReturnedData.length
    console.log("es-ar wrong name URL-B2C Projector-TH685P:",esarReturnedData)
    console.log(esarReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(esarNameCheck > 0){
            TH685PNameError.push("es-ar")
            TH685PNameErrorURL.push(esarReturnedData)
        }
    }
    //B2C-ESCO-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const escoReturnedData = await wrongnameB2CescoURLTH685P();
    const escoNameCheck = escoReturnedData.length
    console.log("es-co wrong name URL-B2C Projector-TH685P:",escoReturnedData)
    console.log(escoReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(escoNameCheck > 0){
            TH685PNameError.push("es-co")
            TH685PNameErrorURL.push(escoReturnedData)
        }
    }
    //B2C-ESES-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const esesReturnedData = await wrongnameB2CesesURLTH685P();
    const esesNameCheck = esesReturnedData.length
    console.log("es-es wrong name URL-B2C Projector-TH685P:",esesReturnedData)
    console.log(esesReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(esesNameCheck > 0){
            TH685PNameError.push("es-es")
            TH685PNameErrorURL.push(esesReturnedData)
        }
    }
    //B2C-ESLA-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const eslaReturnedData = await wrongnameB2CeslaURLTH685P();
    const eslaNameCheck = eslaReturnedData.length
    console.log("es-la wrong name URL-B2C Projector-TH685P:",eslaReturnedData)
    console.log(eslaReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(eslaNameCheck > 0){
            TH685PNameError.push("es-la")
            TH685PNameErrorURL.push(eslaReturnedData)
        }
    }
    //B2C-ESMX-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const esmxReturnedData = await wrongnameB2CesmxURLTH685P();
    const esmxNameCheck = esmxReturnedData.length
    console.log("es-mx wrong name URL-B2C Projector-TH685P:",esmxReturnedData)
    console.log(esmxReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(esmxNameCheck > 0){
            TH685PNameError.push("es-mx")
            TH685PNameErrorURL.push(esmxReturnedData)
        }
    }
    //B2C-ESPE-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const espeReturnedData = await wrongnameB2CespeURLTH685P();
    const espeNameCheck = espeReturnedData.length
    console.log("es-pe wrong name URL-B2C Projector-TH685P:",espeReturnedData)
    console.log(espeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(espeNameCheck > 0){
            TH685PNameError.push("es-pe")
            TH685PNameErrorURL.push(espeReturnedData)
        }
    }
    //B2C-FRCA-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const frcaReturnedData = await wrongnameB2CfrcaURLTH685P();
    const frcaNameCheck = frcaReturnedData.length
    console.log("fr-ca wrong name URL-B2C Projector-TH685P:",frcaReturnedData)
    console.log(frcaReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(frcaNameCheck > 0){
            TH685PNameError.push("fr-ca")
            TH685PNameErrorURL.push(frcaReturnedData)
        }
    }
    //B2C-FRCH-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const frchReturnedData = await wrongnameB2CfrchURLTH685P();
    const frchNameCheck = frchReturnedData.length
    console.log("fr-ch wrong name URL-B2C Projector-TH685P:",frchReturnedData)
    console.log(frchReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(frchNameCheck > 0){
            TH685PNameError.push("fr-ch")
            TH685PNameErrorURL.push(frchReturnedData)
        }
    }
    //B2C-FRFR-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const frfrReturnedData = await wrongnameB2CfrfrURLTH685P();
    const frfrNameCheck = frfrReturnedData.length
    console.log("fr-fr wrong name URL-B2C Projector-TH685P:",frfrReturnedData)
    console.log(frfrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(frfrNameCheck > 0){
            TH685PNameError.push("fr-fr")
            TH685PNameErrorURL.push(frfrReturnedData)
        }
    }
    //B2C-HUHU-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const huhuReturnedData = await wrongnameB2ChuhuURLTH685P();
    const huhuNameCheck = huhuReturnedData.length
    console.log("hu-hu wrong name URL-B2C Projector-TH685P:",huhuReturnedData)
    console.log(huhuReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(huhuNameCheck > 0){
            TH685PNameError.push("hu-hu")
            TH685PNameErrorURL.push(huhuReturnedData)
        }
    }
    //B2C-IDID-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const ididReturnedData = await wrongnameB2CididURLTH685P();
    const ididNameCheck = ididReturnedData.length
    console.log("id-id wrong name URL-B2C Projector-TH685P:",ididReturnedData)
    console.log(ididReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ididNameCheck > 0){
            TH685PNameError.push("id-id")
            TH685PNameErrorURL.push(ididReturnedData)
        }
    }
    //B2C-ITIT-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const ititReturnedData = await wrongnameB2CititURLTH685P();
    const ititNameCheck = ititReturnedData.length
    console.log("it-it wrong name URL-B2C Projector-TH685P:",ititReturnedData)
    console.log(ititReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ititNameCheck > 0){
            TH685PNameError.push("it-it")
            TH685PNameErrorURL.push(ititReturnedData)
        }
    }
    //B2C-JAJP-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const jajpReturnedData = await wrongnameB2CjajpURLTH685P();
    const jajpNameCheck = jajpReturnedData.length
    console.log("ja-jp wrong name URL-B2C Projector-TH685P:",jajpReturnedData)
    console.log(jajpReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(jajpNameCheck > 0){
            TH685PNameError.push("ja-jp")
            TH685PNameErrorURL.push(jajpReturnedData)
        }
    }
    //B2C-KOKR-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const kokrReturnedData = await wrongnameB2CkokrURLTH685P();
    const kokrNameCheck = kokrReturnedData.length
    console.log("ko-kr wrong name URL-B2C Projector-TH685P:",kokrReturnedData)
    console.log(kokrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(kokrNameCheck > 0){
            TH685PNameError.push("ko-kr")
            TH685PNameErrorURL.push(kokrReturnedData)
        }
    }
    //B2C-LTLT-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const ltltReturnedData = await wrongnameB2CltltURLTH685P();
    const ltltNameCheck = ltltReturnedData.length
    console.log("lt-lt wrong name URL-B2C Projector-TH685P:",ltltReturnedData)
    console.log(ltltReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ltltNameCheck > 0){
            TH685PNameError.push("lt-lt")
            TH685PNameErrorURL.push(ltltReturnedData)
        }
    }
    //B2C-NLBE-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const nlbeReturnedData = await wrongnameB2CnlbeURLTH685P();
    const nlbeNameCheck = nlbeReturnedData.length
    console.log("nl-be wrong name URL-B2C Projector-TH685P:",nlbeReturnedData)
    console.log(nlbeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(nlbeNameCheck > 0){
            TH685PNameError.push("nl-be")
            TH685PNameErrorURL.push(nlbeReturnedData)
        }
    }
    //B2C-NLNL-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const nlnlReturnedData = await wrongnameB2CnlnlURLTH685P();
    const nlnlNameCheck = nlnlReturnedData.length
    console.log("nl-nl wrong name URL-B2C Projector-TH685P:",nlnlReturnedData)
    console.log(nlnlReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(nlnlNameCheck > 0){
            TH685PNameError.push("nl-nl")
            TH685PNameErrorURL.push(nlnlReturnedData)
        }
    }
    //B2C-PLPL-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const plplReturnedData = await wrongnameB2CplplURLTH685P();
    const plplNameCheck = plplReturnedData.length
    console.log("pl-pl wrong name URL-B2C Projector-TH685P:",plplReturnedData)
    console.log(plplReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(plplNameCheck > 0){
            TH685PNameError.push("pl-pl")
            TH685PNameErrorURL.push(plplReturnedData)
        }
    }
    //B2C-PTBR-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const ptbrReturnedData = await wrongnameB2CptbrURLTH685P();
    const ptbrNameCheck = ptbrReturnedData.length
    console.log("pt-br wrong name URL-B2C Projector-TH685P:",ptbrReturnedData)
    console.log(ptbrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ptbrNameCheck > 0){
            TH685PNameError.push("pt-br")
            TH685PNameErrorURL.push(ptbrReturnedData)
        }
    }
    //B2C-PTPT-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const ptptReturnedData = await wrongnameB2CptptURLTH685P();
    const ptptNameCheck = ptptReturnedData.length
    console.log("pt-pt wrong name URL-B2C Projector-TH685P:",ptptReturnedData)
    console.log(ptptReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ptptNameCheck > 0){
            TH685PNameError.push("pt-pt")
            TH685PNameErrorURL.push(ptptReturnedData)
        }
    }
    //B2C-RORO-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const roroReturnedData = await wrongnameB2CroroURLTH685P();
    const roroNameCheck = roroReturnedData.length
    console.log("ro-ro wrong name URL-B2C Projector-TH685P:",roroReturnedData)
    console.log(roroReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(roroNameCheck > 0){
            TH685PNameError.push("ro-ro")
            TH685PNameErrorURL.push(roroReturnedData)
        }
    }
    //B2C-RURU-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const ruruReturnedData = await wrongnameB2CruruURLTH685P();
    const ruruNameCheck = ruruReturnedData.length
    console.log("ru-ru wrong name URL-B2C Projector-TH685P:",ruruReturnedData)
    console.log(ruruReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ruruNameCheck > 0){
            TH685PNameError.push("ru-ru")
            TH685PNameErrorURL.push(ruruReturnedData)
        }
    }
    //B2C-SKSK-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const skskReturnedData = await wrongnameB2CskskURLTH685P();
    const skskNameCheck = skskReturnedData.length
    console.log("sk-sk wrong name URL-B2C Projector-TH685P:",skskReturnedData)
    console.log(skskReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(skskNameCheck > 0){
            TH685PNameError.push("sk-sk")
            TH685PNameErrorURL.push(skskReturnedData)
        }
    }
    //B2C-SVSE-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const svseReturnedData = await wrongnameB2CsvseURLTH685P();
    const svseNameCheck = svseReturnedData.length
    console.log("sv-se wrong name URL-B2C Projector-TH685P:",svseReturnedData)
    console.log(svseReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(svseNameCheck > 0){
            TH685PNameError.push("sv-se")
            TH685PNameErrorURL.push(svseReturnedData)
        }
    }
    //B2C-THTH-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const ththReturnedData = await wrongnameB2CththURLTH685P();
    const ththNameCheck = ththReturnedData.length
    console.log("th-th wrong name URL-B2C Projector-TH685P:",ththReturnedData)
    console.log(ththReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ththNameCheck > 0){
            TH685PNameError.push("th-th")
            TH685PNameErrorURL.push(ththReturnedData)
        }
    }
    //B2C-TRTR-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const trtrReturnedData = await wrongnameB2CtrtrURLTH685P();
    const trtrNameCheck = trtrReturnedData.length
    console.log("tr-tr wrong name URL-B2C Projector-TH685P:",trtrReturnedData)
    console.log(trtrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(trtrNameCheck > 0){
            TH685PNameError.push("tr-tr")
            TH685PNameErrorURL.push(trtrReturnedData)
        }
    }
    //B2C-UKUA-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const ukuaReturnedData = await wrongnameB2CukuaURLTH685P();
    const ukuaNameCheck = ukuaReturnedData.length
    console.log("uk-ua wrong name URL-B2C Projector-TH685P:",ukuaReturnedData)
    console.log(ukuaReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ukuaNameCheck > 0){
            TH685PNameError.push("uk-ua")
            TH685PNameErrorURL.push(ukuaReturnedData)
        }
    }
    //B2C-VIVN-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const vivnReturnedData = await wrongnameB2CvivnURLTH685P();
    const vivnNameCheck = vivnReturnedData.length
    console.log("vi-vn wrong name URL-B2C Projector-TH685P:",vivnReturnedData)
    console.log(vivnReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(vivnNameCheck > 0){
            TH685PNameError.push("vi-vn")
            TH685PNameErrorURL.push(vivnReturnedData)
        }
    }
    //B2C-ZHHK-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const zhhkReturnedData = await wrongnameB2CzhhkURLTH685P();
    const zhhkNameCheck = zhhkReturnedData.length
    console.log("zh-hk wrong name URL-B2C Projector-TH685P:",zhhkReturnedData)
    console.log(zhhkReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(zhhkNameCheck > 0){
            TH685PNameError.push("zh-hk")
            TH685PNameErrorURL.push(zhhkReturnedData)
        }
    }
    //B2C-ZHTW-Projector-TH685P-name must be projector/gaming-projector/th685p.html
    const zhtwReturnedData = await wrongnameB2CzhtwURLTH685P();
    const zhtwNameCheck = zhtwReturnedData.length
    console.log("zh-tw wrong name URL-B2C Projector-TH685P:",zhtwReturnedData)
    console.log(zhtwReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(zhtwNameCheck > 0){
            TH685PNameError.push("zh-tw")
            TH685PNameErrorURL.push(zhtwReturnedData)
        }
    }

    //放全部國家的後面
    if(TH685PNameError.length >0){
        throw new Error(`${publishModel} URL name must be ${publishProductURLName}, but it is wrong now. Here is RO name: ${TH685PNameError} And wrong URL: ${TH685PNameErrorURL}`)
    }
})
