//WDC30
const excelToWDC30RONS =[
    "ar-me",
    "cs-cz",
    "de-de",
    "en-ap",
    "en-eu",
    "en-hk",
    "en-uk",
    "en-us",
    "es-es",
    "es-la",
    "es-mx",
    "fr-ca",
    "fr-ch",
    "fr-fr",
    "id-id",
    "it-it",
    "ja-jp",
    "ko-kr",
    "nl-nl",
    "pl-pl",
    "pt-br",
    "ru-ru",
    "tr-tr",
    "vi-vn",
    // "zh-cn",
    "zh-hk",
    "zh-tw"
]
const excelToWDC30Result =[]
const excelToWDC30URL =[]

//B2B-WPS-WDC30 - publish check
const WDC30PublishError=[]
//B2B-ARME-WPS-WDC30-after 20220506
const afterB2BarmeWDC30=[]
const afterB2BarmeURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ar-me/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "wdc30.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BarmeWDC30.push(url)
            }
    })
    return afterB2BarmeWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-CSCZ-WPS-WDC30-after 20220506
const afterB2BcsczWDC30=[]
const afterB2BcsczURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/cs-cz/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "wdc30.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BcsczWDC30.push(url)
            }
    })
    return afterB2BcsczWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-DEDE-WPS-WDC30-after 20220506
const afterB2BdedeWDC30=[]
const afterB2BdedeURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/de-de/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "wdc30.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BdedeWDC30.push(url)
            }
    })
    return afterB2BdedeWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENAP-WPS-WDC30-after 20220506
const afterB2BenapWDC30=[]
const afterB2BenapURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-ap/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "wdc30.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BenapWDC30.push(url)
            }
    })
    return afterB2BenapWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENEU-WPS-WDC30-after 20220506
const afterB2BeneuWDC30=[]
const afterB2BeneuURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-eu/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "wdc30.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BeneuWDC30.push(url)
            }
    })
    return afterB2BeneuWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENHK-WPS-WDC30-after 20220506
const afterB2BenhkWDC30=[]
const afterB2BenhkURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-hk/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "wdc30.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BenhkWDC30.push(url)
            }
    })
    return afterB2BenhkWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENUK-WPS-WDC30-after 20220506
const afterB2BenukWDC30=[]
const afterB2BenukURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-uk/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "wdc30.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BenukWDC30.push(url)
            }
    })
    return afterB2BenukWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENUS-WPS-WDC30-after 20220506
const afterB2BenusWDC30=[]
const afterB2BenusURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-us/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "wdc30.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BenusWDC30.push(url)
            }
    })
    return afterB2BenusWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ESES-WPS-WDC30-after 20220506
const afterB2BesesWDC30=[]
const afterB2BesesURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-es/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "wdc30.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BesesWDC30.push(url)
            }
    })
    return afterB2BesesWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ESLA-WPS-WDC30-after 20220506
const afterB2BeslaWDC30=[]
const afterB2BeslaURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/es-la/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "wdc30.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BeslaWDC30.push(url)
            }
    })
    return afterB2BeslaWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ESMX-WPS-WDC30-after 20220506
const afterB2BesmxWDC30=[]
const afterB2BesmxURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/es-mx/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "wdc30.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BesmxWDC30.push(url)
            }
    })
    return afterB2BesmxWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-FRCA-WPS-WDC30-after 20220506
const afterB2BfrcaWDC30=[]
const afterB2BfrcaURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/fr-ca/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "wdc30.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BfrcaWDC30.push(url)
            }
    })
    return afterB2BfrcaWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-FRCH-WPS-WDC30-after 20220506
const afterB2BfrchWDC30=[]
const afterB2BfrchURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/fr-ch/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "wdc30.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BfrchWDC30.push(url)
            }
    })
    return afterB2BfrchWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-FRFR-WPS-WDC30-after 20220506
const afterB2BfrfrWDC30=[]
const afterB2BfrfrURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/fr-fr/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl="wdc30.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BfrfrWDC30.push(url)
            }
    })
    return afterB2BfrfrWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-IDID-WPS-WDC30-after 20220506
const afterB2BididWDC30=[]
const afterB2BididURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/id-id/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "wdc30.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BididWDC30.push(url)
            }
    })
    return afterB2BididWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ITIT-WPS-WDC30-after 20220506
const afterB2BititWDC30=[]
const afterB2BititURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/it-it/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "wdc30.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BititWDC30.push(url)
            }
    })
    return afterB2BititWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-JAJP-WPS-WDC30-after 20220506
const afterB2BjajpWDC30=[]
const afterB2BjajpURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ja-jp/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "wdc30.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BjajpWDC30.push(url)
            }
    })
    return afterB2BjajpWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-KOKR-WPS-WDC30-after 20220506
const afterB2BkokrWDC30=[]
const afterB2BkokrURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ko-kr/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "wdc30.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BkokrWDC30.push(url)
            }
    })
    return afterB2BkokrWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-NLNL-WPS-WDC30-after 20220506
const afterB2BnlnlWDC30=[]
const afterB2BnlnlURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/nl-nl/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "wdc30.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BnlnlWDC30.push(url)
            }
    })
    return afterB2BnlnlWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-PLPL-WPS-WDC30-after 20220506
const afterB2BplplWDC30=[]
const afterB2BplplURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/pl-pl/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "wdc30.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BplplWDC30.push(url)
            }
    })
    return afterB2BplplWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-PTBR-WPS-WDC30-after 20220506
const afterB2BptbrWDC30=[]
const afterB2BptbrURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/pt-br/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "wdc30.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BptbrWDC30.push(url)
            }
    })
    return afterB2BptbrWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-RURU-WPS-WDC30-after 20220506
const afterB2BruruWDC30=[]
const afterB2BruruURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ru-ru/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "wdc30.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BruruWDC30.push(url)
            }
    })
    return afterB2BruruWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-TRTR-WPS-WDC30-after 20220506
const afterB2BtrtrWDC30=[]
const afterB2BtrtrURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/tr-tr/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "wdc30.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BtrtrWDC30.push(url)
            }
    })
    return afterB2BtrtrWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-VIVN-WPS-WDC30-after 20220506
const afterB2BvivnWDC30=[]
const afterB2BvivnURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/vi-vn/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "wdc30.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BvivnWDC30.push(url)
            }
    })
    return afterB2BvivnWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ZHHK-WPS-WDC30-after 20220506
const afterB2BzhhkWDC30=[]
const afterB2BzhhkURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/zh-hk/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl="wdc30.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BzhhkWDC30.push(url)
            }
    })
    return afterB2BzhhkWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ZHTW-WPS-WDC30-after 20220506
const afterB2BzhtwWDC30=[]
const afterB2BzhtwURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/zh-tw/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "wdc30.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2BzhtwWDC30.push(url)
            }
    })
    return afterB2BzhtwWDC30;
    } catch (error) {
      console.log(error);
    }
};

//WPS - WDC30 - URL name check
const WDC30NameError=[]
const WDC30NameErrorURL=[]
//B2B-ARME-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
const wrongnameB2BarmeWDC30=[]
const wrongnameB2BarmeURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ar-me/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ar-me/business"
            const publishSeries = "wireless-presentation"
            const publishUrlName = "ar-me/business/wireless-presentation"
            const publishProductModel = "wdc30.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BarmeWDC30.push(url)
            }
    })
    return wrongnameB2BarmeWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-CSCZ-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
const wrongnameB2BcsczWDC30=[]
const wrongnameB2BcsczURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/cs-cz/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "cs-cz/business"
            const publishSeries = "wireless-presentation"
            const publishUrlName = "cs-cz//business/wireless-presentation"
            const publishProductModel = "wdc30.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BcsczWDC30.push(url)
            }
    })
    return wrongnameB2BcsczWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-DEDE-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
const wrongnameB2BdedeWDC30=[]
const wrongnameB2BdedeURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/de-de/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "de-de/business"
            const publishSeries = "wireless-presentation"
            const publishUrlName = "de-de//business/wireless-presentation"
            const publishProductModel = "wdc30.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BdedeWDC30.push(url)
            }
    })
    return wrongnameB2BdedeWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENAP-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
const wrongnameB2BenapWDC30=[]
const wrongnameB2BenapURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-ap/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-ap/business"
            const publishSeries = "wireless-presentation"
            const publishUrlName = "en-ap//business/wireless-presentation"
            const publishProductModel = "wdc30.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BenapWDC30.push(url)
            }
    })
    return wrongnameB2BenapWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENEU-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
const wrongnameB2BeneuWDC30=[]
const wrongnameB2BeneuURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-eu/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-eu/business"
            const publishSeries = "wireless-presentation"
            const publishUrlName = "en-eu//business/wireless-presentation"
            const publishProductModel = "wdc30.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BeneuWDC30.push(url)
            }
    })
    return wrongnameB2BeneuWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENHK-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
const wrongnameB2BenhkWDC30=[]
const wrongnameB2BenhkURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-hk/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-hk/business"
            const publishSeries = "wireless-presentation"
            const publishUrlName = "en-hk//business/wireless-presentation"
            const publishProductModel = "wdc30.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BenhkWDC30.push(url)
            }
    })
    return wrongnameB2BenhkWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENUK-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
const wrongnameB2BenukWDC30=[]
const wrongnameB2BenukURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/en-uk/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-uk/business"
            const publishSeries = "wireless-presentation"
            const publishUrlName = "en-uk//business/wireless-presentation"
            const publishProductModel = "wdc30.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 &&  url.indexOf(publishUrlName)<0 ){
                wrongnameB2BenukWDC30.push(url)
            }
    })
    return wrongnameB2BenukWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ENUS-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
const wrongnameB2BenusWDC30=[]
const wrongnameB2BenusURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-us/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-us/business"
            const publishSeries = "wireless-presentation"
            const publishUrlName = "en-us//business/wireless-presentation"
            const publishProductModel = "wdc30.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 &&  url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BenusWDC30.push(url)
            }
    })
    return wrongnameB2BenusWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ESES-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
const wrongnameB2BesesWDC30=[]
const wrongnameB2BesesURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/es-es/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-es/business"
            const publishSeries = "wireless-presentation"
            const publishUrlName = "es-es//business/wireless-presentation"
            const publishProductModel = "wdc30.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BesesWDC30.push(url)
            }
    })
    return wrongnameB2BesesWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ESLA-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
const wrongnameB2BeslaWDC30=[]
const wrongnameB2BeslaURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/es-la/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-la/business"
            const publishSeries = "wireless-presentation"
            const publishUrlName = "es-la/business/wireless-presentation"
            const publishProductModel = "wdc30.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 &&  url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BeslaWDC30.push(url)
            }
    })
    return wrongnameB2BeslaWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ESMX-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
const wrongnameB2BesmxWDC30=[]
const wrongnameB2BesmxURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/es-mx/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "es-mx/business"
            const publishSeries ="wireless-presentation"
            const publishUrlName = "es-mx/business/wireless-presentation"
            const publishProductModel = "wdc30.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BesmxWDC30.push(url)
            }
    })
    return wrongnameB2BesmxBWDC30
    } catch (error) {
      console.log(error);
    }
};
//B2B-FRCA-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
const wrongnameB2BfrcaWDC30=[]
const wrongnameB2BfrcaURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/fr-ca/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "fr-ca/business"
            const publishSeries = "wireless-presentation"
            const publishUrlName = "fr-ca/business/wireless-presentation"
            const publishProductModel = "wdc30.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BfrcaWDC30.push(url)
            }
    })
    return wrongnameB2BfrcaWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-FRCH-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
const wrongnameB2BfrchWDC30=[]
const wrongnameB2BfrchURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/fr-ch/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "fr-ch/business"
            const publishSeries = "wireless-presentation"
            const publishUrlName = "fr-ch/business/wireless-presentation"
            const publishProductModel = "wdc30.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BfrchWDC30.push(url)
            }
    })
    return wrongnameB2BfrchWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-FRFR-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
const wrongnameB2BfrfrWDC30=[]
const wrongnameB2BfrfrURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/fr-fr/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "fr-fr/business"
            const publishSeries = "wireless-presentation"
            const publishUrlName = "fr-fr/business/wireless-presentation"
            const publishProductModel = "wdc30.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BfrfrWDC30.push(url)
            }
    })
    return wrongnameB2BfrfrWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-IDID-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
const wrongnameB2BididWDC30=[]
const wrongnameB2BididURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/id-id/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "id-id/business"
            const publishSeries = "wireless-presentation"
            const publishUrlName = "id-id/business/wireless-presentation"
            const publishProductModel = "wdc30.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BididWDC30.push(url)
            }
    })
    return wrongnameB2BididWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ITIT-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
const wrongnameB2BititWDC30=[]
const wrongnameB2BititURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/it-it/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "it-it/business"
            const publishSeries = "wireless-presentation"
            const publishUrlName = "it-it/business/wireless-presentation"
            const publishProductModel = "wdc30.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BititWDC30.push(url)
            }
    })
    return wrongnameB2BititWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-JAJP-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
const wrongnameB2BjajpWDC30=[]
const wrongnameB2BjajpURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ja-jp/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ja-jp/business"
            const publishSeries = "wireless-presentation"
            const publishUrlName = "ja-jp/business/wireless-presentation"
            const publishProductModel = "wdc30.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BjajpWDC30.push(url)
            }
    })
    return wrongnameB2BjajpWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-KOKR-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
const wrongnameB2BkokrWDC30=[]
const wrongnameB2BkokrURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ko-kr/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ko-kr/business"
            const publishSeries = "wireless-presentation"
            const publishUrlName = "ko-kr/business/wireless-presentation"
            const publishProductModel = "wdc30.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BkokrWDC30.push(url)
            }
    })
    return wrongnameB2BkokrWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-NLNL-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
const wrongnameB2BnlnlWDC30=[]
const wrongnameB2BnlnlURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/nl-nl/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "nl-nl/business"
            const publishSeries = "wireless-presentation"
            const publishUrlName = "nl-nl/business/wireless-presentation"
            const publishProductModel = "wdc30.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 &&  url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BnlnlWDC30.push(url)
            }
    })
    return wrongnameB2BnlnlWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-PLPL-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
const wrongnameB2BplplWDC30=[]
const wrongnameB2BplplURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.eu/pl-pl/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "pl-pl/business"
            const publishSeries = "wireless-presentation"
            const publishUrlName = "pl-pl/business/wireless-presentation"
            const publishProductModel = "wdc30.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BplplWDC30.push(url)
            }
    })
    return wrongnameB2BplplWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-PTBR-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
const wrongnameB2BptbrWDC30=[]
const wrongnameB2BptbrURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/pt-br/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "pt-br/business"
            const publishSeries = "wireless-presentation"
            const publishUrlName = "pt-br/business/wireless-presentation"
            const publishProductModel = "wdc30.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BptbrWDC30.push(url)
            }
    })
    return wrongnameB2BptbrWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-RURU-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
const wrongnameB2BruruWDC30=[]
const wrongnameB2BruruURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/ru-ru/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "ru-ru/business"
            const publishSeries = "wireless-presentation"
            const publishUrlName = "ru-ru/business/wireless-presentation"
            const publishProductModel = "wdc30.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BruruWDC30.push(url)
            }
    })
    return wrongnameB2BruruWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-TRTR-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
const wrongnameB2BtrtrWDC30=[]
const wrongnameB2BtrtrURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/tr-tr/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "tr-tr/business"
            const publishSeries = "wireless-presentation"
            const publishUrlName = "tr-tr/business/wireless-presentation"
            const publishProductModel = "wdc30.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BtrtrWDC30.push(url)
            }
    })
    return wrongnameB2BtrtrWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-VIVN-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
const wrongnameB2BvivnWDC30=[]
const wrongnameB2BvivnURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/vi-vn/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "vi-vn/business"
            const publishSeries = "wireless-presentation"
            const publishUrlName = "vi-vn/business/wireless-presentation"
            const publishProductModel = "wdc30.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BvivnWDC30.push(url)
            }
    })
    return wrongnameB2BvivnWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ZHHK-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
const wrongnameB2BzhhkWDC30=[]
const wrongnameB2BzhhkURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/zh-hk/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "zh-hk/business"
            const publishSeries = "wireless-presentation"
            const publishUrlName = "zh-hk/business/wireless-presentation"
            const publishProductModel = "wdc30.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BzhhkWDC30.push(url)
            }
    })
    return wrongnameB2BzhhkWDC30;
    } catch (error) {
      console.log(error);
    }
};
//B2B-ZHTW-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
const wrongnameB2BzhtwWDC30=[]
const wrongnameB2BzhtwURLWDC30 = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/zh-tw/business/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "zh-tw/business"
            const publishSeries = "wireless-presentation"
            const publishUrlName = "zh-tw/business/wireless-presentation"
            const publishProductModel = "wdc30.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2BzhtwWDC30.push(url)
            }
    })
    return wrongnameB2BzhtwWDC30;
    } catch (error) {
      console.log(error);
    }
};
//Test case
Given("WDC30 must be published on global site after 20220513",{timeout: 1000 * 5000},async function(){
    //After 2022/01/01
    //global setting
    const publishModel = "wdc30"
    const date = new Date()
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()
    function wholeMonth(){
        const getmonth = date.getMonth() + 1
        if(getmonth<10){
            wholeMonth =  "0"+getmonth 
            return wholeMonth
        }else{
            wholeMonth = getmonth 
            return wholeMonth
        }
    }
    function wholeDate(){
        const getDay = date.getDate()
        if(getDay<10){
            wholeDate =  "0"+getDay
            return wholeDate
        }else{
            wholeDate = getDay 
            return wholeDate
        }
    }
    const wholemonth = wholeMonth()
    const wholeday = wholeDate()
    const wholeyear = date.getFullYear()
    const fullDate = `${wholeyear}/${wholemonth}/${wholeday}`
    // console.log("wholemonth:",wholemonth)
    // console.log("wholeday:",wholeday)
    // console.log("wholeyear:",wholeyear)
    console.log("fullDate:",fullDate)
    // console.log("year:",year)
    // console.log("month:",month)
    // console.log("day:",day)
    const launchDate = "2022/04/01"

    //B2B-ARMWPS-WDC30-after 20220506
    const armeReturnedData = await afterB2BarmeURLWDC30();
    // console.log("After returnedData",armeReturnedData)
    const armepublishCheck = armeReturnedData.length
    console.log("ar-me All publish URL-B2B WPS-WDC30:",armeReturnedData)
    console.log(armeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //WDC30PublishError
        if(armepublishCheck ==0){
            // afterB2BarmeWDC30.push("ar-me")
            WDC30PublishError.push("ar-me")

        }
    }
    //B2B-CSCZ-WPS-WDC30-after 20220506
    const csczReturnedData = await afterB2BcsczURLWDC30();
    // console.log("After returnedData",csczReturnedData)
    const csczpublishCheck = csczReturnedData.length
    console.log("cs-cz All publish URL-B2B WPS-WDC30:",csczReturnedData)
    console.log(csczReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //WDC30PublishError
        if(csczpublishCheck ==0){
            // afterB2BcsczWDC30.push("cs-cz")
            WDC30PublishError.push("cs-cz")

        }
    }
    //B2B-DEDE-WPS-WDC30-after 20220506
    const dedeReturnedData = await afterB2BdedeURLWDC30();
    // console.log("After returnedData",dedeReturnedData)
    const dedepublishCheck = dedeReturnedData.length
    console.log("de-de All publish URL-B2B WPS-WDC30:",dedeReturnedData)
    console.log(dedeReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //WDC30PublishError
        if(dedepublishCheck ==0){
            // afterB2BdedeWDC30.push("de-de")
            WDC30PublishError.push("de-de")

        }
    }
    //B2B-ENAP-WPS-WDC30-after 20220506
    const enapReturnedData = await afterB2BenapURLWDC30();
    // console.log("After returnedData",enapReturnedData)
    const enappublishCheck = enapReturnedData.length
    console.log("en-ap All publish URL-B2B WPS-WDC30:",enapReturnedData)
    console.log(enapReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //WDC30PublishError
        if(enappublishCheck ==0){
            // afterB2BenapWDC30.push("en-ap")
            WDC30PublishError.push("en-ap")

        }
    }
    //B2B-ENEU-WPS-WDC30-after 20220506
    const eneuReturnedData = await afterB2BeneuURLWDC30();
    // console.log("After returnedData",eneuReturnedData)
    const eneupublishCheck = eneuReturnedData.length
    console.log("en-eu All publish URL-B2B WPS-WDC30:",eneuReturnedData)
    console.log(eneuReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //WDC30PublishError
        if(eneupublishCheck ==0){
            WDC30PublishError.push("en-eu")
        }
    }
    //B2B-ENHK-WPS-WDC30-after 20220506
    const enhkReturnedData = await afterB2BenhkURLWDC30();
    // console.log("After returnedData",enhkReturnedData)
    const enhkpublishCheck = enhkReturnedData.length
    console.log("en-hk All publish URL-B2B WPS-WDC30:",enhkReturnedData)
    console.log(enhkReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //WDC30PublishError
        if(enhkpublishCheck ==0){
            WDC30PublishError.push("en-hk")
        }
    }
    //B2B-ENUK-WPS-WDC30-after 20220506
    const enukReturnedData = await afterB2BenukURLWDC30();
    // console.log("After returnedData",enukReturnedData)
    const enukpublishCheck = enukReturnedData.length
    console.log("en-uk All publish URL-B2B WPS-WDC30:",enukReturnedData)
    console.log(enukReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //WDC30PublishError
        if(enukpublishCheck ==0){
            WDC30PublishError.push("en-uk")
        }
    }
    //B2B-ENUS-WPS-WDC30-after 20220506
    const enusReturnedData = await afterB2BenusURLWDC30();
    // console.log("After returnedData",enusReturnedData)
    const enuspublishCheck = enusReturnedData.length
    console.log("en-us All publish URL-B2B WPS-WDC30:",enusReturnedData)
    console.log(enusReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //WDC30PublishError
        if(enuspublishCheck ==0){
            WDC30PublishError.push("en-us")
        }
    }
    //B2B-ESES-WPS-WDC30-after 20220506
    const esesReturnedData = await afterB2BesesURLWDC30();
    // console.log("After returnedData",esesReturnedData)
    const esespublishCheck = esesReturnedData.length
    console.log("es-es All publish URL-B2B WPS-WDC30:",esesReturnedData)
    console.log(esesReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //WDC30PublishError
        if(esespublishCheck ==0){
            WDC30PublishError.push("es-es")
        }
    }
    //B2B-ESLA-WPS-WDC30-after 20220506
    const eslaReturnedData = await afterB2BeslaURLWDC30();
    // console.log("After returnedData",eslaReturnedData)
    const eslapublishCheck = eslaReturnedData.length
    console.log("es-la All publish URL-B2B WPS-WDC30:",eslaReturnedData)
    console.log(eslaReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //WDC30PublishError
        if(eslapublishCheck ==0){
            WDC30PublishError.push("es-la")
        }
    }
    //B2B-ESMX-WPS-WDC30-after 20220506
    const esmxReturnedData = await afterB2BesmxURLWDC30();
    // console.log("After returnedData",esmxReturnedData)
    const esmxpublishCheck = esmxReturnedData.length
    console.log("es-mx All publish URL-B2B WPS-WDC30:",esmxReturnedData)
    console.log(esmxReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //WDC30PublishError
        if(esmxpublishCheck ==0){
            WDC30PublishError.push("es-mx")
        }
    }
    //B2B-FRCA-WPS-WDC30-after 20220506
    const frcaReturnedData = await afterB2BfrcaURLWDC30();
    // console.log("After returnedData",frcaReturnedData)
    const frcapublishCheck = frcaReturnedData.length
    console.log("fr-ca All publish URL-B2B WPS-WDC30:",frcaReturnedData)
    console.log(frcaReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //WDC30PublishError
        if(frcapublishCheck ==0){
            WDC30PublishError.push("fr-ca")
        }
    }
    //B2B-FRCH-WPS-WDC30-after 20220506
    const frchReturnedData = await afterB2BfrchURLWDC30();
    // console.log("After returnedData",frchReturnedData)
    const frchpublishCheck = frchReturnedData.length
    console.log("fr-ch All publish URL-B2B WPS-WDC30:",frchReturnedData)
    console.log(frchReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //WDC30PublishError
        if(frchpublishCheck ==0){
            WDC30PublishError.push("fr-ch")
        }
    }
    //B2B-FRFR-WPS-WDC30-after 20220506
    const frfrReturnedData = await afterB2BfrfrURLWDC30();
    // console.log("After returnedData",frfrReturnedData)
    const frfrpublishCheck = frfrReturnedData.length
    console.log("fr-fr All publish URL-B2B WPS-WDC30:",frfrReturnedData)
    console.log(frfrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //WDC30PublishError
        if(frfrpublishCheck ==0){
            WDC30PublishError.push("fr-fr")
        }
    }
    //B2B-IDID-WPS-WDC30-after 20220506
    const ididReturnedData = await afterB2BididURLWDC30();
    // console.log("After returnedData",ididReturnedData)
    const ididpublishCheck = ididReturnedData.length
    console.log("id-id All publish URL-B2B WPS-WDC30:",ididReturnedData)
    console.log(ididReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //WDC30PublishError
        if(ididpublishCheck ==0){
            WDC30PublishError.push("id-id")
        }
    }
    //B2B-ITIT-WPS-WDC30-after 20220506
    const ititReturnedData = await afterB2BititURLWDC30();
    // console.log("After returnedData",ititReturnedData)
    const ititpublishCheck = ititReturnedData.length
    console.log("it-it All publish URL-B2B WPS-WDC30:",ititReturnedData)
    console.log(ititReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //WDC30PublishError
        if(ititpublishCheck ==0){
            WDC30PublishError.push("it-it")
        }
    }
    //B2B-JAJP-WPS-WDC30-after 20220506
    const jajpReturnedData = await afterB2BjajpURLWDC30();
    // console.log("After returnedData",jajpReturnedData)
    const jajppublishCheck = jajpReturnedData.length
    console.log("ja-jp All publish URL-B2B WPS-WDC30:",jajpReturnedData)
    console.log(jajpReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //WDC30PublishError
        if(jajppublishCheck ==0){
            WDC30PublishError.push("ja-jp")
        }
    }
    //B2B-KOKR-WPS-WDC30-after 20220506
    const kokrReturnedData = await afterB2BkokrURLWDC30();
    // console.log("After returnedData",kokrReturnedData)
    const kokrpublishCheck = kokrReturnedData.length
    console.log("ko-kr All publish URL-B2B WPS-WDC30:",kokrReturnedData)
    console.log(kokrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //WDC30PublishError
        if(kokrpublishCheck ==0){
            WDC30PublishError.push("ko-kr")
        }
    }
    //B2B-NLNL-WPS-WDC30-after 20220506
    const nlnlReturnedData = await afterB2BnlnlURLWDC30();
    // console.log("After returnedData",nlnlReturnedData)
    const nlnlpublishCheck = nlnlReturnedData.length
    console.log("nl-nl All publish URL-B2B WPS-WDC30:",nlnlReturnedData)
    console.log(nlnlReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //WDC30PublishError
        if(nlnlpublishCheck ==0){
            WDC30PublishError.push("nl-nl")
        }
    }
    //B2B-PLPL-WPS-WDC30-after 20220506
    const plplReturnedData = await afterB2BplplURLWDC30();
    // console.log("After returnedData",plplReturnedData)
    const plplpublishCheck = plplReturnedData.length
    console.log("pl-pl All publish URL-B2B WPS-WDC30:",plplReturnedData)
    console.log(plplReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //WDC30PublishError
        if(plplpublishCheck ==0){
            WDC30PublishError.push("pl-pl")
        }
    }
    //B2B-PTBR-WPS-WDC30-after 20220506
    const ptbrReturnedData = await afterB2BptbrURLWDC30();
    // console.log("After returnedData",ptbrReturnedData)
    const ptbrpublishCheck = ptbrReturnedData.length
    console.log("pt-br All publish URL-B2B WPS-WDC30:",ptbrReturnedData)
    console.log(ptbrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //WDC30PublishError
        if(ptbrpublishCheck ==0){
            WDC30PublishError.push("pt-br")
        }
    }
    //B2B-RURU-WPS-WDC30-after 20220506
    const ruruReturnedData = await afterB2BruruURLWDC30();
    // console.log("After returnedData",ruruReturnedData)
    const rurupublishCheck = ruruReturnedData.length
    console.log("ru-ru All publish URL-B2B WPS-WDC30:",ruruReturnedData)
    console.log(ruruReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //WDC30PublishError
        if(rurupublishCheck ==0){
            WDC30PublishError.push("ru-ru")
        }
    }
    //B2B-TRTR-WPS-WDC30-after 20220506
    const trtrReturnedData = await afterB2BtrtrURLWDC30();
    // console.log("After returnedData",trtrReturnedData)
    const trtrpublishCheck = trtrReturnedData.length
    console.log("tr-tr All publish URL-B2B WPS-WDC30:",trtrReturnedData)
    console.log(trtrReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //WDC30PublishError
        if(trtrpublishCheck ==0){
            WDC30PublishError.push("tr-tr")
        }
    }
    //B2B-VIVN-WPS-WDC30-after 20220506
    const vivnReturnedData = await afterB2BvivnURLWDC30();
    // console.log("After returnedData",vivnReturnedData)
    const vivnpublishCheck = vivnReturnedData.length
    console.log("vi-vn All publish URL-B2B WPS-WDC30:",vivnReturnedData)
    console.log(vivnReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //WDC30PublishError
        if(vivnpublishCheck ==0){
            WDC30PublishError.push("vi-vn")
        }
    }
    //B2B-ZHHK-WPS-WDC30-after 20220506
    const zhhkReturnedData = await afterB2BzhhkURLWDC30();
    // console.log("After returnedData",zhhkReturnedData)
    const zhhkpublishCheck = zhhkReturnedData.length
    console.log("zh-hk All publish URL-B2B WPS-WDC30:",zhhkReturnedData)
    console.log(zhhkReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //WDC30PublishError
        if(zhhkpublishCheck ==0){
            WDC30PublishError.push("zh-hk")
        }
    }
    //B2B-ZHTW-WPS-WDC30-after 20220506
    const zhtwReturnedData = await afterB2BzhtwURLWDC30();
    // console.log("After returnedData",zhtwReturnedData)
    const zhtwpublishCheck = zhtwReturnedData.length
    console.log("zh-tw All publish URL-B2B WPS-WDC30:",zhtwReturnedData)
    console.log(zhtwReturnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        //WDC30PublishError
        if(zhtwpublishCheck ==0){
            WDC30PublishError.push("zh-tw")
        }
    }
    //放全部的國家的後面
    if(WDC30PublishError.length >0){
        console.log("Not Published Now:",WDC30PublishError)
        throw new Error(`${publishModel} must be published on ${WDC30PublishError} before ${launchDate}, but it is not published now. Today is ${fullDate}`)
    }
})

Then("WDC30 URL name must be 'business wireless-presentation wdc30.html'",{timeout: 1000 * 5000},async function(){
    //After 2022/04/01
    //global setting
    const publishModel = "wdc30"
    const publishProductURLName = "business/wireless-presentation/wdc30.html"
    const date = new Date()
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()
    function wholeMonth(){
        const getmonth = date.getMonth() + 1
        if(getmonth<10){
            wholeMonth =  "0"+getmonth 
            return wholeMonth
        }else{
            wholeMonth = getmonth 
            return wholeMonth
        }
    }
    function wholeDate(){
        const getDay = date.getDate()
        if(getDay<10){
            wholeDate =  "0"+getDay
            return wholeDate
        }else{
            wholeDate = getDay 
            return wholeDate
        }
    }
    const wholemonth = wholeMonth()
    const wholeday = wholeDate()
    const wholeyear = date.getFullYear()
    const fullDate = `${wholeyear}/${wholemonth}/${wholeday}`
    // console.log("wholemonth:",wholemonth)
    // console.log("wholeday:",wholeday)
    // console.log("wholeyear:",wholeyear)
    console.log("fullDate:",fullDate)
    // console.log("year:",year)
    // console.log("month:",month)
    // console.log("day:",day)
    const launchDate = "2022/04/01"
    //B2B-ARME-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
    const armeReturnedData = await wrongnameB2BarmeURLWDC30();
    const armeNameCheck = armeReturnedData.length
    console.log("ar-me wrong name URL-B2B WPS-WDC30:",armeReturnedData)
    console.log(armeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(armeNameCheck > 0){
            WDC30NameError.push('ar-me')
            WDC30NameErrorURL.push(armeReturnedData)
        }
    }
    //B2B-CSCZ-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
    const csczReturnedData = await wrongnameB2BcsczURLWDC30();
    const csczNameCheck = csczReturnedData.length
    console.log("cs-cz wrong name URL-B2B WPS-WDC30:",csczReturnedData)
    console.log(csczReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(csczNameCheck > 0){
            WDC30NameError.push('cs-cz')
            WDC30NameErrorURL.push(csczReturnedData)
        }
    }
    //B2B-DEDE-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
    const dedeReturnedData = await wrongnameB2BdedeURLWDC30();
    const dedeNameCheck = dedeReturnedData.length
    console.log("de-de wrong name URL-B2B WPS-WDC30:",dedeReturnedData)
    console.log(dedeReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(dedeNameCheck > 0){
            WDC30NameError.push('de-de')
            WDC30NameErrorURL.push(dedeReturnedData)
        }
    }
    //B2B-ENAP-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
    const enapReturnedData = await wrongnameB2BenapURLWDC30();
    const enapNameCheck = enapReturnedData.length
    console.log("en-ap wrong name URL-B2B WPS-WDC30:",enapReturnedData)
    console.log(enapReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enapNameCheck > 0){
            WDC30NameError.push("en-ap")
            WDC30NameErrorURL.push(enapReturnedData)
        }
    }
    //B2B-ENEU-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
    const eneuReturnedData = await wrongnameB2BeneuURLWDC30();
    const eneuNameCheck = eneuReturnedData.length
    console.log("en-eu wrong name URL-B2B WPS-WDC30:",eneuReturnedData)
    console.log(eneuReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(eneuNameCheck > 0){
            WDC30NameError.push('en-eu')
            WDC30NameErrorURL.push(eneuReturnedData)
        }
    }
    //B2B-ENHK-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
    const enhkReturnedData = await wrongnameB2BenhkURLWDC30();
    const enhkNameCheck = enhkReturnedData.length
    console.log("en-hk wrong name URL-B2B WPS-WDC30:",enhkReturnedData)
    console.log(enhkReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enhkNameCheck > 0){
            WDC30NameError.push("en-hk")
            WDC30NameErrorURL.push(enhkReturnedData)
        }
    }
    //B2B-ENUK-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
    const enukReturnedData = await wrongnameB2BenukURLWDC30();
    const enukNameCheck = enukReturnedData.length
    console.log("en-uk wrong name URL-B2B WPS-WDC30:",enukReturnedData)
    console.log(enukReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enukNameCheck > 0){
            WDC30NameError.push('en-uk')
            WDC30NameErrorURL.push(enukReturnedData)
        }
    }
    //B2B-ENUS-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
    const enusReturnedData = await wrongnameB2BenusURLWDC30();
    const enusNameCheck = enusReturnedData.length
    console.log("en-us wrong name URL-B2B WPS-WDC30:",enusReturnedData)
    console.log(enusReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(enusNameCheck > 0){
            WDC30NameError.push("en-us")
            WDC30NameErrorURL.push(enusReturnedData)
        }
    }
    //B2B-ESEWPS-WDC30-name must be business/wireless-presentation/wdc30.html
    const esesReturnedData = await wrongnameB2BesesURLWDC30();
    const esesNameCheck = esesReturnedData.length
    console.log("es-es wrong name URL-B2B WPS-WDC30:",esesReturnedData)
    console.log(esesReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(esesNameCheck > 0){
            WDC30NameError.push("es-es")
            WDC30NameErrorURL.push(esesReturnedData)
        }
    }
    //B2B-ESLA-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
    const eslaReturnedData = await wrongnameB2BeslaURLWDC30();
    const eslaNameCheck = eslaReturnedData.length
    console.log("es-la wrong name URL-B2B WPS-WDC30:",eslaReturnedData)
    console.log(eslaReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(eslaNameCheck > 0){
            WDC30NameError.push("es-la")
            WDC30NameErrorURL.push(eslaReturnedData)
        }
    }
    //B2B-ESMX-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
    const esmxReturnedData = await wrongnameB2BesmxURLWDC30();
    const esmxNameCheck = esmxReturnedData.length
    console.log("es-mx wrong name URL-B2B WPS-WDC30:",esmxReturnedData)
    console.log(esmxReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(esmxNameCheck > 0){
            WDC30NameError.push("es-mx")
            WDC30NameErrorURL.push(esmxReturnedData)
        }
    }
    //B2B-FRCA-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
    const frcaReturnedData = await wrongnameB2BfrcaURLWDC30();
    const frcaNameCheck = frcaReturnedData.length
    console.log("fr-ca wrong name URL-B2B WPS-WDC30:",frcaReturnedData)
    console.log(frcaReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(frcaNameCheck > 0){
            WDC30NameError.push("fr-ca")
            WDC30NameErrorURL.push(frcaReturnedData)
        }
    }
    //B2B-FRCH-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
    const frchReturnedData = await wrongnameB2BfrchURLWDC30();
    const frchNameCheck = frchReturnedData.length
    console.log("fr-ch wrong name URL-B2B WPS-WDC30:",frchReturnedData)
    console.log(frchReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(frchNameCheck > 0){
            WDC30NameError.push("fr-ch")
            WDC30NameErrorURL.push(frchReturnedData)
        }
    }
    //B2B-FRFR-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
    const frfrReturnedData = await wrongnameB2BfrfrURLWDC30();
    const frfrNameCheck = frfrReturnedData.length
    console.log("fr-fr wrong name URL-B2B WPS-WDC30:",frfrReturnedData)
    console.log(frfrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(frfrNameCheck > 0){
            WDC30NameError.push("fr-fr")
            WDC30NameErrorURL.push(frfrReturnedData)
        }
    }
    //B2B-IDID-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
    const ididReturnedData = await wrongnameB2BididURLWDC30();
    const ididNameCheck = ididReturnedData.length
    console.log("id-id wrong name URL-B2B WPS-WDC30:",ididReturnedData)
    console.log(ididReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ididNameCheck > 0){
            WDC30NameError.push("id-id")
            WDC30NameErrorURL.push(ididReturnedData)
        }
    }
    //B2B-ITIT-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
    const ititReturnedData = await wrongnameB2BititURLWDC30();
    const ititNameCheck = ititReturnedData.length
    console.log("it-it wrong name URL-B2B WPS-WDC30:",ititReturnedData)
    console.log(ititReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ititNameCheck > 0){
            WDC30NameError.push("it-it")
            WDC30NameErrorURL.push(ititReturnedData)
        }
    }
    //B2B-JAJP-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
    const jajpReturnedData = await wrongnameB2BjajpURLWDC30();
    const jajpNameCheck = jajpReturnedData.length
    console.log("ja-jp wrong name URL-B2B WPS-WDC30:",jajpReturnedData)
    console.log(jajpReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(jajpNameCheck > 0){
            WDC30NameError.push("ja-jp")
            WDC30NameErrorURL.push(jajpReturnedData)
        }
    }
    //B2B-KOKR-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
    const kokrReturnedData = await wrongnameB2BkokrURLWDC30();
    const kokrNameCheck = kokrReturnedData.length
    console.log("ko-kr wrong name URL-B2B WPS-WDC30:",kokrReturnedData)
    console.log(kokrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(kokrNameCheck > 0){
            WDC30NameError.push("ko-kr")
            WDC30NameErrorURL.push(kokrReturnedData)
        }
    }
    //B2B-NLNL-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
    const nlnlReturnedData = await wrongnameB2BnlnlURLWDC30();
    const nlnlNameCheck = nlnlReturnedData.length
    console.log("nl-nl wrong name URL-B2B WPS-WDC30:",nlnlReturnedData)
    console.log(nlnlReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(nlnlNameCheck > 0){
            WDC30NameError.push("nl-nl")
            WDC30NameErrorURL.push(nlnlReturnedData)
        }
    }
    //B2B-PLPL-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
    const plplReturnedData = await wrongnameB2BplplURLWDC30();
    const plplNameCheck = plplReturnedData.length
    console.log("pl-pl wrong name URL-B2B WPS-WDC30:",plplReturnedData)
    console.log(plplReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(plplNameCheck > 0){
            WDC30NameError.push("pl-pl")
            WDC30NameErrorURL.push(plplReturnedData)
        }
    }
    //B2B-PTBR-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
    const ptbrReturnedData = await wrongnameB2BptbrURLWDC30();
    const ptbrNameCheck = ptbrReturnedData.length
    console.log("pt-br wrong name URL-B2B WPS-WDC30:",ptbrReturnedData)
    console.log(ptbrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ptbrNameCheck > 0){
            WDC30NameError.push("pt-br")
            WDC30NameErrorURL.push(ptbrReturnedData)
        }
    }
    //B2B-RURU-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
    const ruruReturnedData = await wrongnameB2BruruURLWDC30();
    const ruruNameCheck = ruruReturnedData.length
    console.log("ru-ru wrong name URL-B2B WPS-WDC30:",ruruReturnedData)
    console.log(ruruReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(ruruNameCheck > 0){
            WDC30NameError.push("ru-ru")
            WDC30NameErrorURL.push(ruruReturnedData)
        }
    }
    //B2B-TRTR-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
    const trtrReturnedData = await wrongnameB2BtrtrURLWDC30();
    const trtrNameCheck = trtrReturnedData.length
    console.log("tr-tr wrong name URL-B2B WPS-WDC30:",trtrReturnedData)
    console.log(trtrReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(trtrNameCheck > 0){
            WDC30NameError.push("tr-tr")
            WDC30NameErrorURL.push(trtrReturnedData)
        }
    }
    //B2B-VIVN-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
    const vivnReturnedData = await wrongnameB2BvivnURLWDC30();
    const vivnNameCheck = vivnReturnedData.length
    console.log("vi-vn wrong name URL-B2B WPS-WDC30:",vivnReturnedData)
    console.log(vivnReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(vivnNameCheck > 0){
            WDC30NameError.push("vi-vn")
            WDC30NameErrorURL.push(vivnReturnedData)
        }
    }
    //B2B-ZHHK-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
    const zhhkReturnedData = await wrongnameB2BzhhkURLWDC30();
    const zhhkNameCheck = zhhkReturnedData.length
    console.log("zh-hk wrong name URL-B2B WPS-WDC30:",zhhkReturnedData)
    console.log(zhhkReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(zhhkNameCheck > 0){
            WDC30NameError.push("zh-hk")
            WDC30NameErrorURL.push(zhhkReturnedData)
        }
    }
    //B2B-ZHTW-WPS-WDC30-name must be business/wireless-presentation/wdc30.html
    const zhtwReturnedData = await wrongnameB2BzhtwURLWDC30();
    const zhtwNameCheck = zhtwReturnedData.length
    console.log("zh-tw wrong name URL-B2B WPS-WDC30:",zhtwReturnedData)
    console.log(zhtwReturnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(zhtwNameCheck > 0){
            WDC30NameError.push("zh-tw")
            WDC30NameErrorURL.push(zhtwReturnedData)
        }
    }

    //放全部國家的後面
    if(WDC30NameError.length >0){
        throw new Error(`${publishModel} URL name must be ${publishProductURLName}, but it is wrong now. Here is RO name: ${WDC30NameError} And wrong URL: ${WDC30NameErrorURL}`)
    }
})
