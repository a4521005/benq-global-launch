const { Given, When, Then, Before, After } = require('@cucumber/cucumber')
const { expect } = require('chai')
const fs = require('fs');
const puppeteer = require('puppeteer')
const request = require("request-promise");
const cheerio = require("cheerio");
const cicGA="?utm_source=autotest&utm_medium=CIC"

Before({timeout: 24 * 5000},async function () {
    this.browser = await puppeteer.launch({ 
        executablePath:
        "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
        // MAC路徑:
        // executablePath:
        // '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome',
        headless:false,//有無需要開視窗,false要開,true不開
        slowMo:100,// slow down by 100ms
        devtools:false//有無需要開啟開發人員工具
    })
    this.page = await this.browser.newPage()
    await this.page.setViewport({width:1200,height:1000})
    await this.page.setDefaultTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
    await this.page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
})

After({timeout: 12 * 5000},async function () {
    await this.browser.close()
})

//example
//B2C-ENUS-Projector-X3000i-after 20211231
const afterB2CenusX3000i=[]
const afterB2CenusURLX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-us/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "x3000i.html"
            if(url.indexOf(publishUrl)!==-1){
                afterB2CenusX3000i.push(url)
            }
    })
    return afterB2CenusX3000i;
    } catch (error) {
      console.log(error);
    }
};

//B2C-ENUS-Projector-X3000i-name must be projector/gaming-projector/x3000i.html
const wrongnameB2CenusX3000i=[]
const wrongnameB2CenusX3000i = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-us/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishRO = "en-us"
            const publishSeries = "projector"
            const publishUrl = "en-us/projector"
            const publishUrlName = "en-us/projector/gaming-projector"
            const publishProductSeries = "gaming-projector"
            const publishProductModel = "x3000i.html"
            if(url.indexOf(publishRO)>0 && url.indexOf(publishSeries)>0 && url.indexOf(publishUrl)>0 && url.indexOf(publishProductModel)>0 && url.indexOf(publishProductSeries)<0 && url.indexOf(publishUrlName)<0 ){
                wrongnameB2CenusX3000i.push(url)
            }
    })
    return wrongnameB2CenusX3000i;
    } catch (error) {
      console.log(error);
    }
};
//Test case-Example
Given("X3000i must be published on global site after 20220228",{timeout: 24 * 5000},async function(){
    //After 2022/01/01
    const returnedData = await afterB2CenusURLX3000i();
    console.log("After returnedData",returnedData)
    const publishCheck = returnedData.length
    const date = new Date()
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()
    function wholeMonth(){
        const getmonth = date.getMonth() + 1
        if(getmonth<10){
            wholeMonth =  "0"+getmonth 
            return wholeMonth
        }else{
            wholeMonth = getmonth 
            return wholeMonth
        }
    }
    function wholeDate(){
        const getDay = date.getDate()
        if(getDay<10){
            wholeDate =  "0"+getDay
            return wholeDate
        }else{
            wholeDate = getDay 
            return wholeDate
        }
    }
    const wholemonth = wholeMonth()
    const wholeday = wholeDate()
    const wholeyear = date.getFullYear()
    const fullDate = `${wholeyear}/${wholemonth}/${wholeday}`
    console.log("wholemonth:",wholemonth)
    console.log("wholeday:",wholeday)
    console.log("wholeyear:",wholeyear)
    console.log("fullDate:",fullDate)
    console.log("year:",year)
    console.log("month:",month)
    console.log("day:",day)
    const launchDate = "2022/02/28"
    console.log(returnedData)
    console.log(returnedData.length)
    if(year>=2022 || month >=1 || day >=1){
        const publishModel = "x3000i"
        //publishCheck  ==  0 => 未發布
        //publishCheck !== 0 => 有發布
        if(publishCheck ==0){
            throw new Error(`${publishModel} must be published on 'en-us' after  ${launchDate}, but it is not published now. Today is ${fullDate}`)
        }
    }
})

Then("X3000i URL name must be projector/gaming-projector/x3000i.html",{timeout: 24 * 5000},async function(){
    //After 2022/01/01
    //name must be photographer/sw321c.html
    const returnedData = await wrongnameB2CenusX3000i();
    const publishCheck = returnedData.length
    const date = new Date()
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()
    function wholeMonth(){
        const getmonth = date.getMonth() + 1
        if(getmonth<10){
            wholeMonth =  "0"+getmonth 
            return wholeMonth
        }else{
            wholeMonth = getmonth 
            return wholeMonth
        }
    }
    function wholeDate(){
        const getDay = date.getDate()
        if(getDay<10){
            wholeDate =  "0"+getDay
            return wholeDate
        }else{
            wholeDate = getDay 
            return wholeDate
        }
    }
    const wholemonth = wholeMonth()
    const wholeday = wholeDate()
    const wholeyear = date.getFullYear()
    const fullDate = `${wholeyear}/${wholemonth}/${wholeday}`
    console.log("wholemonth:",wholemonth)
    console.log("wholeday:",wholeday)
    console.log("wholeyear:",wholeyear)
    console.log("fullDate:",fullDate)
    console.log("year:",year)
    console.log("month:",month)
    console.log("day:",day)
    const launchDate = "2022/02/28"
    console.log(returnedData)
    console.log(returnedData.length)
    if(year>=2022 && month >=1 && day >=1){
        const publishModel = "x3000i"
        const publishProductSeries = "gaming-projector"
        //publishCheck  ==  0 => Series name無誤
        //publishCheck !== 0 => Series name有誤
        if(publishCheck !==0){
            throw new Error(`${publishModel} series name must be ${publishProductSeries}, but it is wrong now. Here is URL: ${returnedData}`)
        }
    }
})

